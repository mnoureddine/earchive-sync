unit SyncClusterStream;

interface

uses
  Windows,CLasses,math,SysUtils,Dialogs;
const
  TAILLE_Cluster =  1024;

type

  TClusterDonnees = Array[0..TAILLE_Cluster] of char;

  TClusterInfo = record
    Taille : integer;
    Crc1 : DWord;
    Crc2 : DWord;
    Adresse : Integer;
  end;

  TCluster = record
    Info : TClusterInfo;
    Donnees : TClusterDonnees;

  end;

  TClusterSystemTime = record
    CreateTime,
    LastAccess,
    LastWrite : TFileTime;
  end;

  PClusterSystemTime = ^TClusterSystemTime;

  TClusterFichierInfo = record
    Taille : integer;
    Modifier : Integer;
    SystemTime : TClusterSystemTime;
  end;


  PCluster = ^TCluster;
  PClusterInfo = ^TClusterInfo;
  TClusterStatus = (ClSLireInfo,ClSCreerMaj,ClSAppliquer,ClSSynchroniser);
  TClusterProgress = procedure (Sender:TObject;Position:integer;Max:Integer;Status:TClusterStatus)of object;

  TSyncClusterStream = class(TObject)
  private
    DernierPosition : integer;
    FOnClusterProgress: TClusterProgress;
    procedure SetOnClusterProgress(const Value: TClusterProgress);
    function LireFichierInfo(Fichier:TFileStream):TClusterFichierInfo;
    procedure EcrireFichierInfo(Fichier:TFileStream;FichierInfo:TClusterFichierInfo);

  protected
    procedure DoProgress(Position:integer;pMax:Integer;Status:TClusterStatus);
  public

    Rapport : TStrings;
    property OnClusterProgress : TClusterProgress read FOnClusterProgress write SetOnClusterProgress;
    function AppliquerMiseAjour(Dest, MiseAJour: string):boolean;overload;
    function AppliquerMiseAjour(Dest, MiseAJour: TStream;pFichierInfo:TClusterFichierInfo):boolean;overload;
    procedure CalcCRC(pClusterFichier: PCluster);
    function CreerMiseAJour(Source, InfoDest, MiseAJour: string):boolean;overload;
    function CreerMiseAJour(Source, InfoDest, MiseAJour: TStream;pFichierInfo:TClusterFichierInfo):boolean;overload;
    procedure EcrireCluster(pClusterFichier: PCluster; Fichier: TFileStream;
      Adresse: Integer);overload;
    procedure EcrireCluster(pClusterFichier: PCluster; Fichier: TStream;
      Adresse: Integer);overload;
    procedure LireCluster(pClusterFichier: PCluster; Fichier: TFileStream;
      Adresse: Integer);overload;
    procedure LireCluster(pClusterFichier: PCluster; Fichier: TStream;
      Adresse: Integer);overload;
    function LireInfo(Source, Info: string):boolean;overload;
    function LireInfo(Source, Info: TStream):boolean;overload;
    function MemeInfo(InfoSource, InfoDest: TClusterInfo): boolean;
    procedure Synchroniser(Source, Dest: string);overload;
    procedure Synchroniser(Source, Dest: TStream);overload;
    constructor Create;
    destructor Destroy;
    
  published

  end;

procedure SyncClusterEcrireFichierDate(Fichier : string; SystemeTime: TClusterSystemTime);overload;
procedure SyncClusterEcrireFichierDate(Fichier : TFileStream; SystemeTime: TClusterSystemTime);overload;
function SyncClusterLireFichierDates(Fichier : string): TClusterSystemTime;overload;
function SyncClusterLireFichierDates(Fichier : TFileStream): TClusterSystemTime;overload;


implementation


function LireTaille(pTaille : Int64) : string;
begin
  if pTaille > power(2,20) then
  begin
    result := Format('%.2f Mo',[pTaille /power(2,20)]);
    exit;
  end;
  if pTaille > power(2,10) then
  begin
    result := Format('%.2f ko',[pTaille /power(2,10)]);
    exit;
  end;


  result := Format('%d O',[pTaille]);
end;


procedure SyncClusterEcrireFichierDate(Fichier : string; SystemeTime: TClusterSystemTime);
var
  F1 : Integer;

begin
  F1 := FileOpen(Fichier, fmOpenWrite);

  SetFileTime(  F1,
                @SystemeTime.CreateTime,
                @SystemeTime.LastAccess,
                @SystemeTime.LastWrite);

  FileClose(F1);
end;

procedure SyncClusterEcrireFichierDate(Fichier : TFileStream; SystemeTime: TClusterSystemTime);
begin
  SetFileTime(  Fichier.Handle,
                @SystemeTime.CreateTime,
                @SystemeTime.LastAccess,
                @SystemeTime.LastWrite);
end;


function SyncClusterLireFichierDates(Fichier : string): TClusterSystemTime;
var
  F1 : Integer;
begin
  F1 := FileOpen(Fichier, fmShareDenyWrite);
  GetFileTime(F1, @Result.CreateTime, @Result.LastAccess, @Result.LastWrite);
  FileClose(F1);
end;


function SyncClusterLireFichierDates(Fichier : TFileStream): TClusterSystemTime;
begin
  GetFileTime(Fichier.Handle, @Result.CreateTime, @Result.LastAccess, @Result.LastWrite);

end;

procedure TSyncClusterStream.CalcCRC(pClusterFichier: PCluster);
var
  Taille : integer;
  pDonnees : TClusterDonnees;
  i : integer;
begin
  pDonnees := pClusterFichier.Donnees;
  pClusterFichier.Info.Crc1 := 512;
  pClusterFichier.Info.Crc2 := 0;
  Taille := pClusterFichier.Info.Taille;
  i := 0;
  while Taille > 0 do
  begin
    pClusterFichier.Info.Crc1 := (pClusterFichier.Info.Crc1 xor byte(pDonnees[i])+i) mod $FFFFFFFF;
    pClusterFichier.Info.Crc2 := (pClusterFichier.Info.Crc2 + byte(pDonnees[i])+i) mod $FFFFFFFF;
    inc(i);
    dec(Taille);
  end;
end;

procedure TSyncClusterStream.EcrireCluster(pClusterFichier: PCluster; Fichier: TFileStream;
  Adresse: Integer);
begin
  Fichier.Seek(Adresse*TAILLE_Cluster,0);
  Fichier.Write(pClusterFichier.Donnees[0],pClusterFichier.Info.Taille);
end;

procedure TSyncClusterStream.LireCluster(pClusterFichier: PCluster; Fichier: TFileStream;Adresse : Integer);
begin
  Fichier.Seek(Adresse*TAILLE_Cluster,0);
  pClusterFichier.Info.Taille := Fichier.Read(pClusterFichier.Donnees[0],TAILLE_Cluster);
  pClusterFichier.Info.Adresse := Adresse;
  CalcCRC(pClusterFichier);

end;

procedure TSyncClusterStream.Synchroniser(Source:String;Dest:string);
var
  FichierSource : TFileStream;
  FichierDest : TFileStream;

begin
  FichierSource := TFileStream.Create(Source,fmOpenRead);
  if not FileExists(Dest) then
    FichierDest := TFileStream.Create(Dest,fmCreate)
  else
    FichierDest := TFileStream.Create(Dest,fmOpenReadWrite);

  Synchroniser(FichierSource,FichierDest);

  EcrireFichierInfo(FichierDest,LireFichierInfo(FichierSource));

  FichierSource.free;
  FichierDest.free;



end;


function TSyncClusterStream.LireInfo(Source, Info: string):boolean;
var
  Fichier : TFileStream;
  FichierInfo : TFileStream;
begin
  result := false;
  try
    Fichier := TFileStream.Create(Source,fmOpenRead);
    FichierInfo := TFileStream.Create(Info,fmCreate);

    if not LireInfo(Fichier,FichierInfo) then
      exit;

    Fichier.Free;
    FichierInfo.Free;
    result := true;

  except

  end;

end;

function TSyncClusterStream.CreerMiseAJour(Source,InfoDest, MiseAJour: string):boolean;
var
  FichierSource : TFileStream;
  FichierInfoDest : TFileStream;
  FichierMiseAJour : TFileStream;

begin
  try
    result := false;
    FichierSource := TFileStream.Create(Source,fmOpenRead);
    FichierInfodest := TFileStream.Create(InfoDest,fmOpenRead);
    FichierMiseAJour := TFileStream.Create(MiseAJour,fmCreate);

    if not CreerMiseAJour(FichierSource,FichierInfoDest,FichierMiseAJour,LireFichierInfo(FichierSource)) then
    begin
      FichierSource.free;
      FichierInfoDest.free;
      FichierMiseAJour.free;
      exit;
    end;

    FichierSource.free;
    FichierInfoDest.free;
    FichierMiseAJour.free;
    result := true;
  except

  end;


end;

function TSyncClusterStream.MemeInfo(InfoSource, InfoDest: TClusterInfo): boolean;
begin
  result := (InfoSource.Crc1 = InfoDest.Crc1) and
          (InfoSource.Crc2 = InfoDest.Crc2) and
          (InfoSource.Taille = InfoDest.Taille);
end;








constructor TSyncClusterStream.Create;
begin
  Rapport := TStringList.Create;
  FOnClusterProgress := nil;
  DernierPosition := 0;
  
end;

destructor TSyncClusterStream.Destroy;
begin

  Rapport.free;
end;

function TSyncClusterStream.AppliquerMiseAjour(Dest, MiseAJour: TStream;pFichierInfo:TClusterFichierInfo):boolean;
var
//  Taille : Int64;
  Cluster : TCluster;
  i : integer;
  NombreBlock : Integer;

begin
  result := false;
  try

    NombreBlock := pFichierInfo.Modifier;

    i := 0;
    while i < NombreBlock do
    begin
      MiseAJour.Read(Cluster,SizeOf(TCluster));
      EcrireCluster(@Cluster,Dest,Cluster.Info.Adresse);
      DoProgress(i,NombreBlock,ClSAppliquer);
      inc(i);
    end;


    DoProgress(i,NombreBlock,ClSAppliquer);

    if Dest.Size > pFichierInfo.Taille then
      Dest.Size := pFichierInfo.Taille;
    result := true;
  except

  end;
end;

function TSyncClusterStream.AppliquerMiseAjour(Dest, MiseAJour: string):boolean;
var
  FichierDest : TFileStream;
  FichierMiseAJour : TFileStream;
  FichierInfo : TClusterFichierInfo;

begin
  result := false;
  try
    FichierDest := TFileStream.Create(Dest,fmOpenReadWrite);
    FichierMiseAJour := TFileStream.Create(MiseAJour,fmOpenRead);

    FichierMiseAJour.Read(FichierInfo,SizeOf(TClusterFichierInfo));

    if not AppliquerMiseAjour(FichierDest,FichierMiseAJour,FichierInfo) then
    begin
      FichierDest.Free;
      FichierMiseAJour.Free;
      exit;
    end;

    EcrireFichierInfo(FichierDest,FichierInfo);
    FichierDest.Free;
    FichierMiseAJour.Free;

    result := true;
  except

  end;
end;

function TSyncClusterStream.CreerMiseAJour(Source, InfoDest,
  MiseAJour: TStream;pFichierInfo : TClusterFichierInfo):boolean;
var
  ClusterSource : TCluster;
  ClusterInfoDest : TClusterInfo;
  NombreModifier,NombreCluster,i : Integer;
  Taille : int64;

begin
  try
    result := true;
    Source.Position := 0;
    InfoDest.Position :=0;
    MiseAJour.Position:=0;

    MiseAJour.Write(pFichierInfo,SizeOF(TClusterFichierInfo));

    NombreCluster := Source.Size div TAILLE_Cluster;
    Taille := Source.Size;
    NombreModifier := 0;
    i := 0;
    while i < NombreCluster do
    begin
        LireCluster(@ClusterSource,Source,i);

        InfoDest.Read(ClusterInfoDest,SizeOf(TClusterInfo));
        if not MemeInfo(ClusterSource.Info,ClusterInfoDest) then
        begin
          MiseAJour.Write(ClusterSource,SizeOf(TCluster));
          Inc(NombreModifier);
        end;

        DoProgress(i,NombreCluster,ClSCreerMaj);
        inc(i);
    end;
    if Source.Position < Source.Size then
    begin
        LireCluster(@ClusterSource,Source,i);
        InfoDest.Read(ClusterInfoDest,SizeOf(TClusterInfo));
        if not MemeInfo(ClusterSource.Info,ClusterInfoDest) then
        begin
            MiseAJour.Write(ClusterSource,SizeOf(TCluster));
            Inc(NombreModifier);
        end;
    end;

    pFichierInfo.Modifier := NombreModifier;
    MiseAJour.Position := 0;
    MiseAJour.Write(pFichierInfo,SizeOF(TClusterFichierInfo));
    Rapport.Clear;
    Rapport.Add('Fichier source ');
    Rapport.Add(Format('Taille = %s ',[LireTaille(Source.Size)]));
    Rapport.Add(Format('Taille ficher dest = %s ',[LireTaille(Taille)]));
    Rapport.Add(Format('Bloc modifer = %d/%d ',[NombreModifier,NombreCluster]));
    Rapport.Add(Format('Taille modifer = %d/%d ',[NombreModifier,NombreCluster]));
    Rapport.Add(Format('Taille donnees modifier = %s/%s ',[LireTaille(NombreModifier*TAILLE_Cluster),LireTaille(Source.Size)]));
    if NombreCluster > 0 then
      Rapport.Add(Format('Taux = %.1f%% ',[(NombreModifier/NombreCluster)*100]))
    else
      if NombreModifier >= 1 then
        Rapport.Add('Taux = %100%% ')
      else
        Rapport.Add('Taux = %0%% ');
    Rapport.Add(Format('Taille Cluster = %d ',[SizeOf(TCluster)]));
    result := true;
    
  except
  end;

end;

procedure TSyncClusterStream.EcrireCluster(pClusterFichier: PCluster;
  Fichier: TStream; Adresse: Integer);
begin
  Fichier.Seek(Adresse*TAILLE_Cluster,0);
  Fichier.Write(pClusterFichier.Donnees[0],pClusterFichier.Info.Taille);
end;

procedure TSyncClusterStream.LireCluster(pClusterFichier: PCluster;
  Fichier: TStream; Adresse: Integer);
begin
  Fichier.Seek(Adresse*TAILLE_Cluster,0);
  pClusterFichier.Info.Taille := Fichier.Read(pClusterFichier.Donnees[0],TAILLE_Cluster);
  pClusterFichier.Info.Adresse := Adresse;
  CalcCRC(pClusterFichier);

end;

function TSyncClusterStream.LireInfo(Source, Info: TStream) : boolean;
var
  NombreCluster : Integer;
  Cluster : TCluster;
  i : integer;
begin
  result := false;
  try
    NombreCluster :=  (Source.Size div TAILLE_Cluster);
    Info.Position := 0;
    i := 0;
    while i < NombreCluster  do
    begin
      LireCluster(@Cluster,Source,i);
      Info.Write(Cluster.Info,SizeOf(TClusterInfo));
      DoProgress(i,NombreCluster,ClSLireInfo);
      inc(i);
    end;
    if Source.Position < Source.Size then
    begin
      LireCluster(@Cluster,Source,i);
      Info.Write(Cluster.Info,SizeOf(TClusterInfo));
      DoProgress(i,NombreCluster,ClSLireInfo);  
    end;

    result := true;
  except
  end;
end;



procedure TSyncClusterStream.Synchroniser(Source, Dest: TStream);
var
  ClusterSource : TCluster;
  ClusterDest : TCluster;
  pClusterSource : PCluster;
  pClusterDest : PCluster;
  NombreCluster : integer;
  i : integer;
  NombreModifier : integer;
begin

  Source.Position := 0;
  Dest.Position :=0;
  NombreCluster :=  (Source.Size div TAILLE_Cluster)+1;

  pClusterSource := @ClusterSource;
  pClusterDest := @ClusterDest;


  ZeroMemory(pClusterSource,Sizeof(TCluster));
  ZeroMemory(pClusterDest,Sizeof(TCluster));


  NombreModifier := 0;
  for i := 0 to NombreCluster  do
  begin
    LireCluster(pClusterSource,Source,i);
    LireCluster(pClusterDest,Dest,i);
    if MemeInfo(pClusterSource.Info,pClusterDest.Info)then
    begin
      EcrireCluster(pClusterSource,Dest,i);
      inc(NombreModifier);
    end;
  end;

  Dest.Size := Source.Size;
  Rapport.Clear;
  Rapport.Add('Fichier source ');
  Rapport.Add(Format('Taille = %s',[LireTaille(Source.Size)]));
  Rapport.Add('Fichier destination');
  Rapport.Add(Format('Taille destination = %s',[LireTaille(Dest.Size)]));
  Rapport.Add(Format('Bloc modifer= %d/%d',[NombreModifier,NombreCluster]));
  Rapport.Add(Format('Taille modifer= %d/%d',[NombreModifier,NombreCluster]));
  Rapport.Add(Format('Taille donnees modifier= %s/%s',[LireTaille(NombreModifier*TAILLE_Cluster),LireTaille(Source.Size)]));
  Rapport.Add(Format('Taux = %.1f%%',[(NombreModifier/NombreCluster)*100]));
  Rapport.Add(Format('Taille Cluster = %d',[SizeOf(TCluster)]));

end;

procedure TSyncClusterStream.SetOnClusterProgress(
  const Value: TClusterProgress);
begin
  FOnClusterProgress := Value;
end;

procedure TSyncClusterStream.DoProgress(Position, pMax: Integer;
  Status: TClusterStatus);
var
  Ecart : integer;
  Pas : integer;
begin


  if Assigned(FOnClusterProgress)then
  begin
    try
      Pas := Max(2,pMax div 2441);
      Ecart := pMax div Pas;
      if Position < Ecart then
        DernierPosition := 0;
      if Position >= DernierPosition+Ecart then
      begin
          FOnClusterProgress(self,Position,pMax,Status);
          DernierPosition := Position;
      end;

    except


    end;
  end;
end;

procedure TSyncClusterStream.EcrireFichierInfo(Fichier: TFileStream;
  FichierInfo: TClusterFichierInfo);
begin
//  if Fichier.Size > FichierInfo.Taille then
//    Fichier.Size := FichierInfo.Taille;
  SyncClusterEcrireFichierDate(Fichier,FichierInfo.SystemTime);

end;

function TSyncClusterStream.LireFichierInfo(
  Fichier: TFileStream): TClusterFichierInfo;
begin
  Result.Taille := Fichier.Size;
  Result.Modifier := 0;
  Result.SystemTime := SyncClusterLireFichierDates(Fichier);
end;


end.
