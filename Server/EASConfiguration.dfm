object FormEASConfiguration: TFormEASConfiguration
  Left = 309
  Top = 174
  BorderStyle = bsDialog
  Caption = 'Boite de configuration'
  ClientHeight = 353
  ClientWidth = 273
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 257
    Height = 305
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'G'#233'n'#233'ral'
      object Label1: TLabel
        Left = 80
        Top = 192
        Width = 28
        Height = 13
        Caption = 'Port : '
        FocusControl = SpinEditPort
      end
      object CheckBoxActifAuDemarrage: TCheckBox
        Left = 8
        Top = 8
        Width = 233
        Height = 17
        Caption = 'Activer le partage au d'#233'marrage'
        TabOrder = 0
      end
      object CheckBoxBarreDesTachesAuDemarrage: TCheckBox
        Left = 8
        Top = 40
        Width = 225
        Height = 17
        Caption = 'Placer dans la barre des taches'
        TabOrder = 1
      end
      object CheckBoxJournalEvenActif: TCheckBox
        Left = 8
        Top = 72
        Width = 185
        Height = 17
        Caption = 'Tenir un journal d'#39#233'v'#232'nements'
        TabOrder = 2
      end
      object LabeledEditDossierJournal: TLabeledEdit
        Left = 8
        Top = 152
        Width = 185
        Height = 21
        EditLabel.Width = 158
        EditLabel.Height = 13
        EditLabel.Caption = 'Adresse du dossier des journaux :'
        TabOrder = 3
      end
      object ButtonParcourir: TButton
        Left = 200
        Top = 149
        Width = 49
        Height = 25
        Caption = '...'
        TabOrder = 4
        OnClick = ButtonParcourirClick
      end
      object SpinEditPort: TSpinEdit
        Left = 120
        Top = 184
        Width = 121
        Height = 22
        MaxValue = 15000
        MinValue = 4000
        TabOrder = 5
        Value = 4451
      end
      object CheckBoxAfficherMessageInfo: TCheckBox
        Left = 8
        Top = 104
        Width = 225
        Height = 17
        Caption = 'Affichier tous les messages d'#39'information.'
        TabOrder = 6
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Web'
      ImageIndex = 1
      object CheckBoxWebActifAuDemarrage: TCheckBox
        Left = 8
        Top = 16
        Width = 233
        Height = 17
        Caption = 'Serveur web actif au d'#233'marrage'
        TabOrder = 0
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Gestion de fichiers'
      ImageIndex = 2
      object ShellComboBox1: TShellComboBox
        Left = 8
        Top = 8
        Width = 233
        Height = 22
        Root = 'rfDesktop'
        ShellListView = ShellListView1
        UseShellImages = True
        DropDownCount = 8
        TabOrder = 0
      end
      object ShellListView1: TShellListView
        Left = 8
        Top = 32
        Width = 233
        Height = 241
        ObjectTypes = [otFolders, otNonFolders]
        Root = 'rfDesktop'
        ShellComboBox = ShellComboBox1
        Sorted = True
        ReadOnly = False
        HideSelection = False
        TabOrder = 1
        ViewStyle = vsReport
      end
    end
  end
  object ButtonAnnuler: TButton
    Left = 192
    Top = 320
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Annuler'
    ModalResult = 2
    TabOrder = 1
  end
  object ButtonOK: TButton
    Left = 112
    Top = 320
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
end
