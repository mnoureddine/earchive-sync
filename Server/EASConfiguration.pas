unit EASConfiguration;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,EASDonnees, ShellCtrls, ExtCtrls,filectrl,
  Spin;

type
  TFormEASConfiguration = class(TForm)
    PageControl1: TPageControl;
    ButtonAnnuler: TButton;
    ButtonOK: TButton;
    TabSheet1: TTabSheet;
    CheckBoxActifAuDemarrage: TCheckBox;
    CheckBoxBarreDesTachesAuDemarrage: TCheckBox;
    CheckBoxJournalEvenActif: TCheckBox;
    TabSheet2: TTabSheet;
    CheckBoxWebActifAuDemarrage: TCheckBox;
    TabSheet3: TTabSheet;
    ShellComboBox1: TShellComboBox;
    ShellListView1: TShellListView;
    LabeledEditDossierJournal: TLabeledEdit;
    ButtonParcourir: TButton;
    SpinEditPort: TSpinEdit;
    Label1: TLabel;
    CheckBoxAfficherMessageInfo: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure ButtonParcourirClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
    procedure Charger;
    procedure Enregistrer;
    function Executer : boolean;
  end;


implementation

{$R *.dfm}

{ TForm1 }

procedure TFormEASConfiguration.Charger;
begin
  with Donnees.Configuration do
  begin
    CheckBoxActifAuDemarrage.Checked := General.ActifAuDemarrage;
    CheckBoxBarreDesTachesAuDemarrage.Checked := General.BarreDesTachesAuDemarrage;
    CheckBoxJournalEvenActif.Checked := General.JournalEvenActif;
    LabeledEditDossierJournal.Text := General.DossierJournal;
    SpinEditPort.Value := General.Port;
    CheckBoxAfficherMessageInfo.Checked := General.AfficherMessageInfo;
  end;


end;

procedure TFormEASConfiguration.Enregistrer;
begin
  with Donnees.Configuration do
  begin
    General.ActifAuDemarrage := CheckBoxActifAuDemarrage.Checked;
    General.BarreDesTachesAuDemarrage := CheckBoxBarreDesTachesAuDemarrage.Checked;
    General.JournalEvenActif := CheckBoxJournalEvenActif.Checked;
    General.DossierJournal := LabeledEditDossierJournal.Text;
    General.Port := SpinEditPort.Value;
    General.AfficherMessageInfo := CheckBoxAfficherMessageInfo.Checked;
    Web.ActifAuDemarrage := CheckBoxWebActifAuDemarrage.Checked;
    
  end;
  Donnees.EnregistrerConfiguration;

end;

function TFormEASConfiguration.Executer : boolean;
begin
  Charger;
  result := ShowModal = mrOK;
  if Result then
    Enregistrer;
end;

procedure TFormEASConfiguration.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TFormEASConfiguration.ButtonParcourirClick(Sender: TObject);
var
  Dossier : string;
begin
  Dossier := LabeledEditDossierJournal.Text;
  if SelectDirectory('Selection du dossier des journaux','',Dossier)then
    LabeledEditDossierJournal.Text := Dossier;

end;

end.
