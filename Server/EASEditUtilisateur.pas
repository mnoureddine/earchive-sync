unit EASEditUtilisateur;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,ArchivesCloudServer,Utiles;

type
  TFormEditUtilisateur = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Image1: TImage;
    LabeledEditNom: TLabeledEdit;
    LabeledEditMotPasse: TLabeledEdit;
    ButtonAnnuler: TButton;
    ButtonOK: TButton;
    procedure LabeledEditMotPasseChange(Sender: TObject);
  private
    FMotPasse : string;
    function GetMotPasse: string;
    function GetNom: string;
    procedure SetMotPasse(const Value: string);
    procedure SetNom(const Value: string);
    { Déclarations privées }
  public
    { Déclarations publiques }
    property Nom : string read GetNom write SetNom;
    property MotPasse : string read GetMotPasse write SetMotPasse;
    function Executer : boolean;overload;
    function Executer(pUtilisateur : TACSUtilisateur): boolean;overload;
  end;

const
  PASSE_NO = 'xxxxxxxxxx';

implementation

{$R *.dfm}

{ TFormEditUtilisateur }

function TFormEditUtilisateur.Executer: boolean;
begin
  result := ShowModal = mrOK;

end;

function TFormEditUtilisateur.Executer(
  pUtilisateur: TACSUtilisateur): boolean;
begin
  Nom := pUtilisateur.Nom;
  MotPasse := pUtilisateur.MotPasse;
  LabeledEditNom.Enabled := false;
  result := ShowModal = mrOK;
end;

function TFormEditUtilisateur.GetMotPasse: string;
begin
  if LabeledEditMotPasse.Text <> PASSE_NO then
    result := MD5CryptMotDePasse(LabeledEditMotPasse.Text)
  else
    Result := FMotPasse;
end;

function TFormEditUtilisateur.GetNom: string;
begin
  Result := LabeledEditNom.Text;
end;

procedure TFormEditUtilisateur.LabeledEditMotPasseChange(Sender: TObject);
begin
   ButtonOK.Enabled := LabeledEditMotPasse.Text <> '';
end;

procedure TFormEditUtilisateur.SetMotPasse(const Value: string);
begin
  FMotPasse := Value;
  LabeledEditMotPasse.Text := PASSE_NO;
end;

procedure TFormEditUtilisateur.SetNom(const Value: string);
begin
  LabeledEditNom.Text := Value;
end;

end.
