program EArchiveCloudServer;

uses
  Forms,
  EASPrinc in 'EASPrinc.pas' {FormEASPrinc},
  EASDonnees in 'EASDonnees.pas' {Donnees: TDataModule},
  ArchivesCloudServer in 'ArchivesCloudServer.pas',
  ArchivesCloudCommun in '..\ArchivesCloudCommun.pas',
  EASConfiguration in 'EASConfiguration.pas' {FormEASConfiguration},
  EASDossierPartage in 'EASDossierPartage.pas' {FormEASDossierPartage},
  EASEditUtilisateur in 'EASEditUtilisateur.pas' {FormEditUtilisateur},
  SyncClusterStream in '..\SyncClusterStream.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'ArchiveSync Server : logiciel serveur de synchronisation de fichier';
  Application.CreateForm(TDonnees, Donnees);
  Application.CreateForm(TFormEASPrinc, FormEASPrinc);
  Application.Run;
end.
