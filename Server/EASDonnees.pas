unit EASDonnees;

interface

uses
  SysUtils, Classes, IdBaseComponent, IdComponent, IdUDPBase, IdUDPServer,ArchivesCloudServer,Inifiles,
  IdTCPServer, IdCustomHTTPServer, IdHTTPServer, ExtCtrls,FileCtrl,ArchivesCloudCommun;

type
  TDonnees = class(TDataModule)
    IdHTTPServer1: TIdHTTPServer;
    Timer1: TTimer;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
    LastDate : TDateTime;
    Serveur : TACSServeur;
    FichierIni : TIniFile;
    Configuration : record
      General : record
        ActifAuDemarrage : boolean;
        BarreDesTachesAuDemarrage : boolean;
        JournalEvenActif : boolean;
        DossierJournal : string;
        DossierTemp : string;
        Port : integer;
        AfficherMessageInfo : boolean;
      end;
      Web : record
        ActifAuDemarrage : boolean;
      end;

    end;
    Journal : TStringList;
    FichierJournal : string;
    procedure ChargerFichierDePartages;
    procedure EnregistrerFichierDePartages;
    procedure ChargerConfiguration;
    procedure EnregistrerConfiguration;
    procedure ChargerUtilisateurs;
    procedure EnregistrerUtilisateurs;
    procedure TracerDansJournal(pMessage : string);
    procedure CreerFichierJournal;

  end;

var
  Donnees: TDonnees;
const
  FICHIER_DE_PARTAGES = 'partages.txt';
  FICHIER_DE_UTILISATEURS = 'utilisateurs.txt';

implementation

uses DateUtils;

{$R *.dfm}

procedure TDonnees.ChargerConfiguration;
begin
  with Configuration do
  begin
    General.ActifAuDemarrage :=
        FichierIni.ReadBool('General','ActifAuDemarrage',false);

    General.BarreDesTachesAuDemarrage :=
        FichierIni.ReadBool('General','BarreDesTachesAuDemarrage',false);

    General.JournalEvenActif :=
      FichierIni.ReadBool('General','JournalEvenActif',true);

    General.DossierJournal :=
      FichierIni.ReadString('General','DossierJournal',
        DossierMesDocuments+ '\ArchiveSync Server\Journal');

    General.DossierTemp :=
      FichierIni.ReadString('General','DossierTemp',GetTempDirectory+'\ArchiveSyncServer');


    General.Port :=
      FichierIni.ReadInteger('General','Port',AC_SERVEUR_TCP_PORT);

    General.AfficherMessageInfo :=
            FichierIni.ReadBool('General','AfficherMessageInfo',false);

    if not DirectoryExists(General.DossierJournal) then
      ForceDirectories(General.DossierJournal);

    if not DirectoryExists(General.DossierTemp) then
      ForceDirectories(General.DossierTemp);

    Web.ActifAuDemarrage :=
      FichierIni.ReadBool('Web','ActifAuDemarrage',false);

    end;
end;

procedure TDonnees.ChargerFichierDePartages;
begin
  if FileExists(GetCurrentDir+'\'+FICHIER_DE_PARTAGES) then
    Serveur.DossierPartages.LoadFromFile(GetCurrentDir+'\'+FICHIER_DE_PARTAGES);
end;

procedure TDonnees.ChargerUtilisateurs;
begin
  if FileExists(GetCurrentDir+'\'+FICHIER_DE_UTILISATEURS) then
    Serveur.Utilisateurs.LoadFromFile(GetCurrentDir+'\'+FICHIER_DE_UTILISATEURS);

end;

procedure TDonnees.DataModuleCreate(Sender: TObject);
begin
  Serveur := TACSServeur.Create;
  FichierIni := TIniFile.Create('EArchiveCloudServeur.ini');
  ChargerConfiguration;
  Serveur.DossierTemp := Configuration.General.DossierTemp;
  Serveur.DefaultPort := Configuration.General.Port;
  ChargerFichierDePartages;
  ChargerUtilisateurs;
  LastDate := Now;

  CreerFichierJournal;


end;

procedure TDonnees.EnregistrerConfiguration;
begin
  with Configuration do
  begin
   FichierIni.WriteBool(
                  'General',
                  'ActifAuDemarrage',
                  General.ActifAuDemarrage);

   FichierIni.WriteBool(
                  'General',
                  'BarreDesTachesAuDemarrage',
                  General.BarreDesTachesAuDemarrage);

   FichierIni.WriteBool(
                  'General',
                  'JournalEvenActif',
                  General.JournalEvenActif);

   FichierIni.WriteString(
                  'General',
                  'DossierJournal',
                  General.DossierJournal);

   FichierIni.WriteInteger(
                  'General',
                  'Port',
                  General.Port);

   FichierIni.WriteBool(
                  'General',
                  'AfficherMessageInfo',
                  General.AfficherMessageInfo);
                  
   FichierIni.WriteBool(
                  'Web',
                  'ActifAuDemarrage',
                  Web.ActifAuDemarrage);



  end;

end;

procedure TDonnees.EnregistrerFichierDePartages;
begin
  Serveur.DossierPartages.SaveToFile(GetCurrentDir+'\'+FICHIER_DE_PARTAGES);
end;

procedure TDonnees.EnregistrerUtilisateurs;
begin
  Serveur.Utilisateurs.SaveToFile(GetCurrentDir+'\'+FICHIER_DE_UTILISATEURS);

end;

procedure TDonnees.DataModuleDestroy(Sender: TObject);
begin

  Serveur.Free;
  Journal.Free;
  
end;

procedure TDonnees.TracerDansJournal(pMessage: string);
begin
  if Configuration.General.JournalEvenActif then
  begin
    Journal.Add(pMessage);
    Journal.SaveToFile(FichierJournal); 
  end;
end;

procedure TDonnees.Timer1Timer(Sender: TObject);
begin
    if not SameDate(now,LastDate)then
    begin
      Journal.Clear;
      CreerFichierJournal
    end;
    LastDate := Now;

end;

procedure TDonnees.CreerFichierJournal;
begin
  if Journal <> nil then
    Journal.Free;
  Journal := TStringList.Create;
  Journal.Clear;
  FichierJournal := Configuration.General.DossierJournal
       +'\JOURNAL_'+FormatDateTime('yyymmdd_hhnnss',now)+'.txt';
end;

end.
