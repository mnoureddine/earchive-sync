
unit ArchivesCloudServer;

interface
uses
  SysUtils, Classes, IdBaseComponent, IdComponent, IdTCPServer, ArchivesCloudCommun,
  Windows,IdCustomHTTPServer,StrUtils,Dialogs,JclSimpleXml,Utiles,SyncClusterStream;


type

  TACSServeur = class;
  TACSUtilisateur = class(TObject)
  private
    FNom: string;
    FMotPasse: string;
    Cle: string;
    procedure SetMotPasse(const Value: string);
    procedure SetNom(const Value: string);


  protected

  public
    property Nom : string read FNom write SetNom;
    property MotPasse : string read FMotPasse write SetMotPasse;
    constructor Create(pNom:string;pMotPasse : string);
    destructor Destroy; override;
  published

  end;

  TACSUtilisateurList = class(TList)
  private
    function Get(Index: Integer): TACSUtilisateur;
    procedure Put(Index: Integer; Item: TACSUtilisateur);
    function GetNom(Index: string): TACSUtilisateur;
  public
    constructor Create;
    destructor Destroy; override;
    function Add(Item: TACSUtilisateur): Integer;overload;
    function Add(pNom : string;pMotPasse: string) : TACSUtilisateur;overload;
    function Extract(Item: TACSUtilisateur): TACSUtilisateur;
    function First: TACSUtilisateur;
    function IndexOf(Item: TACSUtilisateur): Integer;
    procedure Insert(Index: Integer; Item: TACSUtilisateur);
    function Last: TACSUtilisateur;
    procedure Delete(Index: Integer);
    function Remove(Item: TACSUtilisateur): Integer;
    property Items[Index: Integer]: TACSUtilisateur read Get write Put; default;
    property Nom[Index: string]: TACSUtilisateur read GetNom;
    procedure Clear; override;
    function UtilisateurValide(pNom : string;pMotPasse: string):TACSUtilisateur;
    procedure LoadFromFile(FileName : TFileName);
    procedure SaveToFile(FileName : TFileName);
  end;

  TACSDossierPartage = class(TObject)
  private
    FAdresse: string;
    FNom: string;
    procedure SetAdresse(const Value: string);
    procedure SetNom(const Value: string);

  protected

  public
    property Nom : string read FNom write SetNom;
    property Adresse : string read FAdresse write SetAdresse;
    constructor Create(pAdresse : string);
    destructor Destroy; override;
  published

  end;

  TACSDossierPartageList = class(TList)
  private
    function Get(Index: Integer): TACSDossierPartage;
    procedure Put(Index: Integer; Item: TACSDossierPartage);
    function GetNom(Index: string): TACSDossierPartage;
  public
    constructor Create;
    destructor Destroy; override;
    function Add(Item: TACSDossierPartage): Integer;overload;
    function Add(pAdresse : string) : TACSDossierPartage;overload;
    function Extract(Item: TACSDossierPartage): TACSDossierPartage;
    function First: TACSDossierPartage;
    function IndexOf(Item: TACSDossierPartage): Integer;
    procedure Insert(Index: Integer; Item: TACSDossierPartage);
    function Last: TACSDossierPartage;
    procedure Delete(Index: Integer);
    function Remove(Item: TACSDossierPartage): Integer;
    property Items[Index: Integer]: TACSDossierPartage read Get write Put; default;
    property Nom[Index: string]: TACSDossierPartage read GetNom;
    procedure Clear; override;
    procedure LoadFromFile(FileName : TFileName);
    procedure SaveToFile(FileName : TFileName);
  end;


  TACSClient = class(TObject)
  private
    AThread: TIdPeerThread;
    FNom : string;
    DernierePosition : Integer;
    FConnexionAutoriser: boolean;
    function GetNom: string;
  protected
    procedure On7zProgressAdv( Sender: TObject; Filename: Widestring; MaxProgress : Int64;Position: int64 );
    procedure ClusterProgress(Sender:TObject;Position:integer;Max:Integer;Status:TClusterStatus);
  public
    Serveur : TACSServeur;
    Utilisateur : TACSUtilisateur;
    property ConnexionAutoriser : boolean read FConnexionAutoriser;
    property Nom : string read GetNom;
    constructor Create(pAThread: TIdPeerThread;pServeur : TACSServeur;pNom : string;pUtilisateur : TACSUtilisateur);
    destructor Destroy;

  published

  end;

 TACSClientList = class(TList)
  private
    function Get(Index: Integer): TACSClient;
    procedure Put(Index: Integer; Item: TACSClient);
    function GetAThread(Index: TIdPeerThread): TACSClient;
  public
    Serveur : TACSServeur;
    constructor Create(pServeur : TACSServeur);
    destructor Destroy; override;
    function Add(Item: TACSClient): Integer;overload;
    function Add(pAThread: TIdPeerThread;pNom:string;pUtilisateur : TACSUtilisateur) : TACSClient;overload;
    function Extract(Item: TACSClient): TACSClient;
    function First: TACSClient;
    function IndexOf(Item: TACSClient): Integer;
    procedure Insert(Index: Integer; Item: TACSClient);
    function Last: TACSClient;
    procedure Delete(Index: Integer);
    function Remove(pAThread: TIdPeerThread): Integer;overload;
    function Remove(Item: TACSClient): Integer;overload;
    property Items[Index: Integer]: TACSClient read Get write Put; default;
    property AThreads[Index:TIdPeerThread]: TACSClient read GetAThread;
    procedure Clear; override;
    
  end;

  TACSHTTPServeur = class(TIdCustomHTTPServer)

  private
    FDossierWeb: string;
    procedure SetDossierWeb(const Value: string);

  protected

    procedure AfficherDossier(Chemin : string;HTML : TStrings);
    procedure DoCommandGet(AThread: TIdPeerThread;
      ARequestInfo: TIdHTTPRequestInfo;
      AResponseInfo: TIdHTTPResponseInfo); override;

  public
    Serveur : TACSServeur;
    property DossierWeb : string read FDossierWeb write SetDossierWeb;
    constructor Create(pServeur : TACSServeur);
    destructor Destroy; override;
    procedure EcrireMessage(pMessageType :TACMessageType;pMessage : string);
  published

  end;



  TACSServeur = class(TIdTCPServer)
  private
    FDossierTemp: string;
    function GetWebActiver: boolean;
    procedure SetWebActiver(const Value: boolean);
    procedure SetDossierTemp(const Value: string);

  protected
    procedure DoConnect(AThread: TIdPeerThread); override;
    procedure DoDisconnect(AThread: TIdPeerThread); override;
    procedure DoClientChange;
    function DoExecute(AThread: TIdPeerThread): Boolean; override;

    function ReceptionFichiers7z(Athread:TIdPeerThread;pListeFichier7z : TStringList): boolean;
    function ReceptionFichiers(Athread:TIdPeerThread;pDossier:string;pListeFichier : TStrings): boolean;
    function ReceptionFichier(Athread:TIdPeerThread;pFichier : String): boolean;
    function ExtractionFichier7z(Athread:TIdPeerThread;pFichier:string;ListeFichier7z : TStrings): boolean;
    function ExtractionDossier7z(Athread:TIdPeerThread;pDossier:string;ListeFichier7z : TStrings) : boolean;
    procedure RequeteDossiersPartagesLister(Athread:TIdPeerThread);
    procedure RequeteMessage(Athread:TIdPeerThread);
    procedure RequeteDossierLister(Athread:TIdPeerThread);
    procedure RequeteFichierLire(Athread:TIdPeerThread);
    procedure RequeteFichierMAJInfo(Athread:TIdPeerThread);
    procedure RequeteFichierSupprimer(Athread:TIdPeerThread);
    procedure RequeteFichierDeplacer(Athread:TIdPeerThread);
    procedure RequeteFichierCopier(Athread:TIdPeerThread);
    procedure RequeteFichierEcrire(Athread:TIdPeerThread);
    procedure RequeteFichierRenommer(Athread:TIdPeerThread);
    procedure RequeteDossierLire(Athread: TIdPeerThread);
    procedure RequeteDossierMAJInfo(Athread:TIdPeerThread);
    procedure RequeteDossierSupprimer(Athread:TIdPeerThread);
    procedure RequeteDossierDeplacer(Athread:TIdPeerThread);
    procedure RequeteDossierCopier(Athread:TIdPeerThread);
    procedure RequeteDossierEcrire(Athread:TIdPeerThread);
    procedure RequeteDossierCreer(Athread:TIdPeerThread);
    procedure RequeteDossierRenommer(Athread:TIdPeerThread);
    procedure RequeteDossierSauvegarde(Athread:TIdPeerThread);
    function RequeteSyncFichierLire(Athread:TIdPeerThread):boolean;
    function RequeteSyncFichierEcrire(Athread:TIdPeerThread):boolean;
    function RequeteSyncFichierEcrireMAJ(Athread:TIdPeerThread):boolean;
    function CreationDossier7z(Athread:TIdPeerThread;pDossier:string;ListeFichiers7z:TStrings):boolean;
    function CreationFichier7z(Athread:TIdPeerThread;pFichier:string;ListeFichiers7z:TStrings):boolean;
    function EnvoyerFichier(Athread:TIdPeerThread;pFichier:string):boolean;
    function EnvoyerFichiers(Athread:TIdPeerThread;pListeFichiers:TStrings):boolean;
    function RequeteConnexion(Athread:TIdPeerThread) : TACSUtilisateur;
    procedure RequeteExtinctionClient(Athread:TIdPeerThread);
    procedure EnvoieDesFichiersZip(ListeFichiersZip:TStrings;Athread:TIdPeerThread);
    function ReceptionFichierZip(ListeFichierZip : TStrings;DossierReception:string;Athread:TIdPeerThread): boolean;
    procedure RequeteEchoue(Athread:TIdPeerThread;Code : TACCodeFonction;pMessage:string);
    procedure RequeteReussi(Athread:TIdPeerThread;Code : TACCodeFonction;pMessage:string);


  public
    OnMessage : TACMessageEvent;
    Clients :TACSClientList;
    OnClientsChange : TNotifyEvent;
    DossierPartages : TACSDossierPartageList;
    Utilisateurs : TACSUtilisateurList;
    WEB : TACSHTTPServeur;
    Config : TACServeurConfig;
    ExtinctionClientAutorise : boolean;
    property WebActiver : boolean read GetWebActiver write SetWebActiver;
    property DossierTemp : string read FDossierTemp write SetDossierTemp;
    procedure EcrireMessage(pMessageType :TACMessageType;AThread: TIdPeerThread;pMessage : string);overload;
    procedure EcrireMessage(pMessageType :TACMessageType;Client:TACSClient;pMessage : string);overload;
    procedure EcrireMessage(pMessageType :TACMessageType;pSource:string;pMessage : string);overload;
    procedure EcrireMessage(pMessageType :TACMessageType; pMessage : string);overload;
    Function ListeDossiersPartages: string;
    function ObtenirAdresse(Chemin : String) : string;
    constructor Create;
    destructor Destroy;
    procedure DoWork(AWorkMode: TWorkMode; const ACount: Integer);
      override;
    procedure FermerLesConnexionsActive;
  published

  end;

const
  ACSHTTP_SERVEUR_PORT = 8080;
  ACSG7Z_PROGRESS_SEUIL = 5000000; // Seuil de progression

implementation

uses IdTCPConnection, Math;

{ TACSServer }

constructor TACSServeur.Create;
begin
  inherited Create(nil);
  DefaultPort := AC_SERVEUR_TCP_PORT;
  Clients := TACSClientList.Create(self);
  DossierPartages := TACSDossierPartageList.Create;
  Utilisateurs := TACSUtilisateurList.Create;
  WEB := TACSHTTPServeur.Create(self);
  ExtinctionClientAutorise :=  true;
  FDossierTemp := GetTempDirectory+'\ArchiveSyncServer'; 
end;

destructor TACSServeur.Destroy;
begin
  WEB.Active := false;
  WEB.Free;
  Clients.Free;
  DossierPartages.Free;
  Utilisateurs.Free;
end;


procedure TACSServeur.DoConnect(AThread: TIdPeerThread);
var
   ClientProtocole : TACProtocoleVersion;
   ClientNom : string;
   Utilisateur : TACSUtilisateur;
   Client : TACSClient;

begin


    ClientNom := AThread.Connection.ReadLn;
    ClientProtocole.Valeur :=  Athread.Connection.ReadSmallInt;
    EcrireMessage(ACMessageRequete, ClientNom,'Connexion du client '+ClientNom);
//    EcrireMessage(AThread,'Connexion du client '+ClientNom);
//    Athread.Connection.WriteSmallInt(PROTOCOLE_VERSION.Valeur);
    if ACMemeVersionProtocole(ClientProtocole,PROTOCOLE_VERSION) then
    begin
        Athread.Connection.WriteInteger(ACRequeteResult(ACReqReussi));
        if ACCodeFonction(ACCFConnexion)= Athread.Connection.ReadInteger then
        begin
          Utilisateur := RequeteConnexion(AThread);
          if Utilisateur <> nil then
          begin
            Client := Clients.Add(AThread,ClientNom,Utilisateur);
            Client.Utilisateur := Utilisateur;
            Client.FConnexionAutoriser := true;
            DoClientChange;
          end;
        end
        
        else
        begin
        EcrireMessage(ACMessageErreur, AThread,'Erreur de commande');
          Athread.Connection.Disconnect;
        end;

    end
    else
    begin
        EcrireMessage(ACMessageErreur, AThread,'Version du protocole du client "'
                +ClientNom
                +'" V' +ACProtocoleToString(ClientProtocole)+' Incompatible');

        Athread.Connection.WriteInteger(ACRequeteResult(ACReqEchec));
        Athread.Connection.Disconnect;
    end;
    inherited;
end;

procedure TACSServeur.DoDisconnect(AThread: TIdPeerThread);
var
  i : integer;
  Client : TACSClient;
begin
  inherited;
  Client := Clients.AThreads[AThread];
  EcrireMessage(ACMessageRequete, AThread,'D�connexion ');
  Clients.Remove(AThread);
  DoClientChange;
  AThread.Data := nil;



end;

function TACSServeur.DoExecute(AThread: TIdPeerThread): Boolean;
var
  lMessage : TACMessage;
  Code : TACCodeFonction;
  Liste : TStringList;
  Chemin : string;
  Client : TACSClient;
begin
  while not AThread.Terminated and Athread.Connection.Connected do
  begin
      Client := Clients.AThreads[Athread];
      if not Client.ConnexionAutoriser then
      begin
        EcrireMessage(ACMessageInfo,AThread,'**** Attente d''identification ****');
        Code := TACCodeFonction(Athread.Connection.ReadInteger);
        if Code = ACCFConnexion then
              RequeteConnexion(AThread);
      end
      else
      begin
        EcrireMessage(ACMessageInfo, AThread,'**** Attente requetes ****');
        Code := TACCodeFonction(Athread.Connection.ReadInteger);
        case Code of
        ACCFConnexion : RequeteConnexion(AThread);
        ACCFConfigServeur : ;
        ACCFMessage :RequeteMessage(AThread);
        ACCFDossiersPartageLister : RequeteDossiersPartagesLister(AThread);
        ACCFDossierLister : RequeteDossierLister(AThread);
        ACCFFichierlire : RequeteFichierLire(AThread);
        ACCFFichierMAJInfo : RequeteFichierMAJInfo(AThread);
        ACCFFichierSupprimer : RequeteFichierSupprimer(AThread);
        ACCFFichierDeplacer : RequeteFichierDeplacer(AThread);
        ACCFFichierCopier : RequeteFichierCopier(AThread);
        ACCFFichierEcrire : RequeteFichierEcrire(AThread);
        ACCFFichierRenommer : RequeteFichierRenommer(AThread);
        ACCFDossierLire : RequeteDossierLire(AThread);
        ACCFDossierMAJInfo : RequeteDossierMAJInfo(AThread);
        ACCFDossierSupprimer : RequeteDossierSupprimer(AThread);
        ACCFDossierDeplacer : RequeteDossierDeplacer(AThread);
        ACCFDossierCopier : RequeteDossierCopier(AThread);
        ACCFDossierEcrire : RequeteDossierEcrire(AThread);
        ACCFDossierCreer : RequeteDossierCreer(AThread);
        ACCFDossierRenommer : RequeteDossierRenommer(AThread);
        ACCFDossierSauvegarder : RequeteDossierSauvegarde(AThread);
        ACCFExtinctionClient : RequeteExtinctionClient(AThread);
        ACCFSyncFichierLire : RequeteSyncFichierLire(AThread);
        ACCFSyncFichierEcrire : RequeteSyncFichierEcrire(AThread);
        ACCFSyncFichierEcrireMAJ : RequeteSyncFichierEcrireMAJ(AThread) ;

        end;
      end;

  end;
  if AThread <> nil then
  begin
    EcrireMessage(ACMessageRequete, AThread,'D�connexion');
    Clients.Remove(AThread);
  end;


end;

procedure TACSServeur.DoWork(AWorkMode: TWorkMode; const ACount: Integer);
begin
  inherited;

end;

procedure TACSServeur.EcrireMessage(pMessageType :TACMessageType;pMessage: string);
begin
    if Assigned(OnMessage) then
      OnMessage(Self,pMessageType,pMessage);
end;

{ TACSClient }

procedure TACSClient.ClusterProgress(Sender: TObject; Position,
  Max: Integer; Status: TClusterStatus);
begin
  Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationAvancement));
  Athread.Connection.WriteInteger(Max);
  Athread.Connection.WriteInteger(Position);
end;

constructor TACSClient.Create(pAThread: TIdPeerThread;pServeur : TACSServeur;pNom : string;pUtilisateur : TACSUtilisateur);
begin
  AThread := pAThread;
  Serveur := pServeur;
  FNom := pNom;
  FConnexionAutoriser := false;
  Utilisateur := pUtilisateur;
end;

destructor TACSClient.Destroy;
begin

end;

function TACSClient.GetNom: string;
begin
  result := FNom;
end;

procedure TACSClient.On7zProgressAdv(Sender: TObject; Filename: Widestring;
  MaxProgress, Position: int64);

begin
  if  (Position - DernierePosition) > ACSG7Z_PROGRESS_SEUIL then
  begin
    DernierePosition := Position;
    Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationAvancement));
    Athread.Connection.WriteInteger(MaxProgress);
    Athread.Connection.WriteInteger(Position);
  end;
end;

{ TACSClientList }

function TACSClientList.Add(Item: TACSClient): Integer;
begin
  result := inherited Add(Item);
end;

function TACSClientList.Add(pAThread: TIdPeerThread;pNom:string;pUtilisateur : TACSUtilisateur): TACSClient;
begin
  Result := TACSClient.Create(pAThread,Serveur,pNom,pUtilisateur);
  Add(Result);
end;


procedure TACSClientList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;    
  inherited;

end;

constructor TACSClientList.Create(pServeur : TACSServeur);
begin
  inherited Create;
  Serveur := pServeur;
end;

procedure TACSClientList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TACSClientList.Destroy;
begin
  Clear;    
  inherited;
end;

function TACSClientList.Extract(Item: TACSClient): TACSClient;
begin
  result := inherited Extract(Item);
end;

function TACSClientList.First: TACSClient;
begin
  Result := inherited First;
end;

function TACSClientList.Get(Index: Integer): TACSClient;
begin
  Result := inherited Get(Index);
end;

function TACSClientList.GetAThread(Index: TIdPeerThread): TACSClient;
var
  i : integer;
begin
  result := nil;
  for i := 0 to Count - 1 do
    if Items[i].AThread = Index then
      Result := Items[i];
end;

function TACSClientList.IndexOf(Item: TACSClient): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TACSClientList.Insert(Index: Integer; Item: TACSClient);
begin
  inherited Insert(Index,Item);
end;

function TACSClientList.Last: TACSClient;
begin
  Result := inherited Last;
end;

procedure TACSClientList.Put(Index: Integer; Item: TACSClient);
begin
  inherited Put(Index,Item);

end;

function TACSClientList.Remove(Item: TACSClient): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;


function TACSClientList.Remove(pAThread: TIdPeerThread): Integer;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if Items[i].AThread = pAThread then
      Remove(Items[i]);

end;

{ TACSDossierPartage }

constructor TACSDossierPartage.Create(pAdresse : string);
begin
  inherited Create;
  Adresse := pAdresse;
  Nom := ExtractFileName(Adresse);

end;

destructor TACSDossierPartage.Destroy;
begin

  inherited;
end;

procedure TACSDossierPartage.SetAdresse(const Value: string);
begin
  FAdresse := Value;
end;

procedure TACSDossierPartage.SetNom(const Value: string);
begin
  FNom := Value;
end;

{ TACSDossierPartageList }

function TACSDossierPartageList.Add(Item: TACSDossierPartage): Integer;
begin
  result := inherited Add(Item);
end;

function TACSDossierPartageList.Add(pAdresse: string): TACSDossierPartage;
begin
  Result := TACSDossierPartage.Create(pAdresse);
  Add(Result);
end;

procedure TACSDossierPartageList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;
  inherited;

end;

constructor TACSDossierPartageList.Create;
begin
  inherited;

end;

procedure TACSDossierPartageList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TACSDossierPartageList.Destroy;
begin
  Clear;
  inherited;
  
end;

function TACSDossierPartageList.Extract(Item: TACSDossierPartage): TACSDossierPartage;
begin
  result := inherited Extract(Item);
end;

function TACSDossierPartageList.First: TACSDossierPartage;
begin
  Result := inherited First;
end;

function TACSDossierPartageList.Get(Index: Integer): TACSDossierPartage;
begin
  Result := inherited Get(Index);
end;

function TACSDossierPartageList.GetNom(Index: string): TACSDossierPartage;
var
  i : integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if SameText(Index,Items[i].Nom) then
    begin
      Result := Items[i];
      exit;
    end;

end;

function TACSDossierPartageList.IndexOf(Item: TACSDossierPartage): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TACSDossierPartageList.Insert(Index: Integer; Item: TACSDossierPartage);
begin
  inherited Insert(Index,Item);
end;

function TACSDossierPartageList.Last: TACSDossierPartage;
begin
  Result := inherited Last;
end;

procedure TACSDossierPartageList.LoadFromFile(FileName: TFileName);
var
  Fichier : TStrings;
  i : Integer;
  Ligne : string;
  Liste : TStrings;
begin
  Fichier := TStringList.Create;
  Fichier.LoadFromFile(FileName);
  Liste := TStringList.Create;
  for i := 0 to Fichier.Count - 1 do
  begin
    Ligne := Fichier[i];
    Liste.Clear;
    //ExtractStrings(['|'],[' '],PChar(Ligne),Liste);
    ExtractListFromLine('|',Ligne,Liste);
    if Liste.Count = 2 then
      Add(Liste[1]).Nom := Liste[0];
  end;
  Fichier.Free;
end;

procedure TACSDossierPartageList.Put(Index: Integer; Item: TACSDossierPartage);
begin
  inherited Put(Index,Item);

end;

function TACSDossierPartageList.Remove(Item: TACSDossierPartage): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;

procedure TACSServeur.EcrireMessage(pMessageType :TACMessageType;Client: TACSClient; pMessage: string);
begin

  EcrireMessage(pMessageType, Client.Nom,pMessage);
end;

procedure TACSServeur.EcrireMessage(pMessageType :TACMessageType;AThread: TIdPeerThread;
  pMessage: string);
begin

  EcrireMessage(pMessageType,Clients.AThreads[AThread],pMessage);
end;

function TACSServeur.ListeDossiersPartages: string;
var
  i : integer;
begin
    result := '';
    for i := 0 to DossierPartages.Count - 1 do
    begin
      Result := Result + DossierPartages.Items[i].Nom+'|';
    end;
end;

procedure TACSDossierPartageList.SaveToFile(FileName: TFileName);
var
  Fichier : TStrings;
  i : Integer;
begin
  Fichier := TStringList.Create;
  for i := 0 to Count - 1 do
  begin
    Fichier.Add(Items[i].Nom+'|'+Items[i].Adresse);
  end;
  Fichier.SaveToFile(FileName);
  Fichier.Free;
end;

function TACSServeur.ObtenirAdresse(Chemin: String): string;
var
  AdrDossierPartage : string;
  pChemin : pChar;
begin
  pChemin := pChar(Chemin);
  AdrDossierPartage := '';
  while pChemin^ <> #0 do
  begin
    if pChemin^ <> '\' then
      AdrDossierPartage :=  AdrDossierPartage +pChemin^
    else
      break;
    Inc(pChemin);
  end;
  if DossierPartages.Nom[AdrDossierPartage] = nil then
  begin
    Result := '';
    exit;
  end;

  Result := DossierPartages.Nom[AdrDossierPartage].Adresse+pChemin;

end;

procedure TACSServeur.RequeteDossierLire(Athread: TIdPeerThread);
var
  Dossier : string;
  Fichier7z : String;
  ListeFichier7z : TStringList;
begin
  try

    Dossier := ObtenirAdresse(Athread.Connection.ReadLn);

    EcrireMessage(ACMessageRequete,AThread,'1/3 Lecture du dossier "'+ExtractFileName(Dossier)+'"');
    if not DirectoryExists(Dossier) then
    begin
      EcrireMessage(ACMessageErreur, Athread,'1/3 Erreur, dossier inexistant');
//      RequeteEchoue(Athread,ACCFDossierLire,'1/4 Erreur, dossier inexistant');
      exit;
    end;

    RequeteReussi(Athread,ACCFDossierLire,'2/4 Cr�ation des fichier 7z ');

//    EcrireMessage(AThread,'2/3 Cr�ation des fichier 7z ');
    ListeFichier7z := TStringList.Create;
    if not CreationDossier7z(Athread,Dossier,ListeFichier7z)then
    begin
      EcrireMessage(ACMessageErreur, Athread,'2/3 Erreur � la creation des fichiers 7z');
//      RequeteEchoue(Athread,ACCFDossierLire,'2/4 Erreur � la creation des fichiers 7z');
      exit;
    end;

//    RequeteReussi(Athread,ACCFDossierLire,'3/4 Envoie des fichier 7z');
    EcrireMessage(ACMessageInfo,  Athread,'3/4 Envoie des fichier 7z');
    if not EnvoyerFichiers(Athread,ListeFichier7z) then
    begin
      EcrireMessage(ACMessageErreur,Athread,'2/3 Erreur � l''envoie des fichiers 7z');
//      RequeteEchoue(Athread,ACCFDossierLire,'2/4 Erreur � l''envoie des fichiers 7z');
      exit;
    end;


    EcrireMessage(ACMessageInfo,Athread,'3/3 Lecture du dossier termin� ');
    ListeFichier7z.Free;

  except
    RequeteEchoue(Athread,ACCFDossierLire,'4/4 Erreur � la lecture du dossier');
    
  end;

end;

procedure TACSServeur.RequeteFichierLire(Athread: TIdPeerThread);
var
  Fichier : string;
  Fichier7z : String;
  ListeFichier7z : TStringList;
begin
  try

    Fichier := ObtenirAdresse(Athread.Connection.ReadLn);

    EcrireMessage(ACMessageRequete,AThread,'1/3 Lecture du fichier "'+Fichier+'"');
    if not FileExists(Fichier) then
    begin
      RequeteEchoue(Athread,ACCFFichierLire,'1/4 Erreur, fichier inexistant');
      exit;
    end;

    RequeteReussi(Athread,ACCFFichierLire,'2/3 Envoie des fichier 7z');

    ListeFichier7z := TStringList.Create;
    if not CreationFichier7z(Athread,Fichier,ListeFichier7z)then
    begin
      EcrireMessage(ACMessageErreur,Athread,'2/3 Erreur � la creation des fichiers 7z');
//      RequeteEchoue(Athread,ACCFFichierLire,'2/3 Erreur � la creation des fichiers 7z');
      exit;
    end;

    if not EnvoyerFichiers(Athread,ListeFichier7z) then
    begin
//      RequeteEchoue(Athread,ACCFFichierLire,'2/3 Erreur � l''envoie des fichiers 7z');
      EcrireMessage(ACMessageErreur,Athread,'2/3 Erreur � l''envoie des fichiers 7z');
      exit;
    end;

    EcrireMessage(ACMessageInfo,Athread,'3/3 lecture du fichier termin� ');
    ListeFichier7z.Free;
  except
    RequeteEchoue(Athread,ACCFFichierLire,'3/3 Erreur � la lecture du fichier');

  end;

end;

procedure TACSServeur.RequeteDossiersPartagesLister(Athread:TIdPeerThread);
begin
  try
    EcrireMessage(ACMessageRequete,AThread,'1/2 Liste des dossiers partag�s.');
    RequeteReussi(Athread,ACCFDossiersPartageLister,'2/2 Liste envoy�');
    Athread.Connection.WriteLn(ListeDossiersPartages);

  except
    RequeteEchoue(Athread,ACCFDossiersPartageLister,'2/2 Erreur lors de la liste des dossiers partag�s.');

  end;
    
end;

procedure TACSServeur.RequeteDossierLister(Athread: TIdPeerThread);
var

  Chemin : string;
  Adresse : string;
  Contenues : TACFichierInfoList;
  Dossier : TACContenueDossier;
  Nombre : Integer;

begin
  try

    Chemin := Athread.Connection.ReadLn;
    EcrireMessage(ACMessageRequete,AThread,'1/3 Lister le contenue du dossier "'+Chemin+'".');

    Adresse := ObtenirAdresse(Chemin);
    if Adresse = '' then
      exit;

    Dossier := TACContenueDossier.Create(Adresse);
    Dossier.Generer;
    Nombre := Dossier.Nombre+1;
    SetLength(Contenues,Nombre);
    Dossier.ExporteListeFichierInfo(Contenues);
    EcrireMessage(ACMessageInfo,AThread,'2/3 la liste contient '+IntToStr(Dossier.Nombre)+' Fichier(s).');
    RequeteReussi(Athread,ACCFDossierLister,'2/2 Envoie de la liste');
    Athread.Connection.WriteInteger(Nombre);
    Athread.Connection.OpenWriteBuffer();
    Athread.Connection.WriteBuffer(Contenues[0],sizeof(TACFichierInfo)*Nombre);
    Athread.Connection.CloseWriteBuffer();
    EcrireMessage(ACMessageInfo,AThread,'3/3 envoie de la liste termin�e');
    Dossier.Free;
  except
    RequeteEchoue(AThread,ACCFDossierLister,'3/3 Echec lors de la liste du dossier');

  end;
end;

procedure TACSServeur.RequeteMessage(Athread: TIdPeerThread);
begin
  try

    EcrireMessage(ACMessageRequete,AThread,Athread.Connection.ReadLn);
    Athread.Connection.WriteInteger(Integer(ACCFMessage));
    Athread.Connection.WriteInteger(ACRequeteResult(ACReqReussi));
    Athread.Connection.WriteLn('Message de recu.');
  except
    RequeteEchoue(Athread,ACCFMessage,'Echec � l''envoie du message.');
  end;
end;

procedure TACSServeur.RequeteDossierCopier(Athread: TIdPeerThread);
var
  Source : string;
  Destination : string;
  FichierInfo : TACFichierInfo;
begin
  try

    Source := Athread.Connection.ReadLn;
    Destination := Athread.Connection.ReadLn+'\'+ExtractFileName(Source);
    EcrireMessage(ACMessageRequete,Athread,'1/2 Copier le dossier "'+Source+'" Vers "'+Destination+'".');
    if not CopyDirectory(pchar(ObtenirAdresse(Source)),pChar(ObtenirAdresse(Destination))) then
    begin
      RequeteEchoue(Athread,ACCFDossierCopier,'1/2 Erreur lors de la copie du dossier.');
      exit;
    end;
    RequeteReussi(Athread,ACCFDossierCopier,'2/2 Copie termin�e');
  except
    RequeteEchoue(Athread,ACCFDossierCopier,'2/2 Copie du dossier impossible');
    
  end;
end;

procedure TACSServeur.RequeteDossierDeplacer(Athread: TIdPeerThread);
var
  Source : string;
  Destination : string;
  FichierInfo : TACFichierInfo;
begin
  try

    Source := Athread.Connection.ReadLn;
    Destination := Athread.Connection.ReadLn+'\'+ExtractFileName(Source);
    EcrireMessage(ACMessageRequete,Athread,'1/2 D�placement du dossier "'+Source+'" Vers "'+Destination+'"');
    if not MoveDirectory(pchar(ObtenirAdresse(Source)),pChar(ObtenirAdresse(Destination))) then
    begin
        RequeteEchoue(Athread,ACCFDossierDeplacer,'1/2 D�placement du dossier impossible');
        exit;
    end;
    RequeteReussi(Athread,ACCFDossierDeplacer,'2/2 D�placement termin�');
    
  except

    RequeteEchoue(Athread,ACCFDossierDeplacer,'2/2 D�placement du dossier impossible');
  end;


end;

procedure TACSServeur.RequeteDossierEcrire(Athread: TIdPeerThread);
var

  Dossier : string;
  ListeFichiers7z : TStringList;


begin
  try

    Dossier:= ObtenirAdresse(Athread.Connection.ReadLn);
    EcrireMessage(ACMessageRequete,Athread,Format('1/3 demande ecriture du dossier "%s" ',[Dossier]));
    ListeFichiers7z := TStringList.Create;
    if not ReceptionFichiers(Athread,FDossierTemp,ListeFichiers7z)then
    begin
      RequeteEchoue(Athread,ACCFDossierEcrire,'1/2 Erreur � la reception de fichier 7z');
      exit;
    end;

    EcrireMessage(ACMessageInfo,Athread,'2/3 Extraction du dossier ');
    ListeFichiers7z := TStringList.Create;
    if not ExtractionDossier7z(Athread,Dossier,ListeFichiers7z)then
    begin
      RequeteEchoue(Athread,ACCFDossierEcrire,'2/3 Erreur � l''extration de fichier 7z');
      exit;
    end;

    RequeteReussi(Athread,ACCFDossierEcrire,'3/3 Reception Termin�');
    ListeFichiers7z.Free;
  except
    RequeteEchoue(Athread,ACCFDossierEcrire,'3/3 Erreur � � la reception du dossier');

  end;


end;

procedure TACSServeur.RequeteDossierMAJInfo(Athread: TIdPeerThread);
var
  Chemin : string;
  FichierInfo : TACFichierInfo;
  Dossier : TACContenueDossier;
  Contenue : TACFichierInfoList;
  Nombre : Integer;
  Taille : Integer;
begin
  try

    Chemin := Athread.Connection.ReadLn;
    EcrireMessage(ACMessageRequete,Athread,'1/4 Mise a jour des informations du dossier."'+Chemin+'"');
    Dossier := TACContenueDossier.Create(ObtenirAdresse(Chemin));
    Dossier.Generer(true);
    Nombre  := Dossier.Nombre + 1;
    SetLength(Contenue,Nombre);
    EcrireMessage(ACMessageInfo,Athread,'2/4 Sauvegarde des informations du dossier');
    Dossier.ExporteListeFichierInfo(Contenue);
    Taille := SizeOf(TACFichierInfo) *  Nombre;
    RequeteReussi(Athread,ACCFDossierMAJInfo,'3/4 Envoie des informations du dossier.');
//    Athread.Connection.WriteLn(Chemin);
    Athread.Connection.WriteInteger(Nombre);
    Athread.Connection.OpenWriteBuffer;
    Athread.Connection.WriteBuffer(Contenue[0],Taille);
    Athread.Connection.CloseWriteBuffer;
    EcrireMessage(ACMessageInfo,Athread,'4/4 Mise a jour des informations termin�.');
    Dossier.Free;

  except
    RequeteEchoue(Athread,ACCFDossierMAJInfo,'4/4 Mise a jour des informations impossible.');

  end;

end;

procedure TACSServeur.RequeteDossierSupprimer(Athread: TIdPeerThread);
var
  Chemin : string;
  Adresse : string;

begin
  try

    Chemin := Athread.Connection.ReadLn;
    Adresse := ObtenirAdresse(Chemin);
    EcrireMessage(ACMessageRequete,Athread,'1/2 Suppresion du dossier "'+Adresse+'"');
    if not DeleteDirectory(Adresse) then
    begin
      RequeteEchoue(Athread,ACCFDossierSupprimer,'1/2 Erreur lors de la suppression du dossier impossible');
      exit;
    end;
    RequeteReussi(Athread,ACCFDossierSupprimer,'2/2 Supression du dossier termin�');

  except
    RequeteEchoue(Athread,ACCFDossierSupprimer,'2/2 Suppression du dossier impossible');
  end;

end;

procedure TACSServeur.RequeteFichierCopier(Athread: TIdPeerThread);
var
  Source : string;
  Destination : string;
  FichierInfo : TACFichierInfo;
begin
  try

    Source := Athread.Connection.ReadLn;
    Destination := Athread.Connection.ReadLn+'\'+ExtractFileName(Source);
    EcrireMessage(ACMessageRequete,Athread,'1/2 Copie du fichier "'+Source+'" Vers "'+Destination+'"');
    CopyFile(pchar(ObtenirAdresse(Source)),pChar(ObtenirAdresse(Destination)),false);
    RequeteReussi(Athread,ACCFFichierCopier,'2/2 Copie du fichier termin�');

  except
    RequeteEchoue(Athread,ACCFFichierCopier,'2/2 Echec lors de la copie le fichier');

  end;

end;

procedure TACSServeur.RequeteFichierEcrire(Athread: TIdPeerThread);
var
  Fichier : string;
  ListeFichiers7z : TStringList;

begin
  try

    Fichier := ObtenirAdresse(Athread.Connection.ReadLn);
    EcrireMessage(ACMessageRequete,Athread,Format('1/3 demande ecriture du fichier "%s" ',[Fichier]));
    ListeFichiers7z := TStringList.Create;
    if not ReceptionFichiers(Athread,FDossierTemp,ListeFichiers7z)then
    begin
      RequeteEchoue(Athread,ACCFDossierEcrire,'Erreur � la reception des fichiers 7z');
      exit;
    end;

    EcrireMessage(ACMessageInfo,Athread,'2/3 Extraction des fichier 7z');
    if not ExtractionFichier7z(Athread,Fichier,ListeFichiers7z)then
    begin
      RequeteEchoue(Athread,ACCFFichierEcrire,'2/3 Erreur � l''extration des fichiers 7z');
      exit;
    end;

    RequeteReussi(Athread,ACCFFichierEcrire,'3/3 Reception Termin�');
    ListeFichiers7z.Free;
    
  except
    RequeteEchoue(Athread,ACCFFichierEcrire,'3/3 Erreur � � la reception du dossier');

  end;



end;



procedure TACSServeur.RequeteFichierMAJInfo(Athread: TIdPeerThread);
var
  Chemin : string;
  Adresse : string;


  FichierInfo : TACFichierInfo;


begin
  try

    Chemin := Athread.Connection.ReadLn;
    EcrireMessage(ACMessageRequete,AThread,'1/3 Mise � jour des informations du fichier "'+ExtractFileName(Chemin)+'"');

    Adresse := ObtenirAdresse(Chemin);
    if Adresse = '' then
      exit;

    FichierInfo := ACLireFichierInfo(Adresse);
    RequeteReussi(Athread,ACCFFichierMAJInfo,'2/3 envoie des informations');
//    Athread.Connection.WriteLn(Chemin);
    Athread.Connection.OpenWriteBuffer();
    Athread.Connection.WriteBuffer(FichierInfo,sizeof(TACFichierInfo));
    Athread.Connection.CloseWriteBuffer();
    EcrireMessage(ACMessageInfo,AThread,'2/2 Mise a jour termin�');
  except
    RequeteEchoue(Athread,ACCFFichierMAJInfo,'2/2 Echec lors de la mise a jour des informations');

  end;

end;

procedure TACSServeur.RequeteFichierSupprimer(Athread: TIdPeerThread);
var
  Chemin : string;
begin

  try

    Chemin := Athread.Connection.ReadLn;
    EcrireMessage(ACMessageRequete,Athread,'1/2 Suppression du fichier "'+Chemin+'"');

    SysUtils.DeleteFile(ObtenirAdresse(Chemin));
    RequeteReussi(Athread,ACCFFichierSupprimer,'2/2 Supression termin�');
    
  except
    RequeteEchoue(Athread,ACCFFichierSupprimer,'2/2 Echec lors de la suppr�ssion du fichier');

  end;
end;

procedure TACSServeur.RequeteFichierDeplacer(Athread: TIdPeerThread);
var
  Source : string;
  Destination : string;
  FichierInfo : TACFichierInfo;
begin
  try

    Source := Athread.Connection.ReadLn;
    Destination := Athread.Connection.ReadLn+'\'+ExtractFileName(Source);
    EcrireMessage(ACMessageRequete,Athread,'1/2 D�placement le fichier "'+Source+'" Vers "'+Destination);
    MoveFile(pchar(ObtenirAdresse(Source)),pChar(ObtenirAdresse(Destination)));
    RequeteReussi(Athread,ACCFFichierDeplacer,'2/2 D�placement termin�');

  except
    RequeteEchoue(Athread,ACCFFichierDeplacer,'2/2 Echec lors du d�placement du fichier');

  end;

end;

procedure TACSServeur.RequeteDossierCreer(Athread: TIdPeerThread);
var
  Chemin : string;
begin
  try

    Chemin := Athread.Connection.ReadLn;
    EcrireMessage(ACMessageRequete,Athread,'1/2 Creation du dossier "'+Chemin+'"');
    if not ForceDirectories(ObtenirAdresse(Chemin)) then
    begin
      RequeteEchoue(Athread,ACCFDossierCreer,'1/2 Erreur lors de la cr�ation du dossier.');
      exit;
    end;
    RequeteReussi(Athread,ACCFDossierCreer,'2/2 Dossier cr��');

  except
    RequeteEchoue(Athread,ACCFDossierCreer,'2/2 Creation du dossier impossible');

  end;
end;

procedure TACSServeur.RequeteDossierRenommer(Athread: TIdPeerThread);
var
  Chemin : String;
  Nouveau : string;
begin
  try

    Chemin := Athread.Connection.ReadLn;
    Nouveau := Athread.Connection.ReadLn;
    EcrireMessage(ACMessageRequete,Athread,'1/2 Renommer le dossier "'+Chemin+'" en "'+Nouveau+'"');
    if not DirectoryExists(ObtenirAdresse(Chemin))then
    begin
      RequeteEchoue(Athread,ACCFDossierRenommer,'1/2 Erreur, dossier inexistant');
      exit;
    end;

    Chemin := ObtenirAdresse(Chemin);
    Nouveau := ExtractFilePath(Chemin)+Nouveau;
    if not RenameFile(Chemin,Nouveau) then
    begin
      RequeteEchoue(Athread,ACCFDossierRenommer,
        format('1/2 Erreur lors du changement du dossier "%s" vers "%s"',[Chemin,Nouveau]));
      exit
    end;

    RequeteReussi(Athread,ACCFDossierRenommer,'2/2 Renommer le Dossier termin�');
  except
    RequeteEchoue(Athread,ACCFDossierRenommer,'2/2 Impossible de renommer le dossier');
    
  end;


end;

procedure TACSServeur.RequeteFichierRenommer(Athread: TIdPeerThread);
var
  Chemin : String;
  Nouveau : string;
begin
  try

    Chemin := Athread.Connection.ReadLn;
    Nouveau := Athread.Connection.ReadLn;
    EcrireMessage(ACMessageRequete,Athread,format('1/2 Renommer le fichier "%se" en "%s".',
                      [ExtractFileName(Chemin),ExtractFileName(Nouveau)]));
    if not FileExists(ObtenirAdresse(Chemin))then
    begin
      RequeteEchoue(Athread,ACCFFichierRenommer,'1/2 Fichier inexistant.');
      exit;
    end;

    if not RenameFile(ObtenirAdresse(Chemin),ExtractFilePath(ObtenirAdresse(Chemin))+Nouveau) then
    begin
      RequeteEchoue(Athread,ACCFFichierRenommer,'1/2 Impossible de renommer le fichier.');
      exit;
    end;
    RequeteReussi(Athread,ACCFFichierRenommer,'2/2 Changement de nom de fichier termin�');

  except
    RequeteEchoue(Athread,ACCFFichierRenommer,'2/2 Impossible de renommer le fichier.');

  end;


end;

{ TACSUtilisateur }

constructor TACSUtilisateur.Create(pNom:string;pMotPasse : string);
begin
  FNom := pNom;
  FMotPasse := pMotPasse;

end;

destructor TACSUtilisateur.Destroy;
begin

  inherited;
end;


procedure TACSUtilisateur.SetMotPasse(const Value: string);
begin
  FMotPasse := Value;
end;

procedure TACSUtilisateur.SetNom(const Value: string);
begin
  FNom := Value;
end;

{ TACSUtilisateurList }

function TACSUtilisateurList.Add(Item: TACSUtilisateur): Integer;
begin
  result := inherited Add(Item);
end;

function TACSUtilisateurList.Add(pNom : string;pMotPasse: string) : TACSUtilisateur;
begin
  Result := Nom[pNom];
  if Result = nil then
    Result := TACSUtilisateur.Create(pNom,pMotPasse)
  else
    exit;
  Add(Result);
end;

procedure TACSUtilisateurList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;    
  inherited;

end;

constructor TACSUtilisateurList.Create;
begin
  inherited;

end;

procedure TACSUtilisateurList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TACSUtilisateurList.Destroy;
begin
  Clear;
  inherited;
end;

function TACSUtilisateurList.Extract(Item: TACSUtilisateur): TACSUtilisateur;
begin
  result := inherited Extract(Item);
end;

function TACSUtilisateurList.First: TACSUtilisateur;
begin
  Result := inherited First;
end;

function TACSUtilisateurList.Get(Index: Integer): TACSUtilisateur;
begin
  Result := inherited Get(Index);
end;

function TACSUtilisateurList.GetNom(Index: string): TACSUtilisateur;
var
  i : Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
  begin
    if Items[i].FNom = Index then
    begin
      Result := Items[i];
      exit;
    end;
  end;

end;

function TACSUtilisateurList.IndexOf(Item: TACSUtilisateur): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TACSUtilisateurList.Insert(Index: Integer; Item: TACSUtilisateur);
begin
  inherited Insert(Index,Item);
end;

function TACSUtilisateurList.Last: TACSUtilisateur;
begin
  Result := inherited Last;
end;

procedure TACSUtilisateurList.LoadFromFile(FileName: TFileName);
var
  i : integer;
  Fichier : TStringList;
  Liste : TStringList;
  ligne : string;
begin
  Fichier := TStringList.Create;
  Liste := TStringList.Create;
  Fichier.LoadFromFile(FileName);
  for i := 0 to Fichier.Count - 1 do
  begin
    Ligne := Fichier[i];
    Liste.Clear;
    //ExtractStrings(['|'],[' '],pChar(Ligne),Liste);
    ExtractListFromLine('|',Ligne,Liste);
    if Liste.Count >= 2 then
      Add(Liste[0],Liste[1]);
  end;
  Liste.Free;
  Fichier.Free;
end;

procedure TACSUtilisateurList.Put(Index: Integer; Item: TACSUtilisateur);
begin
  inherited Put(Index,Item);

end;

function TACSUtilisateurList.Remove(Item: TACSUtilisateur): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;

procedure TACSUtilisateurList.SaveToFile(FileName: TFileName);
var
  i : integer;
  Fichier : TStringList;
  ligne : string;
begin
  Fichier := TStringList.Create;
  for i := 0 to Count - 1 do
  begin
    Ligne := Items[i].FNom+'|'+Items[i].FMotPasse;
    Fichier.Add(ligne);
  end;
  Fichier.SaveToFile(FileName);
  Fichier.Free;


end;

function TACSUtilisateurList.UtilisateurValide(pNom,
  pMotPasse: string): TACSUtilisateur;
begin
  Result := Nom[pNom];
  if Result <> nil then
    if Result.FMotPasse <> pMotPasse then
      Result := nil;

end;

function TACSServeur.RequeteConnexion(Athread: TIdPeerThread):TACSUtilisateur;
var
  Nom : string;
  MotPasse : string;
  Utilisateur : TACSUtilisateur;
  GrainDeSel : string;
begin
  result := nil;
  try

    Nom := Athread.Connection.ReadLn;
    MotPasse := Athread.Connection.ReadLn;
    EcrireMessage(ACMessageRequete,Nom,'1/2 Demande d''identification de l''utilisateur "'+Nom+'"');
    Utilisateur:=  Utilisateurs.UtilisateurValide(Nom,MotPasse);
    if Utilisateur <> nil then
    begin
      EcrireMessage(ACMessageRequete,Nom,'2/2 Identification Accept�e');
      GrainDeSel := MD5CryptMotDePasse(IntToStr(RandomRange(-100000000,100000000)));
      Utilisateur.Cle := MD5CryptMotDePasse(Utilisateur.FNom+GrainDeSel);
      Athread.Connection.WriteInteger(ACCodeFonction(ACCFConnexion));
      Athread.Connection.WriteInteger(ACRequeteResult(ACReqReussi));
      Athread.Connection.WriteInteger(ACConnexionResult(ACConAccepte));
      Athread.Connection.WriteLn(GrainDeSel);
      result := Utilisateur;
    end
    else
    begin
      EcrireMessage(ACMessageErreur,Nom,'2/2 Identification Refus�e');
      Athread.Connection.WriteInteger(ACCodeFonction(ACCFConnexion));
      Athread.Connection.WriteInteger(ACRequeteResult(ACReqReussi));
      Athread.Connection.WriteInteger(ACConnexionResult(ACConRefuse));
      Athread.Connection.Disconnect;
      result := nil;
    end
  except
    EcrireMessage(ACMessageErreur,Nom,'2/2 Identification impossible');
    Athread.Connection.WriteInteger(ACCodeFonction(ACCFConnexion));
    Athread.Connection.WriteInteger(ACRequeteResult(ACReqEchec));
    Athread.Connection.Disconnect;
    result := nil;
  end;


end;

procedure TACSServeur.DoClientChange;
begin
    if Assigned(OnClientsChange) then
      OnClientsChange(Self);
end;

{ TACSHTTPServeur }

procedure TACSHTTPServeur.AfficherDossier(Chemin: string; HTML: TStrings);
const
  HTML_PAGES_DEBUT =
                '<html><head>'
		              +sLineBreak+'<link type="text/css" rel="stylesheet" media="screen" href="jqtouch/jqtouch.css">'
                  +sLineBreak+'<link type="text/css" rel="stylesheet" media="screen" href="themes/jqt/theme.css">'
		              +sLineBreak+'<script type="text/javascript" src="jqtouch/jquery.js"></script>'
		              +sLineBreak+'<script type="text/javascript" src="jqtouch/jqtouch.js"></script>'
		              +sLineBreak+'<script type="text/javascript">'
                  +sLineBreak+'var jQT = $.jQTouch({icon: "kilo.png"});</script>'
                  +sLineBreak+'<title>Serveur de fichier</title>'
                +sLineBreak+'</head>'
                +sLineBreak+'<body>'
                +sLineBreak+'<div id="home">'
                +sLineBreak+'<div class="toolbar">'
                +sLineBreak+'<h1>Serveur de fichiers</h1>'
                +sLineBreak+'</div>';
  HTML_PAGES_FIN ='</body></html>';
  HTML_DOSSIER = '<ul class="edgetoedge"><li class="arrow"><a href="�1">�2</a></li></ul>';
  HTML_FICHIER = '<ul class="edgetoedge"><li><a href="�1">�2</a></li></ul>';

var

  i : Integer;
  ligne : string;
  Adresse : string;
  Contenues : TACFichierInfoList;
  Dossier : TACContenueDossier;
  Nombre : Integer;

begin
  HTML.Clear;
  HTML.Add(HTML_PAGES_DEBUT);

//  if Chemin = '\' then
//  begin
    for i :=  0 to Serveur.DossierPartages.Count -1 do
    begin
      Ligne := HTML_DOSSIER;
      Ligne := StringReplace(ligne,'�1','#?dossier='+Serveur.DossierPartages[i].Nom,[rfReplaceAll,rfIgnoreCase]);
      Ligne := StringReplace(ligne,'�2',Serveur.DossierPartages[i].Nom,[rfReplaceAll,rfIgnoreCase]);
      HTML.Add(Ligne);
    end;
{
  end
  else
  begin
    Adresse := Serveur.ObtenirAdresse(Chemin);
    if Adresse = '' then
      exit;

    Dossier := TACContenueDossier.Create(Adresse);
    Dossier.Generer;

    for i :=  0 to Dossier.Dossiers.Count -1 do
    begin
      Ligne := HTML_DOSSIER;
      Ligne := StringReplace(ligne,'�1','?dossier='+Dossier.Dossiers[i].CheminLocal,[rfReplaceAll,rfIgnoreCase]);
      Ligne := StringReplace(ligne,'�2',Dossier.Dossiers[i].Nom,[rfReplaceAll,rfIgnoreCase]);
      HTML.Add(Ligne);
    end;

    for i :=  0 to Dossier.Fichiers.Count -1 do
    begin
      Ligne := HTML_FICHIER;
      Ligne := StringReplace(ligne,'�1','?fichier='+Dossier.Fichiers[i].CheminLocal,[rfReplaceAll,rfIgnoreCase]);
      Ligne := StringReplace(ligne,'�2',Dossier.Fichiers[i].Nom,[rfReplaceAll,rfIgnoreCase]);
      HTML.Add(Ligne);
    end;

    Dossier.Free;
  end;
  }
  HTML.Add(HTML_PAGES_FIN);




end;

constructor TACSHTTPServeur.Create(pServeur : TACSServeur);
begin
  inherited Create(nil);
  Serveur := pServeur;
  DefaultPort := ACSHTTP_SERVEUR_PORT;
  FDossierWeb := GetCurrentDir;
  OnCommandGet := DoCommandGet;
end;

destructor TACSHTTPServeur.Destroy;
begin

  inherited;
end;

function TACSServeur.GetWebActiver: boolean;
begin
  Result := WEB.Active;

end;

procedure TACSServeur.SetWebActiver(const Value: boolean);
begin
  WEB.Active := Value;
end;

procedure TACSHTTPServeur.DoCommandGet(AThread: TIdPeerThread;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
  Fichier : string;
  HTML : TStringList;
begin



    if ARequestInfo.Document = '/' then
    begin
        Fichier := 'index.html';
    end
    else
        Fichier := StrUtils.AnsiReplaceStr(ARequestInfo.Document,'/','\');


    if SameText(Fichier,'\Dossier.html')then
    begin
      HTML := TStringList.Create;
//      AResponseInfo.ContentType := 'html/text';

      if ARequestInfo.Params.Count > 0 then
        AfficherDossier(ARequestInfo.Params.Values['dossier'],HTML);
      Fichier := GetCurrentDir+'\page.html';
      HTML.SaveToFile(Fichier);
      ServeFile(AThread,AResponseInfo,Fichier);
//      AResponseInfo.ContentText := HTML.GetText;
      HTML.Free;

    end
    else
    begin

      Fichier := FDossierWeb +'\'+Fichier;
        //    AResponseInfo.ContentType := 'html/text';
        //    AResponseInfo.ContentStream := Fichier;

      try
        if FileExists(Fichier) then
          ServeFile(AThread,AResponseInfo,Fichier)
        else
          MessageDlg('le fichier "'+ExtractFileName(Fichier)+'" N''existe pas!!',mtError,[mbOk],0);
      except

      end;
    end;

    (*
    else
    begin
        Fichier := '';
        Taille := StrLen(pChar(ARequestInfo.Document));
        Fichier := AutoGenFichier(ARequestInfo.Document);
        if Fichier = '/strscr.jpg' then
        begin
            ImageMemoireScrBusy := true;
            AResponseInfo.ContentType := 'html/text';
            AResponseInfo.ContentStream := ImageMemoireScr;
            ImageMemoireScr.Free;
            exit;
        end;

        if Fichier = '/strrcs.jpg' then
        begin
            ImageMemoirercsBusy := true;
            AResponseInfo.ContentType := 'html/text';
            AResponseInfo.ContentStream := ImageMemoireRcs;
            ImageMemoireRcs.Free;
            exit;
        end;


        Fichier := StrUtils.AnsiReplaceStr(Fichier,'/','\');
        texte := ExtractFilePath(Fichier);

        if StrPos(pChar(texte),pCHar('serverfile')) <> nil then
            Fichier := StrUtils.AnsiReplaceStr(Fichier,'\serverfile\',Parametre.Audio.Dossier+'\')
        else
        begin
            Fichier := Parametre.WebServer.Dossier+Fichier;
        end;

        Ext := ExtractFileExt(Fichier);

        if ARequestInfo.Params.Count > 0 then
        begin
            try
                ARequestInfo.Params.SaveToFile(Fichier+'.par');
            finally

            end;
        end;
        if StrIComp(pChar(ARequestInfo.Document),pchar('/homescreen.html'))=0 then
        begin
            HomeScreenGestionParametre;
            HomeScreenCreerFichierHtml(Service.Capture.Actif);
        end;
        if StrComp(pChar(ARequestInfo.Document),pChar('/homescreenfolder.html'))=0 then
            if Parametre.WebServer.GestionDossier then
            begin
              HomeScreenFolderGestionParametre;
              HomeScreenFolderCreerFichierHTML;
            end;

        if StrComp(pChar(ARequestInfo.Document),pChar('/homescreenimage.html'))=0 then
            HomeScreenImageCreerFichierHTML;

        if StrComp(pChar(ARequestInfo.Document),pChar('/homescreenserverfile.html'))=0 then
        begin
            HomeScreenServerFileGestionParametre;
            HomeScreenServerFileCreerFichierHTML;
        end;



        if(Ext ='.html')then
        begin
            HTML := TStringList.Create;
            try
                HTML.LoadFromFile(Fichier);
                Texte := HTML.GetText;
                Texte := StrUtils.AnsiReplaceStr(Texte,'myurl',URL);
                HTML.SetText(pChar(Texte));
                Fichier := Parametre.WebServer.Dossier+'\tmp';
                HTML.SaveToFile(Fichier);
            finally
                HTML.free;
            end;
        end;


        if strPos(pchar('.wav .pdf .mp3 .wma .txt'),pchar(Ext))<> nil then
        begin
                AResponseInfo.ContentType := '';

        end;
//        else
//        begin
            if FileExists(Fichier)then
            begin
                try
                    IdHTTPServerSecours.ServeFile(AThread,AResponseInfo,Fichier);
                finally
                end;
            end;
//       end;
    end;
    *)




end;


procedure TACSHTTPServeur.EcrireMessage(pMessageType :TACMessageType;pMessage: string);
begin
  Serveur.EcrireMessage(pMessageType,'WEB :'+pMessage);
end;

procedure TACSHTTPServeur.SetDossierWeb(const Value: string);
begin
  FDossierWeb := Value;
end;

procedure TACSServeur.FermerLesConnexionsActive;
var
  i : Integer;
begin
  for i := 0 to Clients.Count - 1 do
    Clients[i].Athread.Connection.Disconnect;
end;

procedure TACSServeur.RequeteDossierSauvegarde(Athread: TIdPeerThread);
var
  Chemin : string;
  DossierContenue : string;
  FichierContenue : string;
  FichierContenueZip : string;
  Dossier : TACContenueDossier;
  Contenue : TACFichierInfoList;
  FichierZip : TFileStream;
  Nombre : Integer;
  i,Taille : Integer;
  Client : TACSClient;
  E : Exception;
begin
  try

    // demande de sauvegarde du serveur
    Chemin := Athread.Connection.ReadLn;
    EcrireMessage(ACMessageRequete,Athread,'1/5 Sauvegarde du dossier "'+Chemin+'"');

    // Generer le contenue du dossier
    Dossier := TACContenueDossier.Create(ObtenirAdresse(Chemin));
    Dossier.Generer(true);
    Nombre := Dossier.Nombre;
    Client := Clients.AThreads[Athread];
    // Creer le chemin du contenue
    DossierContenue :=  GetTempDirectory+'\'+Client.Nom;
    if not DirectoryExists(DossierContenue) then
      ForceDirectories(DossierContenue);
    FichierContenue := DossierContenue+'\Contenue.cnt';
    if FileExists(FichierContenue) then
      DeleteFile(pCHar(FichierContenue));

    // Creer le chemin du fichier zip
    FichierContenueZip := DossierContenue+'\Contenue.zip';
    if FileExists(FichierContenueZip) then
      DeleteFile(pCHar(FichierContenueZip));

    // Exporter la srtucture du dossier
    EcrireMessage(ACMessageInfo,Athread,'2/5 Exportation du fichier contenue vers "'+FichierContenue+'"');
    Dossier.ExporteListeFichierInfoVersFichier(FichierContenue);

    EcrireMessage(ACMessageInfo,Athread,'3/5 Compression du contenue vers "'+FichierContenueZip+'"');
    // Compresser le fichier du contenue
    ZipperFichier(FichierContenue,FichierContenueZip);
    // une fois le fichier compresser sur le fichier source
    if FileExists(FichierContenue) then
      DeleteFile(pChar(FichierContenue));

    // Envoie du fichier zip
    FichierZip := TFileStream.Create(FichierContenueZip,fmOpenRead);
    EcrireMessage(ACMessageInfo,Athread,'4/5 Envoie des informations du dossier, '+IntToStr(Nombre)
      +' Fichiers, Taille : ' +IntToStr(FichierZip.Size div 1024 )+'Ko' );
    Athread.Connection.WriteInteger(ACCodeFonction(ACCFDossierSauvegarder));
    Athread.Connection.WriteInteger(ACRequeteResult(ACReqReussi));
    Athread.Connection.WriteLn(Chemin);
    Athread.Connection.WriteInteger(FichierZip.Size);
    Athread.Connection.OpenWriteBuffer;
    Athread.Connection.WriteStream(FichierZip);
    Athread.Connection.CloseWriteBuffer;

    // Une fois le fichier compress� transmit suppimer la source
    if FileExists(FichierContenueZip) then
      DeleteFile(pChar(FichierContenueZip));
    EcrireMessage(ACMessageInfo,Athread,'5/5 Fin d''envoie des informations de sauvegarde');

  except
    on E: Exception do
    begin
      EcrireMessage(ACMessageErreur,Athread,'5/5 Mise a jour des informations impossible');
      if Dossier <> nil then
      begin
        EcrireMessage(ACMessageInfo,Athread,'5/5 Rappor dossier');
        for i := 0 to Dossier.Journal.Count - 1 do
          EcrireMessage(ACMessageInfo,Athread,'5/5 '+Dossier.Journal[i]);

        EcrireMessage(ACMessageInfo,Athread,'5/5 Fin Rapport ');
      end;
      Athread.Connection.WriteInteger(ACCodeFonction(ACCFDossierSauvegarder));
      Athread.Connection.WriteInteger(ACRequeteResult(ACReqEchec));
      EcrireMessage(ACMessageErreur,Athread,E.Message);
    end;
  end;
  if Dossier <> nil then
    Dossier.Free;

  if FichierZip <> nil then
    FichierZip.Free;


end;

procedure TACSServeur.RequeteExtinctionClient(Athread: TIdPeerThread);
var
  Client : TACSClient;
begin
  try

    Client := Clients.AThreads[Athread];
    EcrireMessage(ACMessageRequete,Athread,'1/2 Demande autorisation d''extinction du client "'+Client.Nom+'"');
    Athread.Connection.WriteInteger(ACCodeFonction(ACCFExtinctionClient));
    if ExtinctionClientAutorise then
    begin
      Athread.Connection.WriteInteger(ACRequeteResult(ACReqReussi));
      EcrireMessage(ACMessageRequete, Athread,'2/2 Autorisation accept�');
    end
    else
    begin
      Athread.Connection.WriteInteger(ACRequeteResult(ACReqEchec));
      EcrireMessage(ACMessageRequete, Athread,'2/2 Autorisation Refus�');
    end;

  except
    EcrireMessage(ACMessageErreur,Athread,'2/2 Echec � l''envoie de l''autorisation');
    Athread.Connection.WriteInteger(ACRequeteResult(ACReqEchec));
  end;

end;

procedure TACSServeur.EnvoieDesFichiersZip(ListeFichiersZip: TStrings;
  Athread: TIdPeerThread);

var
  FichierStream : TFileStream;
  i : integer;
begin
  (*
  EcrireMessage(AThread,'1/2 Transmission des fichiers 7z' );
  for i := 0 to ListeFichiersZip.Count - 1 do
  begin
      EcrireMessage(AThread,Format('2/2 Transmission %d/%d  du fichier 7z "%s"',[i+1,ListeFichiersZip.Count,ExtractFileName(ListeFichiersZip[i])]));
      FichierStream := TFileStream.Create(ListeFichiersZip[i],fmOpenRead);
      Athread.Connection.WriteInteger(ACCodeFonction(ACCFEnvoieFichierZip));
      Athread.Connection.WriteLn(ExtractFileName(ListeFichiersZip[i]));
      Athread.Connection.WriteInteger(i);
      Athread.Connection.WriteInteger(ListeFichiersZip.Count);
      Athread.Connection.WriteInteger(FichierStream.Size);
      Athread.Connection.OpenWriteBuffer;
      Athread.Connection.WriteStream(FichierStream);
      Athread.Connection.CloseWriteBuffer;
      FichierStream.Free;
  end;
  EcrireMessage(AThread,'2/2 Transmission des fichiers 7z termin�s');
  *)
end;

function TACSServeur.ReceptionFichierZip(
  ListeFichierZip: TStrings;DossierReception:string;Athread:TIdPeerThread): boolean;
var
  Fichier : string;
  Adresse : string;
  FichierZip : TFileStream;
  Taille : Integer;
  FichierReception : string;
  Numero : Integer;
  Nombre : Integer;
  Client : TACSClient;
begin

    Fichier := Athread.Connection.ReadLn;
    Numero := Athread.Connection.ReadInteger;
    Nombre := Athread.Connection.ReadInteger;
    Taille := Athread.Connection.ReadInteger;
    Client := Clients.AThreads[Athread];

    EcrireMessage(ACMessageInfo,Athread,Format('1/2 Reception du fichier 7z %d/%d %s de "%s"',
                      [Numero+1,Nombre,Fichier,SizeFileToStr(Taille)]));
    if not DirectoryExists(DossierReception) then
      ForceDirectories(DossierReception);
    FichierReception :=  DossierReception+'\'+Fichier;
    ListeFichierZip.Add(FichierReception);
    FichierZip  := TFileStream.Create(FichierReception,fmCreate);
    Athread.Connection.ReadStream(FichierZip,Taille);
    FichierZip.Free;
    EcrireMessage(ACMessageInfo,Athread,' 2/2 Reception des fichiers 7z terminer');

end;

procedure TACSServeur.EcrireMessage(pMessageType:TACMessageType; pSource, pMessage: string);
begin
    EcrireMessage(pMessageType,pSource+#9+' : '+pMessage);

end;

function TACSServeur.CreationDossier7z(Athread:TIdPeerThread;pDossier: string;
  ListeFichiers7z: TStrings): boolean;
var
  Fichier7z : string;
  DossierClient : string;
  Client : TACSClient;

begin

  Result := false;
  try
      // Compression du ficher
      Client := Clients.AThreads[Athread];

      DossierClient := FDossierTemp +'\'+Client.Nom+'_serveur';
      Fichier7z := DossierClient+'\'+Client.Nom+'_serveur.7z';
      EcrireMessage(ACMessageInfo,  Client,format('1/2 Creation des fichiers 7z "%s" du dossier "%s"',
                      [ExtractFileName(Fichier7z),ExtractFileName(pDossier)]));

      if DirectoryExists(DossierClient)then
        DeleteDirectory(DossierClient);
      ForceDirectories(DossierClient);
      Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationEnCours));
      if not SevenZipperDossier(
                          pDossier,
                          Fichier7z,
                          SEVEN_ZIP_TAILLE_VOLUME,
                          ListeFichiers7z,
                          Client.On7zProgressAdv,
                          Client.Utilisateur.Cle) then
      begin
        EcrireMessage(ACMessageErreur,Client,'1/2 Erreur de creation des fichiers 7z');
        Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationEchoue));
        exit;
      end;
      Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationTerminer));
      EcrireMessage(ACMessageInfo, Client,'2/2 Creation des fichiers 7z termin�');
      result := true;
  except
      EcrireMessage(ACMessageErreur, Client,'2/2 Erreur lors de la cr�ation des fichiers 7z');

  end;

end;

function TACSServeur.CreationFichier7z(Athread:TIdPeerThread;pFichier: string;
  ListeFichiers7z: TStrings): boolean;
var
  Fichier7z : string;
  DossierClient : string;
  Client : TACSClient;
begin

  Result := false;
  try
      // Compression du ficher
      Client := Clients.AThreads[Athread];

      DossierClient := FDossierTemp +'\'+Client.Nom+'_serveur';
      Fichier7z := DossierClient+'\'+Client.Nom+'_serveur.7z';
      EcrireMessage(ACMessageRequete, Client,format('1/2 Creation des fichiers 7z "%s" du fichier "%s"',
                      [ExtractFileName(Fichier7z),ExtractFileName(pFichier)]));

      if DirectoryExists(DossierClient)then
        DeleteDirectory(DossierClient);
      ForceDirectories(DossierClient);

      Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationEnCours));
      if not SevenZipperFichier(
                          pFichier,
                          Fichier7z,
                          SEVEN_ZIP_TAILLE_VOLUME,
                          ListeFichiers7z,
                          Client.On7zProgressAdv,
                          Client.Utilisateur.Cle) then
      begin
        EcrireMessage(ACMessageErreur, Client,'1/2 Erreur de creation des fichiers 7z');
        Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationEchoue));
        exit;
      end;
      EcrireMessage(ACMessageInfo, Client,'2/2 Creation des fichiers 7z termin�');
      Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationTerminer));
      result := true;
  except
      EcrireMessage(ACMessageErreur, Client,'2/2 Erreur lors de la cr�ation des fichiers 7z');

  end;
end;



function TACSServeur.EnvoyerFichier(Athread:TIdPeerThread;pFichier: string): boolean;
var
  Fichier : TFileStream;
begin

  result := false;
  try
      EcrireMessage(ACMessageInfo,Athread,format('1/2 Envoie du fichier "%s"',[ExtractFileName(pFichier)]));
      Fichier := TFileStream.Create(pFichier,fmOpenRead);
      EcrireMessage(ACMessageInfo,Athread,'2/3 Taille du fichier '+SizeFileToStr(Fichier.Size));
      Athread.Connection.WriteInteger(Fichier.size);
      Athread.Connection.OpenWriteBuffer;
      Athread.Connection.WriteStream(Fichier);
      Athread.Connection.CloseWriteBuffer;
      Fichier.Free;
      EcrireMessage(ACMessageInfo,Athread,'3/3 Envoie du fichier termin�');
      result := true;
  except
      EcrireMessage(ACMessageErreur, Athread,'3/3 Erreur lors de l''envoie du fichier');
  end;
  
end;

function TACSServeur.EnvoyerFichiers(Athread:TIdPeerThread;pListeFichiers: TStrings): boolean;
var
  i : integer;
begin
  result := false;
  try
    EcrireMessage(ACMessageInfo, Athread,format('1/2 Envoie des %d fichiers ',[pListeFichiers.Count]));
    Athread.Connection.WriteInteger(pListeFichiers.Count);
    for i := 0 to pListeFichiers.Count - 1 do
    begin
      EcrireMessage(ACMessageInfo,Athread,Format('2/3 Envoie du fichier %d/%d',[i+1,pListeFichiers.Count]));
      Athread.Connection.WriteLn(ExtractFileName(pListeFichiers[i]));
      if not EnvoyerFichier(Athread,pListeFichiers[i]) then
      begin
          EcrireMessage(ACMessageErreur,Athread,'2/3 Erreur lors de l''envoie des fichier.');
          exit;
      end;
    end;
    EcrireMessage(ACMessageInfo, Athread,'3/3 Envoie des fichiers termin�.');
    result := true;

  except
    EcrireMessage(ACMessageErreur, Athread,'3/3 Erreur lors de l''envoie des fichiers.');

  end;


end;



function TACSServeur.RequeteSyncFichierEcrire(Athread:TIdPeerThread): boolean;
var
  Chemin : string;
  Client : TACSClient;
  Fichier : string;
  Fichier7z : string;
  FichierInfo : string;
  ListeFichiers7z : TStringList;
  SyncCluster : TSyncClusterStream;
begin


  try
    Chemin := Athread.Connection.ReadLn;
    Client := Clients.AThreads[Athread];
    Fichier := ObtenirAdresse(Chemin);
    FichierInfo := DossierTemp+'\'+Client.Nom+'_serveur.info';

    // 1 :  Lecture des information du fichier
    EcrireMessage(ACMessageRequete, Athread,format('1/3 Demande d''information pour la synchronisation avanc� du fichier "%s"',
      [ExtractFileName(Chemin)]));

    if not FileExists(Fichier) then
    begin
      RequeteEchoue(Athread,ACCFSyncFichierEcrire,'1/3 Erreur, Fichier inexistant.');
      exit;
    end;


    Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationEnCours));
    SyncCluster := TSyncClusterStream.Create;
    SyncCluster.OnClusterProgress := Client.ClusterProgress;
    if not SyncCluster.LireInfo(Fichier,FichierInfo) then
    begin
      RequeteEchoue(Athread,ACCFSyncFichierEcrire,
        '1/3 Erreur lors de la lecture des informations.');
      exit;
    end;
    Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationTerminer));

    // 2 : Creation des fichier 7z du fichier de mise � jour
    RequeteReussi(Athread,ACCFSyncFichierEcrire,
                    '2/3 Creation des fichier 7z.');
    Athread.Connection.WriteLn(ExtractFileName(FichierInfo));

    Fichier7z := DossierTemp+'\'+Client.Nom+'_serveur.info.7z';
    ListeFichiers7z := TStringList.Create;
    ListeFichiers7z.Clear;
    if not CreationFichier7z(Athread,FichierInfo,ListeFichiers7z) then
    begin
        EcrireMessage(ACMessageErreur, '2/3 Erreur de la creation du fichier 7z');
        exit;
    end;

    // 3 /3 Envoie des fichiers 7z
    EcrireMessage(ACMessageInfo,Athread,'3/4 Envoie des fichiers 7z.');
    if not EnvoyerFichiers(Athread,ListeFichiers7z) then
    begin
        EcrireMessage(ACMessageErreur,Athread,'3/4 Erreur � l''envoie du fichier de mise � jour');
        exit;
    end;

    EcrireMessage(ACMessageInfo, Athread,'4/4 Demande d''information termin�.');

    ListeFichiers7z.Free;
    SyncCluster.Free;
    result := true;

  except

    EcrireMessage(ACMessageErreur,'4/4 Erreur lors de la lecture des informations du fichier.');
  end;

end;

function TACSServeur.RequeteSyncFichierLire(Athread:TIdPeerThread): boolean;
var
  Chemin : string;
  Adresse : string;
//  Fichier : string;
  Fichier7z : string;
  FichierInfo : string;
  FichierMAJ : string;
  ListeFichiers7z : TStringList;
  Client : TACSClient;
  SyncCluster : TSyncClusterStream;

begin


  try

    Client := Clients.AThreads[Athread];
    FichierMAJ := DossierTemp+'\'+Client.Nom+'_serveur.maj';
    Chemin := Athread.Connection.ReadLn;
    FichierInfo := DossierTemp+'\'+Athread.Connection.ReadLn;
    Adresse := ObtenirAdresse(Chemin);
    EcrireMessage(ACMessageRequete, Athread,format('1/6 Synchronisation avanc� du fichier "%s"',[ExtractFileName(Chemin)]));
    // 1 : r�ception des fichiers 7z du fichier d'information
    ListeFichiers7z := TStringList.Create;
    if not ReceptionFichiers(Athread,FDossierTemp,ListeFichiers7z)then
    begin
      RequeteEchoue(Athread,ACCFDossierEcrire,'1/6 Erreur � la r�ception des fichiers 7z');
      exit;
    end;

    // 2 : Extration du fichier d'information
    EcrireMessage(ACMessageInfo,Athread,'2/6 Extraction des fichier 7z');
    if not ExtractionFichier7z(Athread,FichierInfo,ListeFichiers7z)then
    begin
      RequeteEchoue(Athread,ACCFFichierEcrire,'2/6 Erreur � l''extration des fichiers 7z');
      exit;
    end;

    // 3 : Cr�ation du fichier de mise � jour.
    EcrireMessage(ACMessageInfo,Athread,'3/6 Cr�ation du fichier de mise � jour.');
    SyncCluster := TSyncClusterStream.Create;
    SyncCluster.OnClusterProgress := Client.ClusterProgress;

    Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationEnCours));
    if not SyncCluster.CreerMiseAJour(Adresse,FichierInfo,FichierMAJ) then
    begin
        Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationEchoue));
        EcrireMessage(ACMessageErreur,Athread,'3/6 Erreur lors de la cr�ation du fichier de mise � jour');
        exit;
    end;
    EcrireMessage(ACMessageInfo,SyncCluster.Rapport.GetText);
    Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationTerminer));


    // 4 : envoie des fichiers 7z du fichier de mise � jour
    RequeteReussi(Athread,ACCFSyncFichierLire,
                    format('4/6 Envoie des %d fichier(s)  d''information 7z du ficher %s',
                    [ListeFichiers7z.Count,ExtractFileName(Chemin)]));

    // 5 : Creation des fichier 7z du fichier de mise � jour
    EcrireMessage(ACMessageInfo,Athread,'5/6 Cr�ation du fichier 7z.');
    Athread.Connection.WriteLn(ExtractFileName(FichierMAJ));
    Fichier7z := DossierTemp+'\'+Client.Nom+'_serveur.maj.7z';
    ListeFichiers7z.Clear;
    if not CreationFichier7z(Athread,FichierMAJ,ListeFichiers7z) then
    begin
        EcrireMessage(ACMessageErreur,'5/6 Erreur de la creation du fichier 7z');
        exit;
    end;

    if not EnvoyerFichiers(Athread,ListeFichiers7z) then
    begin
        EcrireMessage(ACMessageErreur, Athread,'5/6 Erreur � l''envoie du fichier de mise � jour');
        exit;
    end;

    EcrireMessage(ACMessageInfo, Athread,'6/6 Synchronisation avanc� du fichier termin�.');
    ListeFichiers7z.Free;
    SyncCluster.Free;
    result := true;

  except

    EcrireMessage(ACMessageErreur, 'Erreur de synchronisation par cluster du fichier');
  end;



end;
function TACSServeur.ExtractionDossier7z(Athread:TIdPeerThread;
                pDossier: string;ListeFichier7z: TStrings): boolean;
var
  FichierReception : string;
  Client : TACSClient;
begin

  result := false;

  try
    Client := Clients.AThreads[Athread];
    EcrireMessage(ACMessageInfo, Client,Format('1/2 Extraction des %d fichier(s) zip du dossier "%s".',
          [ListeFichier7z.Count,ExtractFileName(pDossier)]));
    FichierReception := ListeFichier7z[0];

    if Not DirectoryExists(ExtractFilePath(pDossier)) then
      ForceDirectories(ExtractFilePath(pDossier));

    if DeSevenZipperDossier(
          ExtractFilePath(pDossier),
          FichierReception,
          Client.On7zProgressAdv,
          Client.Utilisateur.Cle) then
    begin
      EcrireMessage(ACMessageErreur,Client,'1/2 Erreur lors de l''extraction du dossier.');
      exit;
    end;

    DeleteDirectory(ExtractFilePath(FichierReception));
    EcrireMessage(ACMessageInfo,Client,'2/2 R�ception du dossier termin�.');
    result := true;
  except
    EcrireMessage(ACMessageErreur,Client,'2/2 Erreur lors de la r�ception du dossier.');
    
  end;


end;

function TACSServeur.ExtractionFichier7z(Athread:TIdPeerThread;
    pFichier: string;ListeFichier7z: TStrings): boolean;
var
  FichierReception : string;
  Client : TACSClient;
begin

  result := false;

  try
    Client := Clients.AThreads[Athread];
    EcrireMessage(ACMessageInfo,Client,Format('1/2 Extraction des %d fichier(s) zip du fichier "%s".',
          [ListeFichier7z.Count,ExtractFileName(pFichier)]));
    FichierReception := ListeFichier7z[0];

    if Not DirectoryExists(ExtractFilePath(pfichier)) then
      ForceDirectories(ExtractFilePath(pfichier));

    if not DeSevenZipperFichier(
            ExtractFilePath(pfichier),
            FichierReception,
            Client.On7zProgressAdv,
            Client.Utilisateur.Cle) then
    begin
      EcrireMessage(ACMessageErreur,Client,'1/2 Erreur lors de l''extraction du fichier.');
      exit;
    end;

    DeleteDirectory(ExtractFilePath(FichierReception));
    EcrireMessage(ACMessageInfo,Client,'2/2 R�ception du fichier termin�.');
    result := true;
  except
    EcrireMessage(ACMessageErreur,Client,'2/2 Erreur lors de la r�ception du fichier.');
  end;


end;

function TACSServeur.ReceptionFichier(Athread:TIdPeerThread;pFichier: String): boolean;
var
  Fichier : TFileStream;
  Taille : Integer;
begin

  result := false;
  Try
    Taille := Athread.Connection.ReadInteger;
    EcrireMessage(
              ACMessageInfo,
              Athread,
              Format('1/2 R�ception du fichier "%s" taille %s',[ExtractFileName(pFichier),SizeFileToStr(Taille)]));
    Fichier  := TFileStream.Create(pFichier,fmCreate);
    Athread.Connection.ReadStream(Fichier,Taille);
    Fichier.Free;
    EcrireMessage(
              ACMessageInfo,
              Athread,
              ' 2/2 R�ception du fichier termin�');
    result := true;
  except
    EcrireMessage(
              ACMessageErreur,
              Athread,
              '2/2 Erreur lors de la r�ception du fichier.');

  end;

end;

function TACSServeur.ReceptionFichiers(Athread:TIdPeerThread;pDossier: string;
  pListeFichier: TStrings): boolean;
var
  i : Integer;
  Nombre : Integer;
  Fichier : string;
begin

    Result := false;
    try
      Nombre := Athread.Connection.ReadInteger;

      pListeFichier.Clear;
      EcrireMessage(ACMessageInfo,Athread,Format('1/3 Reception des %d fichiers ',[Nombre]));

      for i := 0 to Nombre - 1 do
      begin
          Fichier := pDossier+'\'+Athread.Connection.ReadLn;

          EcrireMessage(ACMessageInfo,Athread,Format('2/3 Reception du fichier %d/%d ',[i+1,Nombre]));
          if not ReceptionFichier(Athread,Fichier) then
          begin
            EcrireMessage(ACMessageErreur,Athread,'2/3 Erreur lors de la reception  du fichier '+IntToStr(i+1));
            exit;
          end;
          pListeFichier.Add(Fichier);
      end;
      EcrireMessage(ACMessageInfo,Athread,' 3/3 Reception des fichiers termin�');
      Result := true;
    except
      EcrireMessage(ACMessageErreur,Athread,'3/3 Erreur lors de la reception de fichiers');

    end;
end;

function TACSServeur.ReceptionFichiers7z(Athread:TIdPeerThread;
                                pListeFichier7z: TStringList): boolean;
var
  DossierReception : string;

begin

  result := false;
  (*
  try
    EcrireMessage('1/3 Attente de creation des fichiers 7z ');
    if not AttenteOperation('Cr�ation des fichiers 7z') then
    begin
      EcrireMessage('1/3 Erreur lorsde l''attante de la lecture du fichiers ');
      exit;
    end;

    EcrireMessage('2/3 Reception des fichiers 7z');

    if not DirectoryExists(Poste.DossierTemp) then
        ForceDirectories(Poste.DossierTemp);

    if not ReceptionFichiers(Poste.DossierTemp,pListeFichier7z) then
    begin
      EcrireMessage('1/4 Erreur lors de la receptions des fichiers 7z');
      exit;
    end;

    EcrireMessage('3/3 Reception des fichiers termin�');
    result := true;

  except
    EcrireMessage('Lecture du fichier impossible');

  end;

  *)
end;

procedure TACSServeur.SetDossierTemp(const Value: string);
begin
  FDossierTemp := Value;
end;

procedure TACSServeur.RequeteEchoue(Athread: TIdPeerThread;
  Code: TACCodeFonction; pMessage: string);
begin
  try
    EcrireMessage(ACMessageErreur,Athread,pMessage);
    Athread.Connection.WriteInteger(ACCodeFonction(Code));
    Athread.Connection.WriteInteger(ACRequeteResult(ACReqEchec));
  except
    EcrireMessage(ACMessageErreur,Athread,'Erreur de communication');
  end;
end;

procedure TACSServeur.RequeteReussi(Athread: TIdPeerThread;
  Code: TACCodeFonction;pMessage:string);
begin
  try
    EcrireMessage(ACMessageInfo,Athread,pMessage);
    Athread.Connection.WriteInteger(ACCodeFonction(Code));
    Athread.Connection.WriteInteger(ACRequeteResult(ACReqReussi));
  except
    EcrireMessage(ACMessageErreur,Athread,'Erreur de communication');
  end;

end;


function TACSServeur.RequeteSyncFichierEcrireMAJ(
  Athread: TIdPeerThread): boolean;
var
  Chemin : string;
  Adresse : string;
  Fichier7z : string;
  FichierMAJ : string;
  ListeFichiers7z : TStringList;
  Client : TACSClient;
  SyncCluster : TSyncClusterStream;

begin


  try

    Client := Clients.AThreads[Athread];
    Chemin := Athread.Connection.ReadLn;
    FichierMAJ := DossierTemp+'\'+Athread.Connection.ReadLn;
    Adresse := ObtenirAdresse(Chemin);
    EcrireMessage(ACMessageRequete,Athread,format('1/4 Sychronisation avanc�, mise � jour du fichier "%s"',[ExtractFileName(Chemin)]));
    EcrireMessage(ACMessageInfo,Athread,format('1/4 Fichier de mis � jour"%s"',[ExtractFileName(FichierMAJ)]));

    // 1 : r�ception des fichiers 7z du fichier de mise a  jour
    ListeFichiers7z := TStringList.Create;
    if not ReceptionFichiers(Athread,FDossierTemp,ListeFichiers7z)then
    begin
      RequeteEchoue(Athread,ACCFDossierEcrire,'1/4 Erreur � la r�ception des fichiers 7z');
      exit;
    end;

    // 2 : Extration du fichier d'information
    EcrireMessage(ACMessageInfo,Athread,'2/4 Extraction des fichier 7z');
    if not ExtractionFichier7z(Athread,FichierMAJ,ListeFichiers7z)then
    begin
      RequeteEchoue(Athread,ACCFFichierEcrire,'2/4 Erreur � l''extration des fichiers 7z');
      exit;
    end;

    // 3 : Appliquer le fichier de mise � jour
    EcrireMessage(ACMessageInfo,Athread,'3/4 Application de la mise � jour.');
    SyncCluster := TSyncClusterStream.Create;
    SyncCluster.OnClusterProgress := Client.ClusterProgress;
    Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationEnCours));
    if not SyncCluster.AppliquerMiseAjour(Adresse,FichierMAJ) then
    begin
        Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationEchoue));
        EcrireMessage(ACMessageErreur,Athread,'3/4 Erreur lors de l''application de la mise � jour');
        exit;
    end;
    Athread.Connection.WriteInteger(ACCodeFonction(ACCFOperationTerminer));

    // 4 : envoie des fichiers 7z du fichier de mise � jour
    RequeteReussi(Athread,ACCFSyncFichierEcrireMAJ,
                    '4/4 Synchronisation avanc� en ecriture du fichier termin�.');

    ListeFichiers7z.Free;
    SyncCluster.Free;
    result := true;

  except
    EcrireMessage(ACMessageErreur,Athread,'4/4 Erreur de synchronisation en ecriture du fichier');
    
  end;

end;

end.
