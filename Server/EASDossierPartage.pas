unit EASDossierPartage;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls,ArchivesCloudServer,FileCtrl;

type
  TFormEASDossierPartage = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    LabeledEditNom: TLabeledEdit;
    LabeledEditAdresse: TLabeledEdit;
    ButtonAdresse: TButton;
    Image1: TImage;
    procedure ButtonAdresseClick(Sender: TObject);
  private
    function GetAdresse: string;
    function GetNom: string;
    { d�clarations priv�es }
  public
    { d�clarations publiques }
    property Nom : string read GetNom;
    property Adresse : string read GetAdresse;
    function Execute(DossierPartage : TACSDossierPartage = nil):boolean;

  end;

implementation

{$R *.dfm}

{ TOKBottomDlg }

function TFormEASDossierPartage.Execute(
  DossierPartage: TACSDossierPartage): boolean;
begin
  if DossierPartage =  nil then
  begin
    ButtonAdresseClick(nil);
  end
  else
  begin

    LabeledEditNom.Text := DossierPartage.Nom;
    LabeledEditNom.Enabled := false;
    LabeledEditAdresse.Text := DossierPartage.Adresse;
    OKBtn.Enabled := true;
  end;
  result := ShowModal = mrok;
end;

function TFormEASDossierPartage.GetAdresse: string;
begin
  Result := LabeledEditAdresse.Text;
end;

function TFormEASDossierPartage.GetNom: string;
begin
  Result := LabeledEditNom.Text;
end;

procedure TFormEASDossierPartage.ButtonAdresseClick(Sender: TObject);
var
  Dossier : String;
begin
  if SelectDirectory('Selection du dossier � partager','',Dossier) then
  begin
    LabeledEditAdresse.Text := Dossier;
    LabeledEditNom.Text := ExtractFileName(Dossier);
    OKBtn.Enabled := true;
  end;
end;

end.
