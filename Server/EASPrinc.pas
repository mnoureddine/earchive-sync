unit EASPrinc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, StdCtrls, ActnList,EASDonnees, IdBaseComponent,
  IdComponent, IdTCPServer,ArchivesCloudServer,EASConfiguration,EASDossierPartage,
  ImgList, JvComponentBase, JvTrayIcon, ExtCtrls,EASEditUtilisateur,Utiles,
  IdCustomHTTPServer, IdHTTPServer,ArchivesCloudCommun, IdThreadMgr,
  IdThreadMgrPool, IdAntiFreezeBase, IdAntiFreeze;

type
  TFormEASPrinc = class(TForm)
    TreeViewServeurs: TTreeView;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    ListBoxMessages: TListBox;
    PopupMenuServeur: TPopupMenu;
    ActionServeurActive: TAction;
    ActionServeurDossierAjouter: TAction;
    MainMenu1: TMainMenu;
    Activ1: TMenuItem;
    ActionServeurPartageDossier1: TMenuItem;
    ActionDossierModifier: TAction;
    ActionDossierSupprimer: TAction;
    Edition1: TMenuItem;
    ImageListMenu: TImageList;
    ActionServeurConfiguration: TAction;
    Configuration1: TMenuItem;
    ImageListTrayIcons: TImageList;
    PopupMenu1: TPopupMenu;
    Activ2: TMenuItem;
    Partagundossier1: TMenuItem;         
    Configuration2: TMenuItem;
    ActionServeurQuitter: TAction;
    Quitter1: TMenuItem;
    Quitter2: TMenuItem;
    ImageList1: TImageList;
    Splitter1: TSplitter;
    JvTrayIcon1: TJvTrayIcon;
    ActionUtilisateurAjouter: TAction;
    ActionUtilisateurModifier: TAction;
    ActionUtilisateurSupprimer: TAction;
    Ajouterunutilisateur1: TMenuItem;
    ActionServeurWebActiver: TAction;
    ActionExtinctionClientAutorise: TAction;
    Autorisationdextinction1: TMenuItem;
    IdTCPServer1: TIdTCPServer;
    IdThreadMgrPool1: TIdThreadMgrPool;
    IdAntiFreeze1: TIdAntiFreeze;
    PopupMenuJournal: TPopupMenu;
    Effacertout1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure TreeViewServeursClick(Sender: TObject);
    procedure ActionServeurActiveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionServeurDossierAjouterExecute(Sender: TObject);
    procedure ActionDossierModifierExecute(Sender: TObject);
    procedure ActionDossierSupprimerExecute(Sender: TObject);
    procedure ActionServeurConfigurationExecute(Sender: TObject);
    procedure ActionServeurQuitterExecute(Sender: TObject);
    procedure ActionUtilisateurAjouterExecute(Sender: TObject);
    procedure ActionUtilisateurModifierExecute(Sender: TObject);
    procedure ActionUtilisateurSupprimerExecute(Sender: TObject);
    procedure PopupMenuServeurPopup(Sender: TObject);
    procedure TreeViewServeursChange(Sender: TObject; Node: TTreeNode);
    procedure TreeViewServeursDblClick(Sender: TObject);
    procedure ActionServeurWebActiverExecute(Sender: TObject);
    procedure IdHTTPServer1AfterCommandHandler(ASender: TIdTCPServer;
      AThread: TIdPeerThread);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure JvTrayIcon1Click(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ActionExtinctionClientAutoriseExecute(Sender: TObject);
    procedure JvTrayIcon1DblClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Effacertout1Click(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }

    NodeServeur : TTreeNode;
    NodeClients : TTreeNode;
    NodeUtilisateurs : TTreeNode;
    BallonMessages : TStringList;
    Demarrage : boolean;
    procedure ServeurMessage(Sender : TObject;pMessageType : TACMessageType;pMessage : string);
    procedure ServeurStatus(ASender: TObject;
      const AStatus: TIdStatus; const AStatusText: String);
    procedure Actualiser;
    procedure ActualiserNoeud(Node : TTreeNode);
    procedure ActualiserServeur;
    procedure ActualiserClients;
    procedure ActualiserUtilisateurs;
    procedure ActualiserMenu(Node : TTreeNode);overload;
    procedure ActualiserUtilisateur(NodeUtilisateur : TTreeNode);
    procedure DoubleClickNoeud(Node : TTreeNode);
    procedure ServeurConnect(AThread: TIdPeerThread);
    procedure ServeurDisconnect(AThread: TIdPeerThread);
    procedure ActualiserDossier(NodeDossier : TTreeNode);
    procedure ActualiserMenu(Categorie : String);overload;
    procedure ServeurClientsChange(Sender : Tobject);
    procedure ServeurActualiser;
    procedure WebServeurConnect(AThread: TIdPeerThread);
    procedure WebServeurDisconnect(AThread: TIdPeerThread);

  end;

var
  FormEASPrinc: TFormEASPrinc;

implementation

{$R *.dfm}



procedure TFormEASPrinc.Actualiser;
begin   
  TreeViewServeurs.Items.Clear;
  NodeServeur := TreeViewServeurs.Items.AddChildFirst(nil,'Serveur');
  NodeServeur.Data := Donnees.Serveur;
  NodeServeur.ImageIndex :=  0;
  NodeServeur.SelectedIndex := 0;

  ActualiserNoeud(NodeServeur);

  NodeClients := TreeViewServeurs.Items.AddChildFirst(nil,'Clients');
  NodeClients.Data := Donnees.Serveur.Clients;
  NodeClients.ImageIndex :=  2;
  NodeClients.SelectedIndex :=  2;
  ActualiserNoeud(NodeClients);

  NodeUtilisateurs := TreeViewServeurs.Items.AddChildFirst(nil,'Utilisateurs');
  NodeUtilisateurs.Data := Donnees.Serveur.Utilisateurs;
  NodeUtilisateurs.ImageIndex :=  6;
  NodeUtilisateurs.SelectedIndex :=  6;
  ActualiserNoeud(NodeUtilisateurs);

  ServeurActualiser;
end;

procedure TFormEASPrinc.ActualiserMenu(Categorie: String);
var
  i : integer;
  lMenu :  TMenuItem;
begin

  PopupMenuServeur.Items.Clear;
  for i := 0 to ActionList1.ActionCount - 1 do
    if SameText(Categorie,ActionList1.Actions[i].Category) then
    begin
        lMenu := TMenuItem.Create(self);
        lMenu.Action := ActionList1.Actions[i];
        PopupMenuServeur.Items.Add(lMenu);
    end;
end;

procedure TFormEASPrinc.ActualiserNoeud(Node: TTreeNode);
var
  p : TRect;
begin

  if Node = nil then
    exit;

  if Node.Data = Donnees.Serveur then
  begin
    ActualiserServeur;
    ActualiserMenu('Serveur');
    exit;
  end;

  if Node.Data = Donnees.Serveur.Clients then
  begin
    ActualiserClients;
    ActualiserMenu('Clients');
    exit;
  end;

  if Node.Data = Donnees.Serveur.Utilisateurs then
  begin
    ActualiserUtilisateurs;
    ActualiserMenu('Utilisateurs');
    exit;
  end;

  if Tobject(Node.Data) is TACSUtilisateur then
  begin
    ActualiserUtilisateur(Node);
    ActualiserMenu('Utilisateur');
    exit;
  end;

  if Tobject(Node.Data) is TACSDossierPartage then
  begin
    ActualiserDossier(Node);
    ActualiserMenu('Dossier');
    exit;
  end;

end;

procedure TFormEASPrinc.ActualiserServeur;
var
  i : Integer;
  NodeDossier : TTreeNode;
begin

  with Donnees.Serveur do
  begin
    NodeServeur.DeleteChildren;
    for i := 0 to DossierPartages.Count - 1 do
    begin
      NodeDossier :=
      TreeViewServeurs.Items.AddChild(
        NodeServeur,DossierPartages.Items[i].Nom);
      NodeDossier.Data := DossierPartages.Items[i];
      NodeDossier.ImageIndex := 4;
      NodeDossier.SelectedIndex := 4;      
    end;
  end;
  NodeServeur.Expand(false);
//  ActualiserMenu('Serveur');
  ServeurActualiser;
end;

procedure TFormEASPrinc.FormCreate(Sender: TObject);
begin
  Caption  := Application.Title +' V'+GetApplicationVersionString
          +' Protocole V:'+ACProtocoleToString(PROTOCOLE_VERSION);
  BallonMessages := TStringList.Create;
  Donnees.Serveur.OnMessage := ServeurMessage;
  Donnees.Serveur.OnStatus := ServeurStatus;
  Donnees.Serveur.OnClientsChange := ServeurClientsChange;
  Donnees.Serveur.OnConnect := ServeurConnect;
  Donnees.Serveur.OnDisconnect := ServeurDisconnect;
  Donnees.Serveur.WEB.OnConnect := ServeurConnect;
  Donnees.Serveur.WEB.OnDisconnect := ServeurDisconnect;

  with Donnees do
  begin
    Serveur.Active := Configuration.General.ActifAuDemarrage;
    Serveur.WebActiver := Configuration.Web.ActifAuDemarrage;
  end;

  Actualiser;
  Demarrage := true;


end;

procedure TFormEASPrinc.ServeurMessage(Sender: TObject; pMessageType : TACMessageType; pMessage: string);
begin
  Donnees.TracerDansJournal(DateTimeToStr(now)+#9+ pMessage);

  if Donnees.Configuration.General.AfficherMessageInfo or (pMessageType <> ACMessageInfo) then
  begin
    ListBoxMessages.Items.Insert(0,DateTimeToStr(now)+#9+ pMessage);
    while ListBoxMessages.Items.Count > 50 do
      ListBoxMessages.Items.Delete(ListBoxMessages.Items.Count-1);
  end;

  if not JvTrayIcon1.ApplicationVisible and (pMessageType <> ACMessageInfo) then
  begin

    BallonMessages.Insert(0,DateTimeToStr(now)+#9+ pMessage);
    JvTrayIcon1.BalloonHint('ArchiveSync Server',BallonMessages.GetText);
  end;
  

end;

procedure TFormEASPrinc.ServeurStatus(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: String);
begin
  ListBoxMessages.Items.Insert(0,DateTimeToStr(now)+#9+AStatusText);
  ServeurActualiser;
end;

procedure TFormEASPrinc.TreeViewServeursClick(Sender: TObject);
begin
  ActualiserNoeud(TreeViewServeurs.Selected);
end;

procedure TFormEASPrinc.ActionServeurActiveExecute(Sender: TObject);
begin
  Donnees.Serveur.Active := not Donnees.Serveur.Active;
  ActualiserServeur;
end;

procedure TFormEASPrinc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  JvTrayIcon1.HideApplication;
  Action := CaNone;


end;

procedure TFormEASPrinc.ActualiserClients;
var
  i : integer;
  NodeClient : TTreeNode;
  NodeUtilisateur : TTreeNode;
begin
  NodeClients.DeleteChildren;
  for i := 0 to Donnees.Serveur.Clients.Count - 1 do
  begin
      NodeClient := TreeViewServeurs.Items.AddChild(
          NodeClients,
          Donnees.Serveur.Clients[i].Nom);
      NodeClient.Data :=
              Donnees.Serveur.Clients[i];
      NodeClient.ImageIndex := 3;
      NodeClient.SelectedIndex := 3;
      if Donnees.Serveur.Clients[i].Utilisateur <> nil then
      begin
        NodeUtilisateur :=
          TreeViewServeurs.Items.AddChild(
            NodeClient,
            Donnees.Serveur.Clients[i].Utilisateur.Nom);
          NodeUtilisateur.Data :=
                    Donnees.Serveur.Clients[i].Utilisateur;
          NodeUtilisateur.ImageIndex := 5;
          NodeUtilisateur.SelectedIndex := 5;
      end;

  end;
  NodeClients.Expand(false);
  ServeurActualiser;
end;

procedure TFormEASPrinc.ServeurClientsChange(Sender: Tobject);
begin
  ActualiserClients;
end;


procedure TFormEASPrinc.ActionServeurDossierAjouterExecute(
  Sender: TObject);
begin
  with  TFormEASDossierPartage.Create(self)do
  begin
    if Execute then
    begin
      Donnees.Serveur.DossierPartages.Add(Adresse).Nom := Nom;
      Donnees.EnregistrerFichierDePartages;
      ActualiserServeur;
    end;

    free;
  end;
end;

procedure TFormEASPrinc.ActionDossierModifierExecute(Sender: TObject);
var
  Dossier : TACSDossierPartage;
begin
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.data) is TACSDossierPartage then
    begin
        Dossier := TACSDossierPartage(TreeViewServeurs.Selected.data);
        with TFormEASDossierPartage.Create(self) do
        begin
          if Execute(Dossier) then
          begin
            Dossier.Nom := Nom;
            Dossier.Adresse := Adresse;
            Donnees.EnregistrerFichierDePartages;
            ActualiserServeur;
          end;
          free;
        end;
    end;
end;

procedure TFormEASPrinc.ActionDossierSupprimerExecute(Sender: TObject);
var
  Dossier : TACSDossierPartage;
begin
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.data) is TACSDossierPartage then
    begin
        Dossier := TACSDossierPartage(TreeViewServeurs.Selected.data);
        Donnees.Serveur.DossierPartages.Remove(Dossier);
        Donnees.EnregistrerFichierDePartages;
        ActualiserServeur;
    end;
end;

procedure TFormEASPrinc.ActualiserDossier(NodeDossier: TTreeNode);
begin
//  ActualiserMenu('Dossier');
end;

procedure TFormEASPrinc.ServeurActualiser;
begin
  if Donnees.Serveur.Active then
  begin
    ActionServeurActive.Caption := 'D�sactiver';
    ActionServeurActive.ImageIndex := 1;
    NodeServeur.ImageIndex := 1;
    NodeServeur.SelectedIndex := 1;
    if Donnees.Serveur.Clients.Count > 0 then
    begin
      JvTrayIcon1.IconIndex := 2;
      JvTrayIcon1.Hint := Caption +' [Clients connect�s]';
    end
    else
    begin
      JvTrayIcon1.IconIndex := 1;
      JvTrayIcon1.Hint := Caption +' [Aucun clients connect�s]'
    end;
  end
  else
  begin
    ActionServeurActive.Caption := 'Activer';
    NodeServeur.ImageIndex := 0;
    NodeServeur.SelectedIndex := 0;
    ActionServeurActive.ImageIndex := 0;
    JvTrayIcon1.IconIndex := 0;
    JvTrayIcon1.Hint := Caption +' [serveur d�sactiv�]'
  end;
end;

procedure TFormEASPrinc.ServeurConnect(AThread: TIdPeerThread);
begin
  ServeurActualiser;
end;

procedure TFormEASPrinc.ServeurDisconnect(AThread: TIdPeerThread);
begin
  ServeurActualiser;
end;


procedure TFormEASPrinc.ActionServeurConfigurationExecute(Sender: TObject);
var
  Actif : boolean;
begin
  with TFormEASConfiguration.Create(self) do
  begin
      if Executer then
      begin
        if Donnees.Serveur.DefaultPort <> Donnees.Configuration.General.Port then
        begin
          Actif := Donnees.Serveur.Active;
          Donnees.Serveur.Active := false;
          Donnees.Serveur.DefaultPort := Donnees.Configuration.General.Port;
          Donnees.Serveur.Active := Actif;

        end;
        
      end;

      Free;
  end;
end;

procedure TFormEASPrinc.ActionServeurQuitterExecute(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFormEASPrinc.ActualiserUtilisateurs;
var
  i : integer;
  NodeUtilisateur : TTreeNode;
begin
  NodeUtilisateurs.DeleteChildren;
  for i := 0 to Donnees.Serveur.Utilisateurs.Count - 1 do
  begin
      NodeUtilisateur := TreeViewServeurs.Items.AddChild(
          NodeUtilisateurs,
          Donnees.Serveur.Utilisateurs[i].Nom);
      NodeUtilisateur.Data :=
              Donnees.Serveur.Utilisateurs[i];
      NodeUtilisateur.ImageIndex := 5;
      NodeUtilisateur.SelectedIndex := 5;

  end;
  NodeUtilisateurs.Expand(false);
//  ActualiserMenu('Utiisateurs');  
end;

procedure TFormEASPrinc.ActionUtilisateurAjouterExecute(Sender: TObject);
begin
  with TFormEditUtilisateur.Create(self)do
  begin
    if executer then
    begin
      Donnees.Serveur.Utilisateurs.Add(Nom,MotPasse);
      Donnees.EnregistrerUtilisateurs;
      ActualiserUtilisateurs;
    end;
    free;
  end;
end;

procedure TFormEASPrinc.ActionUtilisateurModifierExecute(Sender: TObject);
var
  Utilisateur : TACSUtilisateur;
begin
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.Data) is TACSUtilisateur then
    begin
      Utilisateur := TACSUtilisateur(TreeViewServeurs.Selected.Data);
      with TFormEditUtilisateur.Create(Self)do
      begin
        if Executer(Utilisateur)then
        begin
          Utilisateur.Nom := Nom;
          Utilisateur.MotPasse := MotPasse;
        end;
        free;
      end;
    end;

end;

procedure TFormEASPrinc.ActionUtilisateurSupprimerExecute(Sender: TObject);
const
  MSG = 'Etes vous s�r de vouloir supprimer l''utilisateur "';
var
  Utilisateur : TACSUtilisateur;
begin
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.Data) is TACSUtilisateur then
    begin
      Utilisateur := TACSUtilisateur(TreeViewServeurs.Selected.Data);
      if MessageDlg(MSG+Utilisateur.Nom+'" ?',mtWarning,[mbYes,mbNo],0) = mrYes then
      begin
        Donnees.Serveur.Utilisateurs.Remove(Utilisateur);
        Donnees.EnregistrerUtilisateurs;
        ActualiserUtilisateurs;
      end;
    end;
end;

procedure TFormEASPrinc.ActualiserUtilisateur(NodeUtilisateur: TTreeNode);
begin
//  ActualiserMenu('Utilisateur');
end;

procedure TFormEASPrinc.ActualiserMenu(Node: TTreeNode);
begin
  if Node = nil then
    exit;

  if Node.Data = Donnees.Serveur then
  begin
    ActualiserMenu('Serveur');
    exit;
  end;

  if Node.Data = Donnees.Serveur.Clients then
  begin
    ActualiserMenu('Clients');
    exit;
  end;

  if TObject(Node.Data) is TACSClient then
  begin
    ActualiserMenu('Client');
    exit;
  end;

  if Node.Data = Donnees.Serveur.Utilisateurs then
  begin
    ActualiserMenu('Utilisateurs');
    exit;
  end;

  if Tobject(Node.Data) is TACSDossierPartage then
  begin
    ActualiserMenu('Dossier');
    exit;
  end;

  if Tobject(Node.Data) is TACSUtilisateur then
  begin
    ActualiserMenu('Utilisateur');
    exit;
  end;


end;

procedure TFormEASPrinc.PopupMenuServeurPopup(Sender: TObject);
begin
  if TreeViewServeurs.Selected <> nil then
    ActualiserMenu(TreeViewServeurs.Selected);
end;

procedure TFormEASPrinc.TreeViewServeursChange(Sender: TObject;
  Node: TTreeNode);
begin
  ActualiserNoeud(TreeViewServeurs.Selected);
end;

procedure TFormEASPrinc.DoubleClickNoeud(Node: TTreeNode);
begin
  if Node = nil then
    exit;

  if Node.Data = Donnees.Serveur then
  begin
    exit;
  end;

  if Node.Data = Donnees.Serveur.Clients then
  begin
    exit;
  end;

  if TObject(Node.Data) is TACSClient then
  begin
    exit;
  end;

  if Node.Data = Donnees.Serveur.Utilisateurs then
  begin

    exit;
  end;

  if Tobject(Node.Data) is TACSDossierPartage then
  begin
    ActionDossierModifierExecute(nil);
    exit;
  end;

  if Tobject(Node.Data) is TACSUtilisateur then
  begin
    ActionUtilisateurModifierExecute(nil);
    exit;
  end;


end;

procedure TFormEASPrinc.TreeViewServeursDblClick(Sender: TObject);
begin

  DoubleClickNoeud(TreeViewServeurs.Selected);
end;

procedure TFormEASPrinc.ActionServeurWebActiverExecute(Sender: TObject);
begin
  Donnees.Serveur.WEB.Active := not Donnees.Serveur.WEB.Active;
  if Donnees.Serveur.WEB.Active then
    ActionServeurWebActiver.Caption := 'D�sactiver le serveur web'
  else
      ActionServeurWebActiver.Caption := 'Activer le serveur web';


end;

procedure TFormEASPrinc.IdHTTPServer1AfterCommandHandler(
  ASender: TIdTCPServer; AThread: TIdPeerThread);
begin
// fsdfq
end;

procedure TFormEASPrinc.WebServeurConnect(AThread: TIdPeerThread);
begin
  
end;

procedure TFormEASPrinc.WebServeurDisconnect(AThread: TIdPeerThread);
begin

end;

procedure TFormEASPrinc.FormDestroy(Sender: TObject);
begin
  try
    Donnees.Serveur.FermerLesConnexionsActive;
    Donnees.Serveur.Active := false;
  finally
 //   BallonMessages.Free;
  end;

end;

procedure TFormEASPrinc.FormActivate(Sender: TObject);
begin
  if Demarrage then
  begin
    Demarrage := false;
    if Donnees.Configuration.General.BarreDesTachesAuDemarrage then
      JvTrayIcon1.HideApplication;
  end;
end;

procedure TFormEASPrinc.JvTrayIcon1Click(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  BallonMessages.Clear;
end;

procedure TFormEASPrinc.ActionExtinctionClientAutoriseExecute(
  Sender: TObject);
begin
  Donnees.Serveur.ExtinctionClientAutorise := ActionExtinctionClientAutorise.Checked;
end;

procedure TFormEASPrinc.JvTrayIcon1DblClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 JvTrayIcon1.ShowApplication;
end;

procedure TFormEASPrinc.Effacertout1Click(Sender: TObject);
begin
  ListBoxMessages.Items.Clear;
end;

end.
