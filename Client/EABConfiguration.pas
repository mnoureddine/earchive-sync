unit EABConfiguration;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,EABDonnees,ArchivesCloudCommun,FileCtrl, Spin;

type
  TFormEABConfiguration = class(TForm)
    PageControl1: TPageControl;
    ButtonAnnuler: TButton;
    ButtonOK: TButton;
    TabSheet1: TTabSheet;
    LabeledEditDossierTravail: TLabeledEdit;
    ButtonParcourir: TButton;
    GroupBox1: TGroupBox;
    CheckBoxSauvegarder: TCheckBox;
    CheckBoxReduire: TCheckBox;
    CheckBoxFermer: TCheckBox;
    CheckBoxEteindre: TCheckBox;
    CheckBoxTacerJournal: TCheckBox;
    TabSheet2: TTabSheet;
    LabeledEditMailEmetteur: TLabeledEdit;
    LabeledEditMailDestinataire: TLabeledEdit;
    LabeledEditMailSMTP: TLabeledEdit;
    SpinEditEMailPort: TSpinEdit;
    LabeledEditMailUtilisateur: TLabeledEdit;
    LabeledEditMailMotPasse: TLabeledEdit;
    ButtonEmailTester: TButton;
    Label1: TLabel;
    CheckBoxMailActiver: TCheckBox;
    SpinEditAvertissement: TSpinEdit;
    Label2: TLabel;
    CheckBoxExtinctionForcer: TCheckBox;
    CheckBoxAfficherMessagesInfo: TCheckBox;
    procedure ButtonParcourirClick(Sender: TObject);
    procedure CheckBoxSauvegarderClick(Sender: TObject);
    procedure ButtonEmailTesterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBoxEteindreClick(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    procedure Charger;
    procedure Enregistrer;
    procedure Executer;
  end;


implementation

uses Math;

{$R *.dfm}

procedure TFormEABConfiguration.Charger;
begin
  With DonneesEAB.Configuration do
  begin
    LabeledEditDossierTravail.Text := General.DossierTravail;
    CheckBoxSauvegarder.Checked := Demarrage.Sauvegarder;
    CheckBoxReduire.Checked := Demarrage.Reduire;
    CheckBoxFermer.Checked := Demarrage.Fermer;
    CheckBoxEteindre.Checked := Demarrage.Eteindre;
    CheckBoxExtinctionForcer.Checked := Demarrage.ExtinctionForcer;
    SpinEditAvertissement.Value := Demarrage.Avertissement;
    CheckBoxTacerJournal.Checked := General.TracerJournal;
    CheckBoxAfficherMessagesInfo.Checked := General.AfficherMessagesInfo;
    CheckBoxMailActiver.Checked := Mail.Activer;
    LabeledEditMailEmetteur.Text := Mail.Emeteur;
    LabeledEditMailDestinataire.Text := Mail.Destinataire;
    LabeledEditMailSMTP.Text := Mail.SMTP;
    LabeledEditMailUtilisateur.Text := Mail.Utilisateur;
    LabeledEditMailMotPasse.Text := Mail.MotPasse;
    SpinEditEMailPort.Value := Mail.Port;
  end;


end;

procedure TFormEABConfiguration.Enregistrer;
begin
  With DonneesEAB.Configuration do
  begin
    General.DossierTravail := LabeledEditDossierTravail.Text;
    Demarrage.Sauvegarder := CheckBoxSauvegarder.Checked;
    Demarrage.Reduire := CheckBoxReduire.Checked;
    Demarrage.Fermer := CheckBoxFermer.Checked;
    Demarrage.Eteindre := CheckBoxEteindre.Checked;
    Demarrage.ExtinctionForcer := CheckBoxExtinctionForcer.Checked;
    Demarrage.Avertissement := SpinEditAvertissement.Value;
    General.TracerJournal := CheckBoxTacerJournal.Checked ;
    General.AfficherMessagesInfo := CheckBoxSauvegarder.Checked;

    Mail.Activer := CheckBoxMailActiver.Checked;
    Mail.Emeteur := LabeledEditMailEmetteur.Text;
    Mail.Destinataire  := LabeledEditMailDestinataire.Text;
    Mail.SMTP := LabeledEditMailSMTP.Text;
    Mail.Utilisateur := LabeledEditMailUtilisateur.Text;
    Mail.MotPasse := LabeledEditMailMotPasse.Text;
    Mail.Port := SpinEditEMailPort.Value;

  end;
  DonneesEAB.EnregistrerConfiguration;

end;

procedure TFormEABConfiguration.Executer;
begin
  Charger;
  if ShowModal = mrOK then
    Enregistrer;

end;

procedure TFormEABConfiguration.ButtonParcourirClick(Sender: TObject);
var
  Dossier : string;
begin
  Dossier := LabeledEditDossierTravail.Text;
  if SelectDirectory('Selection du dossier de travail','',Dossier)then
    LabeledEditDossierTravail.Text := Dossier;


end;

procedure TFormEABConfiguration.CheckBoxSauvegarderClick(Sender: TObject);
begin
  if Not CheckBoxSauvegarder.Checked then
  begin
    CheckBoxEteindre.Checked := false;
    CheckBoxFermer.Checked := false;
  end;

end;

procedure TFormEABConfiguration.ButtonEmailTesterClick(Sender: TObject);
const
  MailSujet = 'Test de la fonction E-Mail';
  MailMessage = 'Felicitation:! vous recevrez les rapports par e-mail';

var
  Mail  : TEABMailParam;
  MailFichierJoint : TStringList;
  FichierJoint : string;
begin
    Mail.Activer := CheckBoxMailActiver.Checked;
    Mail.Emeteur := LabeledEditMailEmetteur.Text;
    Mail.Destinataire  := LabeledEditMailDestinataire.Text;
    Mail.SMTP := LabeledEditMailSMTP.Text;
    Mail.Utilisateur := LabeledEditMailUtilisateur.Text;
    Mail.MotPasse := LabeledEditMailMotPasse.Text;
    Mail.Port := SpinEditEMailPort.Value;
    MailFichierJoint := TStringList.Create;
    MailFichierJoint.Add(DateTimeToStr(now)+#9+'Message test');
    MailFichierJoint.Add(DateTimeToStr(now)+#9+'Message test');
    MailFichierJoint.Add(DateTimeToStr(now)+#9+'Message test');
    MailFichierJoint.Add(DateTimeToStr(now)+#9+'Message test');
    MailFichierJoint.Add(DateTimeToStr(now)+#9+'Message test');
    FichierJoint := GetTempDirectory+'\FichierMail.txt';
    MailFichierJoint.SaveToFile(FichierJoint);
    MailFichierJoint.Free;



    if DonneesEAB.EnvoyerEmail(MailSujet,MailMessage,Mail,true,FichierJoint) then
      MessageDlg('Envoi de d''un e-mail r�ussi verifier � la reception',mtConfirmation,[mbOK],0)
    else
      MessageDlg('Echec � l''envoie de l''e-mail de test ',mtError,[mbOK],0);


end;

procedure TFormEABConfiguration.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TFormEABConfiguration.CheckBoxEteindreClick(Sender: TObject);
begin
  If not CheckBoxEteindre.Checked then
    CheckBoxExtinctionForcer.Checked := false;
end;

end.
