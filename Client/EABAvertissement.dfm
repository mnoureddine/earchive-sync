object FormEABAvertissement: TFormEABAvertissement
  Left = 421
  Top = 306
  BorderStyle = bsDialog
  Caption = 'Avertissement'
  ClientHeight = 133
  ClientWidth = 378
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 353
    Height = 81
    Caption = 'Message'
    TabOrder = 0
    object LabelMessage: TLabel
      Left = 8
      Top = 16
      Width = 336
      Height = 49
      AutoSize = False
      Caption = 
        'Le logiciel ArchiveSync Backup va proc'#233'der '#224' la sauvegarde du se' +
        'rveur. apr'#232's quoi le pc sera mis hors tension dans :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object LabelCompteur: TLabel
      Left = 160
      Top = 48
      Width = 21
      Height = 20
      Caption = '30'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object ButtonAnnulerSauvegarde: TButton
    Left = 8
    Top = 96
    Width = 137
    Height = 25
    Caption = 'Annuler la sauvegarde'
    TabOrder = 1
    OnClick = ButtonAnnulerSauvegardeClick
  end
  object ButtonAnnulerExtinction: TButton
    Left = 224
    Top = 96
    Width = 137
    Height = 25
    Caption = 'Ne pas eteindre'
    ModalResult = 3
    TabOrder = 2
    OnClick = ButtonAnnulerExtinctionClick
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 160
    Top = 32
  end
end
