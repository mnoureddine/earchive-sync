object FormEABConfiguration: TFormEABConfiguration
  Left = 247
  Top = 210
  Width = 333
  Height = 421
  Caption = 'Configuration'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    325
    388)
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 308
    Height = 341
    ActivePage = TabSheet1
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'G'#233'n'#233'ral'
      object LabeledEditDossierTravail: TLabeledEdit
        Left = 8
        Top = 24
        Width = 209
        Height = 21
        EditLabel.Width = 169
        EditLabel.Height = 13
        EditLabel.Caption = 'Adresse du dossier de sauvegarde :'
        TabOrder = 0
      end
      object ButtonParcourir: TButton
        Left = 224
        Top = 24
        Width = 49
        Height = 25
        Caption = '...'
        TabOrder = 1
        OnClick = ButtonParcourirClick
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 56
        Width = 257
        Height = 185
        Caption = 'Au lancement de l'#39'application'
        TabOrder = 2
        object Label2: TLabel
          Left = 18
          Top = 124
          Width = 128
          Height = 13
          Caption = 'Temps d'#39'avertissement (s) :'
          FocusControl = SpinEditAvertissement
        end
        object CheckBoxSauvegarder: TCheckBox
          Left = 16
          Top = 48
          Width = 233
          Height = 17
          Caption = 'Sauvegarder les dossiers'
          TabOrder = 1
          OnClick = CheckBoxSauvegarderClick
        end
        object CheckBoxReduire: TCheckBox
          Left = 16
          Top = 24
          Width = 209
          Height = 17
          Caption = 'R'#233'duire dans la barre des taches'
          TabOrder = 0
        end
        object CheckBoxFermer: TCheckBox
          Left = 16
          Top = 72
          Width = 209
          Height = 17
          Caption = 'Fermer l'#39'application'
          TabOrder = 2
        end
        object CheckBoxEteindre: TCheckBox
          Left = 16
          Top = 96
          Width = 209
          Height = 17
          Caption = 'Eteindre l'#39'ordinateur'
          TabOrder = 3
          OnClick = CheckBoxEteindreClick
        end
        object SpinEditAvertissement: TSpinEdit
          Left = 160
          Top = 120
          Width = 81
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 4
          Value = 30
        end
        object CheckBoxExtinctionForcer: TCheckBox
          Left = 40
          Top = 144
          Width = 201
          Height = 33
          Caption = 'Eteindre si les serveurs ne sont pas present.'
          TabOrder = 5
          WordWrap = True
        end
      end
      object CheckBoxTacerJournal: TCheckBox
        Left = 8
        Top = 248
        Width = 241
        Height = 17
        Caption = 'Tracer le journal d'#39#233'venements'
        TabOrder = 3
      end
      object CheckBoxAfficherMessagesInfo: TCheckBox
        Left = 8
        Top = 280
        Width = 241
        Height = 17
        Caption = 'Afficher les messsages d'#39'informations.'
        TabOrder = 4
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'E-Mail'
      ImageIndex = 1
      DesignSize = (
        300
        313)
      object Label1: TLabel
        Left = 170
        Top = 156
        Width = 28
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Port : '
        FocusControl = SpinEditEMailPort
      end
      object LabeledEditMailEmetteur: TLabeledEdit
        Left = 8
        Top = 40
        Width = 276
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        EditLabel.Width = 82
        EditLabel.Height = 13
        EditLabel.Caption = 'Adresse '#233'metteur'
        TabOrder = 0
      end
      object LabeledEditMailDestinataire: TLabeledEdit
        Left = 8
        Top = 80
        Width = 276
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        EditLabel.Width = 95
        EditLabel.Height = 13
        EditLabel.Caption = 'Adresse destinataire'
        TabOrder = 1
      end
      object LabeledEditMailSMTP: TLabeledEdit
        Left = 8
        Top = 120
        Width = 276
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        EditLabel.Width = 70
        EditLabel.Height = 13
        EditLabel.Caption = 'Serveur SMTP'
        TabOrder = 2
      end
      object SpinEditEMailPort: TSpinEdit
        Left = 200
        Top = 152
        Width = 81
        Height = 22
        Anchors = [akTop, akRight]
        MaxValue = 0
        MinValue = 0
        TabOrder = 3
        Value = 25
      end
      object LabeledEditMailUtilisateur: TLabeledEdit
        Left = 8
        Top = 185
        Width = 276
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        EditLabel.Width = 46
        EditLabel.Height = 13
        EditLabel.Caption = 'Utilisateur'
        TabOrder = 4
      end
      object LabeledEditMailMotPasse: TLabeledEdit
        Left = 8
        Top = 225
        Width = 276
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        EditLabel.Width = 64
        EditLabel.Height = 13
        EditLabel.Caption = 'Mot de passe'
        EditLabel.Font.Charset = DEFAULT_CHARSET
        EditLabel.Font.Color = clWindowText
        EditLabel.Font.Height = -11
        EditLabel.Font.Name = 'MS Sans Serif'
        EditLabel.Font.Style = []
        EditLabel.ParentFont = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
        PasswordChar = #183
        TabOrder = 5
      end
      object ButtonEmailTester: TButton
        Left = 201
        Top = 280
        Width = 91
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Tester'
        TabOrder = 6
        OnClick = ButtonEmailTesterClick
      end
      object CheckBoxMailActiver: TCheckBox
        Left = 8
        Top = 4
        Width = 97
        Height = 17
        Caption = 'Activer'
        TabOrder = 7
      end
    end
  end
  object ButtonAnnuler: TButton
    Left = 243
    Top = 356
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Annuler'
    ModalResult = 3
    TabOrder = 1
  end
  object ButtonOK: TButton
    Left = 163
    Top = 356
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
end
