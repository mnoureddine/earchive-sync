unit EABPrinc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, ToolWin, ActnList,EABDonnees, StdCtrls,ArchivesCloudClient,
  Menus, IdComponent,ArchivesCloudCommun, ImgList,ShellAPI,FileCtrl,EABConfiguration,
  IdTCPServer, IdBaseComponent, IdTCPConnection, IdTCPClient,EABConnexion,Utiles,
  EACArchiveCloudProgress,EABEditServeur, JvComponentBase, JvTrayIcon,EABAvertissement,
  EABAvertissementExtinction, IdAntiFreezeBase, IdAntiFreeze;

type
  TACCOperationMode = (ACOpModeAucun,ACOpModeCopierFichier,ACOpModeCouperFichier,ACOpModeCopierDossier,ACOpModeCouperDossier);

  TFormEABPrinc = class(TForm)
    TreeViewServeurs: TTreeView;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    ActionServeurConnection: TAction;
    ListBoxMessages: TListBox;
    PopupMenuServeurs: TPopupMenu;
    ActionAjouterServeur: TAction;
    ActionServeurEnvoieMessage: TAction;
    OpenDialog1: TOpenDialog;
    Splitter2: TSplitter;
    ActionDossierSauvegarder: TAction;
    ImageList2: TImageList;
    ActionFichierSyncRetirer: TAction;
    ActionFichierSyncSauvegarder: TAction;
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    ActionDossierOuvrir: TAction;
    ActionDossierTelecharger: TAction;
    ActionDossierMettreAJour: TAction;
    ActionConfiguration: TAction;
    ActionServeurSupprimer: TAction;
    GroupBox1: TGroupBox;
    CoolBar1: TCoolBar;
    EditChemin: TEdit;
    ActionServeurPropriete: TAction;
    ActionServeurSauvegarder: TAction;
    MainMenu1: TMainMenu;
    Poste1: TMenuItem;
    Ajouterunserveur1: TMenuItem;
    Mettrejour1: TMenuItem;
    Configuration1: TMenuItem;
    N1: TMenuItem;
    Quitter1: TMenuItem;
    JvTrayIcon1: TJvTrayIcon;
    PopupMenuTrayIcons: TPopupMenu;
    Ouvrir1: TMenuItem;
    Ajouterunserveur2: TMenuItem;
    Mettrejour2: TMenuItem;
    N2: TMenuItem;
    Quitter2: TMenuItem;
    TimerSauvegarde: TTimer;
    IdAntiFreeze1: TIdAntiFreeze;
    procedure ActionServeurConnectionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActionServeurEnvoieMessageExecute(Sender: TObject);
    procedure TreeViewServeursDblClick(Sender: TObject);
    procedure ActionDossierSauvegarderExecute(Sender: TObject);
    procedure ActionDossierCopierExecute(Sender: TObject);
    procedure ActionDossierCouperExecute(Sender: TObject);
    procedure ActionDossierCollerExecute(Sender: TObject);
    procedure ActionDossierSupprimerExecute(Sender: TObject);
    procedure ActionDossierNouveauDossierExecute(Sender: TObject);
    procedure ServeurWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure ServeurWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure ServeurWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
    procedure ActionFichierSyncSauvegarderExecute(Sender: TObject);
    procedure ActionFichierSyncRetirerExecute(Sender: TObject);
    procedure ActionDossierTelechargerExecute(Sender: TObject);
    procedure ActionDossierEnvoyerExecute(Sender: TObject);
    procedure ActionDossierOuvrirExecute(Sender: TObject);
    procedure ActionDossierRenommerExecute(Sender: TObject);

    procedure ActionDossierMettreAJourExecute(Sender: TObject);
    procedure ActionAjouterServeurExecute(Sender: TObject);
    procedure ActionConfigurationExecute(Sender: TObject);
    procedure ActionServeurSupprimerExecute(Sender: TObject);
    procedure TreeViewServeursChange(Sender: TObject; Node: TTreeNode);
    procedure ActionServeurProprieteExecute(Sender: TObject);
    procedure PopupMenuServeursPopup(Sender: TObject);
    procedure ActionPosteSauvegarderExecute(Sender: TObject);
    procedure ActionServeurSauvegarderExecute(Sender: TObject);
    procedure Quitter1Click(Sender: TObject);
    procedure Quitter2Click(Sender: TObject);
    procedure Ouvrir1Click(Sender: TObject);
    procedure JvTrayIcon1BalloonHide(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerSauvegardeTimer(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    NodePoste : TTreeNode;
    FichierPressePapier : TACCFichier;
    DossierPressePapier : TACCDossier;
    OperatioMode : TACCOperationMode;
    Journal : TStringList;
    BalloonMessage : TStringList;
    FichierJournal : string;
    Demarrage : boolean;
    function ConnectionToStr(Connected : boolean): string;
    procedure ActualiserVue;
    procedure Actualiser;
    procedure ActualiserNoeud(Node : TTReeNode);
    procedure ActualiserPoste;
    procedure ActualiserServeur(NodeServeur: TTreeNode);
    procedure ActualiserMenuServeur(Node : TTReeNode);overload;
    procedure ActualiserMenuServeur(Categorie : string);overload;
    procedure ActualiserDossiersPartages(NodeDossiersPartage : TTreeNode);
    procedure ActualiserDossier(NodeDossier : TTreeNode);
    procedure ActualiserFichiersSync(NodeFichiersSync : TTreeNode);
    procedure ServeurConnected(Sender : TObject);
    procedure ServeurDisconnected(Sender : TObject);
    procedure ServeurDonneesChange(Sender : TObject);
    procedure PosteMessage(Sender : TObject;pMessageType:TACMessageType; pMessage : string);
    procedure PosteServeurAjouter(Sender:TObject;pServeur : TACCServeur);
    procedure PosteServeurStatus(ASender: TObject;
      const AStatus: TIdStatus; const AStatusText: String);
    procedure ServeurDossierPartageLister(Sender:TObject;pServeur : TACCServeur);
    procedure PosteServeurDossierLister(Sender:TObject;pServeur : TACCServeur;pDossier : TACCDossier);
    function FichierSyncSelectionne : TACCFichierSync;
    function DossierSelectionne : TACCDossier;
    function ServeurSelectionne : TACCServeur;
    procedure LancementDemarrage;
    procedure PosteSauvegarder;
    procedure ServeurAvancement(Sender : TObject;Libelle : string;Max : integer;Position : integer);
    procedure LancerSauvegarde;
    procedure EnvoyerRapportSauvegarde;

  end;

var
  FormEABPrinc: TFormEABPrinc;


implementation

uses Math, DateUtils;

{$R *.dfm}

{ TFormEABPrinc }

procedure TFormEABPrinc.Actualiser;
var
  i : integer;

begin
  TreeViewServeurs.Items.Clear;
  NodePoste := TreeViewServeurs.Items.AddChild(nil,'Poste de travail');
  NodePoste.Data := DonneesEAB.Poste;
  NodePoste.ImageIndex := 0;
  NodePoste.SelectedIndex := 0;
  NodePoste.StateIndex := 0;

  ActualiserNoeud(NodePoste);
end;

procedure TFormEABPrinc.ActionServeurConnectionExecute(Sender: TObject);
var
  Serveur : TACCServeur;
  FormEABConnexion: TFormEABConnexion;
begin
  Serveur := ServeurSelectionne;
  if Serveur <> nil then
  begin
    if not Serveur.Connected then
    begin
      if Serveur.Connexion then
        if Serveur.RequeteDossierPartageLister then
          ActualiserVue
        else
          MessageDlg('Erreur lors de la liste de dossier partag�',mtError,[mbOk],0);
    end
    else
      Serveur.Deconnexion;
  end;
end;

procedure TFormEABPrinc.FormCreate(Sender: TObject);
var
  i : Integer;
  
begin
  Journal := TStringList.Create;
  BalloonMessage := TStringList.Create;

  FichierJournal :=
    DonneesEAB.Configuration.General.DossierTravail
      +'\Journal '
      +FormatDateTime('yyyymmdd hhnnss',now)+'.txt';
  Caption  := Application.Title +' V'+GetApplicationVersionString
          +' Protocole V:'+ACProtocoleToString(PROTOCOLE_VERSION);
  JvTrayIcon1.Hint := Caption ;

  DonneesEAB.Poste.OnMessage := PosteMessage;
  DonneesEAB.Poste.OnServeurAjouter := PosteServeurAjouter;
  DonneesEAB.Poste.OnServeurDossierLister := PosteServeurDossierLister;
  DonneesEAB.Poste.OnServeurDossierPartageLister := ServeurDossierPartageLister;
  DonneesEAB.ChargerListeDesServeur;
  Actualiser;
  Demarrage := true;



end;

procedure TFormEABPrinc.ServeurConnected(Sender: TObject);
begin
  ActionServeurConnection.Caption := ConnectionToStr(true);

end;

procedure TFormEABPrinc.ServeurDisconnected(Sender: TObject);
begin
  ActionServeurConnection.Caption := ConnectionToStr(false);
  if TObject(Sender) is TACCServeur then
    if TObject(TACCServeur(Sender).Data) is TTreeNode then
      ActualiserServeur(TTreeNode(TACCServeur(Sender).Data));

end;

procedure TFormEABPrinc.PosteMessage(Sender: TObject; pMessageType:TACMessageType;pMessage: string);
begin
  if (pMessageType <> ACMessageInfo) or
    DonneesEAB.Configuration.General.AfficherMessagesInfo then
  begin
    ListBoxMessages.Items.Insert(0,pMessage);
    Repaint;
  end;
  if DonneesEAB.Configuration.General.TracerJournal then
  begin
    Journal.Add(pMessage);
    Journal.SaveToFile(FichierJournal);
  end;
  if pMessageType <> ACMessageInfo then
    if not JvTrayIcon1.ApplicationVisible then
    begin
      BalloonMessage.Add(pMessage);
      JvTrayIcon1.BalloonHint('ArchiveSync Backup',BalloonMessage.GetText);
    end;

end;

procedure TFormEABPrinc.ActualiserNoeud(Node: TTReeNode);
begin
  if Node = nil then
      exit;

  ActionServeurConnection.Enabled := TObject(Node.Data) is TACCServeur;

  if TObject(Node.Data) is TACCPoste then
  begin
    ActualiserPoste;
    exit;
  end;

  if TObject(Node.Data) is TACCServeur then
  begin
    ActualiserServeur(Node);
    EditChemin.Text := '\\'+TACCServeur(Node.Data).Nom;
    exit;
  end;

  if TObject(Node.Data) is TACCDossierList then
  begin
    if TACCDossierList(Node.Data).Serveur.Connected then
      if TACCDossierList(Node.Data).Serveur.RequeteDossierPartageLister then
      begin
        ActualiserDossiersPartages(Node);
        EditChemin.Text := '\\'+TACCDossierList(Node.Data).Serveur.Nom+'\Fichier Partag�es';
      end
      else
        MessageDlg('Erreur lors de la liste de dossier partag�',mtError,[mbOk],0);


      exit;
  end;


  if TObject(Node.Data) is TACCDossier then
  begin
    if TACCDossier(Node.Data).Serveur.Connected then
      if TACCDossier(Node.Data).RequeteLister then
      begin
        ActualiserDossier(Node);
        EditChemin.Text := '\\'+TACCDossier(Node.Data).Serveur.Nom+'\'+TACCDossier(Node.Data).Chemin;
      end
      else
        MessageDlg('Erreur lors de la liste du dossier',mtError,[mbOk],0);
    exit;

  end;

  if TObject(Node.Data) is TACCFichierSyncList then
  begin
    ActualiserFichiersSync(Node);
    EditChemin.Text := '\\'+TACCFichierSyncList(Node.Data).Serveur.Nom+'\Sauvegarde';
    exit;
  end;

end;

procedure TFormEABPrinc.ActualiserServeur(NodeServeur: TTreeNode);
var
  Serveur: TACCServeur;
  i : Integer;
  Dossier : TACCDossier;
  Node : TTreeNode;

begin
  Serveur := TACCServeur(NodeServeur.data);
  if not Serveur.Connected then
  begin
      NodeServeur.ImageIndex := 1;
      NodeServeur.SelectedIndex := 1;
      NodeServeur.StateIndex := 1;
  end
  else
  begin
      NodeServeur.ImageIndex := 2;
      NodeServeur.SelectedIndex := 2;
      NodeServeur.StateIndex := 2;
  end;


  ActionServeurConnection.Caption := ConnectionToStr(Serveur.Connected);
//  ActionServeurConnection.Checked := Serveur.Connected;
  ActionServeurEnvoieMessage.Enabled := Serveur.Connected;
  ActualiserMenuServeur('Serveur');


  NodeServeur.DeleteChildren;

  Node := TreeViewServeurs.Items.AddChildFirst(
                NodeServeur,'Sauvegarde');
  Node.Data := Serveur.FichiersSync;
  Node.ImageIndex := 7;
  Node.SelectedIndex := 7;
  Node.StateIndex := 7;
  Serveur.FichiersSync.Data := Node;

  Node := TreeViewServeurs.Items.AddChildFirst(
                NodeServeur,'Partag�es');
  Node.Data := Serveur.Dossiers;
  Node.ImageIndex := 3;
  Node.SelectedIndex := 4;
  Node.StateIndex := 3;
  Serveur.Dossiers.Data := Node;


//  NodeServeur.Expand(false);



end;

procedure TFormEABPrinc.ActualiserMenuServeur(Categorie: string);
var
  i : integer;
  lMenu :  TMenuItem;
begin
  PopupMenuServeurs.Items.Clear;
  for i := 0 to ActionList1.ActionCount - 1 do
    if SameText(Categorie,ActionList1.Actions[i].Category) then
    begin
        lMenu := TMenuItem.Create(self);
        if SameText(TAction(ActionList1.Actions[i]).Caption,'-')then
          lMenu.Caption := '-'
        else
          lMenu.Action := ActionList1.Actions[i];

        PopupMenuServeurs.Items.Add(lMenu);
    end;
end;

procedure TFormEABPrinc.ActualiserPoste;
var
  i : integer;
  Node : TTreeNode;
begin
  NodePoste.DeleteChildren;
  for i := 0 to DonneesEAB.Poste.Serveurs.Count -1 do
  begin
    Node := TreeViewServeurs.Items.AddChild(NodePoste,DonneesEAB.Poste.Serveurs[i].Nom);
    Node.Data := DonneesEAB.Poste.Serveurs[i];
    if not DonneesEAB.Poste.Serveurs[i].Connected then
    begin
      Node.ImageIndex := 1;
      Node.SelectedIndex := 1;
      Node.StateIndex := 1;
    end
    else
    begin
      Node.ImageIndex := 2;
      Node.SelectedIndex := 2;
      Node.StateIndex := 2;
    end;
    DonneesEAB.Poste.Serveurs[i].Data := Node;
  end;
//  NodePoste.Expand(false);
  ActualiserMenuServeur('Poste');
end;

procedure TFormEABPrinc.PosteServeurAjouter(Sender: TObject;
  pServeur: TACCServeur);
begin
  pServeur.OnConnected := ServeurConnected;
  pServeur.OnDisconnected := ServeurDisconnected;
  pServeur.OnStatus := PosteServeurStatus;
  pServeur.OnWorkBegin := ServeurWorkBegin;
  pServeur.OnWork := ServeurWork;
  pServeur.OnWorkEnd := ServeurWorkEnd;
  pServeur.OnDonneesChange := ServeurDonneesChange;
  pServeur.OnAvancement := ServeurAvancement;
 


end;

function TFormEABPrinc.ConnectionToStr(Connected: boolean): string;
begin
  if Connected then
    result := 'Deconnecter'
  else
    result := 'Connecter';

end;

procedure TFormEABPrinc.PosteServeurStatus(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: String);
begin
  ListBoxMessages.Items.Insert(0,DateTimeToStr(now)+#9+AStatusText);
end;

procedure TFormEABPrinc.ActionServeurEnvoieMessageExecute(Sender: TObject);
var
  Serveur : TACCServeur;
  lMessage: string;
begin
  Serveur := ServeurSelectionne;
  if Serveur <> nil then
  begin
        lMessage := InputBox(
                        'Boite des saisie de message',
                        'Saisissez un message a envoyer au serveur',
                        'Message');

        if not Serveur.RequeteMessage(lMessage) then
          MessageDlg('Erreur lors de l''envoie du message',mtError,[mbOk],0);
  end;

end;



procedure TFormEABPrinc.PosteServeurDossierLister(Sender: TObject;
  pServeur: TACCServeur; pDossier: TACCDossier);
begin
  if TObject(pDossier.Data) is TTreeNode then
      ActualiserDossier(TTreeNode(pDossier.Data));
end;


procedure TFormEABPrinc.ActualiserDossier(NodeDossier: TTreeNode);
var
  Dossier : TACCDossier;
  i : Integer;
  Node : TTreeNode;
  ListItem : TListItem;
  Column : TListColumn;
begin
  Dossier := TACCDossier(NodeDossier.Data);

  NodeDossier.DeleteChildren;
  for i := 0 to Dossier.Dossiers.Count - 1 do
  begin
    Node := TreeViewServeurs.Items.AddChildFirst(
                NodeDossier,Dossier.Dossiers[i].Nom);
    Node.Data := Dossier.Dossiers[i];

    if Dossier.Dossiers[i].DossierSynchronise = nil then
    begin
      if not Dossier.Dossiers[i].Exist then
      begin
        Node.ImageIndex := 5;
        Node.SelectedIndex := 6;
        Node.StateIndex := 5;
      end
      else
      begin
        Node.ImageIndex := 15;
        Node.SelectedIndex := 16;
        Node.StateIndex := 15;
      end;

    end
    else
    begin

      Node.ImageIndex := 7;
      Node.SelectedIndex := 7;
      Node.StateIndex := 7;
    end;
    Dossier.Dossiers[i].Data := Node;
  end;

  //NodeDossier.Expand(false);
  NodeDossier.AlphaSort(false);
  ActualiserMenuServeur('Dossier');

end;


procedure TFormEABPrinc.ServeurDossierPartageLister(Sender: TObject;
  pServeur: TACCServeur);
begin
  if TObject(pServeur.Dossiers.Data) is TTreeNode then
    ActualiserDossiersPartages(TTreeNode(pServeur.Dossiers.Data));
end;

procedure TFormEABPrinc.TreeViewServeursDblClick(Sender: TObject);
begin
  if TreeViewServeurs.Selected <> nil then
  begin
    if TObject(TreeViewServeurs.Selected.Data) is TACCDossier then
      ActionDossierOuvrirExecute(Sender);
    if TObject(TreeViewServeurs.Selected.Data) is TACCServeur then
      ActionServeurConnectionExecute(Sender);
  end;
end;

procedure TFormEABPrinc.ActualiserDossiersPartages(
  NodeDossiersPartage: TTreeNode);
var
  i : Integer;
  DossiersPartages : TACCDossierList;
  Node : TTreeNode;
begin
  DossiersPartages := TACCDossierList(NodeDossiersPartage.Data);
  NodeDossiersPartage.DeleteChildren;
  for i := 0 to DossiersPartages.Count - 1 do
  begin
    Node := TreeViewServeurs.Items.AddChildFirst(
                NodeDossiersPartage,DossiersPartages[i].Nom);
    Node.Data := DossiersPartages[i];

    Node.ImageIndex := 3;
    Node.SelectedIndex := 4;
    Node.StateIndex := 3;
    DossiersPartages[i].Data := Node;
  end;
//  NodeDossiersPartage.Expand(false);

end;

procedure TFormEABPrinc.ActualiserFichiersSync(NodeFichiersSync : TTreeNode);
var
  FichiersSync : TACCFichierSyncList;
  i : Integer;
  ListItem : TListItem;
  Column : TListColumn;
  NodeFichierSync : TTreeNode;
begin

  FichiersSync := TACCFichierSyncList(NodeFichiersSync.Data);

  NodeFichiersSync.DeleteChildren;

  for i := 0 to FichiersSync.Count -1 do
  begin
    NodeFichierSync := TreeViewServeurs.Items.AddChild(NodeFichiersSync,FichiersSync[i].Chemin);
    NodeFichierSync.Data := FichiersSync[i];
    FichiersSync[i].Data := NodeFichierSync;
    NodeFichierSync.ImageIndex := 7;
    NodeFichierSync.SelectedIndex := 7;
    NodeFichierSync.StateIndex := 7;
  end;

end;






procedure TFormEABPrinc.ActionDossierSauvegarderExecute(Sender: TObject);
var
  Dossier : TACCDossier;
  FichierSync : TACCFichierSync;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      FichierSync := Dossier.AjouterEnSynchronisation;
      DonneesEAB.EnregistrerFichierSync;
    end;

end;

procedure TFormEABPrinc.ActionDossierCopierExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      DossierPressePapier := Dossier;
      OperatioMode := ACOpModeCopierDossier;

  end;

end;

procedure TFormEABPrinc.ActionDossierCouperExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      DossierPressePapier := Dossier;
      OperatioMode := ACOpModeCouperDossier;
      
  end;

end;


procedure TFormEABPrinc.ActionDossierCollerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      Case OperatioMode of
      ACOpModeAucun : ;
      ACOpModeCopierFichier :
          if not FichierPressePapier.RequeteCopier(Dossier) then
                MessageDlg('Erreur lors de la copie du fichier',mtError,[mbOk],0);
      ACOpModeCouperFichier :
          if not FichierPressePapier.RequeteDeplacer(Dossier) then
            MessageDlg('Erreur lors du d�placement du fichier',mtError,[mbOk],0);
      ACOpModeCopierDossier :
        if not DossierPressePapier.RequeteCopier(Dossier) then
          MessageDlg('Erreur lors de la copie du dossier',mtError,[mbOk],0);
      ACOpModeCouperDossier :
        if not DossierPressePapier.RequeteDeplacer(Dossier) then
          MessageDlg('Erreur lors du d�placement du dossier',mtError,[mbOk],0);

      end;
  end;

end;

procedure TFormEABPrinc.ActionDossierSupprimerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;

  if Dossier <> nil then
      if Dossier.RequeteSupprimer then
        MessageDlg('Erreur lors de la suppresion de dossier',mtError,[mbOk],0);

end;

procedure TFormEABPrinc.ActionDossierNouveauDossierExecute(Sender: TObject);
var
  NouveauDossier : string;
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      NouveauDossier := 'Nouveau dossier';
      if InputQuery('Boite de cr�ation de dossier','Nom du dossier',NouveauDossier) then
        if Dossier.RequeteCreer(NouveauDossier) then
          MessageDlg('Erreur lors du changement de nom du dossier',mtError,[mbOk],0);

  end;

end;

procedure TFormEABPrinc.ServeurWork(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  ProgressBar1.Position := AWorkCount;
end;

procedure TFormEABPrinc.ServeurWorkBegin(Sender: TObject;
  AWorkMode: TWorkMode; const AWorkCountMax: Integer);
begin
  ProgressBar1.Position := 0;
  ProgressBar1.Max := AWorkCountMax;
end;

procedure TFormEABPrinc.ServeurWorkEnd(Sender: TObject;
  AWorkMode: TWorkMode);
begin
  ProgressBar1.Position := 0;

end;

procedure TFormEABPrinc.ActionFichierSyncSauvegarderExecute(
  Sender: TObject);
var
  FichierSync : TACCFichierSync;
begin
  FichierSync := FichierSyncSelectionne;
  if FichierSync <> nil then
    if not FichierSync.RequeteSauvegarder then
      MessageDlg('Erreur lors de la sauvegarde du dossier',mtError,[mbOk],0);       

end;

procedure TFormEABPrinc.ActionFichierSyncRetirerExecute(Sender: TObject);
var
  FichierSync : TACCFichierSync;
  Serveur : TACCServeur;
begin
  FichierSync := FichierSyncSelectionne;
  if FichierSync <> nil then
  begin
      Serveur := FichierSync.Serveur;
      Serveur.FichiersSync.Remove(FichierSync);
      DonneesEAB.EnregistrerFichierSync;
      ActualiserFichiersSync(TTReeNode(Serveur.FichiersSync.Data));
  end;

end;


procedure TFormEABPrinc.ActualiserVue;
var
  Fichier : TACCFichier;
  FichierSync : TACCFichierSync;
begin
  if TreeViewServeurs.Selected <> nil then
   ActualiserNoeud(TreeViewServeurs.Selected);

end;

procedure TFormEABPrinc.ServeurDonneesChange(Sender: TObject);
begin
  ActualiserVue;
end;



function TFormEABPrinc.FichierSyncSelectionne: TACCFichierSync;
begin
  result := nil;
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.Data) is TACCFichierSync then
      result := TACCFichierSync(TreeViewServeurs.Selected.Data);

end;

function TFormEABPrinc.DossierSelectionne: TACCDossier;
begin
  Result := nil;
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.Data) is TACCDossier then
      Result := TACCDossier(TreeViewServeurs.Selected.Data);

end;

function TFormEABPrinc.ServeurSelectionne: TACCServeur;
begin
  Result := nil;
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.Data) is TACCServeur then
      Result := TACCServeur(TreeViewServeurs.Selected.Data);


end;



procedure TFormEABPrinc.ActionDossierTelechargerExecute(Sender: TObject);
var
  Dossier  : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
    with TFormEACProgress.Create(self) do
    begin
      Ouvrir(Dossier.Serveur,'Telechargement du dossier '+Dossier.Nom);
      if not Dossier.RequeteLire then
        MessageDlg('Erreur lors de la lecture du dossier',mtError,[mbOk],0);
      Fermer;
      free;
    end;


  end;
end;

procedure TFormEABPrinc.ActionDossierEnvoyerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
  Chemin : String;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      if SelectDirectory('Selectionner le dossier a envoyer sur le serveur '+Dossier.Serveur.Nom
             ,'',Chemin) then
      begin
        with TFormEACProgress.Create(self) do
        begin
          Ouvrir(Dossier.Serveur,'Envoie du dossier '+ExtractFileName(Chemin));
          if not DirectoryExists(Dossier.CheminLocal)then
            ForceDirectories(Dossier.CheminLocal);
          CopyDirectory(Chemin,Dossier.CheminLocal+'\'+ExtractFileName(Chemin));
          if not Dossier.Serveur.RequeteDossierEcrire(Dossier.Chemin+'\'+ExtractFileName(Chemin))then
            MessageDlg('Erreur lors de l''�criture du dossier',mtError,[mbOk],0);
          Fermer;
          Free;
        end;
      end;
  end;
end;

procedure TFormEABPrinc.ActionDossierOuvrirExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      if Dossier.Exist then
        ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
          PChar(ExtractFilePath(Dossier.CheminLocal)), nil,
          SW_SHOWNORMAL)
      else
        if MessageDlg(
            'Le dossier '+Dossier.Nom+' n''existe pas en local,'
              +' souhaitez vous le t�l�charger ?',
              mtWarning,[mbYes,mbNo],0) = mrYes then
        begin
          with TFormEACProgress.Create(self) do
          begin
            Ouvrir(Dossier.Serveur,'Telechargement du dossier '+Dossier.Nom);
            if Dossier.RequeteLire then
                  ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
                  PChar(ExtractFilePath(Dossier.CheminLocal)), nil,
                  SW_SHOWNORMAL)
            else
              MessageDlg('Erreur lors de la lecture du dossier',mtError,[mbOk],0);
            fermer;
            free;
          end;

        end;
  end;

end;



procedure TFormEABPrinc.ActionDossierRenommerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
  Nouveau : string;

begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
    Nouveau := Dossier.Nom;
    if InputQuery('Renommer','Saisissez le nouveau nom',Nouveau) then
      if not Dossier.RequeteRenommer(Nouveau) then
        MessageDlg('Erreur lors du changement de nom du dossier',mtError,[mbOk],0);

  end;
end;


procedure TFormEABPrinc.ActionDossierMettreAJourExecute(Sender: TObject);
var
  FichierSync : TACCFichierSync;
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
    FichierSync := Dossier.DossierSynchronise;
    if FichierSync <> nil then
      with TFormEACProgress.Create(Self) do
      begin
        Ouvrir(FichierSync.Serveur,'Mise a jour du dossier '+FichierSync.Nom);
        if not FichierSync.RequeteMettreAJour then
          MessageDlg('Erreur lors de la mise a jour du fichier',mtError,[mbOk],0);
        Fermer;
        Free;
      end;

  end;

end;

procedure TFormEABPrinc.ActionAjouterServeurExecute(Sender: TObject);
var
  Adresse : String;
  Serveur : TACCServeur;
begin
  Adresse := 'localhost';
  with TFormEABEditServeur.Create(self) do
  begin
    if execute then
    begin
      Serveur := DonneesEAB.Poste.Serveurs.Add(Adresse);
      Serveur.SyncMode := ACSyncModeLecture;
      Serveur.Nom := Nom;
      Serveur.Utilisateur := Utilisateur;
      Serveur.MotPasse := MotPasse;
      Serveur.ConnexionAuto := true;
      Serveur.DemarrageActif := DemarrageActif;
      DonneesEAB.EnregistrerListeDesServeur;
      ActualiserPoste;
    end;
    free;
  end;

end;

procedure TFormEABPrinc.ActionConfigurationExecute(Sender: TObject);
begin
  with TFormEABConfiguration.Create(self)do
  begin
    Executer;
    free;
  end;
end;

procedure TFormEABPrinc.ActionServeurSupprimerExecute(Sender: TObject);
var
  Serveur : TACCServeur;
begin
  Serveur := ServeurSelectionne;
  if Serveur <> nil then
  begin
    if MessageDlg('Attention, Etes vous s�re de vouloir supprimer le serveur '+Serveur.Nom+' ?',mtWarning,[mbYes,mbNo],0)= mrYes then
    begin
      
      DonneesEAB.Poste.Serveurs.Remove(Serveur);
      DonneesEAB.EnregistrerListeDesServeur;
      ActualiserPoste;
    end;
  end;
end;

procedure TFormEABPrinc.TreeViewServeursChange(Sender: TObject;
  Node: TTreeNode);
begin
  ActualiserNoeud(TreeViewServeurs.Selected);
end;


procedure TFormEABPrinc.ActionServeurProprieteExecute(Sender: TObject);
var
  Serveur : TACCServeur;
begin
  Serveur := ServeurSelectionne;
  if Serveur <> nil then
  begin
    with TFormEABEditServeur.Create(Self)do
    begin
      execute(Serveur);


      Free;
    end;

  end;


end;

procedure TFormEABPrinc.LancementDemarrage;
var
  Sauvegarder :Boolean;
  Eteindre :Boolean;
  Fermer :Boolean;
  ExtinctionForcer :Boolean;
  ExtinctionOrdinateur : boolean;

begin
  ExtinctionOrdinateur := false;
  With DonneesEAB do
  begin
    Sauvegarder := Configuration.Demarrage.Sauvegarder;
    Eteindre := Configuration.Demarrage.Eteindre;
    Fermer := Configuration.Demarrage.Fermer;
    ExtinctionForcer := Configuration.Demarrage.ExtinctionForcer;

    if  Sauvegarder then
      With TFormEABAvertissement.Create(self) do
      begin
        LabelMessage.Caption := MESSAGE_SAUVEGARDE;
        if Eteindre then
        begin
          LabelMessage.Caption := MESSAGE_EXTINCTION;
        end
        else
          if Fermer then
          begin
            LabelMessage.Caption := MESSAGE_FERMETURE;
            ButtonAnnulerExtinction.Caption := 'Ne pas fermer';
          end;



        if not Executer(Configuration.Demarrage.Avertissement) then
        begin
          Sauvegarder := not AnnulerSauvegarde;
          Eteindre := not AnnulerExtinction;
          Fermer := not AnnulerExtinction;
        end;
        free;
      end;

    if Sauvegarder then
    begin
      LancerSauvegarde;
      if Eteindre then
        if Poste.DemandeExtinction then
        begin
          with TFormEABAvertissementExtinction.Create(self) do
          begin
            if Executer then
            begin
                Poste.EcrireMessage(ACMessageRequete, 'Extinction de l''ordinateur activ�');
                ExtinctionOrdinateur := true;
            end;
            Free;
          end;
        end
        else
          if ExtinctionForcer and not Poste.ServeursTestConnexion then
          begin
            with TFormEABAvertissementExtinction.Create(self) do
            begin
              if Executer then
              begin
                Poste.EcrireMessage(ACMessageRequete,'Extinction de l''ordinateur activ�');
                ExtinctionOrdinateur := true;
              end;
              Free;
            end;
          end;

      EnvoyerRapportSauvegarde;
      
      if ExtinctionOrdinateur then
      begin
        Poste.EcrireMessage(ACMessageRequete,'Extinction de l''ordinateur');
        EteindreWindows(true);
        Application.Terminate;
      end;

      if Configuration.Demarrage.Fermer then
        Close;

      if Configuration.Demarrage.Reduire then
        JvTrayIcon1.HideApplication;
    end;
  end;

end;

procedure TFormEABPrinc.ActualiserMenuServeur(Node: TTReeNode);
begin
  if Node = nil then
      exit;


  if TObject(Node.Data) is TACCPoste then
  begin
    ActualiserMenuServeur('Poste');
    exit;
  end;

  if TObject(Node.Data) is TACCServeur then
  begin
    ActualiserMenuServeur('Serveur');
    exit;
  end;

  if TObject(Node.Data) is TACCDossierList then
  begin
      ActualiserMenuServeur('Dossiers');
      exit;
  end;


  if TObject(Node.Data) is TACCDossier then
  begin
    ActualiserMenuServeur('Dossier');
    exit;

  end;

  if TObject(Node.Data) is TACCFichierSyncList then
  begin
    ActualiserMenuServeur('Serveur');
    exit;
  end;

  if TObject(Node.Data) is TACCFichierSync then
  begin
    ActualiserMenuServeur('FichierSync');
    exit;
  end;

end;

procedure TFormEABPrinc.PopupMenuServeursPopup(Sender: TObject);
begin
  if TreeViewServeurs.Selected <> nil then
    ActualiserMenuServeur(TreeViewServeurs.Selected);
end;

procedure TFormEABPrinc.ActionPosteSauvegarderExecute(Sender: TObject);
begin
  LancerSauvegarde;
end;

procedure TFormEABPrinc.ActionServeurSauvegarderExecute(Sender: TObject);
var
  Serveur : TACCServeur;
begin
  Serveur := ServeurSelectionne;
  if Serveur <> nil then
  begin
    Serveur.Sauvegarder;
  end;
end;

procedure TFormEABPrinc.Quitter1Click(Sender: TObject);
begin
  Close;
  
end;

procedure TFormEABPrinc.Quitter2Click(Sender: TObject);
begin
  Close;
end;

procedure TFormEABPrinc.Ouvrir1Click(Sender: TObject);
begin
  JvTrayIcon1.ShowApplication;
end;

procedure TFormEABPrinc.JvTrayIcon1BalloonHide(Sender: TObject);
begin
  BalloonMessage.Clear; 
end;

procedure TFormEABPrinc.PosteSauvegarder;
begin
  DonneesEAB.Poste.Sauvegarder;

end;

procedure TFormEABPrinc.FormDestroy(Sender: TObject);
begin
  DonneesEAB.Poste.FermerLesConnexions;
  Journal.free;
end;

procedure TFormEABPrinc.ServeurAvancement(Sender: TObject; Libelle: string;
  Max, Position: integer);
begin
  ProgressBar1.Max := Max;
  ProgressBar1.Position := Position;
  Repaint;
end;

procedure TFormEABPrinc.FormShow(Sender: TObject);
begin
  if Demarrage then
  begin
    Demarrage := false;
    LancementDemarrage;
  end;
end;

procedure TFormEABPrinc.LancerSauvegarde;
begin
  PosteSauvegarder;
//  TimerSauvegarde.Enabled := true;
end;

procedure TFormEABPrinc.TimerSauvegardeTimer(Sender: TObject);
begin
  TimerSauvegarde.Enabled := false;
  PosteSauvegarder;
end;

procedure TFormEABPrinc.EnvoyerRapportSauvegarde;
const
  MailSujet = 'Rapport de sauvegarde';
  MailMessage = 'Une sauvegarde � �t� effectu�e, vous trouverez ci-joint le journal de sauvegarde';
var
  MailContenue : TStringList;
begin
    with DonneesEAB do
    begin
      if Configuration.General.TracerJournal and Configuration.Mail.Activer then
      begin
        MailContenue := TStringList.Create;
        MailContenue.Add(MailMessage);
        MailContenue.Add('Rapport de sauvegarde du '+DateToStr(Now));
        EnvoyerEmail(MailSujet,MailContenue.GetText,DonneesEAB.Configuration.Mail,true,FichierJournal);
        Poste.EcrireMessage(ACMessageRequete,'Mail Envoy� � '+DonneesEAB.Configuration.Mail.Destinataire);
        MailContenue.Free;
      end;
    end;
end;

end.
