program EArchiveBackup;

uses
  Forms,
  EABPrinc in 'EABPrinc.pas' {FormEABPrinc},
  EABDonnees in 'EABDonnees.pas' {DonneesEAB: TDataModule},
  EABConfiguration in 'EABConfiguration.pas' {FormEABConfiguration},
  EACEditServeur in 'EACEditServeur.pas' {FormEACEditServeur},
  ArchivesCloudClient in 'ArchivesCloudClient.pas',
  EABAvertissementExtinction in 'EABAvertissementExtinction.pas' {FormEABAvertissementExtinction},
  ArchivesCloudCommun in '..\ArchivesCloudCommun.pas',
  SyncClusterStream in '..\SyncClusterStream.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'ArchiveSync Backup : Logiciel de sauvegarde de fichiers';
  Application.CreateForm(TDonneesEAB, DonneesEAB);
  Application.CreateForm(TFormEABPrinc, FormEABPrinc);
  Application.Run;
end.
