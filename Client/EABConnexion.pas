unit EABConnexion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,   ExtCtrls, idHash,Utiles,EABDonnees;


type
  TFormEABConnexion = class(TForm)
    Panel1: TPanel;
    StaticText1: TStaticText;
    Panel2: TPanel;
    ButtonQuitter: TButton;
    ButtonValider: TButton;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EditUtilisateur: TEdit;
    EditMotDePasse: TEdit;
    CheckBoxConnexionAutomatique: TCheckBox;
    TimerConnexion: TTimer;
    procedure TimerConnexionTimer(Sender: TObject);
  private
    FMotPasse : string;
    function GetMotPasse: string;
    function GetUtilisateur: string;
    function GetConnexionAuto: boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
    property Utilisateur : string read GetUtilisateur;
    property MotPasse : string read GetMotPasse;
    property ConnexionAuto : boolean read GetConnexionAuto;
    function Connexion :boolean;
  end;

const
  NO_PASSE = 'xxxxx';


implementation



{$R *.dfm}

procedure TFormEABConnexion.TimerConnexionTimer(Sender: TObject);
begin
  TimerConnexion.Enabled:= false;

end;

function TFormEABConnexion.Connexion : boolean;
begin
  with DonneesEAB.Configuration do
  begin
    if General.ConnexionAuto then
    begin
      FMotPasse := General.DernierMotPasse;
      EditMotDePasse.Text := NO_PASSE;
    end;
    EditUtilisateur.Text := General.DernierUtilisateur;
    CheckBoxConnexionAutomatique.Checked := General.ConnexionAuto;
    if not General.ConnexionAuto then
      result := ShowModal = mrok
    else
      result := true;
  end;
end;

function TFormEABConnexion.GetMotPasse: string;
begin
  if EditMotDePasse.Text <> NO_PASSE then
    result := MD5CryptMotDePasse(EditMotDePasse.Text)
  else
    result := FMotPasse;
end;

function TFormEABConnexion.GetUtilisateur: string;
begin
  Result := EditUtilisateur.Text;
end;

function TFormEABConnexion.GetConnexionAuto: boolean;
begin
  result := CheckBoxConnexionAutomatique.Checked;
end;

end.
