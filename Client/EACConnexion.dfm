object FormEACConnexion: TFormEACConnexion
  Left = 510
  Top = 318
  BorderStyle = bsDialog
  Caption = 'Fen'#234'tre de connexion'
  ClientHeight = 181
  ClientWidth = 268
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 268
    Height = 33
    Align = alTop
    Alignment = taLeftJustify
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    DesignSize = (
      268
      33)
    object StaticText1: TStaticText
      Left = 16
      Top = 8
      Width = 239
      Height = 15
      Anchors = [akLeft, akTop, akRight, akBottom]
      AutoSize = False
      Caption = 'Connexion de l'#39'utilisateur.'
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Reference Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 140
    Width = 268
    Height = 41
    Align = alBottom
    TabOrder = 1
    object ButtonQuitter: TButton
      Left = 176
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Annuler'
      ModalResult = 2
      TabOrder = 1
    end
    object ButtonValider: TButton
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Connecter'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 33
    Width = 268
    Height = 107
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 0
      Top = 15
      Width = 97
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Utilisateur : '
    end
    object Label2: TLabel
      Left = 0
      Top = 46
      Width = 97
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Mot de passe : '
    end
    object EditUtilisateur: TEdit
      Left = 104
      Top = 10
      Width = 137
      Height = 21
      TabOrder = 0
    end
    object EditMotDePasse: TEdit
      Left = 104
      Top = 42
      Width = 137
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Symbol'
      Font.Style = []
      ParentFont = False
      PasswordChar = #183
      TabOrder = 1
    end
    object CheckBoxConnexionAutomatique: TCheckBox
      Left = 48
      Top = 72
      Width = 193
      Height = 22
      Alignment = taLeftJustify
      BiDiMode = bdLeftToRight
      Caption = 'Connexion automatique :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Reference Sans Serif'
      Font.Style = []
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 2
    end
  end
  object TimerConnexion: TTimer
    Enabled = False
    Interval = 100
    OnTimer = TimerConnexionTimer
    Left = 40
    Top = 65
  end
end
