unit EACArchiveCloudProgress;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ArchivesCloudClient, StdCtrls, ComCtrls,IdTCPClient,IdComponent;

type
  TFormEACProgress = class(TForm)
    StaticTextMessage: TStaticText;
    ProgressBar1: TProgressBar;
    procedure FormDestroy(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }

    Serveur : TACCServeur;
    procedure Ouvrir(pServeur : TACCServeur;pMessage:string);
    procedure ServeurWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure ServeurWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure ServeurWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
    procedure ServeurAvancement(Sender : TObject;Libelle : string;Max : integer;Position : integer);

    procedure Fermer;
  end;


implementation

{$R *.dfm}

{ TFormEACProgress }

procedure TFormEACProgress.Fermer;
begin
  Serveur.OnWorkBegin := nil;
  Serveur.OnWork := nil;
  Serveur.OnWorkEnd := nil;
  Serveur.OnAvancement := nil;
  
  close;
end;

procedure TFormEACProgress.Ouvrir(pServeur: TACCServeur; pMessage: string);
begin
  StaticTextMessage.Caption := pMessage;
  Show;
  Refresh;
  Serveur := pServeur;
  Serveur.OnWorkBegin := ServeurWorkBegin;
  Serveur.OnWork := ServeurWork;
  Serveur.OnWorkEnd := ServeurWorkEnd;
  Serveur.OnAvancement := ServeurAvancement;
end;

procedure TFormEACProgress.ServeurWork(Sender: TObject;
  AWorkMode: TWorkMode; const AWorkCount: Integer);
begin
  ProgressBar1.Position := AWorkCount;
  Refresh;
  Repaint;

end;

procedure TFormEACProgress.ServeurWorkBegin(Sender: TObject;
  AWorkMode: TWorkMode; const AWorkCountMax: Integer);
begin
  ProgressBar1.Position := 0;
  ProgressBar1.Max := AWorkCountMax;
  Repaint;

end;

procedure TFormEACProgress.ServeurWorkEnd(Sender: TObject;
  AWorkMode: TWorkMode);
begin
  ProgressBar1.Position := 0;
  Repaint;
end;

procedure TFormEACProgress.FormDestroy(Sender: TObject);
begin
 if Serveur <> nil then
 begin
  Serveur.OnWorkBegin := nil;
  Serveur.OnWork := nil;
  Serveur.OnWorkEnd := nil;
  Serveur.OnAvancement := nil; 
 end;

end;

procedure TFormEACProgress.ServeurAvancement(Sender: TObject;
  Libelle: string; Max, Position: integer);
begin
    StaticTextMessage.Caption := Libelle;
    ProgressBar1.Max := Max;
    ProgressBar1.Position :=Position;
    Refresh;
    Repaint;
end;

end.
