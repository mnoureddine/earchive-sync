unit EABDonnees;

interface

uses
  SysUtils, Classes,ArchivesCloudClient,ArchivesCloudCommun,Inifiles,windows,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdMessageClient, IdSMTP,IdMessage,Utiles,ShellAPI;

type
  TEABMailParam = record
        Activer : boolean;
        Emeteur : string;
        Destinataire : string;
        SMTP : string;
        Port : integer;
        Utilisateur : string;
        MotPasse : string;
     end;

  TDonneesEAB = class(TDataModule)
    IdSMTP1: TIdSMTP;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    FichierListe : string;
    FichierIni : TIniFile;
    Poste : TACCPoste;
    Configuration : record
     General : record
         BarreDeTache : boolean;
         DossierTravail : string;
         TracerJournal : boolean;
         DernierUtilisateur : string;
         DernierMotPasse : string;
         ConnexionAuto : boolean;
         AfficherMessagesInfo : boolean;
     end;
     Demarrage : record
        Reduire : boolean;
        Sauvegarder : boolean;
        Fermer : boolean;
        Eteindre : boolean;
        ExtinctionForcer : boolean;
        Avertissement : integer;
     end;
     Mail : TEABMailParam;
    end;
    procedure ChargerConfiguration;
    procedure EnregistrerConfiguration;
    procedure ChargerFichierSync;
    procedure ChargerListeDesServeur;
    procedure EnregistrerFichierSync;
    procedure EnregistrerListeDesServeur;
    function EnvoyerEmail(  pSujet:string; pMessage : string; MailParam : TEABMailParam;
                            FichierAttacher : boolean=false;Fichier : string=''):boolean;
  end;

var
  DonneesEAB: TDonneesEAB;
const
  FichierIniNom = 'ArchiveSyncBackup.ini';
  FichierSyncNom = '_Sync.txt';
Function EteindreWindows(modeForce : Boolean = False) : Boolean;

implementation

{$R *.dfm}

Function EteindreWindows(modeForce : Boolean = False) : Boolean;
begin
(*
   if not modeForce then Result := ExitWindowsEx(EWX_SHUTDOWN, 0)
   else Result := ExitWindowsEx(EWX_SHUTDOWN + EWX_FORCE, 0);
*)
  ShellExecute(0,PChar('open'),PChar('shutdown.exe'),PChar('-s -t 5 -c  "L''ordinateur va �tre �teint !"'),nil,0);
end;

procedure TDonneesEAB.ChargerConfiguration;
begin
  With Configuration  do
  begin
    General.BarreDeTache := FichierIni.ReadBool('General','BarreDeTache',False);
    General.DossierTravail := FichierIni.ReadString('General','DossierTravail',
                                        DossierMesDocuments+'\ArchiveBackup');
    General.TracerJournal := FichierIni.ReadBool('General','TracerJournal',true);
    General.AfficherMessagesInfo := FichierIni.ReadBool('General','AfficherMessagesInfo',false);

    Demarrage.Reduire := FichierIni.ReadBool('Demarrage','Reduire',False);
    Demarrage.Sauvegarder := FichierIni.ReadBool('Demarrage','Sauvegarder',False);
    Demarrage.Fermer := FichierIni.ReadBool('Demarrage','Fermer',False);
    Demarrage.Eteindre := FichierIni.ReadBool('Demarrage','Eteindre',False);
    Demarrage.ExtinctionForcer := FichierIni.ReadBool('Demarrage','ExtinctionForcer',False);
    Demarrage.Avertissement := FichierIni.ReadInteger('Demarrage','Avertissement',30);


    Mail.Emeteur := FichierIni.ReadString('Mail','Emeteur','');
    Mail.Destinataire := FichierIni.ReadString('Mail','Destinataire','');
    Mail.SMTP := FichierIni.ReadString('Mail','SMTP','');
    Mail.Port := FichierIni.ReadInteger('Mail','Port',25);
    Mail.Utilisateur := FichierIni.ReadString('Mail','Utilisateur','');
    Mail.MotPasse := FichierIni.ReadString('Mail','MotPasse','');
    Mail.Activer := FichierIni.ReadBool('Mail','Activer',false);

  end;

end;


procedure TDonneesEAB.DataModuleCreate(Sender: TObject);
begin
  FichierIni := TIniFile.Create(FichierIniNom);
  ChargerConfiguration;
  Poste := TACCPoste.Create(Configuration.General.DossierTravail);


end;

procedure TDonneesEAB.EnregistrerConfiguration;
begin
  with Configuration do
  begin
    FichierIni.WriteBool('General','BarreDeTache',General.BarreDeTache);
    FichierIni.WriteString('General','DossierTravail',General.DossierTravail);
    FichierIni.WriteBool('General','TracerJournal',General.TracerJournal);
    FichierIni.WriteBool('General','AfficherMessagesInfo',General.AfficherMessagesInfo);

    FichierIni.WriteBool('Demarrage','Reduire',Demarrage.Reduire);
    FichierIni.WriteBool('Demarrage','Sauvegarder',Demarrage.Sauvegarder);
    FichierIni.WriteBool('Demarrage','Fermer',Demarrage.Fermer);
    FichierIni.WriteBool('Demarrage','Eteindre',Demarrage.Eteindre);
    FichierIni.WriteBool('Demarrage','ExtinctionForcer',Demarrage.ExtinctionForcer);
    FichierIni.WriteInteger('Demarrage','Avertissement',Demarrage.Avertissement);

    FichierIni.WriteString('Mail','Emeteur',Mail.Emeteur);
    FichierIni.WriteString('Mail','Destinataire',Mail.Destinataire);
    FichierIni.WriteString('Mail','SMTP',Mail.SMTP);
    FichierIni.WriteInteger('Mail','Port',Mail.Port);
    FichierIni.WriteString('Mail','Utilisateur',Mail.Utilisateur);
    FichierIni.WriteString('Mail','MotPasse',Mail.MotPasse);
    FichierIni.WriteBool('Mail','Activer',Mail.Activer);
  end;


end;

procedure TDonneesEAB.DataModuleDestroy(Sender: TObject);
begin
  Poste.Free;
  FichierIni.Free;
  
end;

procedure TDonneesEAB.ChargerFichierSync;
var
  i : Integer;
  FichierSync : string;
begin
  for i := 0 to Poste.Serveurs.Count - 1 do
  begin
    FichierSync := Configuration.General.DossierTravail+'\'+Poste.Serveurs[i].Nom+FichierSyncNom;
    if FileExists(FichierSync) then
      Poste.Serveurs[i].FichiersSync.LoadFromFile(FichierSync);
  end;
end;

procedure TDonneesEAB.EnregistrerFichierSync;
var
  i : integer;
  FichierSync : string;
begin
  for i := 0 to Poste.Serveurs.Count - 1 do
  begin
    FichierSync := Configuration.General.DossierTravail+'\'+Poste.Serveurs[i].Nom+FichierSyncNom;
    Poste.Serveurs[i].FichiersSync.SaveToFile(FichierSync);
  end;

end;

procedure TDonneesEAB.ChargerListeDesServeur;
var
  ListeServeur : TStringList;
  Serveur : TACCServeur;
  FichierSync : string;  
  i : Integer;
begin
  FichierListe := Configuration.General.DossierTravail+'\ListeServeur.txt';
  ListeServeur := TStringList.Create;
  if not FileExists(FichierListe) then
    ListeServeur.SaveToFile(FichierListe);
  ListeServeur.LoadFromFile(FichierListe);
  For i := 0 to ListeServeur.Count - 1 do
  begin
    Serveur := Poste.Serveurs.Add(ListeServeur[i]);
    Serveur.SyncMode := ACSyncModeLecture;
    Serveur.OuvrirDeFichier(Configuration.General.DossierTravail+'\'+ListeServeur[i]+'.cfg');
    FichierSync := Configuration.General.DossierTravail+'\'+Serveur.Nom+FichierSyncNom;
    if FileExists(FichierSync) then
      Serveur.FichiersSync.LoadFromFile(FichierSync);

    if Serveur.DemarrageActif then
      Serveur.Connexion;
  end;
  ListeServeur.Free;
end;

procedure TDonneesEAB.EnregistrerListeDesServeur;
var
  ListeServeur : TStringList;
  i : Integer;
begin

  FichierListe := Configuration.General.DossierTravail+'\ListeServeur.txt';
  ListeServeur := TStringList.Create;
  For i :=  0 to Poste.Serveurs.Count - 1 do
  begin
    ListeServeur.Add(Poste.Serveurs[i].Nom);
    Poste.Serveurs[i].EnregistrerSousFichier(Configuration.General.DossierTravail+'\'+Poste.Serveurs[i].Host+'.cfg');
  end;

  ListeServeur.SaveToFile(FichierListe);
  ListeServeur.Free;

end;

function TDonneesEAB.EnvoyerEmail(  pSujet:string;pMessage : string;MailParam : TEABMailParam;
      FichierAttacher : boolean=false;Fichier : string=''):boolean;
    procedure AjouterFichier(idMessage :TIdMessage;Fichier :string);
    var
      Nom : string;
      Attachement : TIdAttachment;
    begin
      Nom := ExtractFileName(Fichier);
      Attachement := TIdAttachment.Create(idMessage.MessageParts, Fichier);
      Attachement.ContentType := 'text/plain';
      Attachement.DisplayName := Nom;
      Attachement.ContentTransfer := 'base64';
      Attachement.ContentDisposition := 'attachment';
      Attachement.FileName := Nom;
    end;

var
    HTML : TStrings;
    htmpart, txtpart: TIdText;
    Dossier : string;
    MessageEmail : TIdMessage;
    idSMPT : TIdSMTP;
begin
    Result := false;
    Dossier := GetCurrentDir+'\';
    HTML := TStringList.Create;
    HTML.Add('<html><head></head>');
    HTML.Add('<body><h2>ArchiveSync Backup  '+DateTimeToStr(now)+'</h2>');
    HTML.Add('<table border="1"><tr><td>'+pMessage+'</td></tr></table>');
    HTML.Add('</body></html>');

    MessageEmail := TIdMessage.Create(nil);
    MessageEmail.From.Text :=  MailParam.Emeteur;
    MessageEmail.Recipients.EMailAddresses := MailParam.Destinataire;
    MessageEmail.Subject := 'ArchiveSync Backup '+GetApplicationVersionString+' : ' +pSujet;
    MessageEmail.ContentType := 'multipart/mixed';
    MessageEmail.Body.Assign(HTML);

    txtpart := TIdText.Create(MessageEmail.MessageParts);
    txtpart.ContentType := 'text/plain';
    txtpart.Body.Text := '';

    htmpart := TIdText.Create(MessageEmail.MessageParts, html);
    htmpart.ContentType := 'text/html';
    if FichierAttacher then
    begin
        AjouterFichier(MessageEmail,Fichier);
    end;


   with IdSMTP1 do
   begin
    AuthenticationType := atLogin;
    Username := MailParam.Utilisateur;
    Password := MailParam.MotPasse;
    Host := MailParam.SMTP;
    Port := MailParam.Port;
    try
        Connect(10000);
        if Authenticate then
        begin
            if Connected then
            begin
                Send(MessageEmail);
                Result := true;
            end;
        end;


    except
        Result := false;
    end;
    if Connected then
        Disconnect;
    MessageEmail.Free;
    HTML.Free;

   end;

end;
end.
