unit EABAvertissement;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TFormEABAvertissement = class(TForm)
    GroupBox1: TGroupBox;
    ButtonAnnulerSauvegarde: TButton;
    LabelMessage: TLabel;
    ButtonAnnulerExtinction: TButton;
    Timer1: TTimer;
    LabelCompteur: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ButtonAnnulerSauvegardeClick(Sender: TObject);
    procedure ButtonAnnulerExtinctionClick(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    Compteur : integer;
    AnnulerSauvegarde : boolean;
    AnnulerExtinction : boolean;
    function Executer(pCompteur :integer = 30) : boolean;

    
  end;

const
  MESSAGE_SAUVEGARDE = 'Le logiciel ArchiveSync Backup va proc�der � la'
  +' sauvegarde du serveur dans :';

  MESSAGE_EXTINCTION = 'Le logiciel ArchiveSync Backup va proc�der � la'
  +' sauvegarde du serveur. apr�s quoi le pc sera mis hors tension dans :';

  MESSAGE_FERMETURE = 'Le logiciel ArchiveSync Backup va proc�der � la'
  +' sauvegarde du serveur. apr�s quoi l''application se ferm� dans :';

implementation



{$R *.dfm}

procedure TFormEABAvertissement.FormCreate(Sender: TObject);
begin
  Compteur := 30;
  LabelCompteur.Caption := Format('%d Secondes',[Compteur]);
end;

procedure TFormEABAvertissement.Timer1Timer(Sender: TObject);
begin
  Dec(Compteur);
  LabelCompteur.Caption := Format('%d Secondes',[Compteur]);
  if Compteur <= 0 then
  begin
    ModalResult := mrOK;
    Timer1.Enabled := false;

  end;
end;

procedure TFormEABAvertissement.ButtonAnnulerSauvegardeClick(Sender: TObject);
begin
    AnnulerSauvegarde := true;
    ModalResult := mrAbort;

end;

procedure TFormEABAvertissement.ButtonAnnulerExtinctionClick(Sender: TObject);
begin
    AnnulerExtinction := true;
    ModalResult := mrAbort
end;

function TFormEABAvertissement.Executer(pCompteur :integer = 30) : boolean;
begin
  Compteur := pCompteur;
  result := ShowModal = mrOK;
end;

end.

