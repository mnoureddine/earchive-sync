program EArchiveCloud;

uses
  Forms,
  Princ in 'Princ.pas' {FormEACPrinc},
  EACDonnees in 'EACDonnees.pas' {DonneesEAC: TDataModule},
  ArchivesCloudClient in 'ArchivesCloudClient.pas',
  ArchivesCloudCommun in '..\ArchivesCloudCommun.pas',
  EACConfiguration in 'EACConfiguration.pas' {FormEACConfiguration},
  EACConnexion in 'EACConnexion.pas' {FormEACConnexion},
  EACArchiveCloudProgress in 'EACArchiveCloudProgress.pas' {FormEACProgress},
  EACEditServeur in 'EACEditServeur.pas' {FormEACEditServeur},
  SyncClusterStream in '..\SyncClusterStream.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'ArchiveSync : Logiciel de synchronisation de fichier';
  Application.CreateForm(TDonneesEAC, DonneesEAC);
  Application.CreateForm(TFormEACPrinc, FormEACPrinc);
  Application.Run;
end.
