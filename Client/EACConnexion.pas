unit EACConnexion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,   ExtCtrls, idHash,Utiles,EACDonnees;


type
  TFormEACConnexion = class(TForm)
    Panel1: TPanel;
    StaticText1: TStaticText;
    Panel2: TPanel;
    ButtonQuitter: TButton;
    ButtonValider: TButton;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EditUtilisateur: TEdit;
    EditMotDePasse: TEdit;
    CheckBoxConnexionAutomatique: TCheckBox;
    TimerConnexion: TTimer;
    procedure TimerConnexionTimer(Sender: TObject);
  private
    function GetMotPasse: string;
    function GetUtilisateur: string;
    function GetConnexionAuto: boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
    property Utilisateur : string read GetUtilisateur;
    property MotPasse : string read GetMotPasse;
    property ConnexionAuto : boolean read GetConnexionAuto;
    function Connexion :boolean;
  end;



implementation



{$R *.dfm}

procedure TFormEACConnexion.TimerConnexionTimer(Sender: TObject);
begin
  TimerConnexion.Enabled:= false;

end;

function TFormEACConnexion.Connexion : boolean;
begin
  result := ShowModal = mrok
end;

function TFormEACConnexion.GetMotPasse: string;
begin
  result := MD5CryptMotDePasse(EditMotDePasse.Text)
end;

function TFormEACConnexion.GetUtilisateur: string;
begin
  Result := EditUtilisateur.Text;
end;

function TFormEACConnexion.GetConnexionAuto: boolean;
begin
  result := CheckBoxConnexionAutomatique.Checked;
end;

end.
