unit EABAvertissementExtinction;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TFormEABAvertissementExtinction = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    ButtonAnnulerExtinction: TButton;
    Timer1: TTimer;
    LabelCompteur: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ButtonAnnulerSauvegardeClick(Sender: TObject);
    procedure ButtonAnnulerExtinctionClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
    Compteur : integer;
    AnnulerSauvegarde : boolean;
    AnnulerExtinction : boolean;
    function Executer(pCompteur :integer = 30) : boolean;

    
  end;



implementation

{$R *.dfm}

procedure TFormEABAvertissementExtinction.FormCreate(Sender: TObject);
begin
  Compteur := 30;
  LabelCompteur.Caption := Format('%d Secondes',[Compteur]);
end;

procedure TFormEABAvertissementExtinction.Timer1Timer(Sender: TObject);
begin
  Dec(Compteur);
  LabelCompteur.Caption := Format('%d Secondes',[Compteur]);
  if Compteur <= 0 then
  begin
    ModalResult := mrOK;
    Timer1.Enabled := false;

  end;
end;

procedure TFormEABAvertissementExtinction.ButtonAnnulerSauvegardeClick(Sender: TObject);
begin
    AnnulerSauvegarde := true;
    ModalResult := mrAbort;

end;

procedure TFormEABAvertissementExtinction.ButtonAnnulerExtinctionClick(Sender: TObject);
begin
    AnnulerExtinction := true;
    ModalResult := mrAbort
end;

function TFormEABAvertissementExtinction.Executer(pCompteur :integer = 30) : boolean;
begin
  Compteur := pCompteur;
  result := ShowModal = mrOK;
end;

end.

