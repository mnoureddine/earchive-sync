unit EABEditServeur;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,ArchivesCloudClient,ArchivesCloudCommun,Utiles,
  Spin;

type
  TFormEABEditServeur = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    LabeledEditServeurNom: TLabeledEdit;
    LabeledEditServeurAdresse: TLabeledEdit;
    ButtonAnnuler: TButton;
    ButtonOK: TButton;
    LabeledEditUtilisateur: TLabeledEdit;
    LabeledEditMotPasse: TLabeledEdit;
    Bevel1: TBevel;
    CheckBoxDemarrageActif: TCheckBox;
    Label2: TLabel;
    SpinEditPort: TSpinEdit;
    CheckBoxSyncAvanceActive: TCheckBox;
    procedure LabeledEditServeurAdresseChange(Sender: TObject);
  private
    FMotPasse : string;
    function GetAdresse: string;
    function GetMotPasse: string;
    function GetNom: string;
    function GetUtilisateur: string;
    procedure SetAdresse(const Value: string);
    procedure SetMotPasse(const Value: string);
    procedure SetNom(const Value: string);
    procedure SetUtilisateur(const Value: string);
    function GetDemarrageActif: boolean;
    procedure SetDemarrageActif(const Value: boolean);
    function GetPort: Integer;
    procedure SetPort(const Value: Integer);
    function GetSyncAvanceActive: boolean;
    procedure SetSyncAvanceActive(const Value: boolean);
    { Déclarations privées }

  public

    property Nom : string read GetNom write SetNom;
    property Adresse : string read GetAdresse write SetAdresse;
    property Utilisateur : string read GetUtilisateur write SetUtilisateur;
    property MotPasse : string read GetMotPasse write SetMotPasse;
    property DemarrageActif : boolean read GetDemarrageActif write SetDemarrageActif;
    property Port : Integer read GetPort write SetPort;
    property SyncAvanceActive : boolean read GetSyncAvanceActive write SetSyncAvanceActive; 
    function execute(pServeur :TACCServeur = nil):boolean;
    { Déclarations publiques }
  end;

const
  NO_PASSE = 'xxxxx';

implementation

uses EABDonnees;

{$R *.dfm}

{ TFormEACEditServeur }

function TFormEABEditServeur.execute(pServeur: TACCServeur): boolean;
begin
  if pServeur <> nil then
  begin
    Nom := pServeur.Nom;
    Adresse := pServeur.Host;
    Utilisateur := pServeur.Utilisateur;
    MotPasse := NO_PASSE;
    FMotPasse := pServeur.MotPasse;
    DemarrageActif := pServeur.DemarrageActif;
    Port := pServeur.Port;
  end;
  result := ShowModal = mrOk;
  if Result then
  begin
        pServeur.Nom := Nom;
        pServeur.Host := Adresse;
        pServeur.Utilisateur := Utilisateur;
        pServeur.MotPasse := MotPasse;
        pServeur.ConnexionAuto := true;
        pServeur.DemarrageActif := DemarrageActif;
        pServeur.Port := Port;
        pServeur.SyncAvanceActive := SyncAvanceActive;
        DonneesEAB.EnregistrerListeDesServeur;
  end;

end;

function TFormEABEditServeur.GetAdresse: string;
begin
  Result := LabeledEditServeurAdresse.Text;
end;

function TFormEABEditServeur.GetDemarrageActif: boolean;
begin
  Result :=  CheckBoxDemarrageActif.Checked;
end;

function TFormEABEditServeur.GetMotPasse: string;
begin
  if SameText(LabeledEditMotPasse.Text,NO_PASSE) then
    Result := FMotPasse
  else
    Result :=  MD5CryptMotDePasse(LabeledEditMotPasse.Text);
end;

function TFormEABEditServeur.GetNom: string;
begin
  Result := LabeledEditServeurNom.Text;
end;

function TFormEABEditServeur.GetUtilisateur: string;
begin
  Result := LabeledEditUtilisateur.Text;
end;

procedure TFormEABEditServeur.SetAdresse(const Value: string);
begin
  LabeledEditServeurAdresse.Text := Value;
end;

procedure TFormEABEditServeur.SetDemarrageActif(const Value: Boolean);
begin
  CheckBoxDemarrageActif.Checked := Value;
end;

procedure TFormEABEditServeur.SetMotPasse(const Value: string);
begin
  LabeledEditMotPasse.Text := Value;
end;

procedure TFormEABEditServeur.SetNom(const Value: string);
begin
  LabeledEditServeurNom.Text := Value;
end;

procedure TFormEABEditServeur.SetUtilisateur(const Value: string);
begin
  LabeledEditUtilisateur.Text := Value;
end;

procedure TFormEABEditServeur.LabeledEditServeurAdresseChange(
  Sender: TObject);
begin
  LabeledEditServeurNom.Text := LabeledEditServeurAdresse.Text; 
end;

function TFormEABEditServeur.GetPort: Integer;
begin
  result := SpinEditPort.Value
end;

procedure TFormEABEditServeur.SetPort(const Value: Integer);
begin
  SpinEditPort.Value := Value;
end;

function TFormEABEditServeur.GetSyncAvanceActive: boolean;
begin
  Result := CheckBoxSyncAvanceActive.Checked;
end;

procedure TFormEABEditServeur.SetSyncAvanceActive(const Value: boolean);
begin
  CheckBoxSyncAvanceActive.Checked := Value;
end;

end.
