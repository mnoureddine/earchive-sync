unit ArchivesCloud;

interface
uses
  Classes, IdBaseComponent, IdComponent, IdUDPBase, IdUDPClient;


type
  TEACServer = class(TObject)
  private
    function GetAdresse: String;
    procedure SetAdresse(const Value: String);
    function GetConnecte: boolean;

  protected

  public
    IdUDPClient : TIdUDPClient;
    procedure Connecter;
    procedure Deconnecter;
    property Connecte : boolean  read GetConnecte;
    property Adresse :String read GetAdresse write SetAdresse;
    constructor Create(pAdresse : string);
    destructor Destroy;
  published

  end;

  TEACServerList = class(TList)
  private
    function Get(Index: Integer): TEACServer;
    procedure Put(Index: Integer; Item: TEACServer);
  public
    constructor Create;
    destructor Destroy; override;
    function Add(Item: TEACServer): Integer;overload;
    function Add(pAdresse : string) : TEACServer;overload;
    function Extract(Item: TEACServer): TEACServer;
    function First: TEACServer;
    function IndexOf(Item: TEACServer): Integer;
    procedure Insert(Index: Integer; Item: TEACServer);
    function Last: TEACServer;
    procedure Delete(Index: Integer);
    function Remove(Item: TEACServer): Integer;
    property Items[Index: Integer]: TEACServer read Get write Put; default;
    procedure Clear; override;
  end;

  TEACPoste = class(TObject)
  private

  protected

  public
    
    constructor Create;
    destructor Destroy;
  published

  end;

implementation

{ TEACServerList }

function TEACServerList.Add(Item: TEACServer): Integer;
begin
  result := inherited Add(Item);
end;

function TEACServerList.Add(pAdresse : string): TEACServer;
begin
  Result := TEACServer.Create(pAdresse);
  Add(Result);
end;

procedure TEACServerList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;    
  inherited;

end;

constructor TEACServerList.Create;
begin
  inherited;

end;

procedure TEACServerList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TEACServerList.Destroy;
begin
  inherited;
end;

function TEACServerList.Extract(Item: TEACServer): TEACServer;
begin
  result := inherited Extract(Item);
end;

function TEACServerList.First: TEACServer;
begin
  Result := inherited First;
end;

function TEACServerList.Get(Index: Integer): TEACServer;
begin
  Result := inherited Get(Index);
end;

function TEACServerList.IndexOf(Item: TEACServer): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TEACServerList.Insert(Index: Integer; Item: TEACServer);
begin
  inherited Insert(Index,Item);
end;

function TEACServerList.Last: TEACServer;
begin
  Result := inherited Last;
end;

procedure TEACServerList.Put(Index: Integer; Item: TEACServer);
begin
  inherited Put(Index,Item);

end;

function TEACServerList.Remove(Item: TEACServer): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;


{ TEACPoste }

constructor TEACPoste.Create;
begin

end;

destructor TEACPoste.Destroy;
begin

end;

{ TEACServer }

procedure TEACServer.Connecter;
begin

end;

constructor TEACServer.Create(pAdresse: string);
begin
  IdUDPClient := TIdUDPClient.Create(nil);
  Adresse := pAdresse;
end;

procedure TEACServer.Deconnecter;
begin

end;

destructor TEACServer.Destroy;
begin
  IdUDPClient.Free;
end;

function TEACServer.GetAdresse: String;
begin
  result := IdUDPClient.Host;
end;

function TEACServer.GetConnecte: boolean;
begin

end;

procedure TEACServer.SetAdresse(const Value: String);
begin
  IdUDPClient.Host := Value;
end;

end.
