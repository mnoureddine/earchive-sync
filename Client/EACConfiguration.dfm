object FormEACConfiguration: TFormEACConfiguration
  Left = 311
  Top = 189
  BorderStyle = bsDialog
  Caption = 'Configuration de l'#39'application'
  ClientHeight = 360
  ClientWidth = 364
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    364
    360)
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonOK: TButton
    Left = 198
    Top = 328
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object ButtonAnnuler: TButton
    Left = 281
    Top = 328
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Annuler'
    ModalResult = 2
    TabOrder = 1
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 351
    Height = 316
    ActivePage = TabSheet1
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'G'#233'n'#233'ral'
      object Label1: TLabel
        Left = 8
        Top = 45
        Width = 90
        Height = 13
        Caption = 'Dossier de travail : '
        FocusControl = EditDossierTravail
      end
      object Label2: TLabel
        Left = 15
        Top = 142
        Width = 150
        Height = 13
        Caption = 'Fr'#233'quence de mise '#224' jour (min) :'
        FocusControl = SpinEditFrequenceMiseAJour
      end
      object Label3: TLabel
        Left = 8
        Top = 8
        Width = 113
        Height = 13
        Caption = 'Gestion des fichiers'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Bevel1: TBevel
        Left = 8
        Top = 16
        Width = 329
        Height = 10
        Shape = bsBottomLine
      end
      object Label4: TLabel
        Left = 8
        Top = 72
        Width = 154
        Height = 13
        Caption = 'Service de synchronisation'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Bevel2: TBevel
        Left = 6
        Top = 80
        Width = 329
        Height = 10
        Shape = bsBottomLine
      end
      object Label5: TLabel
        Left = 16
        Top = 168
        Width = 31
        Height = 13
        Caption = 'Autre'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Bevel3: TBevel
        Left = 14
        Top = 176
        Width = 329
        Height = 10
        Shape = bsBottomLine
      end
      object EditDossierTravail: TEdit
        Left = 104
        Top = 40
        Width = 177
        Height = 21
        TabOrder = 0
      end
      object ButtonParcourir: TButton
        Left = 287
        Top = 38
        Width = 51
        Height = 25
        Caption = '...'
        TabOrder = 1
        OnClick = ButtonParcourirClick
      end
      object SpinEditFrequenceMiseAJour: TSpinEdit
        Left = 208
        Top = 136
        Width = 121
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 2
        Value = 0
      end
      object CheckBoxAutoMAJActive: TCheckBox
        Left = 56
        Top = 104
        Width = 166
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Actif au demarrage : '
        TabOrder = 3
      end
      object CheckBoxReduireDemarrage: TCheckBox
        Left = 40
        Top = 192
        Width = 177
        Height = 41
        Alignment = taLeftJustify
        Caption = 'Mettre l'#39'application dans la barre des taches : '
        TabOrder = 4
        WordWrap = True
      end
      object CheckBoxAfficherInformationsActive: TCheckBox
        Left = 40
        Top = 232
        Width = 177
        Height = 33
        Alignment = taLeftJustify
        Caption = 'Afficher les informations de synchronisation  : '
        TabOrder = 5
        WordWrap = True
      end
    end
  end
end
