object FormEABEditServeur: TFormEABEditServeur
  Left = 198
  Top = 113
  BorderStyle = bsDialog
  Caption = 'Configuration du serveur'
  ClientHeight = 363
  ClientWidth = 311
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    311
    363)
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 294
    Height = 312
    ActivePage = TabSheet1
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'G'#233'n'#233'ral'
      object Bevel1: TBevel
        Left = 8
        Top = 128
        Width = 257
        Height = 10
        Shape = bsBottomLine
      end
      object Label2: TLabel
        Left = 113
        Top = 102
        Width = 28
        Height = 13
        Caption = 'Port : '
        FocusControl = SpinEditPort
      end
      object LabeledEditServeurNom: TLabeledEdit
        Left = 8
        Top = 64
        Width = 257
        Height = 21
        EditLabel.Width = 81
        EditLabel.Height = 13
        EditLabel.Caption = 'Nom du serveur :'
        TabOrder = 1
        Text = 'localhost'
      end
      object LabeledEditServeurAdresse: TLabeledEdit
        Left = 8
        Top = 24
        Width = 257
        Height = 21
        EditLabel.Width = 44
        EditLabel.Height = 13
        EditLabel.Caption = 'Adresse :'
        TabOrder = 0
        Text = 'localhost'
        OnChange = LabeledEditServeurAdresseChange
      end
      object LabeledEditUtilisateur: TLabeledEdit
        Left = 8
        Top = 160
        Width = 257
        Height = 21
        EditLabel.Width = 52
        EditLabel.Height = 13
        EditLabel.Caption = 'Utilisateur :'
        TabOrder = 2
      end
      object LabeledEditMotPasse: TLabeledEdit
        Left = 8
        Top = 200
        Width = 257
        Height = 21
        EditLabel.Width = 70
        EditLabel.Height = 13
        EditLabel.Caption = 'Mot de passe :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
        PasswordChar = #183
        TabOrder = 3
      end
      object CheckBoxDemarrageActif: TCheckBox
        Left = 8
        Top = 232
        Width = 257
        Height = 17
        Caption = 'Actif au d'#233'marrage'
        TabOrder = 4
      end
      object SpinEditPort: TSpinEdit
        Left = 144
        Top = 96
        Width = 121
        Height = 22
        MaxValue = 15000
        MinValue = 4000
        TabOrder = 5
        Value = 4451
      end
      object CheckBoxSyncAvanceActive: TCheckBox
        Left = 8
        Top = 260
        Width = 257
        Height = 17
        Caption = 'Synchronisation avanc'#233'.'
        TabOrder = 6
      end
    end
  end
  object ButtonAnnuler: TButton
    Left = 229
    Top = 327
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Annuler'
    ModalResult = 2
    TabOrder = 2
  end
  object ButtonOK: TButton
    Left = 148
    Top = 327
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
end
