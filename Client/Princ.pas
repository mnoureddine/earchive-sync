unit Princ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, ToolWin, ActnList,EACDonnees, StdCtrls,ArchivesCloudClient,
  Menus, IdComponent,ArchivesCloudCommun, ImgList,ShellAPI,FileCtrl,EACConfiguration,
  IdTCPServer, IdBaseComponent, IdTCPConnection, IdTCPClient,EACConnexion,Utiles,
  EACArchiveCloudProgress, JvComponentBase, JvTrayIcon,EACEditServeur,
  IdAntiFreezeBase, IdAntiFreeze;

type
  TACCOperationMode = (ACOpModeAucun,ACOpModeCopierFichier,ACOpModeCouperFichier,ACOpModeCopierDossier,ACOpModeCouperDossier);

  TFormEACPrinc = class(TForm)
    TreeViewServeurs: TTreeView;
    ListViewFichiers: TListView;
    StatusBar1: TStatusBar;
    Splitter1: TSplitter;
    ActionList1: TActionList;
    ActionServeurConnection: TAction;
    Panel1: TPanel;
    ListBoxMessages: TListBox;
    PopupMenuServeurs: TPopupMenu;
    ActionAjouterServeur: TAction;
    ActionServeurEnvoieMessage: TAction;
    OpenDialog1: TOpenDialog;
    Splitter2: TSplitter;
    ActionDossierAjouterSynchroniser: TAction;
    ImageList2: TImageList;
    ActionDossierCopier: TAction;
    ActionDossierCouper: TAction;
    ActionDossierColler: TAction;
    ActionDossierSupprimer: TAction;
    ActionDossierNouveauDossier: TAction;
    ActionFichierAjouterSynchroniser: TAction;
    ActionFichierCopier: TAction;
    ActionFichierCouper: TAction;
    ActionFichierSupprimer: TAction;
    ActionDossierEnvoyer: TAction;
    ActionFichierSyncRetirer: TAction;
    ActionFichierSyncSynchroniser: TAction;
    PopupMenuFichiers: TPopupMenu;
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    ActionFichierColler: TAction;
    ActionFichierEnvoyer: TAction;
    ActionFichierOuvrir: TAction;
    ActionFichierOuvrirAvec: TAction;
    ActionFichierOuvrirDossier: TAction;
    ActionFichierTelecharger: TAction;
    ActionDossierOuvrir: TAction;
    ActionDossierTelecharger: TAction;
    ActionDossierEnvoyerFichier: TAction;
    ActionDossierRenommer: TAction;
    ActionFichierRenommer: TAction;
    ActionFichierMettreAJour: TAction;
    ActionDossierMettreAJour: TAction;
    CoolBar1: TCoolBar;
    EditChemin: TEdit;
    GroupBox1: TGroupBox;
    ActionConfiguration: TAction;
    ActionSynchronisationAuto: TAction;
    ActionServeurSupprimer: TAction;
    ActionFichierNouveauDossier: TAction;
    ActionFichierSepare1: TAction;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    JvTrayIcon1: TJvTrayIcon;
    MainMenu1: TMainMenu;
    Poste1: TMenuItem;
    Ajouterunserveur1: TMenuItem;
    Configuration1: TMenuItem;
    Mettrejour1: TMenuItem;
    N1: TMenuItem;
    Quitter1: TMenuItem;
    PopupMenuTrayIcon: TPopupMenu;
    Ouvrir1: TMenuItem;
    Ajouterunserveur2: TMenuItem;
    Configuration2: TMenuItem;
    Miseajourauto1: TMenuItem;
    N2: TMenuItem;
    Quitter2: TMenuItem;
    ActionQuitter: TAction;
    ActionPosteSynchroniser: TAction;
    ActionServeurSynchroniser: TAction;
    ActionDossierFichierExplorer: TAction;
    ImageList1: TImageList;
    ActionServeurPropriete: TAction;
    ActionFichierSyncOuvrir: TAction;
    ActionFichierSyncOuvrirAvec: TAction;
    ActionOuvrirFichierRecent: TAction;
    IdAntiFreeze1: TIdAntiFreeze;
    PopupMenuJournal: TPopupMenu;
    Effacertout1: TMenuItem;
    ActionActualiser: TAction;
    Actualiser1: TMenuItem;
    ActionDossierFichierRenommer: TAction;
    ActionDossierFichierSupprimer: TAction;
    ActionDossierFichierCopier: TAction;
    ActionDossierFichierCouper: TAction;
    ActionDossierFichierColler: TAction;
    ActionDossierFichierTelecharger: TAction;
    ActionDossierFichierAjouterSynchroniser: TAction;
    ActionDossierFichierMettreAJour: TAction;
    Action4: TAction;
    Action5: TAction;
    ActionDossierFichierOuvrir: TAction;
    ActionFichierSynchroniserLire: TAction;
    ActionFichierSynchroniserEcrire: TAction;
    procedure ActionServeurConnectionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActionServeurEnvoieMessageExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TreeViewServeursDblClick(Sender: TObject);
    procedure ActionFichierAjouterSynchroniserExecute(Sender: TObject);
    procedure ActionFichierCopierExecute(Sender: TObject);
    procedure ActionFichierCouperExecute(Sender: TObject);
    procedure ActionFichierSupprimerExecute(Sender: TObject);
    procedure ActionDossierAjouterSynchroniserExecute(Sender: TObject);
    procedure ActionDossierCopierExecute(Sender: TObject);
    procedure ActionDossierCouperExecute(Sender: TObject);
    procedure ActionDossierCollerExecute(Sender: TObject);
    procedure ActionDossierSupprimerExecute(Sender: TObject);
    procedure ActionDossierNouveauDossierExecute(Sender: TObject);
    procedure ServeurWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure ServeurWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure ServeurWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
    procedure ActionFichierSyncSynchroniserExecute(Sender: TObject);
    procedure ActionFichierSyncRetirerExecute(Sender: TObject);
    procedure ActionFichierCollerExecute(Sender: TObject);
    procedure ActionFichierEnvoyerExecute(Sender: TObject);
    procedure ActionFichierOuvrirExecute(Sender: TObject);
    procedure ActionFichierOuvrirAvecExecute(Sender: TObject);
    procedure ActionFichierOuvrirDossierExecute(Sender: TObject);
    procedure ActionFichierTelechargerExecute(Sender: TObject);
    procedure ActionDossierTelechargerExecute(Sender: TObject);
    procedure ActionDossierEnvoyerExecute(Sender: TObject);
    procedure ActionDossierOuvrirExecute(Sender: TObject);
    procedure ListViewFichiersDblClick(Sender: TObject);
    procedure ActionFichierRenommerExecute(Sender: TObject);
    procedure ActionDossierRenommerExecute(Sender: TObject);
    procedure ActionFichierMettreAJourExecute(Sender: TObject);
    procedure ActionDossierMettreAJourExecute(Sender: TObject);
    procedure ActionAjouterServeurExecute(Sender: TObject);
    procedure ActionConfigurationExecute(Sender: TObject);
    procedure ActionSynchronisationAutoExecute(Sender: TObject);
    procedure ActionServeurSupprimerExecute(Sender: TObject);
    procedure TreeViewServeursChange(Sender: TObject; Node: TTreeNode);
    procedure ActionFichierNouveauDossierExecute(Sender: TObject);
    procedure ActionQuitterExecute(Sender: TObject);
    procedure ActionPosteSynchroniserExecute(Sender: TObject);
    procedure ActionServeurSynchroniserExecute(Sender: TObject);
    procedure Ouvrir1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure JvTrayIcon1BalloonHide(Sender: TObject);
    procedure PopupMenuFichiersPopup(Sender: TObject);
    procedure ActionDossierFichierExplorerExecute(Sender: TObject);
    procedure JvTrayIcon1DblClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ActionServeurProprieteExecute(Sender: TObject);
    procedure ActionFichierSyncOuvrirExecute(Sender: TObject);
    procedure ActionFichierSyncOuvrirAvecExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Effacertout1Click(Sender: TObject);
    procedure ActionActualiserExecute(Sender: TObject);
    procedure ActionDossierFichierOuvrirExecute(Sender: TObject);
    procedure ActionDossierFichierTelechargerExecute(Sender: TObject);
    procedure ActionDossierFichierAjouterSynchroniserExecute(
      Sender: TObject);
    procedure ActionDossierFichierMettreAJourExecute(Sender: TObject);
    procedure ActionDossierFichierSupprimerExecute(Sender: TObject);
    procedure ActionDossierFichierRenommerExecute(Sender: TObject);
    procedure ActionDossierFichierCouperExecute(Sender: TObject);
    procedure ActionDossierFichierCopierExecute(Sender: TObject);
    procedure ActionDossierFichierCollerExecute(Sender: TObject);
    procedure ActionFichierSynchroniserLireExecute(Sender: TObject);
    procedure ActionFichierSynchroniserEcrireExecute(Sender: TObject);
  private
    function DossierFichierSelectionne: TACCDossier;
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    BalloonMessages : TStringList;
    NodePoste : TTreeNode;
    FichierPressePapier : string;
    DossierPressePapier : string;
    FichiersRecent : TStringList;
    DossiersRecent : TStringList;
    FichiersSyncRecent : TStringList;
    OperatioMode : TACCOperationMode;
    Demarrage : boolean;
    function ConnectionToStr(Connected : boolean): string;
    procedure ActualiserVue;
    procedure Actualiser;
    procedure ActualiserNoeud(Node : TTReeNode);
    procedure ActualiserPoste;
    procedure ActualiserServeur(NodeServeur: TTreeNode);
    procedure ActualiserMenuServeur(Categorie : string);
    procedure ActualiserMenuFichier(Categorie : string);
    procedure ActualiserDossiersPartages(NodeDossiersPartage : TTreeNode);
    procedure ActualiserDossier(NodeDossier : TTreeNode);
    procedure ActualiserFichiersSync(NodeFichiersSync : TTreeNode);
    procedure ServeurConnected(Sender : TObject);
    procedure ServeurDisconnected(Sender : TObject);
    procedure ServeurDonneesChange(Sender : TObject);
    procedure PosteMessage(Sender : TObject;pMessageType:TACMessageType;pMessage : string);
    procedure PosteServeurAjouter(Sender:TObject;pServeur : TACCServeur);
    procedure PosteServeurStatus(ASender: TObject;
      const AStatus: TIdStatus; const AStatusText: String);
    procedure ServeurDossierPartageLister(Sender:TObject;pServeur : TACCServeur);
    procedure PosteServeurDossierLister(Sender:TObject;pServeur : TACCServeur;pDossier : TACCDossier);
    procedure ServeurAvancement(Sender : TObject;Libelle : string;Max : integer;Position : integer);
    function FichierSyncSelectionne : TACCFichierSync;
    function FichierSelectionne : TACCFichier;
    function DossierSelectionne : TACCDossier;
    function FichierDossierSelectionne : TACCDossier;
    function ServeurSelectionne : TACCServeur;
    procedure ChargerFichiersRecents;
    procedure EnregistrerFichiersRecents;

  end;

var
  FormEACPrinc: TFormEACPrinc;
const
  FichiersRecentNom = 'FichiersRecent.txt';
  DossiersRecentNom = 'DossiersRecent.txt';
  FichiersSyncRecentNom = 'FichiersSyncRecent.txt';


implementation

uses Math;

{$R *.dfm}

{ TFormEACPrinc }

procedure TFormEACPrinc.Actualiser;
var
  i : integer;

begin
  TreeViewServeurs.Items.Clear;
  NodePoste := TreeViewServeurs.Items.AddChild(nil,'Poste de travail');
  NodePoste.Data := DonneesEAC.Poste;
  NodePoste.ImageIndex := 0;
  NodePoste.SelectedIndex := 0;
  NodePoste.StateIndex := 0;
  ActualiserNoeud(NodePoste);
  NodePoste.Expand(false);
end;

procedure TFormEACPrinc.ActionServeurConnectionExecute(Sender: TObject);
var
  Serveur : TACCServeur;
  FormEACConnexion: TFormEACConnexion;
begin
  Serveur := ServeurSelectionne;
  if Serveur <> nil then
  begin
    if not Serveur.Connected then
    begin
      if Serveur.ConnexionAuto then
      begin
        if not Serveur.Connexion then
          Serveur.ConnexionAuto := false;
      end
      else
      begin
        with TFormEACConnexion.Create(self) do
        begin
          if Connexion then
          begin
            if Serveur.Connexion(Utilisateur,MotPasse) then
            begin
              Serveur.Utilisateur := Utilisateur;
              Serveur.MotPasse := MotPasse;
              Serveur.ConnexionAuto := ConnexionAuto;
              DonneesEAC.EnregistrerListeDesServeur;
              if not Serveur.RequeteDossierPartageLister then
                MessageDlg('Erreur lors de la liste de dossier partag�',mtError,[mbOk],0);
              ActualiserVue;
            end;
          end;
          free;
        end;
      end;
    end
    else
      Serveur.Deconnexion;
  end;
end;

procedure TFormEACPrinc.FormCreate(Sender: TObject);
var
  i : Integer;
  
begin
  Caption  := Application.Title +' V'+GetApplicationVersionString
    +' Protocole V'+ACProtocoleToString(PROTOCOLE_VERSION);
  JvTrayIcon1.Hint := Caption;
  BalloonMessages := TStringList.Create;
  DonneesEAC.Poste.OnMessage := PosteMessage;
  DonneesEAC.Poste.OnServeurAjouter := PosteServeurAjouter;
  DonneesEAC.Poste.OnServeurDossierLister := PosteServeurDossierLister;
  DonneesEAC.Poste.OnServeurDossierPartageLister := ServeurDossierPartageLister;
  DonneesEAC.ChargerListeDesServeur;
  Actualiser;

  DonneesEAC.Poste.SynchronisationActive :=  DonneesEAC.Configuration.General.AutoMAJActive;
  DonneesEAC.TimerSynchronisation.Enabled :=  DonneesEAC.Configuration.General.AutoMAJActive;
  ActionSynchronisationAuto.Checked := DonneesEAC.TimerSynchronisation.Enabled;

  FichiersRecent := TStringList.Create;
  DossiersRecent := TStringList.Create;
  FichiersSyncRecent := TStringList.Create;
  Demarrage := true;

  (*
  if DonneesEAC.Configuration.Demarrage.ConnexionAuto then
    for i := 0 to DonneesEAC.Poste.Serveurs.Count - 1 do
      DonneesEAC.Poste.Serveurs[i].Connection;
    *)

end;

procedure TFormEACPrinc.ServeurConnected(Sender: TObject);
begin
  ActionServeurConnection.Caption := ConnectionToStr(true);
  if TObject(Sender) is TACCServeur then
    if TObject(TACCServeur(Sender).Data) is TTreeNode then
      ActualiserServeur(TTreeNode(TACCServeur(Sender).Data));


end;

procedure TFormEACPrinc.ServeurDisconnected(Sender: TObject);
begin
  ActionServeurConnection.Caption := ConnectionToStr(false);
  if TObject(Sender) is TACCServeur then
    if TObject(TACCServeur(Sender).Data) is TTreeNode then
      ActualiserServeur(TTreeNode(TACCServeur(Sender).Data));

end;

procedure TFormEACPrinc.PosteMessage(Sender: TObject; pMessageType:TACMessageType; pMessage: string);
begin

  if DonneesEAC.Configuration.General.AfficherMessageInfo or (pMessageType <> ACMessageInfo) then
    ListBoxMessages.Items.Insert(0,ACMessageTypeVersStr(pMessageType)+#9+pMessage);
    Refresh;
end;

procedure TFormEACPrinc.ActualiserNoeud(Node: TTReeNode);
var
  Serveur : TACCServeur;
  Dossier : TACCDossier;
  Dossiers : TACCDossierList;
  FichiersSync : TACCFichierSyncList;

begin
  if Node = nil then
      exit;

  ActionServeurConnection.Enabled := TObject(Node.Data) is TACCServeur;

  if TObject(Node.Data) is TACCPoste then
  begin
    ActualiserPoste;
    exit;
  end;

  if TObject(Node.Data) is TACCServeur then
  begin

    ActualiserServeur(Node);
    EditChemin.Text := '\\'+TACCServeur(Node.Data).Nom;
    exit;
  end;

  if TObject(Node.Data) is TACCDossierList then
  begin
    Dossiers := TACCDossierList(Node.Data);
    if Dossiers.Serveur.Connected then
      if Dossiers.Serveur.RequeteDossierPartageLister then
      begin
        ActualiserDossiersPartages(Node);
        EditChemin.Text := '\\'+Dossiers.Serveur.Nom+'\Fichier Partag�es';
      end
      else
        MessageDlg('Erreur lors de la liste de dossier partag�',mtError,[mbOk],0);


      exit;
  end;


  if TObject(Node.Data) is TACCDossier then
  begin
    Dossier := TACCDossier(Node.Data);
    if Dossier.Serveur.Connected then
      if Dossier.RequeteLister then
      begin
        ActualiserDossier(Node);
        EditChemin.Text := '\\'+Dossier.Serveur.Nom+'\'+Dossier.Chemin;
      end
      else
        MessageDlg('Erreur lors de la liste du dossier',mtError,[mbOk],0);
    exit;

  end;

  if TObject(Node.Data) is TACCFichierSyncList then
  begin
    ActualiserFichiersSync(Node);
    exit;
    EditChemin.Text := '\\'+TACCFichierSyncList(Node.Data).Serveur.Nom+'\Fichiers synchronis�es';

  end;

end;

procedure TFormEACPrinc.ActualiserServeur(NodeServeur: TTreeNode);
var
  Serveur: TACCServeur;
  i : Integer;
  Dossier : TACCDossier;
  Node : TTreeNode;

begin
  Serveur := TACCServeur(NodeServeur.data);
  if not Serveur.Connected then
  begin
      NodeServeur.ImageIndex := 1;
      NodeServeur.SelectedIndex := 1;
      NodeServeur.StateIndex := 1;
  end
  else
  begin
      NodeServeur.ImageIndex := 2;
      NodeServeur.SelectedIndex := 2;
      NodeServeur.StateIndex := 2;
  end;


  ActionServeurConnection.Caption := ConnectionToStr(Serveur.Connected);

  ActionServeurEnvoieMessage.Enabled := Serveur.Connected;
  ActualiserMenuServeur('Serveur');


  NodeServeur.DeleteChildren;

  Node := TreeViewServeurs.Items.AddChildFirst(
                NodeServeur,'Synchronis�es');
  Node.Data := Serveur.FichiersSync;
  Node.ImageIndex := 7;
  Node.SelectedIndex := 7;
  Node.StateIndex := 7;
  Serveur.FichiersSync.Data := Node;

  Node := TreeViewServeurs.Items.AddChildFirst(
                NodeServeur,'Partag�es');
  Node.Data := Serveur.Dossiers;
  Node.ImageIndex := 3;
  Node.SelectedIndex := 4;
  Node.StateIndex := 3;
  Serveur.Dossiers.Data := Node;




  NodeServeur.Expand(false);



end;

procedure TFormEACPrinc.ActualiserMenuServeur(Categorie: string);
var
  i : integer;
  lMenu :  TMenuItem;
begin
  PopupMenuServeurs.Items.Clear;
  for i := 0 to ActionList1.ActionCount - 1 do
    if SameText(Categorie,ActionList1.Actions[i].Category) then
    begin
        lMenu := TMenuItem.Create(self);
        if SameText(TAction(ActionList1.Actions[i]).Caption,'-')then
          lMenu.Caption := '-'
        else
          lMenu.Action := ActionList1.Actions[i];

        PopupMenuServeurs.Items.Add(lMenu);
    end;
end;

procedure TFormEACPrinc.ActualiserPoste;
var
  i : integer;
  Node : TTreeNode;
begin
  NodePoste.DeleteChildren;
  for i := 0 to DonneesEAC.Poste.Serveurs.Count -1 do
  begin
    Node := TreeViewServeurs.Items.AddChild(NodePoste,DonneesEAC.Poste.Serveurs[i].Nom);
    Node.Data := DonneesEAC.Poste.Serveurs[i];
    if not DonneesEAC.Poste.Serveurs[i].Connected then
    begin
      Node.ImageIndex := 1;
      Node.SelectedIndex := 1;
      Node.StateIndex := 1;
    end
    else
    begin
      Node.ImageIndex := 2;
      Node.SelectedIndex := 2;
      Node.StateIndex := 2;
    end;
    DonneesEAC.Poste.Serveurs[i].Data := Node;
  end;
  NodePoste.Expand(false);
  ActualiserMenuServeur('Poste');

  ActionSynchronisationAuto.Checked := DonneesEAC.TimerSynchronisation.Enabled;
end;

procedure TFormEACPrinc.PosteServeurAjouter(Sender: TObject;
  pServeur: TACCServeur);
begin
  pServeur.OnConnected := ServeurConnected;
  pServeur.OnDisconnected := ServeurDisconnected;
  pServeur.OnStatus := PosteServeurStatus;
  pServeur.OnWorkBegin := ServeurWorkBegin;
  pServeur.OnWork := ServeurWork;
  pServeur.OnWorkEnd := ServeurWorkEnd;
//  pServeur.OnDonneesChange := ServeurDonneesChange;
  pServeur.OnAvancement := ServeurAvancement;
 


end;

function TFormEACPrinc.ConnectionToStr(Connected: boolean): string;
begin
  if Connected then
    result := 'Deconnecter'
  else
    result := 'Connecter';

end;

procedure TFormEACPrinc.PosteServeurStatus(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: String);
begin
  if TObject(ASender) is TACCServeur then
    DonneesEAC.Poste.EcrireMessage(TACCServeur(ASender),ACMessageInfo,AStatusText);
//      procedure EcrireMessage(pServeur:TACCServeur;pMessage : string);overload;
//  ListBoxMessages.Items.Insert(0,DateTimeToStr(now)+#9+AStatusText);
end;

procedure TFormEACPrinc.ActionServeurEnvoieMessageExecute(Sender: TObject);
var
  Serveur : TACCServeur;
  lMessage: string;
begin
  Serveur := ServeurSelectionne;
  if Serveur <> nil then
  begin
        lMessage := InputBox(
                        'Boite des saisie de message',
                        'Saisissez un message a envoyer au serveur',
                        'Message');

        if not Serveur.RequeteMessage(lMessage) then
          MessageDlg('Erreur lors de l''envoie du message',mtError,[mbOk],0);
  end;

end;



procedure TFormEACPrinc.PosteServeurDossierLister(Sender: TObject;
  pServeur: TACCServeur; pDossier: TACCDossier);
begin
  if TObject(pDossier.Data) is TTreeNode then
      ActualiserDossier(TTreeNode(pDossier.Data));
end;


procedure TFormEACPrinc.ActualiserDossier(NodeDossier: TTreeNode);
var
  Dossier : TACCDossier;
  i : Integer;
  Node : TTreeNode;
  ListItem : TListItem;
  Column : TListColumn;
begin
  Dossier := TACCDossier(NodeDossier.Data);

  NodeDossier.DeleteChildren;
  for i := 0 to Dossier.Dossiers.Count - 1 do
  begin
    Node := TreeViewServeurs.Items.AddChildFirst(
                NodeDossier,Dossier.Dossiers[i].Nom);
    Node.Data := Dossier.Dossiers[i];

    if Dossier.Dossiers[i].DossierSynchronise = nil then
    begin
      if not Dossier.Dossiers[i].Exist then
      begin
        Node.ImageIndex := 5;
        Node.SelectedIndex := 6;
        Node.StateIndex := 5;
      end
      else
      begin
        Node.ImageIndex := 15;
        Node.SelectedIndex := 16;
        Node.StateIndex := 15;
      end;

    end
    else
    begin

      Node.ImageIndex := 7;
      Node.SelectedIndex := 7;
      Node.StateIndex := 7;
    end;
    Dossier.Dossiers[i].Data := Node;
  end;

  //NodeDossier.Expand(false);
  NodeDossier.AlphaSort(false);
  ListViewFichiers.Columns.Clear;
  Column := ListViewFichiers.Columns.Add;
  Column.Caption := 'Nom';
  Column.Width := 255;

  Column := ListViewFichiers.Columns.Add;
  Column.Caption := 'Taille';
  Column.Width := 100;
  Column.Alignment := taRightJustify;


  Column := ListViewFichiers.Columns.Add;
  Column.Caption := 'Date de modification';
  Column.Width := 150;



  ListViewFichiers.Items.Clear;
  for i := 0 to Dossier.Dossiers.Count -1 do
  begin
    ListItem := ListViewFichiers.Items.Add;
    ListItem.Caption := Dossier.Dossiers[i].Nom;
    ListItem.SubItems.Add('--');
    ListItem.SubItems.Add(DateTimeToStr(Dossier.Dossiers[i].DateModification));
    ListItem.Data := Dossier.Dossiers[i];
    if Dossier.Dossiers[i].DossierSynchronise = nil then
    begin
      if not Dossier.Dossiers[i].Exist then
      begin
        ListItem.ImageIndex := 5;
      end
      else
      begin
        ListItem.ImageIndex := 15;
      end
    end
    else
    begin
      ListItem.ImageIndex := 7;
    end;
  end;

  for i := 0 to Dossier.Fichiers.Count -1 do
  begin
    ListItem := ListViewFichiers.Items.Add;
    ListItem.Caption := Dossier.Fichiers[i].Nom;
    ListItem.SubItems.Add(SizeFileToStr(Dossier.Fichiers[i].Taille));
    ListItem.SubItems.Add(DateTimeToStr(Dossier.Fichiers[i].DateModification));
    ListItem.Data := Dossier.Fichiers[i];
    if Dossier.Fichiers[i].FichierSynchronise = nil then
    begin
      if not Dossier.Fichiers[i].Exist then
      begin
        ListItem.ImageIndex := 8;
      end
      else
      begin
        if Dossier.Fichiers[i].Ajour then
        begin
          ListItem.ImageIndex := 14;
        end
        else
        begin
          ListItem.ImageIndex := 17;

        end;
      end
    end
    else
    begin
      ListItem.ImageIndex := 9;
    end;
    Dossier.Fichiers[i].Data := ListItem.Data
  end;
  ActualiserMenuServeur('Dossier');
  ActualiserMenuFichier('Fichier');


end;


procedure TFormEACPrinc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  JvTrayIcon1.HideApplication;
  Action := caNone;

end;

procedure TFormEACPrinc.ServeurDossierPartageLister(Sender: TObject;
  pServeur: TACCServeur);
begin
  if TObject(pServeur.Dossiers.Data) is TTreeNode then
    ActualiserDossiersPartages(TTreeNode(pServeur.Dossiers.Data));
end;

procedure TFormEACPrinc.TreeViewServeursDblClick(Sender: TObject);
begin
  if TreeViewServeurs.Selected <> nil then
  begin
    if TObject(TreeViewServeurs.Selected.Data) is TACCDossier then
      ActionDossierOuvrirExecute(Sender);
    if TObject(TreeViewServeurs.Selected.Data) is TACCServeur then
      ActionServeurProprieteExecute(Sender);
  end;
end;

procedure TFormEACPrinc.ActualiserDossiersPartages(
  NodeDossiersPartage: TTreeNode);
var
  i : Integer;
  DossiersPartages : TACCDossierList;
  Node : TTreeNode;
begin
  DossiersPartages := TACCDossierList(NodeDossiersPartage.Data);
  NodeDossiersPartage.DeleteChildren;
  for i := 0 to DossiersPartages.Count - 1 do
  begin
    Node := TreeViewServeurs.Items.AddChildFirst(
                NodeDossiersPartage,DossiersPartages[i].Nom);
    Node.Data := DossiersPartages[i];

    Node.ImageIndex := 3;
    Node.SelectedIndex := 4;
    Node.StateIndex := 3;
    DossiersPartages[i].Data := Node;
  end;
//  NodeDossiersPartage.Expand(false);

end;

procedure TFormEACPrinc.ActualiserFichiersSync(NodeFichiersSync : TTreeNode);
var
  FichiersSync : TACCFichierSyncList;
  i : Integer;
  ListItem : TListItem;
  Column : TListColumn;
begin

  FichiersSync := TACCFichierSyncList(NodeFichiersSync.Data);

  ListViewFichiers.Columns.Clear;
  (*
  Column := ListViewFichiers.Columns.Add;
  Column.Caption := 'Nom';
  Column.Width := 255;

  Column := ListViewFichiers.Columns.Add;
  Column.Caption := 'Date de modification';
  Column.Width := 150;
    *)
  Column := ListViewFichiers.Columns.Add;
  Column.Caption := 'Chemin';
  Column.Width := 500;



  ListViewFichiers.Items.Clear;
  for i := 0 to FichiersSync.Count -1 do
  begin
    ListItem := ListViewFichiers.Items.Add;
    ListItem.Caption := FichiersSync[i].Chemin;
    (*
    ListItem.Caption := FichiersSync[i].Nom;
    ListItem.SubItems.Add(DateTimeToStr(FichiersSync[i].Info.Date.Modification));
    ListItem.SubItems.Add(FichiersSync[i].Chemin);
    *)
    ListItem.Data := FichiersSync[i];
    if FichiersSync[i].Info.TypeDossier then
    begin
      ListItem.ImageIndex := 7;
      ListItem.StateIndex := 7;
    end
    else
    begin
      ListItem.ImageIndex := 9;
      ListItem.StateIndex := 9;
    end;
    FichiersSync[i].Data := ListItem;
    
  end;

  ActualiserMenuFichier('FichierSync');
end;

procedure TFormEACPrinc.ActualiserMenuFichier(Categorie: string);
var
  i : integer;
  lMenu :  TMenuItem;
begin
  PopupMenuFichiers.Items.Clear;
  for i := 0 to ActionList1.ActionCount - 1 do
    if SameText(Categorie,ActionList1.Actions[i].Category) then
    begin
        lMenu := TMenuItem.Create(self);
        if SameText(TAction(ActionList1.Actions[i]).Caption,'-')then
          lMenu.Caption := '-'
        else
          lMenu.Action := ActionList1.Actions[i];
        PopupMenuFichiers.Items.Add(lMenu);
    end;
end;

procedure TFormEACPrinc.ActionFichierAjouterSynchroniserExecute(Sender: TObject);
var
  Fichier  : TACCFichier;
  FormEACProgress : TFormEACProgress;
  FichierSync : TACCFichierSync;
begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
    FichierSync := Fichier.AjouterEnSynchronisation;
    DonneesEAC.EnregistrerFichierSync;
    if FichierSync <> nil then
    begin
      if MessageDlg('Souhaitez-vous t�l�charger le fichier "'+FichierSync.Nom+'" maintenant ?'
          ,mtConfirmation,[mbYes,mbNo],0)=mrYes then
      begin
        FormEACProgress := TFormEACProgress.Create(Self);
        FormEACProgress.Ouvrir(FichierSync.Serveur,'T�l�chargement du fichier "'+Fichier.Nom+'"');
        FichierSync.RequeteLire;
        ActualiserVue;
        FormEACProgress.Fermer;
        FormEACProgress.Free;
      end;
    end;  
  end;
end;

procedure TFormEACPrinc.ActionFichierCopierExecute(Sender: TObject);
var
  Fichier  : TACCFichier;
begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
      FichierPressePapier := Fichier.Chemin;
      OperatioMode := ACOpModeCopierFichier;
  end;

end;

procedure TFormEACPrinc.ActionFichierCouperExecute(Sender: TObject);
var
  Fichier  : TACCFichier;
begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
      FichierPressePapier := Fichier.Chemin;
      OperatioMode := ACOpModeCouperFichier;
  end;

end;

procedure TFormEACPrinc.ActionFichierSupprimerExecute(Sender: TObject);
var
  Fichier  : TACCFichier;
begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
      if MessageDlg(
          'Etes-vous s�r de vouloir supprimer le fichier "'+Fichier.Nom+'" du serveur',
          mtWarning,[mbYes,MbNo],0)= mrYes then
      begin
        if not Fichier.RequeteSupprimer then
          MessageDlg('Erreur lors de la supression du fichier',mtError,[mbOk],0);
        ActualiserVue;
      end;


end;

procedure TFormEACPrinc.ActionDossierAjouterSynchroniserExecute(Sender: TObject);
var
  Dossier : TACCDossier;
  FichierSync : TACCFichierSync;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      FichierSync := Dossier.AjouterEnSynchronisation;
      if FichierSync <> nil then
      begin
        if MessageDlg('Souhaitez-vous t�l�charger le dossier "'+FichierSync.Chemin+'" maintenant ?'
          ,mtConfirmation,[mbYes,mbNo],0)=mrYes then
        begin
          with TFormEACProgress.Create(self)do
          begin
            Ouvrir(Dossier.Serveur,'Mise en synchronisation du dosser '+Dossier.Nom);
            DonneesEAC.EnregistrerFichierSync;
            ActualiserVue;
            Fermer;
            free;
          end;
        end;
      end;
    end;

end;

procedure TFormEACPrinc.ActionDossierCopierExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      DossierPressePapier := Dossier.Chemin;
      OperatioMode := ACOpModeCopierDossier;

  end;

end;

procedure TFormEACPrinc.ActionDossierCouperExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      DossierPressePapier := Dossier.Chemin;
      OperatioMode := ACOpModeCouperDossier;
      
  end;

end;


procedure TFormEACPrinc.ActionDossierCollerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
  FormEACProgress : TFormEACProgress;
  Serveur : TACCServeur;
begin
  Dossier := DossierSelectionne;

  if Dossier <> nil then
  begin
      Serveur := Dossier.Serveur;
      FormEACProgress := TFormEACProgress.Create(self);
      Case OperatioMode of
      ACOpModeAucun : ;
      ACOpModeCopierFichier :
      begin
          FormEACProgress.Ouvrir(Serveur,'Copie du fichier en cours');
          if not  Serveur.RequeteFichierCopier(FichierPressePapier,Dossier.Chemin) then
                MessageDlg('Erreur lors de la copie du fichier',mtError,[mbOk],0);
      end;
      ACOpModeCouperFichier :
      begin
          FormEACProgress.Ouvrir(Serveur,'D�placement du fichier en cours');
          if not Serveur.RequeteFichierDeplacer(FichierPressePapier,Dossier.Chemin) then
            MessageDlg('Erreur lors du d�placement du fichier',mtError,[mbOk],0);
      end;
      ACOpModeCopierDossier :
      begin
        FormEACProgress.Ouvrir(Serveur,'Copie du dossier en cours');
        if not Serveur.RequeteDossierCopier(DossierPressePapier,Dossier.Chemin) then
          MessageDlg('Erreur lors de la copie du dossier',mtError,[mbOk],0);
      end;
      ACOpModeCouperDossier :
      begin
        FormEACProgress.Ouvrir(Serveur,'D�placement du dossier en cours');
        if not Serveur.RequeteDossierDeplacer(DossierPressePapier,Dossier.Chemin) then
          MessageDlg('Erreur lors du d�placement du dossier',mtError,[mbOk],0);
      end;

      end;
      ActualiserVue;
      FormEACProgress.Fermer;
      FormEACProgress.Free;
  end;

end;

procedure TFormEACPrinc.ActionDossierSupprimerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  if not TreeViewServeurs.Focused then
    exit;
  Dossier := DossierSelectionne;
  if Dossier <> nil then
    if MessageDlg(
          'Etes-vous s�r de vouloir supprimer le dossier "'+Dossier.Nom+'" du serveur',
          mtWarning,[mbYes,MbNo],0)= mrYes then
    begin
      with TFormEACProgress.Create(Self) do
      begin
        Ouvrir(Dossier.Serveur,'Suppression du dossier "'+Dossier.Nom+'" en cours');
        if not Dossier.RequeteSupprimer then
          MessageDlg('Erreur lors de la suppresion de dossier',mtError,[mbOk],0)
        else
          if TreeViewServeurs.Selected <> nil then
            if TreeViewServeurs.Selected.Parent <> nil then
              ActualiserNoeud(TreeViewServeurs.Selected.Parent);

        Fermer;
        Free;
      end;

    end;

end;
procedure TFormEACPrinc.ActionDossierNouveauDossierExecute(Sender: TObject);
var
  NouveauDossier : string;
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      NouveauDossier := 'Nouveau dossier';
      if InputQuery('Boite de cr�ation de dossier','Nom du dossier',NouveauDossier) then
      begin
        if not Dossier.RequeteCreer(NouveauDossier) then
          MessageDlg('Erreur lors de la creation du dossier',mtError,[mbOk],0);
        ActualiserVue;
      end;

  end;

end;

procedure TFormEACPrinc.ServeurWork(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  ProgressBar1.Position := AWorkCount;
end;

procedure TFormEACPrinc.ServeurWorkBegin(Sender: TObject;
  AWorkMode: TWorkMode; const AWorkCountMax: Integer);
begin
  ProgressBar1.Position := 0;
  ProgressBar1.Max := AWorkCountMax;
end;

procedure TFormEACPrinc.ServeurWorkEnd(Sender: TObject;
  AWorkMode: TWorkMode);
begin
  ProgressBar1.Position := 0;

end;

procedure TFormEACPrinc.ActionFichierSyncSynchroniserExecute(
  Sender: TObject);
var
  FichierSync : TACCFichierSync;
begin
  FichierSync := FichierSyncSelectionne;
  if FichierSync <> nil then
  begin
    with TFormEACProgress.Create(self) do
    begin
      Ouvrir(FichierSync.Serveur,'Synchronisation des donn�es');
      if not FichierSync.RequeteMettreAJour then
        MessageDlg('Erreur lors de la mise � jour du fichier',mtError,[mbOk],0);
      Fermer;
      Free;
    end;
  end;

end;

procedure TFormEACPrinc.ActionFichierSyncRetirerExecute(Sender: TObject);
var
  FichierSync : TACCFichierSync;
  Serveur : TACCServeur;
begin
  FichierSync := FichierSyncSelectionne;
  if FichierSync <> nil then
  begin
      if  MessageDlg(
          'Etes-vous s�r de vouloir retirer le fichier "'+FichierSync.Nom+'" de la synchronisation',
          mtConfirmation,[mbYes,mbNo],0)= mrYes then
      begin
        Serveur := FichierSync.Serveur;
        Serveur.FichiersSync.Remove(FichierSync);
        DonneesEAC.EnregistrerFichierSync;
        ActualiserVue;
      end;

  end;

end;

procedure TFormEACPrinc.ActionFichierCollerExecute(Sender: TObject);
begin
  ActionDossierCollerExecute(sender);
end;

procedure TFormEACPrinc.ActualiserVue;
var
  Fichier : TACCFichier;
  FichierSync : TACCFichierSync;
begin
  if TreeViewServeurs.Selected <> nil then
   ActualiserNoeud(TreeViewServeurs.Selected)
  else
  begin
    Fichier := FichierSelectionne;
    if Fichier <> nil then
    begin
        if TObject(Fichier.Dossier.Data) is TTreeNode then
          ActualiserNoeud(TTreeNode(Fichier.Dossier.Data));
        exit;
    end;
    FichierSync := FichierSyncSelectionne;
    if FichierSync <> nil then
    begin
        if TObject(FichierSync.Serveur.FichiersSync.Data) is TTreeNode then
          ActualiserNoeud(TTreeNode(FichierSync.Serveur.FichiersSync.Data));
        exit;
    end;
  end;
  Repaint;

end;

procedure TFormEACPrinc.ServeurDonneesChange(Sender: TObject);
begin
  ActualiserVue;
end;

procedure TFormEACPrinc.ActionFichierEnvoyerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
  FormEACProgress : TFormEACProgress;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      with TOpenDialog.Create(Self)do
      begin
        Filter := 'Tous les fichiers (*.*)|*.*';
        DefaultExt := '*.*';
        if Execute then
        begin
          FormEACProgress := TFormEACProgress.Create(Self);
          FormEACProgress.Ouvrir(Dossier.Serveur,'Envoie du fichier "'+ExtractFileName(FileName)+'"');
          try
            if not DirectoryExists(Dossier.CheminLocal)then
            ForceDirectories(Dossier.CheminLocal);
            CopyFile(pChar(FileName),pChar(Dossier.CheminLocal+'\'+ExtractFileName(FileName)),False);
            if not Dossier.Serveur.RequeteFichierEcrire(Dossier.Chemin+'\'+ExtractFileName(FileName))then
              MessageDlg('Erreur lors de l''envoie du dossier',mtError,[mbOk],0);
          except
            MessageDlg('Erreur lors de l''envoie du dossier',mtError,[mbOk],0);
          end;
          ActualiserVue;
          FormEACProgress.Fermer;
          FormEACProgress.Free;
        end;
        free;
      end;
  end;
end;

procedure TFormEACPrinc.ActionFichierOuvrirExecute(Sender: TObject);
Var
  Fichier : TACCFichier;
begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
    if Fichier.Exist then
    begin
      if Fichier.Ajour then
        ShellExecute(Handle,'Open',pChar(Fichier.CheminLocal),nil,Nil,SW_SHOWDEFAULT)
      else
      begin
        if MessageDlg('Attention!!, Le fichier '+Fichier.Nom+' n''est pas a jour, souhaitez vous le mettre a jour ?',mtWarning,[mbYes,mbNo],0) = mrYes then
        begin
          with TFormEACProgress.Create(self)do
          begin
            Ouvrir(Fichier.Serveur,'Mise � jour du fichier '+Fichier.Nom);
            if Fichier.RequeteMettreAJour then
            begin
              ShellExecute(Handle,'Open',pChar(Fichier.CheminLocal),nil,Nil,SW_SHOWDEFAULT);
              ActualiserVue;
            end
            else
              MessageDlg('Erreur lors de la mise a jour du fichier',mtError,[mbOk],0);
            Fermer;
            Free;
          end;
        end
        else
           ShellExecute(Handle,'Open',pChar(Fichier.CheminLocal),nil,Nil,SW_SHOWDEFAULT);
      end;
    end
    else
      if MessageDlg('Le fichier '+Fichier.Nom+' n''existe pas en local, souhaitez vous le t�l�charger ?',mtWarning,[mbYes,mbNo],0) = mrYes then
      begin
        with TFormEACProgress.Create(self)do
        begin
            Ouvrir(Fichier.Serveur,'lecture du fichier '+Fichier.Nom);
            if Fichier.RequeteLire then
            begin
              ShellExecute(Handle,'Open',pChar(Fichier.CheminLocal),nil,Nil,SW_SHOWDEFAULT);
              ActualiserVue;
            end
            else
              MessageDlg('Erreur lors de la lecture du fichier',mtError,[mbOk],0);
            Fermer;
            Free;
        end;
      end;
  end;


end;

function TFormEACPrinc.FichierSyncSelectionne: TACCFichierSync;
begin
  result := nil;
  if ListViewFichiers.Selected <> nil then
    if TObject(ListViewFichiers.Selected.Data) is TACCFichierSync then
      result := TACCFichierSync(ListViewFichiers.Selected.Data);

end;

function TFormEACPrinc.DossierSelectionne: TACCDossier;
begin
  Result := nil;
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.Data) is TACCDossier then
      Result := TACCDossier(TreeViewServeurs.Selected.Data);

end;

function TFormEACPrinc.ServeurSelectionne: TACCServeur;
begin
  Result := nil;
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.Data) is TACCServeur then
      Result := TACCServeur(TreeViewServeurs.Selected.Data);


end;

function TFormEACPrinc.FichierSelectionne: TACCFichier;
begin
  result := nil;
  if ListViewFichiers.Selected <> nil then
    if TObject(ListViewFichiers.Selected.Data) is TACCFichier then
      result := TACCFichier(ListViewFichiers.Selected.Data);

end;

function TFormEACPrinc.DossierFichierSelectionne : TACCDossier;
begin
  result := nil;
  if ListViewFichiers.Selected <> nil then
    if TObject(ListViewFichiers.Selected.Data) is TACCDossier then
      result := TACCDossier(ListViewFichiers.Selected.Data);

end;

procedure TFormEACPrinc.ActionFichierOuvrirAvecExecute(Sender: TObject);
var
  Fichier  : TACCFichier;
begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
      if Fichier.Exist then
      begin
        if Fichier.Ajour then
        begin
          ShellExecute(Application.Handle, 'open', PChar('rundll32.exe'),
                PChar('shell32.dll,OpenAs_RunDLL ' + Fichier.CheminLocal), nil,
                SW_SHOWNORMAL);
        end
        else
        begin
          if MessageDlg('Attention!!, Le fichier '
                +Fichier.Nom
                +' n''est pas a jour, souhaitez vous le mettre a jour ?',
                mtWarning,[mbYes,mbNo],0) = mrYes then
          begin
            with TFormEACProgress.Create(self)do
            begin
              Ouvrir(Fichier.Serveur,'Mise � jour du fichier '+Fichier.Nom);
              if Fichier.RequeteMettreAJour then
              begin
                ShellExecute(Application.Handle, 'open', PChar('rundll32.exe'),
                    PChar('shell32.dll,OpenAs_RunDLL ' + Fichier.CheminLocal), nil,
                    SW_SHOWNORMAL);
                ActualiserVue;
              end
              else
                MessageDlg('Erreur lors de la mise a jour du fichier',mtError,[mbOk],0);
              Fermer;
              Free;
            end;
          end;
        end;
      end
      else
        if MessageDlg('Le fichier '+Fichier.Nom+' n''existe pas en local, souhaitez vous le t�l�charger ?',mtWarning,[mbYes,mbNo],0) = mrYes then
        begin
          with TFormEACProgress.Create(self)do
          begin
            Ouvrir(Fichier.Serveur,'Lecture du fichier '+Fichier.Nom);
            if Fichier.RequeteLire then
            begin
              ShellExecute(Application.Handle, 'open', PChar('rundll32.exe'),
                    PChar('shell32.dll,OpenAs_RunDLL ' + Fichier.CheminLocal), nil,SW_SHOWNORMAL)
            end
            else
              MessageDlg('Erreur lors de la lecture du fichier',mtError,[mbOk],0);
            Fermer;
            Free;
          end;
        end;
  end;
end;
procedure TFormEACPrinc.ActionFichierOuvrirDossierExecute(Sender: TObject);
var
  Fichier  : TACCFichier;
begin

  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
    if Fichier.Exist then
    begin
      if Fichier.Ajour then
        ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
              PChar(ExtractFilePath(Fichier.CheminLocal)), nil,SW_SHOWNORMAL)
      else
      begin
        if MessageDlg('Attention!!, Le fichier '+Fichier.Nom+' n''est pas a jour, souhaitez vous le mettre a jour ?',mtWarning,[mbYes,mbNo],0) = mrYes then
        begin
          with TFormEACProgress.Create(self)do
          begin
            Ouvrir(Fichier.Serveur,'Mise � jour du fichier '+Fichier.Nom);
            if Fichier.RequeteMettreAJour then
            begin
              ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
                  PChar(ExtractFilePath(Fichier.CheminLocal)), nil,SW_SHOWNORMAL);
              ActualiserVue;
            end
            else
              MessageDlg('Erreur lors de la mise a jour du fichier',mtError,[mbOk],0);
            Fermer;
            Free;
          end;
        end
        else
          ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
              PChar(ExtractFilePath(Fichier.CheminLocal)), nil,SW_SHOWNORMAL)

      end;
    end
    else
      if MessageDlg('Le fichier '+Fichier.Nom+' n''existe pas en local, souhaitez vous le t�l�charger ?',mtWarning,[mbYes,mbNo],0) = mrYes then
      begin
        with TFormEACProgress.Create(self)do
        begin
            Ouvrir(Fichier.Serveur,'lecture du fichier '+Fichier.Nom);
            if Fichier.RequeteLire then
            begin
              ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
                PChar(ExtractFilePath(Fichier.CheminLocal)), nil,SW_SHOWNORMAL);
              ActualiserVue;
            end
            else
              MessageDlg('Erreur lors de la lecture du fichier',mtError,[mbOk],0);
            Fermer;
            Free;
        end;
      end;
  end;

end;

procedure TFormEACPrinc.ActionFichierTelechargerExecute(Sender: TObject);
var
  Fichier  : TACCFichier;
begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
    if Fichier.Exist then
      if not MessageDlg(
          'le fchier "'+Fichier.Nom+'" existe deja en locale, souhaitez-vous ecraser celui-ci?',
          mtWarning,[mbYes,mbNo],0 )=mrYes then
        exit;
    with TFormEACProgress.Create(self) do
    begin

      Ouvrir(Fichier.Serveur,'Telechargement du fichier '+Fichier.Nom);
      if not Fichier.RequeteLire then
        MessageDlg('Erreur lors de la lecture du fichier',mtError,[mbOk],0);
      ActualiserVue;
      fermer;
      Free;
    end;

  end;
end;

procedure TFormEACPrinc.ActionDossierTelechargerExecute(Sender: TObject);
var
  Dossier  : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
    with TFormEACProgress.Create(self) do
    begin
      Ouvrir(Dossier.Serveur,'Telechargement du dossier '+Dossier.Nom);
      if not Dossier.RequeteLire then
        MessageDlg('Erreur lors de la lecture du dossier',mtError,[mbOk],0);
      ActualiserVue;
      Fermer;
      free;
    end;


  end;
end;

procedure TFormEACPrinc.ActionDossierEnvoyerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
  Chemin : String;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      if SelectDirectory('Selectionner le dossier a envoyer sur le serveur '+Dossier.Serveur.Nom
             ,'',Chemin) then
      begin
        with TFormEACProgress.Create(self) do
        begin
          Ouvrir(Dossier.Serveur,'Envoie du dossier '+ExtractFileName(Chemin));
          if not DirectoryExists(Dossier.CheminLocal)then
            ForceDirectories(Dossier.CheminLocal);
          CopyDirectory(Chemin,Dossier.CheminLocal+'\'+ExtractFileName(Chemin));
          if not Dossier.Serveur.RequeteDossierEcrire(Dossier.Chemin+'\'+ExtractFileName(Chemin))then
            MessageDlg('Erreur lors de l''�criture du dossier',mtError,[mbOk],0);
          ActualiserVue;
          Fermer;
          Free;
        end;
      end;
  end;
end;

procedure TFormEACPrinc.ActionDossierOuvrirExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
      if Dossier.Exist then
        ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
          PChar(ExtractFilePath(Dossier.CheminLocal)), nil,
          SW_SHOWNORMAL)
      else
        if MessageDlg(
            'Le dossier '+Dossier.Nom+' n''existe pas en local,'
              +' souhaitez vous le t�l�charger ?',
              mtWarning,[mbYes,mbNo],0) = mrYes then
        begin
          with TFormEACProgress.Create(self) do
          begin
            Ouvrir(Dossier.Serveur,'Telechargement du dossier '+Dossier.Nom);
            if Dossier.RequeteLire then
                  ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
                  PChar(ExtractFilePath(Dossier.CheminLocal)), nil,
                  SW_SHOWNORMAL)
            else
              MessageDlg('Erreur lors de la lecture du dossier',mtError,[mbOk],0);
            ActualiserVue;
            fermer;
            free;
          end;

        end;
  end;

end;

procedure TFormEACPrinc.ListViewFichiersDblClick(Sender: TObject);
begin
  if FichierSelectionne <> nil then
  begin
    ActionFichierOuvrirExecute(Sender);
    exit;
  end;
  if DossierFichierSelectionne <> nil then
  begin
    ActionDossierFichierExplorerExecute(Sender);
    exit;
  end;
  if FichierSyncSelectionne <> nil then
  begin
    ActionFichierSyncOuvrirExecute(Sender);
    exit;
  end;

end;

procedure TFormEACPrinc.ActionFichierRenommerExecute(Sender: TObject);
var
  Fichier : TACCFichier;
  Nouveau : string;

begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
    Nouveau := Fichier.Nom;
    if InputQuery('Renommer','Saisissez le nouveau nom',Nouveau) then
    begin
      if not Fichier.RequeteRenommer(Nouveau) then
        MessageDlg('Erreur lors du changement de nom du fichier',mtError,[mbOk],0);
      ActualiserVue;
    end;

  end;
end;

procedure TFormEACPrinc.ActionDossierRenommerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
  Nouveau : string;

begin
  if not TreeViewServeurs.Focused then
    exit;
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
    Nouveau := Dossier.Nom;
    if InputQuery('Renommer','Saisissez le nouveau nom',Nouveau) then
    begin
      if not Dossier.RequeteRenommer(Nouveau) then
        MessageDlg('Erreur lors du changement de nom du dossier',mtError,[mbOk],0);

      if TreeViewServeurs.Selected <> nil then
        if TreeViewServeurs.Selected.Parent <> nil then
          ActualiserNoeud(TreeViewServeurs.Selected.Parent);

    end;

  end;
end;

procedure TFormEACPrinc.ActionFichierMettreAJourExecute(Sender: TObject);
var
  FichierSync : TACCFichierSync;
  Fichier : TACCFichier;
begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
    FichierSync := Fichier.FichierSynchronise;
    if FichierSync <> nil then
    begin
      with TFormEACProgress.Create(self) do
      begin
        Ouvrir(FichierSync.Serveur,'Mise � a jour du fichier '+FichierSync.Nom);
        if not FichierSync.RequeteMettreAJour then
          MessageDlg('Erreur lors de la mise a jour du fichier',mtError,[mbOk],0);
        ActualiserVue;
        Fermer;
        free;
      end;
    end
    else
    begin
      with TFormEACProgress.Create(self) do
      begin
        Ouvrir(Fichier.Serveur,'Mise � jour du fichier '+Fichier.Nom);
        if not Fichier.RequeteMettreAJour then
          MessageDlg('Erreur lors de la mise a jour du fichier',mtError,[mbOk],0);
        ActualiserVue;
        fermer;
        Free;
      end;
    end;

  end;

end;

procedure TFormEACPrinc.ActionDossierMettreAJourExecute(Sender: TObject);
var
  FichierSync : TACCFichierSync;
  Dossier : TACCDossier;
begin
  Dossier := DossierSelectionne;
  if Dossier <> nil then
  begin
    FichierSync := Dossier.DossierSynchronise;
    if FichierSync <> nil then
      with TFormEACProgress.Create(Self) do
      begin
        Ouvrir(FichierSync.Serveur,'Mise a jour du dossier '+FichierSync.Nom);
        if not FichierSync.RequeteMettreAJour then
          MessageDlg('Erreur lors de la mise a jour du fichier',mtError,[mbOk],0);
        ActualiserVue;          
        Fermer;
        Free;
      end;

  end;

end;

procedure TFormEACPrinc.ActionAjouterServeurExecute(Sender: TObject);
var
  Adresse : String;
begin
  Adresse := 'localhost';
  if InputQuery('Ajout d''un nouveau serveur','Sasissez l''adresse du serveur',Adresse) then
  begin
    DonneesEAC.Poste.Serveurs.Add(Adresse);
    DonneesEAC.EnregistrerListeDesServeur;
    ActualiserPoste;

  end;

end;

procedure TFormEACPrinc.ActionConfigurationExecute(Sender: TObject);
begin
  with TFormEACConfiguration.Create(self)do
  begin
    Executer;
    free;
  end;
end;

procedure TFormEACPrinc.ActionSynchronisationAutoExecute(Sender: TObject);


begin
  DonneesEAC.TimerSynchronisation.Enabled := ActionSynchronisationAuto.Checked;
  DonneesEAC.Poste.SynchronisationActive := ActionSynchronisationAuto.Checked;

end;

procedure TFormEACPrinc.ActionServeurSupprimerExecute(Sender: TObject);
var
  Serveur : TACCServeur;
begin
  Serveur := ServeurSelectionne;
  if Serveur <> nil then
  begin
    if MessageDlg('Attention, Etes vous s�re de vouloir supprimer le serveur "'+Serveur.Nom+'" ?',mtWarning,[mbYes,mbNo],0)= mrYes then
    begin
      DonneesEAC.Poste.Serveurs.Remove(Serveur);
      DonneesEAC.EnregistrerListeDesServeur;
      ActualiserPoste;
    end;
  end;
end;

procedure TFormEACPrinc.TreeViewServeursChange(Sender: TObject;
  Node: TTreeNode);
begin
  ActualiserNoeud(TreeViewServeurs.Selected);
end;

procedure TFormEACPrinc.ActionFichierNouveauDossierExecute(
  Sender: TObject);
begin
  ActionDossierNouveauDossierExecute(sender);
end;

procedure TFormEACPrinc.ActionQuitterExecute(Sender: TObject);
begin

  Application.Terminate;
//  close;
end;

procedure TFormEACPrinc.ActionPosteSynchroniserExecute(Sender: TObject);
begin
  DonneesEAC.Poste.MettreAJour;
end;

procedure TFormEACPrinc.ActionServeurSynchroniserExecute(Sender: TObject);
var
  lServeur : TACCServeur;
  i : integer;
begin
  lServeur := ServeurSelectionne;
  if lServeur <> nil then
  begin
    with TFormEACProgress.Create(Self) do
    begin
      Ouvrir(lServeur,'Syncronisation du serveur en cours');
      for i := 0 to lServeur.FichiersSync.Count - 1 do
        lServeur.FichiersSync[i].RequeteMettreAJour;
      Fermer;
      free;
    end;
  end;

end;

procedure TFormEACPrinc.Ouvrir1Click(Sender: TObject);
begin
  JvTrayIcon1.ShowApplication;
end;

procedure TFormEACPrinc.FormDestroy(Sender: TObject);
begin
  DonneesEAC.Poste.FermerLesConnexions;
  BalloonMessages.Free;
end;

procedure TFormEACPrinc.JvTrayIcon1BalloonHide(Sender: TObject);
begin
  BalloonMessages.Clear;
end;

procedure TFormEACPrinc.PopupMenuFichiersPopup(Sender: TObject);
begin
  if ListViewFichiers.Selected <> nil then
    if TObject(ListViewFichiers.Selected.Data) is TACCFichier then
    begin
      ActualiserMenuFichier('Fichier');
      exit;
    end;
  if ListViewFichiers.Selected <> nil then
    if TObject(ListViewFichiers.Selected.Data) is TACCDossier then
    begin
      ActualiserMenuFichier('DossierFichier');
      exit;
    end;
  if TreeViewServeurs.Selected <> nil then
    if TObject(TreeViewServeurs.Selected.Data) is TACCDossier then
      ActualiserMenuFichier('Dossier');

end;

procedure TFormEACPrinc.ActionDossierFichierExplorerExecute(
  Sender: TObject);
var
  Dossier : TACCDossier;
begin
  if ListViewFichiers.Selected<> nil then
    if TObject(ListViewFichiers.Selected.Data) is TACCDossier then
    begin
      Dossier := TACCDossier(ListViewFichiers.Selected.Data);
      if TObject(Dossier.Data) is TTreeNode then
      begin
          TreeViewServeurs.Selected := TTreeNode(Dossier.Data);
          ActualiserNoeud(TTreeNode(Dossier.Data));
      end;

    end;

end;

procedure TFormEACPrinc.JvTrayIcon1DblClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    if not JvTrayIcon1.ApplicationVisible then
    begin
      JvTrayIcon1.ShowApplication;
      BringToFront;
    end
    else
      JvTrayIcon1.HideApplication;

end;

procedure TFormEACPrinc.ServeurAvancement(Sender: TObject; Libelle: string;
  Max, Position: integer);
begin

end;

procedure TFormEACPrinc.ActionServeurProprieteExecute(Sender: TObject);
var
  Serveur : TACCServeur;
begin
  Serveur := ServeurSelectionne;
  if Serveur <> nil then
  begin
    with TFormEACEditServeur.Create(Self)do
    begin
      execute(Serveur);

      Free;
    end;

  end;


end;


procedure TFormEACPrinc.ActionFichierSyncOuvrirExecute(Sender: TObject);
Var
  FichierSync : TACCFichierSync;
begin
  FichierSync := FichierSyncSelectionne;
  if FichierSync <> nil then
  begin
    if FichierSync.Exist then
    begin
      ShellExecute(Handle,'Open',pChar(FichierSync.CheminLocal),nil,Nil,SW_SHOWDEFAULT)

    end
    else
      if MessageDlg('"'+FichierSync.Nom+'" n''existe pas en local, souhaitez vous le t�l�charger ?',mtWarning,[mbYes,mbNo],0) = mrYes then
      begin
        with TFormEACProgress.Create(self)do
        begin
            Ouvrir(FichierSync.Serveur,'lecture de "'+FichierSync.Chemin+'"');
            if FichierSync.RequeteLire then
            begin
              ShellExecute(Handle,'Open',pChar(FichierSync.CheminLocal),nil,Nil,SW_SHOWDEFAULT);
              ActualiserVue;
            end
            else
              MessageDlg('Erreur lors de la lecture du fichier',mtError,[mbOk],0);
            Fermer;
            Free;
        end;
      end;
  end;


end;
procedure TFormEACPrinc.ActionFichierSyncOuvrirAvecExecute(
  Sender: TObject);
var
  FichierSync  : TACCFichierSync;
begin
  FichierSync := FichierSyncSelectionne;
  if FichierSync <> nil then
  begin
      if FichierSync.Exist then
      begin
          ShellExecute(Application.Handle, 'open', PChar('rundll32.exe'),
                PChar('shell32.dll,OpenAs_RunDLL ' + FichierSync.CheminLocal), nil,
                SW_SHOWNORMAL);
      end
      else
        if MessageDlg('"'+FichierSync.Nom+'" n''existe pas en local, souhaitez vous le t�l�charger ?',mtWarning,[mbYes,mbNo],0) = mrYes then
        begin
          with TFormEACProgress.Create(self)do
          begin
            Ouvrir(FichierSync.Serveur,'Lecture du fichier "'+FichierSync.Chemin+'"');
            if FichierSync.RequeteLire then
            begin
              ShellExecute(Application.Handle, 'open', PChar('rundll32.exe'),
                    PChar('shell32.dll,OpenAs_RunDLL ' + FichierSync.CheminLocal), nil,SW_SHOWNORMAL)
            end
            else
              MessageDlg('Erreur lors de la lecture du fichier',mtError,[mbOk],0);
            Fermer;
            Free;
          end;
      end;
  end;
end;
procedure TFormEACPrinc.FormShow(Sender: TObject);
begin
(*  if DonneesEAC.Configuration.Demarrage.Reduire then
    JvTrayIcon1.HideApplication;
  *)
end;

procedure TFormEACPrinc.ChargerFichiersRecents;
var
  Fichier : string;
  i : Integer;
  MenuItem  : TMenuItem;
begin
(*
  Fichier := DonneesEAC.Configuration.General.DossierTravail
                          +'\'+FichiersRecentNom;
  if FileExists(Fichier) then
  begin
    FichiersRecent.LoadFromFile(Fichier);
    MenuItemFichiersRecent.Clear;
    for i := 0 to FichiersRecent.Count - 1 do
    begin

      MenuItem := TMenuItem.Create(Self);
      MenuItemFichiersRecent.Add(Menu);

    end;
  end;
    *)
end;

procedure TFormEACPrinc.EnregistrerFichiersRecents;
begin

end;

procedure TFormEACPrinc.FormActivate(Sender: TObject);
begin
  if Demarrage then
  begin
    Demarrage := false;
    if DonneesEAC.Configuration.Demarrage.Reduire then
      JvTrayIcon1.HideApplication;
  end;
end;

procedure TFormEACPrinc.Effacertout1Click(Sender: TObject);
begin
  ListBoxMessages.Clear;
end;

procedure TFormEACPrinc.ActionActualiserExecute(Sender: TObject);
begin
  ActualiserVue;
end;

procedure TFormEACPrinc.ActionDossierFichierOuvrirExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := FichierDossierSelectionne;
  if Dossier <> nil then
  begin
      if Dossier.Exist then
        ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
          PChar(ExtractFilePath(Dossier.CheminLocal)), nil,
          SW_SHOWNORMAL)
      else
        if MessageDlg(
            'Le dossier '+Dossier.Nom+' n''existe pas en local,'
              +' souhaitez vous le t�l�charger ?',
              mtWarning,[mbYes,mbNo],0) = mrYes then
        begin
          with TFormEACProgress.Create(self) do
          begin
            Ouvrir(Dossier.Serveur,'Telechargement du dossier '+Dossier.Nom);
            if Dossier.RequeteLire then
                  ShellExecute(Application.Handle, 'open', PChar('explorer.exe'),
                  PChar(ExtractFilePath(Dossier.CheminLocal)), nil,
                  SW_SHOWNORMAL)
            else
              MessageDlg('Erreur lors de la lecture du dossier',mtError,[mbOk],0);
            ActualiserVue;
            fermer;
            free;
          end;

        end;
  end;

end;
function TFormEACPrinc.FichierDossierSelectionne: TACCDossier;
begin
  result := nil;
  if ListViewFichiers.Selected<> nil then
    if TObject(ListViewFichiers.Selected.Data) is TACCDossier then
      Result := TACCDossier(ListViewFichiers.Selected.Data);

end;

procedure TFormEACPrinc.ActionDossierFichierTelechargerExecute(
  Sender: TObject);
var
  Dossier  : TACCDossier;
begin
  Dossier := FichierDossierSelectionne;
  if Dossier <> nil then
  begin
    with TFormEACProgress.Create(self) do
    begin
      Ouvrir(Dossier.Serveur,'Telechargement du dossier '+Dossier.Nom);
      if not Dossier.RequeteLire then
        MessageDlg('Erreur lors de la lecture du dossier',mtError,[mbOk],0);
      ActualiserVue;
      Fermer;
      free;
    end;


  end;
end;


procedure TFormEACPrinc.ActionDossierFichierAjouterSynchroniserExecute(
  Sender: TObject);
var
  Dossier : TACCDossier;
  FichierSync : TACCFichierSync;
begin
  Dossier := FichierDossierSelectionne;
  if Dossier <> nil then
  begin
      FichierSync := Dossier.AjouterEnSynchronisation;
      if FichierSync <> nil then
      begin
        if MessageDlg('Souhaitez-vous t�l�charger le dossier "'+FichierSync.Chemin+'" maintenant ?'
          ,mtConfirmation,[mbYes,mbNo],0)=mrYes then
        begin
          with TFormEACProgress.Create(self)do
          begin
            Ouvrir(Dossier.Serveur,'Mise en synchronisation du dosser '+Dossier.Nom);
            DonneesEAC.EnregistrerFichierSync;
            ActualiserVue;
            Fermer;
            free;
          end;
        end;
      end;
    end;

end;

procedure TFormEACPrinc.ActionDossierFichierMettreAJourExecute(
  Sender: TObject);
var
  FichierSync : TACCFichierSync;
  Dossier : TACCDossier;
begin
  Dossier := FichierDossierSelectionne;
  if Dossier <> nil then
  begin
    FichierSync := Dossier.DossierSynchronise;
    if FichierSync <> nil then
      with TFormEACProgress.Create(Self) do
      begin
        Ouvrir(FichierSync.Serveur,'Mise a jour du dossier '+FichierSync.Nom);
        if not FichierSync.RequeteMettreAJour then
          MessageDlg('Erreur lors de la mise a jour du fichier',mtError,[mbOk],0);
        ActualiserVue;
        Fermer;
        Free;
      end;

  end;

end;

procedure TFormEACPrinc.ActionDossierFichierSupprimerExecute(
  Sender: TObject);
var
  Dossier : TACCDossier;
begin

  Dossier := FichierDossierSelectionne;
  if Dossier <> nil then
    if MessageDlg(
          'Etes-vous s�r de vouloir supprimer le dossier "'+Dossier.Nom+'" du serveur',
          mtWarning,[mbYes,MbNo],0)= mrYes then
    begin
      with TFormEACProgress.Create(Self) do
      begin
        Ouvrir(Dossier.Serveur,'Suppression du dossier "'+Dossier.Nom+'" en cours');
        if not Dossier.RequeteSupprimer then
          MessageDlg('Erreur lors de la suppresion de dossier',mtError,[mbOk],0)
        else
          ActualiserVue;

        Fermer;
        Free;
      end;

    end;

end;

procedure TFormEACPrinc.ActionDossierFichierRenommerExecute(
  Sender: TObject);
var
  Dossier : TACCDossier;
  Nouveau : string;
  
begin
  Dossier := FichierDossierSelectionne;
  if Dossier <> nil then
  begin
    Nouveau := Dossier.Nom;
    if InputQuery('Renommer','Saisissez le nouveau nom',Nouveau) then
    begin
      if not Dossier.RequeteRenommer(Nouveau) then
        MessageDlg('Erreur lors du changement de nom du dossier',mtError,[mbOk],0);
      ActualiserVue;

    end;

  end;
end;

procedure TFormEACPrinc.ActionDossierFichierCouperExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := FichierDossierSelectionne;
  if Dossier <> nil then
  begin
      DossierPressePapier := Dossier.Chemin;
      OperatioMode := ACOpModeCouperDossier;
      
  end;

end;

procedure TFormEACPrinc.ActionDossierFichierCopierExecute(Sender: TObject);
var
  Dossier : TACCDossier;
begin
  Dossier := FichierDossierSelectionne;
  if Dossier <> nil then
  begin
      DossierPressePapier := Dossier.Chemin;
      OperatioMode := ACOpModeCopierDossier;

  end;

end;

procedure TFormEACPrinc.ActionDossierFichierCollerExecute(Sender: TObject);
var
  Dossier : TACCDossier;
  FormEACProgress : TFormEACProgress;
  Serveur : TACCServeur;
begin
  Dossier := FichierDossierSelectionne;

  if Dossier <> nil then
  begin
      Serveur := Dossier.Serveur;
      FormEACProgress := TFormEACProgress.Create(self);
      Case OperatioMode of
      ACOpModeAucun : ;
      ACOpModeCopierFichier :
      begin
          FormEACProgress.Ouvrir(Serveur,'Copie du fichier en cours');
          if not  Serveur.RequeteFichierCopier(FichierPressePapier,Dossier.Chemin) then
                MessageDlg('Erreur lors de la copie du fichier',mtError,[mbOk],0);
      end;
      ACOpModeCouperFichier :
      begin
          FormEACProgress.Ouvrir(Serveur,'D�placement du fichier en cours');
          if not Serveur.RequeteFichierDeplacer(FichierPressePapier,Dossier.Chemin) then
            MessageDlg('Erreur lors du d�placement du fichier',mtError,[mbOk],0);
      end;
      ACOpModeCopierDossier :
      begin
        FormEACProgress.Ouvrir(Serveur,'Copie du dossier en cours');
        if not Serveur.RequeteDossierCopier(DossierPressePapier,Dossier.Chemin) then
          MessageDlg('Erreur lors de la copie du dossier',mtError,[mbOk],0);
      end;
      ACOpModeCouperDossier :
      begin
        FormEACProgress.Ouvrir(Serveur,'D�placement du dossier en cours');
        if not Serveur.RequeteDossierDeplacer(DossierPressePapier,Dossier.Chemin) then
          MessageDlg('Erreur lors du d�placement du dossier',mtError,[mbOk],0);
      end;

      end;
      ActualiserVue;
      FormEACProgress.Fermer;
      FormEACProgress.Free;
  end;

end;
procedure TFormEACPrinc.ActionFichierSynchroniserLireExecute(Sender: TObject);
var
  Fichier : TACCFichier;
  Nouveau : string;

begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
      with TFormEACProgress.Create(self)do
      begin
          Ouvrir(Fichier.Serveur,'Synchronisation en lecture du fichier "'+Fichier.Nom+'"');
          if Fichier.RequeteSynchLire then
          begin
             // ShellExecute(Handle,'Open',pChar(Fichier.CheminLocal),nil,Nil,SW_SHOWDEFAULT);
              ActualiserVue;
            end
            else
              MessageDlg('Erreur lors de la synchrinisation du fichier',mtError,[mbOk],0);
            Fermer;
            Free;
        end;

  end;
end;

procedure TFormEACPrinc.ActionFichierSynchroniserEcrireExecute(
  Sender: TObject);
var
  Fichier : TACCFichier;
  Nouveau : string;

begin
  Fichier := FichierSelectionne;
  if Fichier <> nil then
  begin
      with TFormEACProgress.Create(self)do
      begin
          Ouvrir(Fichier.Serveur,'Synchronisation en ecriture du fichier "'+Fichier.Nom+'"');
          if Fichier.RequeteSynchEcrire then
          begin
              ShellExecute(Handle,'Open',pChar(Fichier.CheminLocal),nil,Nil,SW_SHOWDEFAULT);
              ActualiserVue;
            end
            else
              MessageDlg('Erreur lors de la synchrinisation du fichier',mtError,[mbOk],0);
            Fermer;
            Free;
        end;

  end;
end;

end.
