object FormEABAvertissementExtinction: TFormEABAvertissementExtinction
  Left = 421
  Top = 306
  BorderStyle = bsDialog
  Caption = 'Avertissement'
  ClientHeight = 133
  ClientWidth = 378
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 353
    Height = 81
    Caption = 'Message'
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 185
      Height = 25
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'L'#39'odinateur va s'#39'eteindre dans :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object LabelCompteur: TLabel
      Left = 209
      Top = 13
      Width = 21
      Height = 20
      Caption = '30'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object ButtonAnnulerExtinction: TButton
    Left = 112
    Top = 96
    Width = 137
    Height = 25
    Caption = 'Ne pas eteindre'
    ModalResult = 3
    TabOrder = 1
    OnClick = ButtonAnnulerExtinctionClick
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 112
    Top = 40
  end
end
