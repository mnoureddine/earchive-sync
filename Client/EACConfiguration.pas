unit EACConfiguration;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Spin,EACDonnees,FileCtrl, ExtCtrls;

type
  TFormEACConfiguration = class(TForm)
    ButtonOK: TButton;
    ButtonAnnuler: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    EditDossierTravail: TEdit;
    ButtonParcourir: TButton;
    Label1: TLabel;
    Label2: TLabel;
    SpinEditFrequenceMiseAJour: TSpinEdit;
    CheckBoxAutoMAJActive: TCheckBox;
    CheckBoxReduireDemarrage: TCheckBox;
    Label3: TLabel;
    Bevel1: TBevel;
    Label4: TLabel;
    Bevel2: TBevel;
    Label5: TLabel;
    Bevel3: TBevel;
    CheckBoxAfficherInformationsActive: TCheckBox;
    procedure ButtonParcourirClick(Sender: TObject);
  private
    { Déclarations privées }

  public
    { Déclarations publiques }
    procedure Executer;
    procedure Charger;
    procedure Enregistrer;

  end;


implementation

uses Math;

{$R *.dfm}

{ TFormEACConfiguration }

procedure TFormEACConfiguration.Charger;
begin
  with DonneesEAC.Configuration do
  begin
    EditDossierTravail.Text := General.DossierTravail;
    SpinEditFrequenceMiseAJour.Value := General.FrequenceMAJ;
    CheckBoxAutoMAJActive.Checked := General.AutoMAJActive;
    CheckBoxReduireDemarrage.Checked := Demarrage.Reduire;
    CheckBoxAfficherInformationsActive.Checked := General.AfficherMessageInfo;
  end;
end;

procedure TFormEACConfiguration.Enregistrer;
begin
  with DonneesEAC.Configuration do
  begin
    General.DossierTravail := EditDossierTravail.Text;
    General.FrequenceMAJ := SpinEditFrequenceMiseAJour.Value;
    General.AutoMAJActive := CheckBoxAutoMAJActive.Checked;
    Demarrage.Reduire := CheckBoxReduireDemarrage.Checked;
    General.AfficherMessageInfo := CheckBoxAfficherInformationsActive.Checked;
  end;

  DonneesEAC.EnregistrerConfiguration;
end;

procedure TFormEACConfiguration.Executer;
begin
  Charger;
  If ShowModal = mrok then
  begin
    Enregistrer;    
  end;
end;

procedure TFormEACConfiguration.ButtonParcourirClick(Sender: TObject);
var
  Dossier : string;
begin
  Dossier := EditDossierTravail.Text;
  if SelectDirectory('Selection du dossier de travail','',Dossier)then
    EditDossierTravail.Text := Dossier;

end;

end.
