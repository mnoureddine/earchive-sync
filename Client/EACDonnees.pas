unit EACDonnees;

interface

uses
  SysUtils, Classes, IdBaseComponent, IdComponent, IdUDPBase, IdUDPClient,
  IdTCPConnection, IdTCPClient, ArchivesCloudClient,Inifiles,ArchivesCloudCommun,
  ExtCtrls;

type
  TDonneesEAC = class(TDataModule)
    TimerSynchronisation: TTimer;
    procedure DataModuleCreate(Sender: TObject);
    procedure TimerSynchronisationTimer(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
    Poste : TACCPoste;
    FichierListe : string;
    FichierIni : TIniFile;
    Configuration : record
     General : record
        DossierTravail : string;
        AutoMAJActive : boolean;
        FrequenceMAJ : integer;
        DossierTemp : string;
        AfficherMessageInfo : boolean;
     end;
     Demarrage : record
        ConnexionAuto : boolean;
        Reduire : boolean;
     end;
    end;
    procedure ChargerConfiguration;
    procedure EnregistrerConfiguration;
    procedure ChargerFichierSync;
    procedure EnregistrerFichierSync;
    procedure ChargerListeDesServeur;
    procedure EnregistrerListeDesServeur;


  end;

var
  DonneesEAC: TDonneesEAC;

const
  FichierSyncNom = '_Sync.txt';
  FichierIniNom = 'ArchiveSync.ini';

implementation

{$R *.dfm}

procedure TDonneesEAC.ChargerConfiguration;
begin
  with Configuration do
  begin
    General.DossierTravail :=
        FichierIni.ReadString('General','DossierTravail',DossierMesDocuments+'\ArchiveSync');
    General.FrequenceMAJ :=
        FichierIni.ReadInteger('General','FrequenceMAJ',10);
    General.AutoMAJActive :=
        FichierIni.ReadBool('General','AutoMAJActive',false);
    General.AfficherMessageInfo :=
        FichierIni.ReadBool('General','AfficherMessageInfo',false);
    General.DossierTemp :=
        FichierIni.ReadString('General','DossierTemp',GetTempDirectory+'\ArchiveSync');
    if not DirectoryExists(General.DossierTemp) then
      ForceDirectories(General.DossierTemp);

    Demarrage.ConnexionAuto :=
        FichierIni.ReadBool('Demarrage','ConnexionAuto',false);

    Demarrage.Reduire :=
        FichierIni.ReadBool('Demarrage','Reduire',false);



  end;

end;

procedure TDonneesEAC.ChargerFichierSync;
var
  i : Integer;
  FichierSync : string;
begin
  for i := 0 to Poste.Serveurs.Count - 1 do
  begin
    FichierSync := Configuration.General.DossierTravail+'\'+Poste.Serveurs[i].Nom+FichierSyncNom;
    if FileExists(FichierSync) then
      Poste.Serveurs[i].FichiersSync.LoadFromFile(FichierSync);
  end;
end;

procedure TDonneesEAC.ChargerListeDesServeur;
var
  ListeServeur : TStringList;
  Serveur : TACCServeur;
  FichierSync : string;  
  i : Integer;
begin
  FichierListe := Configuration.General.DossierTravail+'\ListeServeur.txt';
  ListeServeur := TStringList.Create;
  if not FileExists(FichierListe) then
    ListeServeur.SaveToFile(FichierListe);
  ListeServeur.LoadFromFile(FichierListe);
  For i := 0 to ListeServeur.Count - 1 do
  begin
    Serveur := Poste.Serveurs.Add(ListeServeur[i]);
    Serveur.OuvrirDeFichier(Configuration.General.DossierTravail+'\'+ListeServeur[i]+'.cfg');
    FichierSync := Configuration.General.DossierTravail+'\'+Serveur.Nom+FichierSyncNom;
    if FileExists(FichierSync) then
      Serveur.FichiersSync.LoadFromFile(FichierSync);

    if Serveur.DemarrageActif then
      Serveur.Connexion;
  end;
  ListeServeur.Free;
end;

procedure TDonneesEAC.DataModuleCreate(Sender: TObject);
begin
  FichierIni := TIniFile.Create(FichierIniNom);
  ChargerConfiguration;
  Poste := TACCPoste.Create(Configuration.General.DossierTravail);
  Poste.DossierTemp := Configuration.General.DossierTemp;
  
end;

procedure TDonneesEAC.EnregistrerConfiguration;
begin
  with Configuration do
  begin
    FichierIni.WriteString('General','DossierTravail',General.DossierTravail);
    FichierIni.WriteInteger('General','FrequenceMAJ',General.FrequenceMAJ);
    FichierIni.WriteBool('General','AutoMAJActive',General.AutoMAJActive);
    FichierIni.WriteBool('General','AfficherMessageInfo',General.AfficherMessageInfo);
    FichierIni.WriteString('General','DossierTemp',General.DossierTemp);

    FichierIni.WriteBool('Demarrage','ConnexionAuto',Demarrage.ConnexionAuto);
    FichierIni.WriteBool('Demarrage','Reduire',Demarrage.Reduire);


    Poste.DossierACC := General.DossierTravail;

//    TimerSynchronisation.Interval :=  General.FrequenceMAJ*60000;
    TimerSynchronisation.Enabled := General.AutoMAJActive;
    Poste.SynchronisationActive := General.AutoMAJActive;

    
  end;
end;

procedure TDonneesEAC.EnregistrerFichierSync;
var
  i : integer;
  FichierSync : string;
begin
  for i := 0 to Poste.Serveurs.Count - 1 do
  begin

    FichierSync := Configuration.General.DossierTravail+'\'+Poste.Serveurs[i].Nom+FichierSyncNom;
    Poste.Serveurs[i].FichiersSync.SaveToFile(FichierSync);
  end;

end;

procedure TDonneesEAC.EnregistrerListeDesServeur;
var
  ListeServeur : TStringList;
  i : Integer;
begin

  FichierListe := Configuration.General.DossierTravail+'\ListeServeur.txt';
  ListeServeur := TStringList.Create;
  For i :=  0 to Poste.Serveurs.Count - 1 do
  begin
    ListeServeur.Add(Poste.Serveurs[i].Nom);
    Poste.Serveurs[i].EnregistrerSousFichier(Configuration.General.DossierTravail+'\'+Poste.Serveurs[i].Host+'.cfg');
  end;

  ListeServeur.SaveToFile(FichierListe);
  ListeServeur.Free;

end;

procedure TDonneesEAC.TimerSynchronisationTimer(Sender: TObject);
begin
  TimerSynchronisation.Interval :=  Configuration.General.FrequenceMAJ*60000;
  Poste.Synchronisation;
end;

end.
