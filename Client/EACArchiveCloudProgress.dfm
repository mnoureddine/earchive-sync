object FormEACProgress: TFormEACProgress
  Left = 285
  Top = 273
  BorderStyle = bsDialog
  Caption = 'Boite de progresion'
  ClientHeight = 104
  ClientWidth = 355
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  DesignSize = (
    355
    104)
  PixelsPerInch = 96
  TextHeight = 13
  object StaticTextMessage: TStaticText
    Left = 12
    Top = 8
    Width = 332
    Height = 41
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight, akBottom]
    AutoSize = False
    TabOrder = 0
  end
  object ProgressBar1: TProgressBar
    Left = 8
    Top = 64
    Width = 337
    Height = 17
    TabOrder = 1
  end
end
