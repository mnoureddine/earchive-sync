unit EACEditServeur;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,ArchivesCloudClient,ArchivesCloudCommun,Utiles,EACDonnees,
  Spin;

type
  TFormEACEditServeur = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    LabeledEditServeurNom: TLabeledEdit;
    LabeledEditServeurAdresse: TLabeledEdit;
    ButtonAnnuler: TButton;
    ButtonOK: TButton;
    LabeledEditUtilisateur: TLabeledEdit;
    LabeledEditMotPasse: TLabeledEdit;
    Bevel1: TBevel;
    CheckBoxDemarrageActif: TCheckBox;
    ComboBoxSyncMode: TComboBox;
    Label1: TLabel;
    CheckBoxSynchronisationActive: TCheckBox;
    CheckBoxSyncAvanceActive: TCheckBox;
    Label2: TLabel;
    SpinEditPort: TSpinEdit;
    procedure LabeledEditServeurAdresseChange(Sender: TObject);
  private
    FMotPasse : string;
    function GetAdresse: string;
    function GetMotPasse: string;
    function GetNom: string;
    function GetUtilisateur: string;
    procedure SetAdresse(const Value: string);
    procedure SetMotPasse(const Value: string);
    procedure SetNom(const Value: string);
    procedure SetUtilisateur(const Value: string);
    function GetDemarrageActif: boolean;
    procedure SetDemarrageActif(const Value: boolean);
    function GetSyncMode: TACSyncMode;
    procedure SetSyncMode(const Value: TACSyncMode);
    function GetSynchronisationActive: boolean;
    procedure SetSynchronisationActive(const Value: boolean);
    procedure SetSyncAvanceActive(const Value: boolean);
    function GetSyncAvanceActive: boolean;
    function GetPort: Integer;
    procedure SetPort(const Value: Integer);
    { Déclarations privées }

  public

    property Nom : string read GetNom write SetNom;
    property Adresse : string read GetAdresse write SetAdresse;
    property Utilisateur : string read GetUtilisateur write SetUtilisateur;
    property MotPasse : string read GetMotPasse write SetMotPasse;
    property DemarrageActif : boolean read GetDemarrageActif write SetDemarrageActif;
    property SyncMode : TACSyncMode read GetSyncMode write SetSyncMode;
    property SynchronisationActive : boolean  read GetSynchronisationActive write SetSynchronisationActive;
    property SyncAvanceActive: boolean read GetSyncAvanceActive write SetSyncAvanceActive;
    property Port : Integer read GetPort write SetPort;
    function execute(pServeur :TACCServeur = nil):boolean;
    { Déclarations publiques }
  end;

const
  NO_PASSE = 'xxxxx';

implementation



{$R *.dfm}

{ TFormEACEditServeur }

function TFormEACEditServeur.execute(pServeur: TACCServeur): boolean;
begin
  if pServeur <> nil then
  begin
    Nom := pServeur.Nom;
    Adresse := pServeur.Host;
    Port := pServeur.Port;
    Utilisateur := pServeur.Utilisateur;
    MotPasse := NO_PASSE;
    FMotPasse := pServeur.MotPasse;
    DemarrageActif := pServeur.DemarrageActif;
    SyncMode := pServeur.SyncMode;
    SynchronisationActive := pServeur.SynchronisationActive;
    SyncAvanceActive := pServeur.SyncAvanceActive;
  end;
  result := ShowModal = mrOk;
  if Result then
  begin
    pServeur.Nom := Nom;
    pServeur.Host := Adresse;
    pServeur.Port := Port;
    pServeur.Utilisateur := Utilisateur;
    pServeur.MotPasse := MotPasse;
    pServeur.ConnexionAuto := true;
    pServeur.DemarrageActif := DemarrageActif;
    pServeur.SyncMode := SyncMode;
    pServeur.SynchronisationActive := SynchronisationActive;
    pServeur.SyncAvanceActive := SyncAvanceActive;
    DonneesEAC.EnregistrerListeDesServeur;
  end;


end;

function TFormEACEditServeur.GetAdresse: string;
begin
  Result := LabeledEditServeurAdresse.Text;
end;

function TFormEACEditServeur.GetDemarrageActif: boolean;
begin
  Result :=  CheckBoxDemarrageActif.Checked;
end;

function TFormEACEditServeur.GetMotPasse: string;
begin
  if SameText(LabeledEditMotPasse.Text,NO_PASSE) then
    Result := FMotPasse
  else
    Result :=  MD5CryptMotDePasse(LabeledEditMotPasse.Text);
end;

function TFormEACEditServeur.GetNom: string;
begin
  Result := LabeledEditServeurNom.Text;
end;

function TFormEACEditServeur.GetUtilisateur: string;
begin
  Result := LabeledEditUtilisateur.Text;
end;

procedure TFormEACEditServeur.SetAdresse(const Value: string);
begin
  LabeledEditServeurAdresse.Text := Value;
end;

procedure TFormEACEditServeur.SetDemarrageActif(const Value: Boolean);
begin
  CheckBoxDemarrageActif.Checked := Value;
end;

procedure TFormEACEditServeur.SetMotPasse(const Value: string);
begin
  LabeledEditMotPasse.Text := Value;
end;

procedure TFormEACEditServeur.SetNom(const Value: string);
begin
  LabeledEditServeurNom.Text := Value;
end;

procedure TFormEACEditServeur.SetUtilisateur(const Value: string);
begin
  LabeledEditUtilisateur.Text := Value;
end;

procedure TFormEACEditServeur.LabeledEditServeurAdresseChange(
  Sender: TObject);
begin
  LabeledEditServeurNom.Text := LabeledEditServeurAdresse.Text; 
end;


function TFormEACEditServeur.GetSyncMode: TACSyncMode;
begin
  case ComboBoxSyncMode.ItemIndex of
  0 : Result := ACSyncModeMAJ;
  1 : Result := ACSyncModeLecture;
  2 : Result := ACSyncModeEcriture;
  3 : Result := ACSyncModeNoSupress;
  end;
end;

procedure TFormEACEditServeur.SetSyncMode(const Value: TACSyncMode);
begin
  case Value of
  ACSyncModeMAJ : ComboBoxSyncMode.ItemIndex := 0;
  ACSyncModeLecture : ComboBoxSyncMode.ItemIndex := 1;
  ACSyncModeEcriture : ComboBoxSyncMode.ItemIndex := 2;
  ACSyncModeNoSupress : ComboBoxSyncMode.ItemIndex := 3;
  end;

end;

function TFormEACEditServeur.GetSynchronisationActive: boolean;
begin
  Result := CheckBoxSynchronisationActive.Checked;
end;

procedure TFormEACEditServeur.SetSynchronisationActive(
  const Value: boolean);
begin
  CheckBoxSynchronisationActive.Checked := Value;
end;

procedure TFormEACEditServeur.SetSyncAvanceActive(const Value: boolean);
begin
  CheckBoxSyncAvanceActive.Checked := Value;
end;

function TFormEACEditServeur.GetSyncAvanceActive: boolean;
begin
     Result := CheckBoxSyncAvanceActive.Checked;
end;

function TFormEACEditServeur.GetPort: Integer;
begin
  Result := SpinEditPort.Value;
end;

procedure TFormEACEditServeur.SetPort(const Value: Integer);
begin
  SpinEditPort.Value := Value;
end;

end.
