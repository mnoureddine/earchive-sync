unit ArchivesCloudClient;

interface
uses
  SysUtils, Classes, IdBaseComponent, IdComponent, IdTCPClient,Registry, Windows,
  ArchivesCloudCommun,FileCtrl,IniFiles,Utiles,SyncClusterStream;



type
  TACCPoste = class;
  TACCServeur = class;
  TACCServeurEvent = procedure (Sender:TObject; pServeur : TACCServeur) of object;
  TACCFichierSync = class;
  TACCDossier = class;
  TACCDossierList = class;
  TACCServeurDossierEvent = procedure (Sender:TObject; pServeur : TACCServeur;Dossier : TACCDossier) of object;

  TACCFichier = class(TObject)
  private
    FTaille: Integer;
    FNom: String;
    FDateCreation: TDatetime;
    FDateModification: TDatetime;
    FData: Pointer;
    FInfo: TACFichierInfo;
    procedure SetDateCreation(const Value: TDatetime);
    procedure SetDateModification(const Value: TDatetime);
    procedure SetNom(const Value: String);
    procedure SetTaille(const Value: Integer);
    procedure SetData(const Value: Pointer);
    procedure SetInfo(const Value: TACFichierInfo);
    function GetAJour: boolean;

  protected

  public
    Dossier : TACCDossier;
    Serveur : TACCServeur;
    property Data : Pointer read FData write SetData;
    property Nom : String read FNom write SetNom;
    property DateCreation : TDatetime read FDateCreation write SetDateCreation;
    property DateModification : TDatetime read FDateModification write SetDateModification;
    property Taille : Integer read FTaille write SetTaille;
    property Info : TACFichierInfo read FInfo write SetInfo;
    property Ajour : boolean read GetAJour;
    function Chemin: string;
    function CheminLocal : string;
    constructor Create(pDossier : TACCDossier);overload;
    destructor Destroy; override;
    function RequeteLire : boolean;
    function RequeteMAJInfo: boolean;
    function RequeteSupprimer: boolean;
    function RequeteDeplacer(pDossier : TACCDossier): boolean;
    function RequeteCopier(pDossier : TACCDossier): boolean;
    function RequeteRenommer(pNouveau : String): boolean;
    function RequeteEcrire: boolean;
    function RequeteMettreAJour: boolean;
    function RequeteSynchLire : boolean;
    function RequeteSynchEcrire : boolean;
    function AjouterEnSynchronisation : TACCFichierSync;
    procedure EcrireMessage(pMessageType:TACMessageType; pMessage : string);
    function Exist : boolean;
    function FichierSynchronise : TACCFichierSync;
    function Fichier : TFileStream;
  published

  end;

  TACCFichierList = class(TList)
  private
    function Get(Index: Integer): TACCFichier;
    procedure Put(Index: Integer; Item: TACCFichier);
    function GetNom(Index: string): TACCFichier;
  public
    Dossier : TACCDossier;
    constructor Create(pDossier : TACCDossier);
    destructor Destroy; override;
    function Add(Item: TACCFichier): Integer;overload;
    function Add(pNom:string;pDateModification:TDateTime;pTaille:Integer) : TACCFichier;overload;
    function Add(pFichierInfo: TACFichierInfo) : TACCFichier;overload;
    function Extract(Item: TACCFichier): TACCFichier;
    function First: TACCFichier;
    function IndexOf(Item: TACCFichier): Integer;
    procedure Insert(Index: Integer; Item: TACCFichier);
    function Last: TACCFichier;
    procedure Delete(Index: Integer);
    function Remove(Item: TACCFichier): Integer;
    property Items[Index: Integer]: TACCFichier read Get write Put; default;
    property Noms[Index: string]: TACCFichier read GetNom;
    procedure Clear; override;
  end;

  TACCDossier = class(TObject)
  private
    FNom: string;
    FDossiers: TACCDossierList;
    FDateModification: TDateTime;
    FDateCreation: TDateTime;
    FData: Pointer;
    procedure SetDateCreation(const Value: TDateTime);
    procedure SetDateModification(const Value: TDateTime);
    procedure SetNom(const Value: string);
    procedure SetData(const Value: Pointer);
    procedure EcrireMessage(pMessageType:TACMessageType; pMessage : string);
  public
    Parent : TACCDossier;
    Serveur : TACCServeur;
    Fichiers : TACCFichierList;
    Info : TACFichierInfo;
    function Chemin : string;
    function CheminLocal : string;
    property Data : Pointer read FData write SetData;
    property Dossiers : TACCDossierList read FDossiers ;
    property Nom : string read FNom write SetNom;
    property DateCreation : TDateTime read FDateCreation write SetDateCreation;
    property DateModification : TDateTime read FDateModification write SetDateModification;
    constructor Create(pServeur: TACCServeur);overload;
    constructor Create(pParent : TACCDossier);overload;
    destructor Destroy; override;
    function RequeteLister : boolean;
    function RequeteLire : boolean;
    function RequeteSupprimer: boolean;
    function RequeteDeplacer(Destination : TACCDossier): boolean;
    function RequeteCopier(Destination : TACCDossier): boolean;
    function RequeteEcrire: boolean;
    function RequeteMAJInfo: boolean;
    function RequeteCreer(pNom :string): boolean;
    function RequeteRenommer(pNouveau : String): boolean;
    function AjouterEnSynchronisation : TACCFichierSync;
    function DossierSynchronise : TACCFichierSync;
    function Exist : boolean;
  end;

  TACCDossierList = class(TList)
  private
    FData: Pointer;
    function Get(Index: Integer): TACCDossier;
    procedure Put(Index: Integer; Item: TACCDossier);
    function GetNom(Index: string): TACCDossier;
    procedure SetData(const Value: Pointer);

  public
    Parent : TACCDossier;
    Serveur : TACCServeur;
    property Data : Pointer read FData write SetData;
    constructor Create(pParent : TACCDossier);overload;
    constructor Create(pServeur : TACCServeur);overload;
    destructor Destroy; override;
    function Add(Item: TACCDossier): Integer;overload;
    function Add(pNom:string;pDateModification : TDateTime) : TACCDossier;overload;
    function Add(pInfo : TACFichierInfo) : TACCDossier;overload;
    function Extract(Item: TACCDossier): TACCDossier;
    function First: TACCDossier;
    function IndexOf(Item: TACCDossier): Integer;
    procedure Insert(Index: Integer; Item: TACCDossier);
    function Last: TACCDossier;
    procedure Delete(Index: Integer);
    function Remove(Item: TACCDossier): Integer;
    property Items[Index: Integer]: TACCDossier read Get write Put; default;
    property Noms[Index: string]: TACCDossier read GetNom;
    procedure Clear; override;

  end;

  TACCFichierSync = class(TObject)
  private
    FTaille: Integer;
    FNom: String;
    FData: Pointer;
    FChemin: string;
    FInfo : TACFichierInfo;
    procedure SetData(const Value: Pointer);
    procedure SetChemin(const Value: string);
    function GetNom: String;
    function GetInfo: TACFichierInfo;

  protected

  public
    Serveur :TACCServeur;
    function Exist : boolean;
    function Ajour : boolean;
    property Chemin: string read FChemin write SetChemin;
    property Data : Pointer read FData write SetData;
    property Nom : String read GetNom;
    property Info : TACFichierInfo read GetInfo;
    function CheminLocal : string;
    constructor Create(pServeur :TACCServeur);overload;
    destructor Destroy; override;
    function RequeteLire :boolean;
    function Fichier : TFileStream;
    function RequeteMettreAJour:boolean;
    function RequeteSauvegarder:boolean;
  published

  end;

  TACCFichierSyncList = class(TList)
  private
    FData: Pointer;
    function Get(Index: Integer): TACCFichierSync;
    procedure Put(Index: Integer; Item: TACCFichierSync);
    function GetChemin(Index: string): TACCFichierSync;
    procedure SetData(const Value: Pointer);

  public
    Serveur : TACCServeur;
    property Data : Pointer read FData write SetData;
    constructor Create(pServeur : TACCServeur);
    destructor Destroy; override;
    function Add(Item: TACCFichierSync): Integer;overload;
    function Add(pChemin:string;pTypeDossier : boolean = false) : TACCFichierSync;overload;
    function Extract(Item: TACCFichierSync): TACCFichierSync;
    function First: TACCFichierSync;
    function IndexOf(Item: TACCFichierSync): Integer;
    procedure Insert(Index: Integer; Item: TACCFichierSync);
    function Last: TACCFichierSync;
    procedure Delete(Index: Integer);
    function Remove(Item: TACCFichierSync): Integer;
    property Items[Index: Integer]: TACCFichierSync read Get write Put; default;
    property Chemins[Index: string]: TACCFichierSync read GetChemin;
    procedure Clear; override;
    procedure SaveToFile(FileName : TFileName);
    procedure LoadFromFile(FileName : TFileName);

  end;


  TACCServeur = class(TIdTCPClient)

  private
    Ecart : integer;
    Cle : string;
    SynchronisationServeur : TACCServeur;
    FNom: string;
    FData: Pointer;
    FServiceNom: string;
    FMachineNom: string;
    FCompteurOctetsEmi : Integer;
    FCompteurFichiersEmi : Integer;
    FCompteurOctetsRecu : Integer;
    FCompteurFichiersRecu : Integer;
    FSyncAvanceActive: boolean;
    procedure SetNom(const Value: string);
    procedure SetData(const Value: Pointer);
    function EcartTab:string;
    function RequeteReussi(Code : TACCodeFonction;pMessageErreur : string): boolean;
//    function ReponseDossierLister: boolean;
//    function ReponseFichierMAJInfo(pChemin : string): boolean;
//    function ReponseDossierMAJInfo: boolean;
    function ReponseDossierSauvegarder: boolean;
    function ReceptionAvancement(Libelle : string): boolean;
    function ReceptionFichiers7z(pListeFichier7z : TStringList;pFichier:string): boolean;
    function ReceptionFichiers(pDossier:string;pListeFichier : TStrings): boolean;
    function ReceptionFichier(pFichier : String): boolean;
    function ExtractionFichier7z(pFichier:string;ListeFichier7z : TStrings): boolean;
    function ExtractionDossier7z(pDossier:string;ListeFichier7z : TStrings) : boolean;
    function AttenteOperation(pOperation : string) : boolean;
    procedure EcrireMessage(pMessageType:TACMessageType; pMessage : string);
    procedure SetServiceNom(const Value: string);
    procedure SetSyncAvanceActive(const Value: boolean);

  protected
    procedure DoOnConnected; override;
    procedure DoOnDisconnected; override;
    procedure DoDonneesChange;
    procedure Do7zProgressAdv( Sender: TObject; Filename: Widestring; MaxProgress : Int64;Position: int64 );
    procedure GenererListeDossierPartage(Liste : String);
    function TestConnexion : boolean;

  public
    Utilisateur : string;
    MotPasse : string;
    ConnexionAuto : boolean;
    Poste : TACCPoste;

//    AThread : TACCServeurThread;
    Dossiers : TACCDossierList;
    FichiersSync : TACCFichierSyncList;
    OnDonneesChange : TNotifyEvent;
    DemarrageActif : boolean;
    SyncMode : TACSyncMode;
    SynchronisationActive : boolean;
    OnAvancement : TACAvancementEven;
    TypeServeurSynchronisation : boolean;
    property CompteurOctetsEmi : Integer read FCompteurOctetsEmi;
    property CompteurFichiersEmi : Integer read FCompteurFichiersEmi;
    property CompteurOctetsRecu : Integer read FCompteurOctetsRecu;
    property CompteurFichiersRecu : Integer read FCompteurFichiersRecu;

    property SyncAvanceActive : boolean read FSyncAvanceActive write SetSyncAvanceActive;
    procedure ResetCompteurs;
    function Connexion(pUtilisateur : String;pMotPasse : string):Boolean;overload;
    function Connexion :Boolean; overload;
    procedure EnregistrerSousFichier(FichierNom : string);
    procedure OuvrirDeFichier(FichierNom : string);
    procedure Deconnexion;
    function Dupliquer : TACCServeur;
    property Data : Pointer read FData write SetData;
    property Nom : string read FNom write SetNom;
    property ServiceNom : string read FServiceNom write SetServiceNom;
    property MachineNom : string read FMachineNom;
    constructor Create(pPoste : TACCPoste; pAdresse : string;pTypeServeurSynchronisation : boolean = false);
    destructor Destroy;
    procedure DoWork(AWorkMode: TWorkMode; const ACount: Integer);
      override;
    function RequeteDossierPartageLister :boolean;
    procedure GenererContenueDossier(pChemin : string;pContenueDossier: TACContenueDossier);
    function CreerFichier(pChemin : String):TFileStream;
    function ObtenirAdresse(pChemin : string) : string;
    function RequeteMessage(pMessage : string):boolean;
    function RequeteFichierLire(pChemin : string):boolean;
    function RequeteFichierMAJInfo(pChemin : string):boolean;
    function RequeteFichierSupprimer(pChemin : string):boolean;
    function RequeteFichierDeplacer(pChemin : string;pDestination :String):boolean;
    function RequeteFichierCopier(pChemin : string;pDestination :String):boolean;
    function RequeteFichierEcrire(pChemin : string):boolean;
    function RequeteFichierRenommer(pChemin : string;pNouveau:string):boolean;
    function RequeteDossierLister(pChemin : string):boolean;
    function RequeteDossierLire(pChemin : string):boolean;
    function RequeteDossierSupprimer(pChemin : string):boolean;
    function RequeteDossierDeplacer(pChemin : string;pDestination :String):boolean;
    function RequeteDossierCopier(pChemin : string;pDestination : string):boolean;
    function RequeteDossierEcrire(pChemin : string):boolean;
    function RequeteDossierCreer(pChemin : string):boolean;
    function RequeteDossierMAJInfo(pChemin : string):boolean;
    function RequeteConnexion(pUtilisateur : string;pMotPasse : string):boolean;
    function RequeteDossierSauvegarder(pChemin : string):boolean;
    function RequeteDossierRenommer(pChemin : string;pNouveau:string):boolean;
    function RequeteExtinctionClient : boolean;
    function RequeteSyncFichierLire(pChemin:string):boolean;
    function RequeteSyncFichierEcrire(pChemin:string):boolean;
    function CreationDossier7z(pDossier:string;ListeFichiers7z:TStrings):boolean;
    function CreationFichier7z(pFichier:string;ListeFichiers7z:TStrings):boolean;
    function EnvoyerFichier(pFichier:string):boolean;
    function EnvoyerFichiers(pListeFichiers:TStrings):boolean;
    procedure AppliquerSynchronisation(ContenueDossier:TACContenueDossier);
    procedure Synchronisation;
    procedure MettreAjour;
    procedure Sauvegarder;


  published

  end;

  TACCServeurList = class(TList)
  private

    function Get(Index: Integer): TACCServeur;
    procedure Put(Index: Integer; Item: TACCServeur);
    function GetAdresse(Index: String): TACCServeur;
    function GetNom(Index: String): TACCServeur;


  public
    Poste  : TACCPoste; 
    constructor Create(pPoste  : TACCPoste);
    destructor Destroy; override;
    function Add(Item: TACCServeur): Integer;overload;
    function Add(pAdresse : string) : TACCServeur;overload;
    function Extract(Item: TACCServeur): TACCServeur;
    function First: TACCServeur;
    function IndexOf(Item: TACCServeur): Integer;
    procedure Insert(Index: Integer; Item: TACCServeur);
    function Last: TACCServeur;
    procedure Delete(Index: Integer);
    function Remove(Item: TACCServeur): Integer;
    property Items[Index: Integer]: TACCServeur read Get write Put; default;
    property Adresse[Index: String]: TACCServeur read GetAdresse;
    property Nom[Index: String]: TACCServeur read GetNom;
    procedure Clear; override;

  end;

  TACCPoste = class(TObject)
  private
    FDossierACC: string;
    FTimeoutConnexion: integer;
    FSynchronisationActive: boolean;

    FDossierTemp: string;
    procedure SetDossierACC(const Value: string);
    procedure SetTimeoutConnexion(const Value: integer);
    procedure SetSynchronisationActive(const Value: boolean);

    procedure SetDossierTemp(const Value: string);

  protected

  public
    Serveurs : TACCServeurList;
    OnMessage : TACMessageEvent;
    OnServeurAjouter : TACCServeurEvent;
    OnServeurDossierPartageLister : TACCServeurEvent;
    OnServeurDossierLister : TACCServeurDossierEvent;
    property SynchronisationActive : boolean read FSynchronisationActive write SetSynchronisationActive;
    property TimeoutConnexion : integer read FTimeoutConnexion write SetTimeoutConnexion;
    property DossierACC : string read FDossierACC write SetDossierACC;
    property DossierTemp : string read FDossierTemp write SetDossierTemp;
    procedure FermerLesConnexions;
    procedure Synchronisation;
    procedure MettreAJour;
    function DemandeExtinction:boolean;
    procedure Sauvegarder;
    procedure EcrireMessage(pServeur:TACCServeur;pMessageType:TACMessageType; pMessage : string);overload;
    procedure EcrireMessage(pMessageType:TACMessageType; pMessage : string);overload;
    constructor Create(pDossierACC:String);
    destructor Destroy;
    function ServeursTestConnexion:boolean;

  published

  end;

implementation

uses ComObj, IdTCPConnection, DateUtils;



{ TACCServeurList }

function TACCServeurList.Add(Item: TACCServeur): Integer;
begin
  result := inherited Add(Item);
  if Assigned(Poste.OnServeurAjouter) then
    Poste.OnServeurAjouter(Poste,Item);

end;

function TACCServeurList.Add(pAdresse : string): TACCServeur;
begin
  Result := nil;
  if GetAdresse(pAdresse) = nil then
  begin
    Result := TACCServeur.Create(Poste,pAdresse);
    Add(Result);
  end;
end;

procedure TACCServeurList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;    
  inherited;

end;

constructor TACCServeurList.Create(pPoste  : TACCPoste);
begin
  inherited create;
  Poste :=pPoste;

end;

procedure TACCServeurList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TACCServeurList.Destroy;
begin
  Clear;
  inherited;
end;

function TACCServeurList.Extract(Item: TACCServeur): TACCServeur;
begin
  result := inherited Extract(Item);
end;

function TACCServeurList.First: TACCServeur;
begin
  Result := inherited First;
end;

function TACCServeurList.Get(Index: Integer): TACCServeur;
begin
  Result := inherited Get(Index);
end;

function TACCServeurList.GetAdresse(Index: String): TACCServeur;
var
  i : integer;
begin
  result := nil;
  for i := 0 to Count - 1 do
    if SameText(Index,Items[i].Host)then
    begin
      result := Items[i];
      exit;
    end;

end;

function TACCServeurList.GetNom(Index: String): TACCServeur;
var
  i : integer;
begin
  result := nil;
  for i := 0 to Count - 1 do
    if SameText(Index,Items[i].FNom)then
    begin
      result := Items[i];
      exit;
    end;

end;

function TACCServeurList.IndexOf(Item: TACCServeur): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TACCServeurList.Insert(Index: Integer; Item: TACCServeur);
begin
  inherited Insert(Index,Item);
end;

function TACCServeurList.Last: TACCServeur;
begin
  Result := inherited Last;
end;

procedure TACCServeurList.Put(Index: Integer; Item: TACCServeur);
begin
  inherited Put(Index,Item);

end;

function TACCServeurList.Remove(Item: TACCServeur): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;


{ TEACPoste }

constructor TACCPoste.Create(pDossierACC:String);
begin

  DossierACC := pDossierACC;
  if not DirectoryExists(DossierACC) then
    ForceDirectories(DossierACC);

  OnMessage := nil;
  OnServeurAjouter := nil;
  OnServeurDossierPartageLister := nil;
  OnServeurDossierLister := nil;
  FTimeoutConnexion := 5000;
  FDossierTemp := GetTempDirectory+'\ArchiveSync';
  Serveurs := TACCServeurList.Create(self);
end;

function TACCPoste.DemandeExtinction : boolean;
var
  i : integer;
begin

  Result := true;
  for i := 0 to Serveurs.Count - 1 do
    if not Serveurs[i].RequeteExtinctionClient then
      Result := false;

end;

destructor TACCPoste.Destroy;
begin
  Serveurs.Free;
  inherited;
end;


{ TACCServeur }


function TACCServeur.Connexion(pUtilisateur : String;pMotPasse : string):boolean;
begin
  Utilisateur := pUtilisateur;
  MotPasse := pMotPasse;
  result := Connexion;
end;

constructor TACCServeur.Create(pPoste : TACCPoste;pAdresse: string;pTypeServeurSynchronisation : boolean = false);
begin
  inherited Create(nil);
  Host := pAdresse;
  Port := AC_SERVEUR_TCP_PORT;
  Poste := pPoste;
  FNom := pAdresse;
  FData := nil;
  Dossiers := TACCDossierList.Create(Self);
  FichiersSync := TACCFichierSyncList.Create(Self);
  OnDonneesChange := nil;
  OnAvancement := nil;
  DemarrageActif := false;
  SyncAvanceActive := false;
//  AThread := nil;
  TypeServeurSynchronisation := pTypeServeurSynchronisation;
  if TypeServeurSynchronisation then
  begin
    SyncMode := ACSyncModeMAJ;
    ServiceNom := 'synchro';
  end
  else
  begin
    SyncMode := ACSyncModeMAJ;
    FServiceNom := 'explore';
    SynchronisationServeur := TACCServeur.Create(Poste,Host,true);
  end;
  ResetCompteurs;
  EcrireMessage(ACMessageRequete,'Cr�ation du serveur "'+pAdresse+'"');

end;


function TACCServeur.CreerFichier(pChemin: String): TFileStream;
begin
  pChemin := Poste.DossierACC +'\'+Nom+'\'+pChemin;
  if Not DirectoryExists(ExtractFilePath(pChemin)) then
    ForceDirectories(ExtractFilePath(pChemin));
  Result := TFileStream.Create(pChemin,fmCreate);
end;

destructor TACCServeur.Destroy;
begin
//  AThread.Terminate;
//  AThread.Free;
  Dossiers.Free;
  FichiersSync.Free;
  if SynchronisationServeur <> nil then;
    SynchronisationServeur.Free;

  inherited;
end;




procedure TACCPoste.EcrireMessage(pMessageType:TACMessageType; pMessage: string);
begin
  pMessage :=   DateTimeToStr(now)+#9+pMessage;
  if Assigned(OnMessage) then
    OnMessage(Self,pMessageType,pMessage);
end;

procedure TACCPoste.EcrireMessage(pServeur: TACCServeur; pMessageType:TACMessageType; pMessage: string);
begin
  EcrireMessage(pMessageType,pServeur.Nom+'-'+pServeur.ServiceNom+#9+' : '+pMessage);
end;

procedure TACCPoste.FermerLesConnexions;
var
  i : Integer;
begin
  for i  := 0 to Serveurs.Count - 1 do
    Serveurs[i].DeConnexion;
end;

procedure TACCPoste.MettreAJour;
var
  i : integer;
  Serveur  : TACCServeur;

begin
  for i := 0 to Serveurs.Count - 1 do
  begin
    Serveurs[i].MettreAjour;

  end;

end;

procedure TACCPoste.Sauvegarder;
var
  i : integer;
begin
  for i := 0 to Serveurs.Count - 1 do
    Serveurs[i].Sauvegarder;

end;

function TACCPoste.ServeursTestConnexion: boolean;
var
  i : integer;
begin
  Result := true;
  for i := 0 to Serveurs.Count - 1 do
    if not Serveurs[i].TestConnexion then
      Result := false;
end;


procedure TACCPoste.SetDossierACC(const Value: string);
begin
  FDossierACC := Value;
end;

procedure TACCServeur.DoOnConnected;
begin
  inherited;
    WriteLn(LocalName+'-'+FServiceNom);
    WriteSmallInt(PROTOCOLE_VERSION.Valeur);
//    SeveurProtocole.Valeur := ReadSmallInt;
  //  if ACMemeVersionProtocole(PROTOCOLE_VERSION,SeveurProtocole) then
    if ACRequeteResult(ACReqReussi) = ReadInteger then
    begin
      if RequeteConnexion(Utilisateur,MotPasse) then
        begin
          EcrireMessage(ACMessageRequete,'Identification accept�.');

        end
        else
        begin
          EcrireMessage(ACMessageErreur,'Identification refus�, mot de passe ou utilisateur erron�.');
          Disconnect;
        end;
      end
      else
      begin
          EcrireMessage(ACMessageErreur,'Version du protocole serveur incompatible, veuillez mettte � jour.');
          Disconnect;
      end;

end;

procedure TACCServeur.DoOnDisconnected;
begin
  inherited;
  if SynchronisationServeur <> nil then
    SynchronisationServeur.DeConnexion;

end;

procedure TACCServeur.DoWork(AWorkMode: TWorkMode; const ACount: Integer);
begin
  inherited;

end;

procedure TACCServeur.GenererContenueDossier(pChemin:string; pContenueDossier: TACContenueDossier);
var
  Liste : TStringList;
  Dossier : TACCDossier;
  i : Integer;

begin

  Dossier := nil;
  Liste := TStringList.Create;
//  ExtractStrings(['\'],[' '],pChar(pChemin),Liste);
  ExtractListFromLine('\',pChemin,Liste);
  for i := 0 to Liste.Count - 1 do
  begin
      if Dossier = nil then
      begin
        Dossier := Dossiers.Noms[Liste[i]];
        if Dossier = nil then
          exit;
      end
      else
      begin
        Dossier := Dossier.Dossiers.Noms[Liste[i]];
        if Dossier = nil then
          exit;
      end;

  end;

  Dossier.Dossiers.Clear;
  Dossier.Fichiers.Clear;

  for i := 0 to pContenueDossier.Dossiers.Count -1 do
  begin
    Dossier.Dossiers.Add(pContenueDossier.Dossiers[i].Info);
  end;

  for i := 0 to pContenueDossier.Fichiers.Count -1 do
  begin
    Dossier.Fichiers.Add(pContenueDossier.Fichiers[i].Info);
  end;

  if Assigned(Poste.OnServeurDossierLister) then
    Poste.OnServeurDossierLister(Poste,Self,Dossier);


end;

procedure TACCServeur.GenererListeDossierPartage(Liste: String);
var
  lListe : TStrings;
  i : Integer;
begin
  lListe := TStringList.Create;
//  ExtractStrings(['|'],[' '],pChar(Liste),lListe);
  ExtractListFromLine('|',Liste,lListe);
  Dossiers.Clear;
  for i := 0 to lListe.Count - 1 do
  begin
    Dossiers.Add(lListe[i],0);
  end;
  lListe.Free;
  if Assigned(Poste.OnServeurDossierPartageLister) then
    Poste.OnServeurDossierPartageLister(Poste,Self);
end;

(*
function TACCServeur.ReponseDossierLister : boolean;
var
  Chemin : string;
  Liste : TStrings;
  Nombre : Integer;
  Contenue : TACFichierInfoList;
  ContenueDossier : TACContenueDossier;
  i : integer;

begin
  result := TACRequeteResult(ReadInteger) = ACReqReussi;
  if Result then
  begin
    Chemin := ReadLn;
    Nombre := ReadInteger;

    EcrireMessage('Contenue de '+Chemin +' Nombre = '+IntToStr(Nombre));
    SetLength(Contenue,Nombre);
    ReadBuffer(Contenue[0],SizeOf(TACFichierInfo)*Nombre);

    ContenueDossier := TACContenueDossier.Create('');
    ContenueDossier.ImporterListeFichierInfo(Contenue,Nombre);

    GenererContenueDossier(Chemin,ContenueDossier);
//  FreeMemory(Contenue);
    ContenueDossier.Free;
    EcrireMessage('R�ception de la liste du contenue');
  end
  else
  begin
    EcrireMessage('Echec lors de la r�ception de la liste du contenue');
  end;


end;
*)


function TACCServeur.ExtractionFichier7z(pFichier:string;ListeFichier7z : TStrings): boolean;
var
  FichierReception : string;
begin
  inc(Ecart);
  result := false;

  try
    EcrireMessage(ACMessageInfo,Format('1/2 Extraction des %d fichier(s) zip du fichier "%s"',
          [ListeFichier7z.Count,ExtractFileName(pFichier)]));
    FichierReception := ListeFichier7z[0];

    if Not DirectoryExists(ExtractFilePath(pfichier)) then
      ForceDirectories(ExtractFilePath(pfichier));

    if not DeSevenZipperFichier(ExtractFilePath(pfichier),FichierReception,Do7zProgressAdv,Cle) then
    begin
      EcrireMessage(ACMessageErreur,'1/2 Erreur lors de l''extraction du fichier');
      exit;
    end;

    DeleteDirectory(ExtractFilePath(FichierReception));
    EcrireMessage(ACMessageRequete,'2/2 Extraction du fichier termin�');
    result := true;
  except
    EcrireMessage(ACMessageErreur,'2/2 Erreur lors de la r�ception du dossier');
  end;
end;

function TACCServeur.RequeteDossierPartageLister : boolean;
begin
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 Liste des dossiers partag�s');
    WriteInteger(ACCodeFonction(ACCFDossiersPartageLister));

    if not RequeteReussi(ACCFDossiersPartageLister,
          '1/2 Erreur lors de la lecture des fichiers partag�s.') then
      exit;
    EcrireMessage(ACMessageInfo,'1/2 R�ception des dossiers partag�s termin�.');
    GenererListeDossierPartage(Self.ReadLn);
    result := true;

  except
    EcrireMessage(ACMessageErreur,'2/2 Echec lors de la r�ception de la liste des dossiers partag�s');
    
  end;
end;

procedure TACCServeur.SetData(const Value: Pointer);
begin
  FData := Value;
end;

procedure TACCServeur.SetNom(const Value: string);
begin
  FNom := Value;
end;


procedure TACCPoste.SetDossierTemp(const Value: string);
begin

  FDossierTemp := Value;
end;

procedure TACCPoste.SetSynchronisationActive(const Value: boolean);
var
  i  : integer;
begin
 FSynchronisationActive := Value;

end;

procedure TACCPoste.SetTimeoutConnexion(const Value: integer);
begin
  FTimeoutConnexion := Value;
end;

function TACCServeur.ObtenirAdresse(pChemin: string): string;
begin
   Result := Poste.DossierACC +'\'+Nom+'\'+pChemin;
end;

procedure TACCServeur.DeConnexion;
begin
  if Connected then
  begin
    Disconnect;
    EcrireMessage(ACMessageRequete,'D�connect�');
  end;

end;



(*
function TACCServeur.ReponseDossierMAJInfo : boolean;
var
  ContenueLocal : TACContenueDossier;
  ContenueDistant : TACContenueDossier;
  Chemin : string;
  Adresse : string;
  Nombre : Integer;
  Taille : Integer;
  ContenueDistantTemp : TACFichierInfoList;
begin
  result := TACRequeteResult(ReadInteger) = ACReqReussi;
  if Result then
  begin
    Chemin := ReadLn;
    Nombre := ReadInteger;
    EcrireMessage('2/6 r�ception des informations de '+IntToStr(Nombre)+' Fichier(s) du dossier "'+Chemin+'" ');
    SetLength(ContenueDistantTemp,Nombre);
    Taille := SizeOF(TACFichierInfo)*Nombre;
    ReadBuffer(ContenueDistantTemp[0],Taille);
    ContenueDistant := TACContenueDossier.Create('');
    ContenueDistant.SyncMode := SyncMode;
    ContenueDistant.ImporterListeFichierInfo(ContenueDistantTemp,Nombre);

    EcrireMessage('3/6 Actualisation des informations local');
    ContenueLocal := TACContenueDossier.Create(ObtenirAdresse(Chemin));
    ContenueLocal.SyncMode := SyncMode;
    ContenueLocal.Generer(true);

    //  ACContenueDossier(Adresse,ContenueLocal,true);
    EcrireMessage('4/6 Comparaison des contenues');
    ContenueLocal.Comparer(ContenueDistant);
    EcrireMessage('5/6 Synchronisation en local');
    ContenueLocal.Adresse := Chemin;
    AppliquerSynchronisation(ContenueLocal);

    ContenueLocal.Free;
    ContenueDistant.Free;
//  FreeMemory(ContenueDistantTemp);

  end
  else
  begin
    EcrireMessage('Echec du serveur lors de la mise � jour des informations');
  end;
end;
*)
(*
function TACCServeur.ReponseFichierMAJInfo(pChemin : string) : boolean;
var
  Adresse : string;
  FichierInfoLocal : TACFichierInfo;
  FichierInfoDistant : TACFichierInfo;

begin
  Result := false;
  try
    EcrireMessage('1/4 Reception des informations du fichier "'+ExtractFileName(pChemin)+'"');
    ReadBuffer(FichierInfoDistant,SizeOf(TACFichierInfo));
    Adresse :=ObtenirAdresse(pChemin);
    EcrireMessage('2/4 Synchronisation de fichier en mode '+ACSyncModeToStr(SyncMode));
    FichierInfoLocal := ACLireFichierInfo(Adresse);
    case SyncMode of
    ACSyncModeMAJ :
    begin
      case ACCompareDateTime(FichierInfoLocal.Date.Modification,
          FichierInfoDistant.Date.Modification) of
        1 :
        begin
          EcrireMessage('3/4 Fichier local recent.');
          if not RequeteFichierEcrire(pChemin) then
            exit;
        end;
        0 :
          EcrireMessage('3/4 Fichier � jour.');
        -1 :
        begin
          EcrireMessage('3/4 Fichier local obsol�te');
          if not RequeteFichierLire(pChemin) then
            exit;
        end;
      end;
    end;
    ACSyncModeLecture :
        if not ACSameDateTime(FichierInfoLocal.Date.Modification,
                FichierInfoDistant.Date.Modification) then
        begin
          EcrireMessage('3/4 Fichier diff�rent');
          if not RequeteFichierLire(pChemin) then
            exit;
        end;
    ACSyncModeEcriture :
        if not ACSameDateTime(FichierInfoLocal.Date.Modification,
                FichierInfoDistant.Date.Modification) then
        begin
          EcrireMessage('3/4 Fichier diff�rent');
          if not RequeteFichierEcrire(pChemin) then
            exit;
        end;

    end;
    EcrireMessage('5/4 Mise � jour du fichier termin�');
    result := true;

  except
    EcrireMessage('5/5 Echec lors de la r�ception des informations du fichier.');

  end;
end;
 *)


function TACCServeur.ExtractionDossier7z(pDossier:string;ListeFichier7z : TStrings) : boolean;
var
  FichierReception : string;
begin
  inc(Ecart);
  result := false;

  try
    EcrireMessage(ACMessageInfo,Format('1/2 Extraction des %d fichier(s) zip du dossier "%s"',
          [ListeFichier7z.Count,ExtractFileName(pDossier)]));
    FichierReception := ListeFichier7z[0];

    if Not DirectoryExists(ExtractFilePath(pDossier)) then
      ForceDirectories(ExtractFilePath(pDossier));

    if not DeSevenZipperDossier(ExtractFilePath(pDossier),FichierReception,Do7zProgressAdv,Cle) then
    begin
      EcrireMessage(ACMessageErreur,'1/2 Erreur lors de l''extraction du dossier');
      exit;
    end;

    DeleteDirectory(ExtractFilePath(FichierReception));
    EcrireMessage(ACMessageRequete,'2/2 Extraction du dossier termin�');
    result := true;
  except
    EcrireMessage(ACMessageErreur,'2/2 Erreur lors de la r�ception du dossier');
  end;

end;

procedure TACCServeur.EcrireMessage(pMessageType:TACMessageType; pMessage: string);
begin
  Poste.EcrireMessage(self,pMessageType, EcartTab+pMessage);
end;

function TACCServeur.RequeteDossierCopier(pChemin, pDestination: string):boolean;
begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 Copier le dossier "'+pChemin+'"');
    EcrireMessage(ACMessageRequete,'    Vers "'+pDestination+'".');
    WriteInteger(ACCodeFonction(ACCFDossierCopier));
    WriteLn(pChemin);
    WriteLn(pDestination);
    if not RequeteReussi(ACCFDossierCopier,'1/2 Echec lors de la copie du dossier') then
      exit;

    EcrireMessage(ACMessageInfo,'2/2 Copie du dossier termin�');
    result := true;

  except
    EcrireMessage(ACMessageErreur, '2/2 Copie du dossier impossible');
  end;

end;

function TACCServeur.RequeteDossierDeplacer(pChemin, pDestination: String):boolean;
begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 D�placer le dossier "'+pChemin+'"');
    EcrireMessage(ACMessageRequete,'    Vers "'+pDestination+'".');
    WriteInteger(ACCodeFonction(ACCFDossierDeplacer));
    WriteLn(pChemin);
    WriteLn(pDestination);

    if not RequeteReussi(ACCFDossierDeplacer,'1/2 Echec lors du d�placement du dossier.') then
      exit;

    EcrireMessage(ACMessageInfo,'2/2 D�placement du dossier termin�.');
    Result := true;

  except
    EcrireMessage(ACMessageInfo,'2/2 D�placement du dossier impossible.');
  end;

end;

function TACCServeur.RequeteDossierEcrire(pChemin:string):boolean;
var
  Dossier : string;
  ListeFichier7z : TStringList;
begin
  Ecart := 0;
   result := false;
   if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    // requete d''ecriture du dossier
    EcrireMessage(ACMessageRequete,format('1/5 Ecrire le dossier "%s"',[ExtractFileName(pChemin)]));
    Dossier := ObtenirAdresse(pChemin);
    if not DirectoryExists(Dossier) then
    begin
      EcrireMessage(ACMessageErreur,'1/5 Erreur dossier inexistant');
      exit;
    end;

    // Creation des fichiers 7z
    EcrireMessage(ACMessageInfo,'2/5 Creation des fichiers 7z');
    ListeFichier7z := TStringList.Create;
    if not CreationDossier7z(Dossier,ListeFichier7z) then
    begin
      EcrireMessage(ACMessageErreur,'2/5 Erreur lors de la creation des fichiers 7z');
      exit;
    end;

    // Envoie des fichiers 7z
    EcrireMessage(ACMessageInfo,format('3/5 Envoie des %d fichier(s) 7z du ficher "%s"',
                    [ListeFichier7z.Count,ExtractFileName(pChemin)]));
    WriteInteger(ACCodeFonction(ACCFFichierEcrire));
    WriteLn(pChemin);

    if not EnvoyerFichiers(ListeFichier7z)then
    begin
      EcrireMessage(ACMessageErreur,'3/5 Erreur lors de l''envoie des fichiers 7z');
      exit;
    end;

    EcrireMessage(ACMessageInfo,'4/5 Envoie des fichiers 7z termin� ');
    if not RequeteReussi(ACCFFichierEcrire,'4/5 Erreur � l''ecriture du dossier') then
      exit;

    EcrireMessage(ACMessageInfo,'5/5 Ecriture du dossier termin�');
    Result := true;

  except
    EcrireMessage(ACMessageErreur,'5/5 Erreur � l''ecriture du dossier');

  end;
end;

function TACCServeur.RequeteDossierLire(pChemin: string):boolean;
var
  ListeFichier7z : TStringList;

begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    // Envoyer la requete
    EcrireMessage(ACMessageRequete,'1/4 Lire le dossier "'+pChemin+'"');
    WriteInteger(ACCodeFonction(ACCFDossierLire));
    WriteLn(pChemin);


    if not RequeteReussi(ACCFDossierLire,'1/4 Erreur Lecture du dossier') then
      exit;

    // reception du dossier
    EcrireMessage(ACMessageInfo,'2/4 Reception du dossier');
    ListeFichier7z := TStringList.Create;
    if not ReceptionFichiers7z(ListeFichier7z,ObtenirAdresse(pChemin))then
    begin
      EcrireMessage(ACMessageErreur,'2/4 Erreur lors de la reception du dossier');
      exit;
    end;

(*    // Extraction du dossier
    EcrireMessage(Format('3/4 Extraction des %d fichier(s) 7z du dossier "%s"',
                [ListeFichier7z.Count,ExtractFileName(pChemin)]));
    if not ExtractionDossier7z(ObtenirAdresse(pChemin),ListeFichier7z)then
    begin
      EcrireMessage('3/4 Erreur lors de l''extraction du dossier');
      exit;
    end;
    *)

    EcrireMessage(ACMessageInfo,'4/4 Lecture du dossier termin�');
    result := true;
  except
    EcrireMessage(ACMessageErreur,'4/4 Erreur lors de la reception du dossier');

  end;

end;


function TACCServeur.RequeteDossierLister(pChemin: string):boolean;
var
  Nombre : integer;
  Contenue : TACFichierInfoList;
  ContenueDossier : TACContenueDossier;

begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/3 Lister le contenue du chemin "'+pChemin+'".');
    WriteInteger(ACCodeFonction(ACCFDossierLister));
    WriteLn(pChemin);

    if not RequeteReussi(ACCFDossierLister,
            '1/2 Echec lors du liste du chemin.') then
                exit;

    Nombre := ReadInteger;

    EcrireMessage(ACMessageInfo,'2/3 Lecture du contenue du dossier.');
    SetLength(Contenue,Nombre);
    ReadBuffer(Contenue[0],SizeOf(TACFichierInfo)*Nombre);

    ContenueDossier := TACContenueDossier.Create('');
    ContenueDossier.ImporterListeFichierInfo(Contenue,Nombre);

    GenererContenueDossier(pChemin,ContenueDossier);
    //  FreeMemory(Contenue);
    ContenueDossier.Free;
    EcrireMessage(ACMessageInfo,'3/3 R�ception de la liste du contenue');
    result := true;

  except
    EcrireMessage(ACMessageErreur,'3/3 Impossible de lister le contenue du dossier');
  end;


end;

function TACCServeur.RequeteDossierMAJInfo(pChemin: string):boolean;
var
  ContenueLocal : TACContenueDossier;
  ContenueDistant : TACContenueDossier;
  Adresse : string;
  Nombre : Integer;
  Taille : Integer;
  ContenueDistantTemp : TACFichierInfoList;

begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageInfo,
            Format('1/6 Mise � jour des informations du dossier "%s" en mode "%s".',
                                            [pChemin,
                                            ACSyncModeToStr(SyncMode)]));
    WriteInteger(ACCodeFonction(ACCFDossierMAJInfo));
    WriteLn(pChemin);
    if not RequeteReussi(ACCFDossierMAJInfo,'1/6 Erreur lors de la mise a jour de information du dossier.') then
      exit;

    Nombre := ReadInteger;
    EcrireMessage(ACMessageInfo,'2/6 r�ception des informations de '+IntToStr(Nombre)+' Fichier(s) du dossier ');
    SetLength(ContenueDistantTemp,Nombre);
    Taille := SizeOF(TACFichierInfo)*Nombre;
    ReadBuffer(ContenueDistantTemp[0],Taille);

    ContenueDistant := TACContenueDossier.Create('');
    ContenueDistant.SyncMode := SyncMode;
    ContenueDistant.SyncAvanceMode := SyncAvanceActive;
    ContenueDistant.ImporterListeFichierInfo(ContenueDistantTemp,Nombre);

    EcrireMessage(ACMessageInfo,'3/6 Actualisation des informations local');
    ContenueLocal := TACContenueDossier.Create(ObtenirAdresse(pChemin));
    ContenueLocal.SyncMode := SyncMode;
    ContenueLocal.SyncAvanceMode := SyncAvanceActive;
    ContenueLocal.Generer(true);

    //  ACContenueDossier(Adresse,ContenueLocal,true);
    EcrireMessage(ACMessageInfo,'4/6 Comparaison des contenues');
    ContenueLocal.Comparer(ContenueDistant);

    EcrireMessage(ACMessageInfo,'5/6 Synchronisation en local');
    ContenueLocal.Adresse := pChemin;
    AppliquerSynchronisation(ContenueLocal);

    ContenueLocal.Free;
    ContenueDistant.Free;

    EcrireMessage(ACMessageInfo,'6/6 Mise a jour des informations termin�.');
    result := true;

  except
    EcrireMessage(ACMessageErreur,'2/2 Erreur lors de la mise a jour des informations du dossier impossible.');
  end;

end;

function TACCServeur.RequeteDossierSupprimer(pChemin: string):boolean;
begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 Supression sur le serveur du dossier "'+pChemin+'".');
    WriteInteger(ACCodeFonction(ACCFDossierSupprimer));
    WriteLn(pChemin);

    if not RequeteReussi(ACCFDossierSupprimer,'1/2 Echec lors de la suppression du dossier sur le serveur.') then
      exit;

    EcrireMessage(ACMessageInfo,'2/2 Suppresion du dossier termin�.');
    result := true;

  except
    EcrireMessage(ACMessageErreur,'2/2 Suppression du dossier sur le serveur impossible.');
  end;

end;

function TACCServeur.RequeteFichierCopier(pChemin, pDestination: String):boolean;
begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 Copier sur le serveur le fichier "'+pChemin+'"');
    EcrireMessage(ACMessageRequete,'    Vers "'+pDestination+'".');
    WriteInteger(ACCodeFonction(ACCFFichierCopier));
    WriteLn(pChemin);
    WriteLn(pDestination);
    if not RequeteReussi(
              ACCFFichierCopier,
              '1/2 Echec lors de la copie du fichier sur le serveur.')
          then
            exit;

    result := true;
    EcrireMessage(ACMessageInfo,'2/2 Copie du fichier termin�.');
  except
    EcrireMessage(ACMessageErreur,'2/2 Copie du fichier impossible sur le serveur.');
  end;

end;

function TACCServeur.RequeteFichierDeplacer(pChemin, pDestination: String):boolean;
begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 D�placement sur le serveur le fichier "'+pChemin+'"');
    EcrireMessage(ACMessageRequete,'    Vers "'+pDestination+'".');
    WriteInteger(ACCodeFonction(ACCFFichierDeplacer));
    WriteLn(pChemin);
    WriteLn(pDestination);
    if not
        RequeteReussi(ACCFFichierDeplacer,
            '1/2 Echec lors du d�placement du fichier sur le serveur.') then
        exit;
    EcrireMessage(ACMessageInfo,'2/2 D�placement du fichier termin�.');
    result := true;

  except
    EcrireMessage(ACMessageErreur,'2/2 D�placement du fichier impossible sur le serveur.');
  end;

end;

function TACCServeur.RequeteFichierEcrire(pChemin :string):boolean;
var
  Fichier : string;
  ListeFichier7z : TStringList;
begin
  Ecart := 0;

  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    // requete d''ecriture de fichier
    EcrireMessage(ACMessageRequete,format('1/4 Ecrire le fichier "%s".',[ExtractFileName(pChemin)]));
    Fichier := ObtenirAdresse(pChemin);
    if not FileExists(Fichier) then
    begin
      EcrireMessage(ACMessageErreur,'1/4 Erreur, fichier inexistant.');
      exit;
    end;

    // Creation des fichiers 7z
    EcrireMessage(ACMessageInfo,'2/4 Creation des fichiers 7z.');
    ListeFichier7z := TStringList.Create;
    if not CreationFichier7z(Fichier,ListeFichier7z) then
    begin
      EcrireMessage(ACMessageErreur,'2/4 Erreur lors de la creation des fichiers 7z.');
      exit;
    end;

    // Envoie des fichiers 7z
    EcrireMessage(ACMessageInfo,format('3/4 Envoie des %d fichier(s) 7z du ficher "%s".',
                              [ListeFichier7z.Count,ExtractFileName(pChemin)]));
    WriteInteger(ACCodeFonction(ACCFFichierEcrire));
    WriteLn(pChemin);

    if not EnvoyerFichiers(ListeFichier7z)then
    begin
      EcrireMessage(ACMessageErreur,'2/4 Erreur lors de l''envoie des fichiers 7z.');
      exit;
    end;

    EcrireMessage(ACMessageInfo,'3/4 Envoie des fichiers 7z termin�.');
    if not RequeteReussi(
                ACCFFichierEcrire,
                '3/3 Erreur � l''ecriture du fichier.') then
          exit;

    EcrireMessage(ACMessageInfo,'4/4 Ecriture du fichier termin�.');
    Result := true;

  except
    EcrireMessage(ACMessageErreur,'4/4 Erreur � l''ecriture du fichier.');

  end;

end;

function TACCServeur.RequeteFichierLire(pChemin: string):boolean;
var
  ListeFichier7z : TStringList;

begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try

    EcrireMessage(ACMessageInfo,'***** Requete *****');
    // Envoyer la requete
    EcrireMessage(ACMessageRequete,format('1/4 Lire du fichier "%s".',[ExtractFileName(pChemin)]));
    WriteInteger(ACCodeFonction(ACCFFichierLire));
    WriteLn(pChemin);
    if not RequeteReussi(
              ACCFFichierLire,
              '1/4 Erreur lors de la lecture du fichier.') then
      exit;

    // reception du fichier
    EcrireMessage(ACMessageInfo,'2/4 R�ception du fichier.');
    ListeFichier7z := TStringList.Create;
    if not ReceptionFichiers7z(ListeFichier7z,ObtenirAdresse(pChemin))then
    begin
      EcrireMessage(ACMessageErreur,'2/4 Erreur lors de la r�ception du fichier.');
      exit;
    end;
    Ecart := 0;
  (*
    // Extraction du fichier
    EcrireMessage(Format('3/4 Extraction des %d fichier(s) 7z du fichier "%s".',
                [ListeFichier7z.Count,ExtractFileName(pChemin)]));
    if not ExtractionFichier7z(ObtenirAdresse(pChemin),ListeFichier7z)then
    begin
      EcrireMessage('3/4 Erreur lors de l''extraction du fichier.');
      exit;
    end;
    Ecart := 0;
    *)

    EcrireMessage(ACMessageInfo,'4/4 Lecture du fichier termin�.');
    result := true;

  except
    EcrireMessage(ACMessageErreur,'4/4 Erreur lors de la r�ception du fichier.');

  end;

end;

function TACCServeur.RequeteFichierMAJInfo(pChemin: string):boolean;
var
  Adresse : string;
  FichierInfoLocal : TACFichierInfo;
  FichierInfoDistant : TACFichierInfo;

begin
  Ecart := 0;

  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,
            format('1/4 Mise a jour du fichier "%s" en mode "%s"',
            [ExtractFileName(pChemin),
            ACSyncModeToStr(SyncMode)]));
    WriteInteger(ACCodeFonction(ACCFFichierMAJInfo));
    WriteLn(pChemin);
    if not RequeteReussi(
              ACCFFichierMAJInfo,
              '1/4 Erreur lors de la mise � jour des informations du fichiers.') then
      exit;


    EcrireMessage(ACMessageInfo,'2/4 Reception des informations du fichier "'+ExtractFileName(pChemin)+'"');
    ReadBuffer(FichierInfoDistant,SizeOf(TACFichierInfo));
    Adresse :=ObtenirAdresse(pChemin);
    EcrireMessage(ACMessageInfo,'3/4 Synchronisation de fichier en mode '+ACSyncModeToStr(SyncMode));
    FichierInfoLocal := ACLireFichierInfo(Adresse);
    case SyncMode of
    ACSyncModeMAJ :
    begin
      case ACCompareDateTime(FichierInfoLocal.Date.Modification,
          FichierInfoDistant.Date.Modification) of
        1 :
        begin
          EcrireMessage(ACMessageInfo,'3/4 Fichier local recent.');
          if FSyncAvanceActive and (FichierInfoLocal.Taille > SYNC_MODE_SEUIL_TAILLE) then
          begin
            if not RequeteSyncFichierEcrire(pChemin) then
            begin
              EcrireMessage(ACMessageErreur,'3/4 Erreur de synchronisation du fichier.');
              exit;
            end;
          end
          else
            if not RequeteFichierEcrire(pChemin) then
            begin
              EcrireMessage(ACMessageErreur,'3/4 Erreur lors de la mise � jour du fichier.');
              exit;
            end;
        end;
        0 :
          EcrireMessage(ACMessageInfo,'3/4 Fichier � jour.');
        -1 :
        begin
          EcrireMessage(ACMessageInfo,'3/4 Fichier local obsol�te');
          if FSyncAvanceActive and (FichierInfoLocal.Taille > SYNC_MODE_SEUIL_TAILLE) then
          begin
            if not RequeteSyncFichierLire(pChemin) then
            begin
              EcrireMessage(ACMessageErreur,'3/4 Erreur de synchronisation du fichier.');
              exit;
            end;
          end
          else
            if not RequeteFichierLire(pChemin) then
            begin
              EcrireMessage(ACMessageErreur,'3/4 Erreur lors de la mise � jour du fichier.');
              exit;
            end;
        end;
      end;
    end;
    ACSyncModeLecture :
        if not ACSameDateTime(FichierInfoLocal.Date.Modification,
                FichierInfoDistant.Date.Modification) then
        begin
          EcrireMessage(ACMessageInfo,'3/4 Fichier diff�rent');
          if FSyncAvanceActive and (FichierInfoLocal.Taille > SYNC_MODE_SEUIL_TAILLE) then
          begin
            if not RequeteSyncFichierLire(pChemin) then
            begin
              EcrireMessage(ACMessageErreur,'3/4 Erreur de synchronisation du fichier.');
              exit;
            end;
          end
          else
            if not RequeteFichierLire(pChemin) then
            begin
              EcrireMessage(ACMessageErreur,'3/4 Erreur lors de la mise � jour du fichier.');
              exit;
            end;
        end;
    ACSyncModeEcriture :
        if not ACSameDateTime(FichierInfoLocal.Date.Modification,
                FichierInfoDistant.Date.Modification) then
        begin
          EcrireMessage(ACMessageInfo,'3/4 Fichier diff�rent');
          if FSyncAvanceActive and (FichierInfoLocal.Taille > SYNC_MODE_SEUIL_TAILLE) then
          begin
            if not RequeteSyncFichierEcrire(pChemin) then
            begin
              EcrireMessage(ACMessageErreur,'3/4 Erreur de synchronisation du fichier.');
              exit;
            end;
          end
          else
            if not RequeteFichierEcrire(pChemin) then
            begin
              EcrireMessage(ACMessageErreur,'3/4 Erreur lors de la mise � jour du fichier.');
              exit;
            end;
        end;
    ACSyncModeNoSupress:;

    end;



    EcrireMessage(ACMessageInfo,'4/4 Mise � jour reussi.');
    result := true;

  except
    EcrireMessage(ACMessageErreur,'4/4 Mise � jour du fichier impossible.');
    
  end;


end;

function TACCServeur.RequeteFichierSupprimer(pChemin: string):boolean;
begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,
              format('1/2 Suppression sur le serveur du fichier "%s".',
              [ExtractFileName(pChemin)]));
    WriteInteger(ACCodeFonction(ACCFFichierSupprimer));
    WriteLn(pChemin);

    if not RequeteReussi(
              ACCFFichierSupprimer,
              '1/2 Echec lors de la suppression du fichier sur le serveur.') then
      exit;

    EcrireMessage(ACMessageInfo,'2/2 Suppression du fichier termin�.');
    result := true;
  except
    EcrireMessage(ACMessageErreur,'2/2 Suppression du fichier impossible sur le serveur.');
    
  end;

end;


function TACCServeur.RequeteDossierCreer(pChemin: string):boolean;
begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 Creation sur le serveur du dossier "'+pChemin+'".');
    WriteInteger(ACCodeFonction(ACCFDossierCreer));
    WriteLn(pChemin);
    if not RequeteReussi(ACCFDossierCreer,'1/2 Echec lors de la cr�ation du dossier sur le serveur.') then
      exit;
    EcrireMessage(ACMessageInfo,'2/2 Dossier cr��.');
    result := true;
  except
    EcrireMessage(ACMessageErreur,'2/2 Erreur lors de la xreation du dossier.');
  end;


end;

function TACCServeur.RequeteMessage(pMessage: string):boolean;
begin
  Ecart := 0;
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,format('1/2 Envoyer le message  : "%s".',[pMessage]));
    WriteInteger(ACCodeFonction(ACCFMessage));
    WriteLn(pMessage);
    if not RequeteReussi(ACCFMessage,'1/2 Echec lors de la r�ception du message.')then
        exit;
        
    Result := true;
    EcrireMessage(ACMessageInfo,'2/2 Message envoy�.');

  except
    EcrireMessage(ACMessageErreur,'2/2 Echec lors de l''envoie du message.');

  end;

end;

procedure TACCServeur.DoDonneesChange;
begin
  (*
  if Assigned(OnDonneesChange) then
      OnDonneesChange(Self);
  *)
end;




function TACCServeur.RequeteDossierRenommer(pChemin, pNouveau: string):boolean;
var
  Ancien : string;
begin
  result := false;
  if not TestConnexion then
    exit;
  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 Renommer le dossier "'+ExtractFileName(pChemin)+'" en "'+ExtractFileName(pNouveau)+'"');
    WriteInteger(ACCodeFonction(ACCFDossierRenommer));
    WriteLn(pChemin);
    WriteLn(pNouveau);
    if not RequeteReussi(ACCFDossierRenommer,'1/2 Erreur lors du changement de nom du dossier sur le serveur.') then
      exit;

    Ancien := ObtenirAdresse(pChemin);
    pNouveau := ExtractFilePath(ObtenirAdresse(pChemin))+pNouveau;
    if DirectoryExists(Ancien)then
      if not RenameFile(Ancien,pNouveau)then
      begin
        EcrireMessage(ACMessageErreur,'2/2 Erreur lors du changement de nom du dossier en local.');
        exit;
      end;

    EcrireMessage(ACMessageInfo,'2/2 Changement de nom du dossier termin�');
    result := true;
  except
    EcrireMessage(ACMessageErreur,'2/2 Erreur lors du changement de nom du dossier');
    
  end;

end;

function TACCServeur.RequeteFichierRenommer(pChemin, pNouveau: string):boolean;
var
  Adresse : string;
begin
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,format('1/2 Renommer le fichier "%s" en "%s".',[ExtractFileName(pChemin),ExtractFileName(pNouveau)]));
    WriteInteger(ACCodeFonction(ACCFFichierRenommer));
    WriteLn(pChemin);
    WriteLn(pNouveau);
    if not RequeteReussi(ACCFFichierRenommer,'1/2 Echec lors du changement de nom.' )then
      exit;

    Adresse := ObtenirAdresse(pChemin);
    if FileExists(Adresse)then
      RenameFile(Adresse,ExtractFilePath(Adresse)+pNouveau);

    EcrireMessage(ACMessageInfo,'2/2 Changement de nom termin�.');
    result := true;
  except
    EcrireMessage(ACMessageErreur,'2/2 Impossible de renommer le fichier.');
  end;

end;


procedure TACCServeur.AppliquerSynchronisation(ContenueDossier: TACContenueDossier);
  procedure RequeteDossierOperation(Dossier : TACContenueDossier);
  var
    i :  integer;
    lDossier : TACContenueDossier;
    lFichier : TACContenueFichier;
    lChemin : string;
    lAdresse : string;
  begin
    for i := 0 to Dossier.Dossiers.Count - 1 do
    begin
      lDossier := Dossier.Dossiers[i];
      lChemin := lDossier.Chemin;
      lAdresse := ObtenirAdresse(lChemin);

      case lDossier.Info.Operation of
      AcFoAJour : RequeteDossierOperation(lDossier);
      AcFoLire : RequeteDossierLire(lChemin);
      AcFoEcrire : RequeteDossierEcrire(lChemin);
      AcFoSupprimerLocal :
      begin
        DeleteDirectory(lAdresse);
        EcrireMessage(ACMessageInfo,'Supression en local du dossier "'+lChemin+'"');
      end;
      AcFoSupprimerDistant : RequeteDossierSupprimer(lChemin);

      end;

    end;

    for i := 0 to Dossier.Fichiers.Count - 1 do
    begin
      lFichier := Dossier.Fichiers[i];
      lChemin := lFichier.Chemin;
      lAdresse := ObtenirAdresse(lChemin);

      case lFichier.Info.Operation of
      AcFoAJour : ;
      AcFoLire : RequeteFichierLire(lChemin);
      AcFoEcrire : RequeteFichierEcrire(lChemin);
      AcFoSupprimerLocal :
      begin
        DeleteFile(pChar(lAdresse));
        EcrireMessage(ACMessageInfo,'Supression en local du fichier '+lChemin);
      end;
      AcFoSupprimerDistant : RequeteFichierSupprimer(lChemin);
      AcFoSyncLire : RequeteSyncFichierLire(lChemin);
      AcFoSyncEcrire : RequeteSyncFichierEcrire(lChemin);
      end;

    end;

  end;
begin
  RequeteDossierOperation(ContenueDossier);
  EcrireMessage(ACMessageInfo,'6/6 Synchronisation de dossier termin�');
end;
procedure TACCServeur.MettreAjour;
var
  i : integer;
begin
  if TestConnexion then
  begin
    for i := 0 to FichiersSync.Count - 1 do
      FichiersSync[i].RequeteMettreAJour;
  end;

end;

function TACCServeur.RequeteConnexion(pUtilisateur,
  pMotPasse: string): boolean;
var
  GrainDeSel : String;
begin
  result := false;
  if not TestConnexion then
    exit;
  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 Identification de l''utilisateur "'+pUtilisateur+'"');
    WriteInteger(ACCodeFonction(ACCFConnexion));
    WriteLn(pUtilisateur);
    WriteLn(pMotPasse);
    if not RequeteReussi(ACCFConnexion, '1/2 Erreur � l''identification') then
        exit;
        
    if ReadInteger = ACConnexionResult(ACConAccepte)then
    begin
      GrainDeSel := ReadLn;
      Cle := MD5CryptMotDePasse(pUtilisateur+GrainDeSel);
      result := true;
    end;
  except
    EcrireMessage(ACMessageErreur,'2/2 Identification impossible');
  end;
end;


function TACCServeur.Connexion: Boolean;
var
  SeveurProtocole : TACProtocoleVersion;
begin
  result := false;
  try
    Connect(Poste.FTimeoutConnexion);

  except
    EcrireMessage(ACMessageErreur,'Erreur de Connexion, verifi� que le serveur � l''adresse "'+Host +'" soit activ�.');
  end;

end;

procedure TACCServeur.EnregistrerSousFichier(FichierNom: string);
var
  Fichier : TIniFile;
begin
  Fichier := TIniFile.Create(FichierNom);
  Fichier.WriteString('General','Adresse',Host);
  Fichier.WriteInteger('General','Port',Port);
  Fichier.WriteString('General','Nom',Nom);
  Fichier.WriteString('General','Utilisateur',Utilisateur);
  Fichier.WriteString('General','MotPasse',MotPasse);
  Fichier.WriteBool('General','ConnexionAuto',ConnexionAuto);
  Fichier.WriteBool('General','DemarrageActif',DemarrageActif and ConnexionAuto);
  Fichier.WriteInteger('General','SyncMode',Integer(SyncMode));
  Fichier.WriteBool('General','SynchronisationActive',SynchronisationActive);
  Fichier.WriteBool('General','SyncAvanceActive',SyncAvanceActive);

  Fichier.Free;


end;

procedure TACCServeur.OuvrirDeFichier(FichierNom: string);
var
  Fichier : TIniFile;
begin
  Fichier := TIniFile.Create(FichierNom);
  Host :=  Fichier.ReadString('General','Adresse','localhost');
  Port :=  Fichier.ReadInteger('General','Port',AC_SERVEUR_TCP_PORT);

  Nom :=  Fichier.ReadString('General','Nom','localhost');
  Utilisateur := Fichier.ReadString('General','Utilisateur','');
  MotPasse := Fichier.ReadString('General','MotPasse','');
  ConnexionAuto := Fichier.ReadBool('General','ConnexionAuto',false);
  DemarrageActif := Fichier.ReadBool('General','DemarrageActif',false) and ConnexionAuto;
  SynchronisationActive := Fichier.ReadBool('General','SynchronisationActive',false);
  SyncMode :=   TACSyncMode(Fichier.ReadInteger('General','SyncMode',Integer(ACSyncModeMAJ)));
  SyncAvanceActive := Fichier.ReadBool('General','SyncAvanceActive',false);
  Fichier.Free;

end;

function TACCServeur.RequeteDossierSauvegarder(pChemin: string): boolean;
begin
  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 Sauvegarde du dossier "'+pChemin+'"');
    WriteInteger(ACCodeFonction(ACCFDossierSauvegarder));
    WriteLn(pChemin);
    if not RequeteReussi(ACCFDossierSauvegarder,'1/3 Erreur, lors de la sauvagarde') then
        exit;
    if not ReponseDossierSauvegarder then
      exit;

    result := true;

  except
    EcrireMessage(ACMessageErreur,'Mise a jour des informations du dossier  sur le serveur impossible');
  end;

end;

function TACCServeur.ReponseDossierSauvegarder: boolean;

var

  FichierZip : TFileStream;
  FichierContenueZip : string;
  FichierContenue : string;

  ContenueLocal : TACContenueDossier;
  ContenueDistant : TACContenueDossier;
  Chemin : string;
  Adresse : string;
  Nombre : Integer;
  Taille : Integer;
begin
  Result := false;
  try
    Chemin := ReadLn;
    Taille := ReadInteger;

    EcrireMessage(ACMessageInfo,format('2/6 R�ception de la structure du dossier, Taille : %d ko Fichiers du dossier "%s".',
              [intToStr(Taille div 1024),Chemin]));

    FichierContenueZip :=  GetTempDirectory+'\ArchiveSync';
    if not DirectoryExists(FichierContenueZip) then
      ForceDirectories(FichierContenueZip);
    FichierContenueZip :=  FichierContenueZip+'\Contenue.zip';
    if FileExists(FichierContenueZip) then
      DeleteFile(pChar(FichierContenueZip));

    FichierContenue :=  GetTempDirectory+'\ArchiveSync';
    if not DirectoryExists(FichierContenue) then
      ForceDirectories(FichierContenue);
    FichierContenue :=  FichierContenue+'\Contenue.cnt';
    if FileExists(FichierContenue) then
      DeleteFile(pChar(FichierContenue));

    EcrireMessage(ACMessageInfo,'2/6 Reception du fichier +'+FichierContenueZip);
    FichierZip  := TFileStream.Create(FichierContenueZip,fmCreate);
    ReadStream(FichierZip,Taille);
    FichierZip.Free;

    EcrireMessage(ACMessageInfo,'2/6 Extraction du fichier +'+FichierContenue);
    DeZipperFichier(FichierContenue,FichierContenueZip);
    ContenueDistant := TACContenueDossier.Create('');
    ContenueDistant.SyncMode := ACSyncModeLecture ;

    EcrireMessage(ACMessageInfo,'2/6 Analyse du fichier');
    ContenueDistant.ImporterListeFichierInfoDeFichier(FichierContenue);
    Nombre := ContenueDistant.Nombre;

    EcrireMessage(ACMessageInfo,'3/6 Actualisation des informations local, Nombre de fichiers distants '+IntToStr(Nombre));
    ContenueLocal := TACContenueDossier.Create(ObtenirAdresse(Chemin));
    ContenueLocal.SyncMode := ACSyncModeLecture;
    ContenueLocal.Generer(true);
    //  ACContenueDossier(Adresse,ContenueLocal,true);
    EcrireMessage(ACMessageInfo,'4/6 Comparaison des contenues');
    ContenueLocal.Comparer(ContenueDistant);

    EcrireMessage(ACMessageInfo,'5/6 Synchronisation en local');
    ContenueLocal.Adresse := Chemin;
    AppliquerSynchronisation(ContenueLocal);
    ContenueLocal.Free;
    ContenueDistant.Free;
    result := true;
  except
    EcrireMessage(ACMessageErreur,'6/6 Echec de lecture des informations sur le serveur');
  end;

end;

procedure TACCServeur.Sauvegarder;
var
  i : integer;
begin
  if TestConnexion then
  begin
    for i := 0 to FichiersSync.Count - 1 do
      FichiersSync[i].RequeteSauvegarder;
  end;
end;

function TACCServeur.RequeteExtinctionClient: boolean;
begin
  result := false;

  if not TestConnexion then
    exit;
  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,'1/2 Demande d''extinction');
    WriteInteger(ACCodeFonction(ACCFExtinctionClient));
    if ACCodeFonction(ACCFExtinctionClient) = ReadInteger  then
    begin
      result := ACRequeteResult(ACReqReussi) = ReadInteger;
      if Result then
        EcrireMessage(ACMessageRequete,'2/2 demande d''extinction accept�')
      else
        EcrireMessage(ACMessageRequete,'2/2 demande d''extinction refus�');

    end;
  except
    EcrireMessage(ACMessageErreur,'2/2 Erreur lors de la demande d''extinction du PC');

  end;

end;

function TACCServeur.RequeteSyncFichierLire(pChemin: string):boolean;
var
  Fichier : string;
  Fichier7z : string;
  FichierInfo : string;
  FichierMAJ : string;
  ListeFichier7z : TStringList;
  SyncCluster : TSyncClusterStream;
begin

  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    EcrireMessage(ACMessageRequete,format('1/9 Synchronisation avanc� du fichier "%s"',[ExtractFileName(pChemin)]));
    fichier := ObtenirAdresse(pChemin);
    if not FileExists(fichier) then
    begin
        EcrireMessage(ACMessageErreur,'1/9 Fichier inexistant');
        exit;
    end;

    // 1: Lecture de informations du fichiers
    EcrireMessage(ACMessageInfo,'2/9 Lecture des informations du fichier.');
    FichierInfo := Poste.FDossierTemp+'\'+LocalName+'-'+ServiceNom+'_client.info';
    SyncCluster := TSyncClusterStream.Create;
    if not SyncCluster.LireInfo(fichier,FichierInfo) then
    begin
        EcrireMessage(ACMessageErreur,'2/9 Erreur d''acces au fichier');
        exit;
    end;


    // 3 : Creation des fichier 7z du fichier d'information
    EcrireMessage(ACMessageInfo,'3/9 Creation du fichier 7z');
    Fichier7z := Poste.FDossierTemp+'\'+LocalName+'-'+ServiceNom+'_client.info.7z';
    ListeFichier7z := TStringList.Create;
    if not CreationFichier7z(FichierInfo,ListeFichier7z) then
    begin
        EcrireMessage(ACMessageErreur,'3/9 Erreur de la creation du fichier 7z');
        exit;
    end;

    // 3 : envoie des fichiers 7z  du fichier d'information
    EcrireMessage(ACMessageInfo,format('4/8 Envoie des %d fichier(s) d''information 7z du ficher "%s".',
                 [ListeFichier7z.Count,ExtractFileName(pChemin)]));
    WriteInteger(ACCodeFonction(ACCFSyncFichierLire));
    WriteLn(pChemin);
    WriteLn(ExtractFileName(FichierInfo));

    if not EnvoyerFichiers(ListeFichier7z) then
    begin
        EcrireMessage(ACMessageErreur,'4/9 Erreur l''envoi du fichier d''information');
        exit;
    end;

    // 4 : Attente de mise � jour du fichier
    EcrireMessage(ACMessageInfo,'5/9 Attente du fichier de mise � jour.');
    if not AttenteOperation('mise � jour � du fichier') then
    begin
        EcrireMessage(ACMessageErreur,'5/9 Erreur lors de mise � jour du fichier');
        exit;
    end;

    // 5 : reception des fichiers 7z de mise � jour
    EcrireMessage(ACMessageInfo,'6/9 R�ception des fichiers 7z de mise � jour.');
    if not RequeteReussi(ACCFSyncFichierLire,
              '6/9 Erreur � la creation du fichier de mise � jour.') then
      exit;
    (*
    if ReadInteger = ACResultSync(ACRSyncAJour)then
    begin
        EcrireMessage('6/9 fichier � jour.');
        result := true;
        exit;
    end;
    *)

    // 6 : reception des fichiers de mise � jour.
//    FichierMAJ := Poste.FDossierTemp+'\'+LocalName+'-'+ServiceNom+'_recu.maj';
    FichierMAJ := Poste.FDossierTemp+'\'+ReadLn;
    EcrireMessage(ACMessageInfo,format('6/9 Reception du fichier "%s"',[ExtractFileName(FichierMAJ)]));
    if not ReceptionFichiers7z(ListeFichier7z,FichierMAJ) then
    begin
        EcrireMessage(ACMessageErreur,'6/9 Erreur lors de la reception du fichier de mise � jour.');
        exit;
    end;

    // 7 : Extraction du fichier de mise � jour
    (*
    EcrireMessage(Format('7/9 Extraction des %d fichier(s) 7z du fichier %s',
								[ListeFichier7z.Count,ExtractFileName(FichierMAJ)]));
    if not ExtractionFichier7z(FichierMAJ,ListeFichier7z) then
    begin
        EcrireMessage('7/9 Erreur lors de l''extraction du fichier de mise � jour');
        exit;
    end;
    *)
    // applicatin de la mise � jour
    EcrireMessage(ACMessageInfo,'8/9 Application de la mise a jour');
    if not SyncCluster.AppliquerMiseAjour(Fichier,FichierMAJ) then
    begin
        EcrireMessage(ACMessageErreur,'8/9 Erreur lors de l''aplication de la mise � jour.');
        exit;
    end;
    EcrireMessage(ACMessageInfo,'9/9 Mise � jour termin�.');
    ListeFichier7z.Free;
    SyncCluster.Free;
    result := true;
  except

    EcrireMessage(ACMessageErreur,'Erreur de synchronisation avanc� du fichier.');
  end;



end;

function TACCServeur.RequeteSyncFichierEcrire(pChemin :string):boolean;
var
  Fichier : string;
  Fichier7z : string;
  FichierInfo : string;
  FichierMAJ : string;
  ListeFichier7z : TStringList;
  SyncCluster : TSyncClusterStream;
begin

  result := false;
  if not TestConnexion then
    exit;

  try
    EcrireMessage(ACMessageInfo,'***** Requete *****');
    Fichier := ObtenirAdresse(pChemin);
    // 1 : demande du fichier d'information
    EcrireMessage(ACMessageRequete,'1/9 demande du fichier d''information');
    WriteInteger(ACCodeFonction(ACCFSyncFichierEcrire));
    WriteLn(pChemin);

    // 2 : Attente du ficheir d'information
    EcrireMessage(ACMessageInfo,'1/9 Attente du fichier d''information ');
    if not AttenteOperation('Attente du fichier d''information') then
    begin
        EcrireMessage(ACMessageErreur,'2/9 Erreur lors de la creation du fichier d''information');
        exit;
    end;

    // 3 : Reception des fichiers 7z du fichier d'information
    if not RequeteReussi(ACCFSyncFichierEcrire,
        '2/7 Erreur � la creation du fichier d''information') then
        exit;
    ListeFichier7z := TStringList.create;

    //FichierInfo := Poste.FDossierTemp+'\'+LocalName+'-'+ServiceNom+'_recu.info';
    FichierInfo := Poste.FDossierTemp+'\'+ReadLn;
    ReceptionFichiers7z(ListeFichier7z,FichierInfo);
(*
    // 3 : Attente du fichier d'information
    EcrireMessage('3/9 Attente du fichier d''information ');
    if not AttenteOperation('Attente du fichier d''information') then
    begin
        EcrireMessage('3/9 Erreur lors de mise � jour du fichier');
        exit;
    end;

    //  4 : reception des fichier 7z d'information
    if not ReceptionFichiers(Poste.FDossierTemp,ListeFichier7z) then
    begin
        EcrireMessage('4/9 Erreur lors de la reception des fichiers d''informations');
        exit;
    end;

    // 5 : Extraction du fichier d'information
    FichierInfo := Poste.FDossierTemp+'\'+LocalName+'-'+ServiceNom+'.info';
    EcrireMessage(Format('5/9 Extraction des %d fichier(s) 7z du fichier %s',
								[ListeFichier7z.Count,ExtractFileName(FichierMAJ)]));
    if not ExtractionFichier7z(FichierMAJ,ListeFichier7z) then
    begin
        EcrireMessage('5/9 Erreur lors de l''extraction du fichier d''information');
        exit;
    end;
    *)

    // 6 : Cr�ation du fichier de mise � jour
    EcrireMessage(ACMessageInfo,'6/9 Creation du fichier de mise � jour');
    FichierMAJ := Poste.FDossierTemp+'\'+LocalName+'-'+ServiceNom+'_client.maj';
    SyncCluster := TSyncClusterStream.Create;
    if not SyncCluster.CreerMiseAJour(Fichier,FichierInfo,FichierMAJ) then
    begin
        EcrireMessage(ACMessageErreur,'6/9 Creation du fichier de mise � jour');
        exit;
    end;
    EcrireMessage(ACMessageInfo, SyncCluster.Rapport.GetText);



    // 7 : Creation du fichier 7z
    EcrireMessage(ACMessageInfo,'7/9 Creation des fichiers 7z.');
    Fichier7z := Poste.FDossierTemp+'\'+LocalName+'-'+ServiceNom+'_client.maj.7z';
    ListeFichier7z := TStringList.Create;
    if not CreationFichier7z(FichierMAJ,ListeFichier7z) then
    begin
        EcrireMessage(ACMessageInfo,'7/9 Erreur de la creation du fichier 7z');
        exit;
    end;

    WriteInteger(ACCodeFonction(ACCFSyncFichierEcrireMAJ));
    WriteLn(pChemin);
    WriteLn(ExtractFileName(FichierMAJ));

    EcrireMessage(ACMessageInfo,'7/9 Envoie du fichier de mise � jour.');
    if not EnvoyerFichiers(ListeFichier7z) then
    begin
        EcrireMessage(ACMessageErreur,'7/9 Erreur lors d''envoie du fichier de mise � jour du fichier');
        exit;

    end;

     // 8 : Attente du de mise � jour du fichier
    EcrireMessage(ACMessageInfo,'7/9 Attente de mise � jour du fichier ');
    if not AttenteOperation('Attente de mise � jour du fichier') then
    begin
        EcrireMessage(ACMessageErreur,'7/9 Erreur lors de la mise � jour du fichier');
        exit;
    end;

    if not RequeteReussi(ACCFSyncFichierEcrireMAJ,
        '8/9 Erreur lors de la mise � jour du fichier') then
            exit;

    // 9 mise � jour termin�.
    EcrireMessage(ACMessageInfo,'9/9 Mise � jour du fichier termin�');

    ListeFichier7z.Free;
    SyncCluster.Free;
    result := true;
  except

    EcrireMessage(ACMessageErreur,'Erreur de synchronisation par cluster du fichier');
  end;


end;


function TACCServeur.Dupliquer: TACCServeur;
begin
  result := Result
end;

procedure TACCServeur.Synchronisation;
var
  i : Integer;
begin
  EcrireMessage(ACMessageRequete,'1/2 Lancement de la synchronisation');
  if TestConnexion then
  begin
    SynchronisationServeur.Connexion(Utilisateur,MotPasse);
    SynchronisationServeur.FichiersSync.Clear;
    for i  := 0 to FichiersSync.Count - 1 do
        SynchronisationServeur.FichiersSync.Add(FichiersSync[i].FChemin,FichiersSync[i].FInfo.TypeDossier);
    SynchronisationServeur.MettreAjour;
    SynchronisationServeur.DeConnexion;
  end
  else
    EcrireMessage(ACMessageErreur,'Vous devez etre connect� pour lancer la synchronisation');
  EcrireMessage(ACMessageInfo,'2/2 Synchronisation termin�');

end;

procedure TACCServeur.SetServiceNom(const Value: string);
begin
  FServiceNom := Value;
end;

function TACCServeur.ReceptionAvancement(Libelle : string): boolean;
var
  Max : integer;
  Position :integer;
begin
  Max := ReadInteger;
  Position := ReadInteger;
//  EcrireMessage(format('Avancement %d/%d',[Position,Max]));
  if Assigned(OnAvancement) then
      OnAvancement(Self,Libelle,Max,Position);
end;





procedure TACCServeur.Do7zProgressAdv(Sender: TObject;
  Filename: Widestring; MaxProgress, Position: int64);
begin
  if Assigned(OnAvancement) then
      OnAvancement(Self,ExtractfileName(Filename),MaxProgress,Position);
end;



function TACCServeur.TestConnexion: boolean;
begin
  result := Connected;
  if Result then
    exit
  else
    if ConnexionAuto then
      result := Connexion
    else
      EcrireMessage(ACMessageErreur,'Serveur non connect�, veuillez activer la Connexion automatique');


end;

procedure TACCServeur.ResetCompteurs;
begin
  FCompteurOctetsEmi := 0;
  FCompteurFichiersEmi := 0;
  FCompteurOctetsRecu := 0;
  FCompteurFichiersRecu := 0;
end;











function TACCServeur.EnvoyerFichier(pFichier: string): boolean;
var
  Fichier : TFileStream;
begin
  result := false;
  try
      EcrireMessage(ACMessageInfo,'1/2 Envoie du fichier '+ExtractFileName(pFichier));
      Fichier := TFileStream.Create(pFichier,fmOpenRead);
      EcrireMessage(ACMessageInfo,'2/3 Taille du fichier '+SizeFileToStr(Fichier.Size));
      WriteInteger(Fichier.size);
      OpenWriteBuffer;
      WriteStream(Fichier);
      CloseWriteBuffer;
      Fichier.Free;
      EcrireMessage(ACMessageInfo,'3/3 Envoie du fichier termin�');
      result := true;
  except
      EcrireMessage(ACMessageErreur,'3/3 Erreur lors de l''envoie du fichier "'+pFichier+'"');
  end;
end;

function TACCServeur.EnvoyerFichiers(pListeFichiers: TStrings): boolean;
var
  i : integer;
begin
  result := false;
  EcrireMessage(ACMessageInfo,format('1/2 Envoie des %d fichiers ',[pListeFichiers.Count]));
  WriteInteger(pListeFichiers.Count);
  for i := 0 to pListeFichiers.Count - 1 do
  begin
    EcrireMessage(ACMessageRequete,Format('2/3 Envoie du fichier %d/%d',[i+1,pListeFichiers.Count]));
    WriteLn(ExtractFileName(pListeFichiers[i]));
    if not EnvoyerFichier(pListeFichiers[i]) then
    begin
      EcrireMessage(ACMessageErreur,'2/3 erreur lors de l''envoie des fichier');
      exit;
    end;
  end;
  EcrireMessage(ACMessageInfo,'3/3 Transmission des fichier termin�');
  result := true;

end;

function TACCServeur.ReceptionFichier(pFichier: String): boolean;
var
  Fichier : TFileStream;
  Taille : Integer;
begin
  inc(Ecart);
  result := false;
    Try
      Taille := ReadInteger;
      EcrireMessage(ACMessageInfo,Format('1/2 Reception du fichier %s taille %s',[ExtractFileName(pFichier),SizeFileToStr(Taille)]));
      Fichier  := TFileStream.Create(pFichier,fmCreate);
      ReadStream(Fichier,Taille);
      Fichier.Free;
      EcrireMessage(ACMessageInfo,' 2/2 Reception du fichier terminer');
      result := true;
    except
      EcrireMessage(ACMessageErreur,' Erreur lors de la reception du fichier terminer');

    end;

end;

function TACCServeur.ReceptionFichiers(pDossier:string;pListeFichier: TStrings): boolean;
var
  i : Integer;
  Nombre : Integer;
  Fichier : string;
begin
  inc(Ecart);
    Result := false;
    try
      Nombre := ReadInteger;
      pListeFichier.Clear;
      EcrireMessage(ACMessageInfo,Format('1/3 Reception des %d fichiers ',[Nombre]));

      for i := 0 to Nombre - 1 do
      begin
          Fichier := pDossier+'\'+ReadLn;

          EcrireMessage(ACMessageRequete,Format('2/3 Reception du fichier %d/%d ',[i+1,Nombre]));
          if not ReceptionFichier(Fichier) then
          begin
            EcrireMessage(ACMessageErreur,'2/3 Erreur lors de la reception  du fichier '+IntToStr(i+1));
            exit;
          end;
          pListeFichier.Add(Fichier);
      end;
      dec(Ecart);
      EcrireMessage(ACMessageInfo,' 3/3 Reception des fichiers termin�');
      Result := true;
    except
      EcrireMessage(ACMessageErreur,' Erreur lors de la reception de fichiers');

    end;


end;


function TACCServeur.ReceptionFichiers7z(pListeFichier7z : TStringList;pFichier:string): boolean;
var
  DossierReception : string;

begin
  inc(Ecart);
  result := false;
  try
    EcrireMessage(ACMessageInfo,'1/3 Attente de creation des fichiers 7z.');
    if not AttenteOperation('Cr�ation des fichiers 7z.') then
    begin
      EcrireMessage(ACMessageErreur,'1/3 Erreur lorsde l''attante de la lecture du fichiers.');
      exit;
    end;

    EcrireMessage(ACMessageInfo,'2/3 Reception des fichiers 7z.');

    if not DirectoryExists(Poste.DossierTemp) then
        ForceDirectories(Poste.DossierTemp);

    if not ReceptionFichiers(Poste.DossierTemp,pListeFichier7z) then
    begin
      EcrireMessage(ACMessageErreur,'1/4 Erreur lors de la receptions des fichiers 7z.');
      exit;
    end;
    dec(Ecart);

    // Extraction du dossier
    EcrireMessage(ACMessageInfo,Format('3/4 Extraction des %d fichier(s) 7z du dossier "%s"',
                [pListeFichier7z.Count,pFichier]));
    if not ExtractionFichier7z(pFichier,pListeFichier7z)then
    begin
      EcrireMessage(ACMessageErreur,'3/4 Erreur lors de l''extraction du dossier');
      exit;
    end;

    EcrireMessage(ACMessageInfo,'3/3 Reception des fichiers termin�.');
    result := true;

  except
    EcrireMessage(ACMessageErreur,'3/3 Lecture du fichier impossible.');

  end;

end;


function TACCServeur.AttenteOperation(pOperation : string): boolean;
var
  Code : TACCodeFonction;
begin
    inc(Ecart);
    result := false;
    try
       // Attente operation en cours
      Code := TACCodeFonction(ReadInteger);
      if Code = ACCFOperationEnCours then
        EcrireMessage(ACMessageInfo,'1/3 Attente '+pOperation)
      else
      begin
        EcrireMessage(ACMessageErreur,'1/3 Erreur lors de l''attente '+pOperation);
        exit;
      end;

      while (Code <> ACCFOperationTerminer) do
      begin
        Case Code of
        ACCFOperationAvancement : ReceptionAvancement(pOperation);
        ACCFOperationEnCours :;
        ACCFOperationEchoue :
          begin
            EcrireMessage(ACMessageErreur,'2/3 Echec : '+pOperation);
            exit;
          end;
        else
          begin
            EcrireMessage(ACMessageErreur,'2/3 Erreur lors de l''attente '+pOperation);
            exit;
          end;

        end;

        Code := TACCodeFonction(ReadInteger);
      end;
      EcrireMessage(ACMessageInfo,'3/3 Attente '+pOperation+' termin�.');
      result := true;
    except
      EcrireMessage(ACMessageErreur,'3/3 Erreur lors de l''attente '+pOperation);

    end;
end;

function TACCServeur.CreationDossier7z(pDossier: string;
  ListeFichiers7z: TStrings): boolean;
var
  Fichier7z : string;

begin
  inc(Ecart);
  Result := false;
  try
      // Compression du ficher
      Fichier7z := Poste.FDossierTemp+'\'+LocalName+'-'+ServiceNom+'_client.7z';
      if DirectoryExists(ExtractFilePath(Fichier7z))then
        DeleteDirectory(ExtractFilePath(Fichier7z));
      ForceDirectories(ExtractFilePath(Fichier7z));
      EcrireMessage(ACMessageInfo,format('1/2 Creation des fichiers 7z "%s" du dossier "%s"',
                      [ExtractFileName(Fichier7z),ExtractFileName(pDossier)]));

      if not SevenZipperDossier(pDossier,Fichier7z,SEVEN_ZIP_TAILLE_VOLUME,
                                          ListeFichiers7z,Do7zProgressAdv,cle) then
      begin
        EcrireMessage(ACMessageErreur,'1/2 Erreur d''acces au fichier');
        exit;
      end;
      EcrireMessage(ACMessageInfo,'2/2 Creation des fichiers 7z termin�');
      result := true;
  except
      EcrireMessage(ACMessageErreur,'2/2 Erreur lors de la cr�ation des fichiers 7z');

  end;

end;

function TACCServeur.CreationFichier7z(pFichier: string;
  ListeFichiers7z: TStrings): boolean;
var
  Fichier7z : string;

begin
  inc(Ecart);
  Result := false;
  try
      // Compression du ficher
      Fichier7z := Poste.FDossierTemp+'\'+LocalName+'-'+ServiceNom+'_client.7z';
      if DirectoryExists(ExtractFilePath(Fichier7z))then
        DeleteDirectory(ExtractFilePath(Fichier7z));
      ForceDirectories(ExtractFilePath(Fichier7z));
      EcrireMessage(ACMessageInfo,format('1/2 Creation des fichiers 7z "%s" du fichier "%s"',
                      [ExtractFileName(Fichier7z),ExtractFileName(pFichier)]));
      ListeFichiers7z.Clear;
      if not SevenZipperFichier(pFichier,Fichier7z,SEVEN_ZIP_TAILLE_VOLUME,
                                          ListeFichiers7z,Do7zProgressAdv,cle) then
      begin
        EcrireMessage(ACMessageErreur,'1/2 Erreur d''acces au fichier');
        exit;
      end;
      EcrireMessage(ACMessageInfo,'2/2 Creation des fichiers 7z termin�');
      result := true;
  except
      EcrireMessage(ACMessageErreur,'2/2 Erreur lors de la cr�ation des fichiers 7z');

  end;

end;

function TACCServeur.EcartTab: string;
var
  i : Integer;
begin
  result := '';
  (*
  for i := 0 to Ecart do
    result := result +#9;
    *)


end;

function TACCServeur.RequeteReussi(Code: TACCodeFonction;
  pMessageErreur: string): boolean;
var
  lCode : integer;
begin

  result := false;

  try
    // attente de la reponse
    lCode := ReadInteger;
    if lCode <> ACCodeFonction(code) then
    begin
      EcrireMessage(ACMessageErreur,format('!!! Erreur de code reponse "%s":"%d"',[ACCodeFonctionVersStr(TACCodeFonction(lCode)),lCode]));
      exit;
    end;

    // reponse reussi
    if ReadInteger <> ACRequeteResult(ACReqReussi) then
    begin
      EcrireMessage(ACMessageErreur,pMessageErreur);
      exit;
    end;
    result := true;
  except
  end;
end;

procedure TACCServeur.SetSyncAvanceActive(const Value: boolean);
begin
  FSyncAvanceActive := Value;
end;

{ TACCDossierList }

function TACCDossierList.Add(Item: TACCDossier): Integer;
begin
  result := inherited Add(Item);
end;


function TACCDossierList.Add(
                  pNom:string;
                  pDateModification : TDateTime) : TACCDossier;
begin

  if Parent = nil then
      Result := TACCDossier.Create(Serveur)
  else
      Result := TACCDossier.Create(Parent);

  Result.FNom := pNom;
  Result.FDateModification := pDateModification;
  Add(Result);
end;

function TACCDossierList.Add(pInfo: TACFichierInfo): TACCDossier;
begin
  result := Add(pInfo.Nom,pInfo.Date.Modification);
  result.Info := pInfo;
end;

procedure TACCDossierList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;    
  inherited;

end;

constructor TACCDossierList.Create(pServeur : TACCServeur);
begin
  inherited Create;
  Serveur := pServeur;
  Parent := nil;
  FData := nil;
end;

constructor TACCDossierList.Create(pParent : TACCDossier);
begin
  Create(pParent.Serveur);
  Parent := pParent;

end;


procedure TACCDossierList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TACCDossierList.Destroy;
begin
  Clear;
  inherited;
end;

function TACCDossierList.Extract(Item: TACCDossier): TACCDossier;
begin
  result := inherited Extract(Item);
end;

function TACCDossierList.First: TACCDossier;
begin
  Result := inherited First;
end;

function TACCDossierList.Get(Index: Integer): TACCDossier;
begin
  Result := inherited Get(Index);
end;

function TACCDossierList.GetNom(Index: string): TACCDossier;
var
  i : Integer;
begin
  Result := nil;
  for i :=  0 to Count - 1 do
  begin
    if SameText(Index,Items[i].Nom) then
    begin
      Result := Items[i];
      exit;
    end;
  end;
end;

function TACCDossierList.IndexOf(Item: TACCDossier): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TACCDossierList.Insert(Index: Integer; Item: TACCDossier);
begin
  inherited Insert(Index,Item);
end;

function TACCDossierList.Last: TACCDossier;
begin
  Result := inherited Last;
end;

procedure TACCDossierList.Put(Index: Integer; Item: TACCDossier);
begin
  inherited Put(Index,Item);

end;

function TACCDossierList.Remove(Item: TACCDossier): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;




procedure TACCDossierList.SetData(const Value: Pointer);
begin
  FData := Value;
end;

{ TACCFichier }


function TACCFichier.CheminLocal: string;
begin
    Result := Dossier.CheminLocal+'\'+Nom;
end;

function TACCFichier.Chemin: string;
begin
    Result := Dossier.Chemin +'\' +Nom;
end;


constructor TACCFichier.Create(pDossier: TACCDossier);
begin
  Inherited  Create;
  Dossier := pDossier;
  Serveur := Dossier.Serveur;
end;


destructor TACCFichier.Destroy;
begin

  inherited;
end;

function TACCFichier.Fichier: TFileStream;
var
  Adresse : String;
begin
  Adresse := ExtractFilePath(CheminLocal);
  if not DirectoryExists(Adresse) then
    CreateDir(Adresse);
  Result := TFileStream.Create(CheminLocal,fmCreate);
end;

function TACCFichier.RequeteLire : boolean;
begin
  result := Serveur.RequeteFichierLire(Chemin);
end;

procedure TACCFichier.SetData(const Value: Pointer);
begin
  FData := Value;
end;

procedure TACCFichier.SetDateCreation(const Value: TDatetime);
begin
  FDateCreation := Value;
end;

procedure TACCFichier.SetDateModification(const Value: TDatetime);
begin
  FDateModification := Value;
end;

procedure TACCFichier.SetNom(const Value: String);
begin
  FNom := Value;
end;

procedure TACCFichier.SetTaille(const Value: Integer);
begin
  FTaille := Value;
end;

function TACCFichier.AjouterEnSynchronisation : TACCFichierSync;
begin
  Result := nil;
  if Dossier.Serveur.FichiersSync.Chemins[Chemin] = nil then
  begin
    Result := Dossier.Serveur.FichiersSync.Add(Chemin);
    EcrireMessage(ACMessageRequete,'Ajout du fichier "'+Chemin+'" en synchronisation');
  end;
end;

function TACCFichier.FichierSynchronise: TACCFichierSync;
begin
  result := Dossier.Serveur.FichiersSync.Chemins[Chemin];
end;

procedure TACCFichier.SetInfo(const Value: TACFichierInfo);
begin
  FInfo := Value;
end;

function TACCFichier.RequeteCopier(pDossier : TACCDossier) : boolean;
begin
  result := Serveur.RequeteFichierCopier(Chemin,pDossier.Chemin);

end;

function TACCFichier.RequeteDeplacer(pDossier : TACCDossier) : boolean;
begin
  result := Serveur.RequeteFichierDeplacer(Chemin,pDossier.Chemin);

end;

function TACCFichier.RequeteEcrire : boolean;
begin
  result := Serveur.RequeteFichierEcrire(Chemin);
end;

function TACCFichier.RequeteMAJInfo: boolean;
begin
  Result := Serveur.RequeteFichierMAJInfo(Chemin);
  
end;

function TACCFichier.RequeteSupprimer : boolean;
begin
  Result :=Serveur.RequeteFichierSupprimer(Chemin);

end;

procedure TACCFichier.EcrireMessage(pMessageType:TACMessageType; pMessage: string);
begin
  Serveur.EcrireMessage(pMessageType, pMessage);
end;

function TACCFichier.Exist: boolean;
begin
  result := FileExists(CheminLocal);
end;

function TACCFichier.RequeteRenommer(pNouveau: String):boolean;
begin
  Result := Serveur.RequeteFichierRenommer(Chemin,pNouveau);
  
end;

function TACCFichier.GetAJour: boolean;
var
  lInfo : TACFichierInfo;
begin
 result := false;
 if Exist then
 begin
  lInfo := ACLireFichierInfo(CheminLocal);
  result := ACSameDateTime(lInfo.Date.Modification,FInfo.Date.Modification);
 end;

end;

function TACCFichier.RequeteMettreAJour:boolean;
begin
    Result := Serveur.RequeteFichierMAJInfo(Chemin);
end;

function TACCFichier.RequeteSynchEcrire: boolean;
begin
  Result := Serveur.RequeteSyncFichierEcrire(Chemin);
end;

function TACCFichier.RequeteSynchLire: boolean;
begin
  Result :=   Serveur.RequeteSyncFichierLire(Chemin);
end;

{ TACCFichierList }

function TACCFichierList.Add(Item: TACCFichier): Integer;
begin
  result := inherited Add(Item);
end;


function TACCFichierList.Add(pNom: string;
  pDateModification: TDateTime;pTaille: Integer): TACCFichier;
begin
  Result := TACCFichier.Create(Dossier);
  Result.FNom := pNom;
  Result.FDateModification := pDateModification;
  Result.FTaille := pTaille;
  Add(Result);
end;

function TACCFichierList.Add(pFichierInfo: TACFichierInfo): TACCFichier;
begin
  Result := TACCFichier.Create(Dossier);
  Result.FNom := pFichierInfo.Nom;
  Result.FDateModification := pFichierInfo.Date.Modification;
  Result.FTaille := pFichierInfo.Taille;
  Result.FInfo := pFichierInfo;
  Add(Result);

end;

procedure TACCFichierList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;
  inherited;

end;


constructor TACCFichierList.Create(pDossier: TACCDossier);
begin
  inherited Create;
  Dossier := pDossier;
end;

procedure TACCFichierList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TACCFichierList.Destroy;
begin
  Clear;
  inherited;
end;

function TACCFichierList.Extract(Item: TACCFichier): TACCFichier;
begin
  result := inherited Extract(Item);
end;

function TACCFichierList.First: TACCFichier;
begin
  Result := inherited First;
end;

function TACCFichierList.Get(Index: Integer): TACCFichier;
begin
  Result := inherited Get(Index);
end;

function TACCFichierList.GetNom(Index: string): TACCFichier;
var
  i : Integer;
begin
  Result := nil;
  for i :=  0 to Count - 1 do
  begin
    if SameText(Index,Items[i].Nom) then
    begin
      Result := Items[i];
      exit;
    end;
  end;
end;

function TACCFichierList.IndexOf(Item: TACCFichier): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TACCFichierList.Insert(Index: Integer; Item: TACCFichier);
begin
  inherited Insert(Index,Item);
end;

function TACCFichierList.Last: TACCFichier;
begin
  Result := inherited Last;
end;

procedure TACCFichierList.Put(Index: Integer; Item: TACCFichier);
begin
  inherited Put(Index,Item);

end;

function TACCFichierList.Remove(Item: TACCFichier): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;

{ TACCDossier }
constructor TACCDossier.Create(pServeur: TACCServeur);
begin
  inherited Create;
  Parent := nil;
  Serveur := pServeur;
  FDossiers := TACCDossierList.Create(Self);
  Fichiers := TACCFichierList.Create(Self);

end;


constructor TACCDossier.Create(pParent: TACCDossier);
begin
  Create(pParent.Serveur);
  Parent := pParent;
end;

function TACCDossier.Chemin: string;
begin
  result := Nom;
  if Parent <> nil then
    Result := Parent.Chemin +'\'+Nom;
end;


destructor TACCDossier.Destroy;
begin
  FDossiers.Free;
  Fichiers.Free;
  inherited;
end;


function TACCDossier.RequeteLister: boolean;
begin
  result := Serveur.RequeteDossierLister(Chemin);
end;

procedure TACCDossier.SetData(const Value: Pointer);
begin
  FData := Value;
end;

procedure TACCDossier.SetDateCreation(const Value: TDateTime);
begin
  FDateCreation := Value;
end;

procedure TACCDossier.SetDateModification(const Value: TDateTime);
begin
  FDateModification := Value;
end;

procedure TACCDossier.SetNom(const Value: string);
begin
  FNom := Value;
end;

function TACCDossier.CheminLocal: string;
begin
  Result := Serveur.ObtenirAdresse(Chemin);

end;

function TACCDossier.RequeteLire : boolean;
begin
  result := Serveur.RequeteDossierLire(Chemin);
end;

function TACCDossier.RequeteCopier(Destination: TACCDossier): boolean;
begin
  result := Serveur.RequeteDossierCopier(Chemin,Destination.Chemin);
end;

function TACCDossier.RequeteDeplacer(Destination: TACCDossier): boolean;
begin
  result := Serveur.RequeteDossierDeplacer(Chemin,Destination.Chemin);
end;

function TACCDossier.RequeteMAJInfo: boolean;
begin
//  EcrireMessage('Mise � jour du dossier '+Chemin);
  result := Serveur.RequeteDossierMAJInfo(Chemin);
//  Serveur.WriteInteger(ACCodeFonction(ACCFDossierMAJInfo));
//  Serveur.WriteLn(Chemin);

end;

function TACCDossier.RequeteSupprimer: boolean;
begin
  Result := Serveur.RequeteDossierSupprimer(Chemin);
end;

procedure TACCDossier.EcrireMessage(pMessageType:TACMessageType; pMessage: string);
begin
    Serveur.EcrireMessage(pMessageType,pMessage);
end;

function TACCDossier.RequeteEcrire: boolean;
begin
  Result := Serveur.RequeteDossierEcrire(Chemin);
end;

function TACCDossier.AjouterEnSynchronisation : TACCFichierSync;
begin
  result := nil;
  if Serveur.FichiersSync.Chemins[Chemin] = nil then
  begin
    result := Serveur.FichiersSync.Add(Chemin,true);
    EcrireMessage(ACMessageRequete,'Ajout du Dossier "'+Chemin+'" en synchronisation');


  end;
end;



function TACCDossier.RequeteCreer(pNom :string): boolean;
begin
  Result := Serveur.RequeteDossierCreer(Chemin+'\'+pNom);
end;

function TACCDossier.DossierSynchronise: TACCFichierSync;
begin
  result := Serveur.FichiersSync.Chemins[Chemin];
end;

function TACCDossier.Exist: boolean;
begin
  result := DirectoryExists(CheminLocal);
end;

function TACCDossier.RequeteRenommer(pNouveau: String): boolean;
begin
  result := Serveur.RequeteDossierRenommer(Chemin,pNouveau);

end;

{ TACCFichierSync }


function TACCFichierSync.CheminLocal: string;
begin

    Result := Serveur.ObtenirAdresse(Chemin);
end;





constructor TACCFichierSync.Create(pServeur: TACCServeur);
begin
  Serveur := pServeur;  
end;

destructor TACCFichierSync.Destroy;
begin

  inherited;
end;

function TACCFichierSync.Fichier: TFileStream;
var
  Adresse : String;
begin
  Adresse := ExtractFilePath(CheminLocal);
  if not DirectoryExists(Adresse) then
    CreateDir(Adresse);
  Result := TFileStream.Create(CheminLocal,fmCreate);
end;

function TACCFichierSync.GetInfo: TACFichierInfo;
begin
  result := FInfo;
end;

function TACCFichierSync.GetNom: String;
begin
  result := ExtractFileName(FChemin);
end;

function TACCFichierSync.RequeteLire:boolean;
begin
  if not Info.TypeDossier then
    Result := Serveur.RequeteFichierLire(Chemin)
  else
    Result := Serveur.RequeteDossierLire(Chemin);

end;

procedure TACCFichierSync.SetChemin(const Value: string);
begin
  FChemin := Value;
end;

procedure TACCFichierSync.SetData(const Value: Pointer);
begin
  FData := Value;
end;




function TACCFichierSync.RequeteMettreAJour : boolean;
begin
  if Info.TypeDossier then
    result :=Serveur.RequeteDossierMAJInfo(Chemin)
  else
    result := Serveur.RequeteFichierMAJInfo(Chemin);

end;

function TACCFichierSync.RequeteSauvegarder: boolean;
begin
  if Info.TypeDossier then
    result :=Serveur.RequeteDossierSauvegarder(Chemin)
  else
    result := Serveur.RequeteFichierMAJInfo(Chemin);

end;

function TACCFichierSync.Exist: boolean;
begin
  if not Info.TypeDossier then
    result := FileExists(CheminLocal)
  else
    result := DirectoryExists(CheminLocal);
end;

function TACCFichierSync.Ajour: boolean;
var
  lInfo : TACFichierInfo;
begin
 result := false;
 if Exist then
 begin
  lInfo := ACLireFichierInfo(CheminLocal);
  result := ACSameDateTime(lInfo.Date.Modification,FInfo.Date.Modification);
 end;

end;

{ TACCFichierList }

function TACCFichierSyncList.Add(Item: TACCFichierSync): Integer;
begin
  result := inherited Add(Item);
end;


function TACCFichierSyncList.Add(pChemin: string;pTypeDossier : boolean = false): TACCFichierSync;
begin
  Result := TACCFichierSync.Create(Serveur);
  Result.FChemin := pChemin;
  Result.FInfo.TypeDossier := pTypeDossier;
  Add(Result);
end;

procedure TACCFichierSyncList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;
  inherited;

end;


constructor TACCFichierSyncList.Create(pServeur: TACCServeur);
begin
  inherited Create;
  Serveur := pServeur;
  FData := nil;
end;

procedure TACCFichierSyncList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TACCFichierSyncList.Destroy;
begin
  Clear;
  inherited;
end;

function TACCFichierSyncList.Extract(Item: TACCFichierSync): TACCFichierSync;
begin
  result := inherited Extract(Item);
end;

function TACCFichierSyncList.First: TACCFichierSync;
begin
  Result := inherited First;
end;

function TACCFichierSyncList.Get(Index: Integer): TACCFichierSync;
begin
  Result := inherited Get(Index);
end;

function TACCFichierSyncList.GetChemin(Index: string): TACCFichierSync;
var
  i : Integer;
begin
  Result := nil;
  for i :=  0 to Count - 1 do
  begin
    if SameText(Index,Items[i].FChemin) then
    begin
      Result := Items[i];
      exit;
    end;
  end;

end;



function TACCFichierSyncList.IndexOf(Item: TACCFichierSync): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TACCFichierSyncList.Insert(Index: Integer; Item: TACCFichierSync);
begin
  inherited Insert(Index,Item);
end;

function TACCFichierSyncList.Last: TACCFichierSync;
begin
  Result := inherited Last;
end;

procedure TACCFichierSyncList.LoadFromFile(FileName: TFileName);
var
  Ligne : string;
  Liste : TStringList;
  Fichier : TStringList;
  i : integer;
begin
  Fichier := TStringList.Create;
  Fichier.LoadFromFile(FileName);
  Liste := TStringList.Create;
  for i := 0 to Fichier.Count - 1 do
  begin
    Ligne := Fichier[i];
    Liste.Clear;
    ExtractListFromLine('|',Ligne,Liste);
//    ExtractStrings(['|'],[' '],pChar(Ligne),Liste);

    Add(Liste[0],StrToBool(Liste[1]));
  end;
  Liste.Free;
  Fichier.Free;

end;

procedure TACCFichierSyncList.Put(Index: Integer; Item: TACCFichierSync);
begin
  inherited Put(Index,Item);

end;

function TACCFichierSyncList.Remove(Item: TACCFichierSync): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;


procedure TACCFichierSyncList.SaveToFile(FileName: TFileName);
var
  Fichier : TStringList;
  i : Integer;
begin
  Fichier := TStringList.Create;
  for i := 0 to Count - 1 do
    Fichier.Add(
      Items[i].FChemin+'|'
      +BoolToStr(Items[i].FInfo.TypeDossier));
  Fichier.SaveToFile(FileName);
  Fichier.Free;

end;



procedure TACCFichierSyncList.SetData(const Value: Pointer);
begin
  FData := Value;
end;

procedure TACCPoste.Synchronisation;
var
  i : integer;

begin
  for i := 0 to Serveurs.Count - 1 do
  begin
    if Serveurs[i].SynchronisationActive then
      Serveurs[i].Synchronisation;
  end

end;

end.
