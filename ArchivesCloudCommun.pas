unit ArchivesCloudCommun;

interface
uses
  classes,Zip, Windows,ShellAPI,Dialogs,Registry,FileCtrl,ComObj,DateUtils,Types,SevenZipVCL,math;
type
    TACContenueDossierList = class;

    TACContenueDossier = class;
    TACCodeFonction = (
      ACCFConnexion,
      ACCFConfigServeur,
      ACCFMessage,
      ACCFDossiersPartageLister,
      ACCFDossierLister,
      ACCFFichierLire,
      ACCFFichierMAJInfo,
      ACCFFichierSupprimer,
      ACCFFichierDeplacer,
      ACCFFichierCopier,
      ACCFFichierEcrire,
      ACCFFichierRenommer,
      ACCFDossierLire,
      ACCFDossierMAJInfo,
      ACCFDossierSupprimer,
      ACCFDossierDeplacer,
      ACCFDossierCopier,
      ACCFDossierEcrire,
      ACCFDossierEcrireTerminer,
      ACCFDossierCreer,
      ACCFDossierRenommer,
      ACCFDossierSauvegarder,
      ACCFExtinctionClient,
      ACCFOperationAvancement,
      ACCFOperationEnCours,
      ACCFOperationTerminer,
      ACCFOperationEchoue,
      ACCFSyncFichierLire,
      ACCFSyncFichierEcrire,
      ACCFSyncFichierEcrireMAJ
      );

    TACProtocoleVersion = packed record
      case Integer of
      0 : (Majeur,Mineur : byte);
      1 : (Valeur : smallint);
    end;
    TACTypeClient = (TACArchiveSync,TACArchiveBackup);

    TACServeurConfig = record
      Nom : string[255];
      AnnulerExtinctionClient : boolean;
    end;

    TACClientConfig = record
      Nom : string[255];
      TypeClient : TACTypeClient;
    end;

    TACAvancementEven = procedure (Sender : TObject;
      Libelle : string;
      Max : integer;
      Position : integer)of object;



    TACConnexionResult = (ACConRefuse,ACConAccepte);
    TACRequeteResult = (ACReqReussi,ACReqEchec);
    TACResultSync = (ACRSyncAJour,ACRSyncNonAJour);

    TACSyncMode = (ACSyncModeMAJ,ACSyncModeLecture,ACSyncModeEcriture,ACSyncModeNoSupress);

    TACParamBlock = array[0..1024] of byte;

    TACMessage = record
        Code : TACCodeFonction;
        Params : TACParamBlock;
    end;

    TACFichierDate = record
      Creation : TDateTime;
      Modification : TDateTime;
      DernierAcces : TDateTime;
      SystemeTime : record
        CreateTime, LastAccess, LastWrite : TFileTime;
      end;
    end;

    TACFichierOperation = (AcFoAJour, AcFoLire,AcFoEcrire,AcFoSupprimerLocal,AcFoSupprimerDistant,AcFoSyncLire,AcFoSyncEcrire);

    TACFichierInfo = record
        TypeDossier : boolean;
        NiveauDossier  : integer;
//        DossierParent : String[255];
        Nom : String[255];
        Taille : Integer;
        DateModificationDossier : TDateTime;
        Date : TACFichierDate;
        Operation : TACFichierOperation;

    end;

    TACMessageType = (ACMessageRequete,ACMessageInfo,ACMessageErreur);
    TACMessageEvent = procedure(Sender: TObject;pMessageType : TACMessageType; pMessage : string) of object;

    PACFichierInfo = ^TACFichierInfo;

    TACFichierInfoList = array of TACFichierInfo;

  TACContenueFichier = class(TObject)
  private
    function GetDateModification: TDateTime;
    function GetNom: string;
    function GetSyncMode: TACSyncMode;

  protected

  public
    Dossier : TACContenueDossier;
    Info : TACFichierInfo;
    property SyncMode : TACSyncMode read GetSyncMode;
    function Chemin : string;
    function CheminLocal : string;
    function ObtenirAdresse : string;
    property Nom : string read GetNom;
    property DateModification : TDateTime read GetDateModification;
    constructor Create(pDossier : TACContenueDossier);
    destructor Destroy; override;
    
  published

  end;

  TACContenueFichierList = class(TList)
  private
    function Get(Index: Integer): TACContenueFichier;
    procedure Put(Index: Integer; Item: TACContenueFichier);
    function GetNoms(Index: String): TACContenueFichier;
  public
    Dossier : TACContenueDossier;
    constructor Create(pDossier : TACContenueDossier);overload;
    destructor Destroy; override;
    function Add(Item: TACContenueFichier): Integer;overload;
    function Add : TACContenueFichier;overload;
    function Extract(Item: TACContenueFichier): TACContenueFichier;
    function First: TACContenueFichier;
    function IndexOf(Item: TACContenueFichier): Integer;
    procedure Insert(Index: Integer; Item: TACContenueFichier);
    function Last: TACContenueFichier;
    procedure Delete(Index: Integer);
    function Remove(Item: TACContenueFichier): Integer;
    property Items[Index: Integer]: TACContenueFichier read Get write Put; default;
    property Noms[Index: String]: TACContenueFichier read GetNoms;
    procedure Clear; override;
    
  end;

  TACContenueDossier = class(TObject)
  private
    FSyncMode : TACSyncMode;
    FSyncAvanceMode: boolean;
    function GetDateModification: TDateTime;
    function GetNom: string;
    function GetSyncMode: TACSyncMode;
    procedure SetSyncMode(const Value: TACSyncMode);
    procedure SetSyncAvanceMode(const Value: boolean);

  protected

  public
    Adresse : string;
    Parent : TACContenueDossier;
    Info : TACFichierInfo;
    Dossiers : TACContenueDossierList;
    Fichiers : TACContenueFichierList;
    Journal : TStrings;
    property SyncMode : TACSyncMode read GetSyncMode write SetSyncMode;
    property Nom : string read GetNom;
    property DateModification : TDateTime read GetDateModification;
    property SyncAvanceMode : boolean read FSyncAvanceMode write SetSyncAvanceMode;
    function Chemin : string;
    function CheminLocal : string;
    function Nombre : integer;
    constructor Create(pParent : TACContenueDossier);overload;
    constructor Create(pAdresse : String);overload;
    destructor Destroy; override;
    procedure Generer(Recursif : boolean = false);
    procedure ExporteListeFichierInfo(FichiersInfo : TACFichierInfoList);
    procedure ImporterListeFichierInfo(FichiersInfo : TACFichierInfoList;Nombre:Integer);
    procedure ExporteListeFichierInfoVersStream(Stream : TStream);
    procedure ImporterListeFichierInfoDeStream(Stream: TStream);
    procedure ExporteListeFichierInfoVersFichier(FichierNom : string);
    procedure ImporterListeFichierInfoDeFichier(FichierNom : string);
    procedure Comparer(pContenueDossier : TACContenueDossier);
    procedure GenererListeFichierALire(Liste : TStrings;Adresse : string);
    procedure GenererListeFichierAEcrire(Liste : TStrings;Adresse : string);
    procedure GenererListeDesOperations(Liste : TStrings;Adresse : string;Operation : TACFichierOperation);

  published

  end;

  TACContenueDossierList = class(TList)
  private
    function Get(Index: Integer): TACContenueDossier;
    procedure Put(Index: Integer; Item: TACContenueDossier);
    function GetNoms(Index: String): TACContenueDossier;
    
  public
    Parent : TACContenueDossier;
    constructor Create(pParent : TACContenueDossier);overload;
    destructor Destroy; override;
    function Add(Item: TACContenueDossier): Integer;overload;
    function Add : TACContenueDossier;overload;
    function Extract(Item: TACContenueDossier): TACContenueDossier;
    function First: TACContenueDossier;
    function IndexOf(Item: TACContenueDossier): Integer;
    procedure Insert(Index: Integer; Item: TACContenueDossier);
    function Last: TACContenueDossier;
    procedure Delete(Index: Integer);
    function Remove(Item: TACContenueDossier): Integer;
    property Items[Index: Integer]: TACContenueDossier read Get write Put; default;
    property Noms[Index: String]: TACContenueDossier read GetNoms; 
    procedure Clear; override;

  end;






const
    AC_SERVEUR_TCP_PORT = 4451;
    PROTOCOLE_VERSION : TACProtocoleVersion = (Majeur:3;Mineur:0);
    SEVEN_ZIP_TAILLE_VOLUME = 10000000; // 10 Mo
    SYNC_MODE_SEUIL_TAILLE = 3000000;

function ACConnexionResult(Resultat : TACConnexionResult):integer;
function ACCodeFonction(Code : TACCodeFonction):Integer;
function ACRequeteResult(Resultat : TACRequeteResult):Integer;
procedure ACContenueDossier(Chemin:String;Contenues : TACContenueDossierList;Recursif : boolean = false;NiveauDossier : integer = 0);
procedure ZipperFichier(Fichier : String;FichierZip:String);
procedure ZipperFichierListe(Dossier : String;Liste:TStrings; FichierZip:String);
procedure ZipperDossier(Dossier : String;FichierZip:String);
procedure DeZipperFichier(Fichier : String;FichierZip:String);
procedure DeZipperDosssier(Dossier : String;FichierZip:String);
function GetTempDirectory: String;
function ACLireFichierDates(Fichier : string): TACFichierDate;
function ACLireFichierInfo(Fichier : string): TACFichierInfo;
function CopyDirectory(Source:String;Destination:string;CopyEvent : TNotifyEvent = nil): boolean;
Function DeleteDirectory(Dir : string): Boolean;
Function MoveDirectory(FromDir : string;ToDir :String): Boolean;
function DossierMesDocuments: string;
function ACCompareDateTime(const A,B:TDateTime;MinuteTolerance : integer = 1):TValueRelationship;
function ACSameDateTime(const A,B:TDateTime;MinuteTolerance : integer = 1):boolean;
function ACMemeVersionProtocole(A , B : TACProtocoleVersion):boolean;
function ACProtocoleToString(Protocole : TACProtocoleVersion):string;
function DeSevenZipperFichier(Fichier : String;FichierSevenZip:String;OnProgressAdv: T7zProgressAdvEvent =nil;pPassword : string = '') : boolean;
function DeSevenZipperDossier(Dossier : String;FichierSevenZip:String;OnProgressAdv: T7zProgressAdvEvent =nil;pPassword : string = '') : boolean;
function SevenZipperFichier(Fichier : String;FichierSevenZip:String;VolumeTaille : integer = 0;VolumeListe : TStrings=nil;OnProgressAdv: T7zProgressAdvEvent =nil;pPassword : string = ''): boolean;
function SevenZipperFichierListe(Dossier : String;Liste:TStrings; FichierSevenZip:String;VolumeTaille : integer = 0;VolumeListe : TStrings=nil;OnProgressAdv: T7zProgressAdvEvent =nil;pPassword : string = ''): boolean;
function SevenZipperDossier(Dossier : String;FichierSevenZip:String;VolumeTaille : integer = 0;VolumeListe : TStrings=nil;OnProgressAdv: T7zProgressAdvEvent =nil;pPassword : string = ''): boolean;
function SizeFileToStr(Taille : integer):string;
function ACSyncModeToStr(Mode : TACSyncMode):string;
procedure ExtractListFromLine(car : char;Line : string;List : TStrings);
function ACResultSync(Resultat : TACResultSync):Integer;
function ACCodeFonctionVersStr(Code : TACCodeFonction):string;
function ACMessageTypeVersStr(pMessageType : TACMessageType):string;

implementation

uses SysUtils, TypInfo;
function ACMessageTypeVersStr(pMessageType : TACMessageType):string;
begin
  Case pMessageType of
  ACMessageRequete: result := 'REQ';
  ACMessageInfo : result := 'INFO';
  ACMessageErreur : Result := 'ERR';
  end;

end;

function ACResultSync(Resultat : TACResultSync):Integer;
begin
  result := Integer(Resultat);
end;

function ACCodeFonctionVersStr(Code : TACCodeFonction):string;
begin
  result := GetEnumName(TypeInfo(TACCodeFonction), integer(Code)) ;
end;

procedure ExtractListFromLine(car : char;Line : string;List : TStrings);
var
  pLine : pChar;
  Text : string;
begin
  pLine := pChar(Line);
  Text := '';
  while pLine^<>#0 do
  begin
    if pLine^<> car then
       Text := Text + pLine^
    else
    begin
      List.Add(Text);
      Text :='';
    end;
    inc(pLine);
  end;
  if Text <> '' then
    List.Add(Text);



end;


function ACSyncModeToStr(Mode : TACSyncMode):string;
begin
  Case Mode of
  ACSyncModeMAJ : result := 'mise � jour des fichiers';
  ACSyncModeLecture : result := 'lecture des fichiers';
  ACSyncModeEcriture : result := '�criture des fichiers';
  ACSyncModeNoSupress : result := 'sans suppresion de fichiers';
  end;
end;

function SizeFileToStr(Taille : integer):string;
begin
  if Taille < power(2,10) then
  begin
    Result := Format('%d o',[Taille]);
    exit;
  end;

  if Taille < power(2,20) then
  begin
    Result := Format('%.2f Ko',[Taille / power(2,10)]);
    exit;
  end;

  if Taille < power(2,30) then
  begin
    Result := Format('%.2f Mo',[Taille / power(2,20)]);
    exit;
  end;

  (*
  if Taille < power(2,40) then
  begin
    Result := Format('%.2f Go',[Taille / power(2,30)]);
    exit;
  end;
  *)
  Result := Format('%.2f Go',[Taille / power(2,30)]);
end;


function ACProtocoleToString(Protocole : TACProtocoleVersion):string;
begin
  With Protocole do
  begin
    Result := Format('%d.%d',[Majeur,Mineur]);
  end;
end;

function ACMemeVersionProtocole(A , B : TACProtocoleVersion):boolean;
begin
  result := (A.Majeur = B.Majeur)and(A.Mineur = B.Mineur);
end;
function ACRequeteResult(Resultat : TACRequeteResult):Integer;
begin
  Result := integer(Resultat);
end;

function ACCompareDateTime(const A,B:TDateTime;MinuteTolerance : integer = 1):TValueRelationship;
begin
  if CompareDateTime(A,IncMinute(B,-MinuteTolerance)) = LessThanValue then
  begin
    result := LessThanValue;
    exit;
  end;
  if CompareDateTime(A,IncMinute(B,MinuteTolerance))= GreaterThanValue then
  begin
    result := GreaterThanValue;
    exit;
  end;
  result := EqualsValue;
end;

function ACSameDateTime(const A,B:TDateTime;MinuteTolerance : integer = 1):boolean;
begin
  Result := ACCompareDateTime(A,B,MinuteTolerance) = EqualsValue;
end;

function DossierMesDocuments: string;
const
  MyShellFolder = 'Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders';
begin
  Result := GetRegStringValue(
                        MyShellFolder,
                        'Personal',
                        HKEY_CURRENT_USER);

end;


function GetTempDirectory: String;
begin
  result := GetEnvironmentVariable('TEMP')
end;

function DeSevenZipperFichier(Fichier : String;FichierSevenZip:String;OnProgressAdv: T7zProgressAdvEvent =nil;pPassword : string = ''):boolean;
var
  SevenZip : TSevenZip;
  Overwrite : Boolean;
begin
  result := false;
  try
    SevenZip := TSevenZip.create(nil);
    SevenZip.Password := pPassword;
    SevenZip.SZFileName := FichierSevenZip;
    SevenZip.OnProgressAdv := OnProgressAdv;
    SevenZip.ExtractOptions := [ExtractOverwrite];

  //  Overwrite := (SevenZip.ExtractOptions * [eoFreshen, eoUpdate, eoTest] = []);
  //  if Overwrite then
  //    ShowMessage('Ecrasement Valide');
    SevenZip.ExtrBaseDir := ExtractFilePath(Fichier);
    Result := SevenZip.Extract = kOK;
  except

  end;
  SevenZip.Free;
end;


function DeSevenZipperDossier(Dossier : String;FichierSevenZip:String;OnProgressAdv: T7zProgressAdvEvent =nil;pPassword : string = '') : boolean;
var
  SevenZip : TSevenZip;
begin
  try
    SevenZip := TSevenZip.create(nil);
    SevenZip.Password := pPassword;
    SevenZip.SZFilename := FichierSevenZip;
    SevenZip.OnProgressAdv := OnProgressAdv;
    SevenZip.ExtractOptions := [ExtractOverwrite];

  //  SevenZip.ShowProgressDialog := false;
  //  SevenZip.ExtractOptions := [eoUpdate, eoWithPaths];
    SevenZip.ExtrBaseDir := Dossier;
    result := SevenZip.Extract = kOK;
  finally
    SevenZip.Free;
  end
end;



function SevenZipperFichier(
      Fichier : String;
      FichierSevenZip:String;
      VolumeTaille : integer = 0;
      VolumeListe : TStrings=nil;
      OnProgressAdv: T7zProgressAdvEvent =nil;
      pPassword : string = ''): boolean;
var
  SevenZip : TSevenZip;
begin
  Result := false;

  try
    SevenZip := TSevenZip.create(nil);
    SevenZip.Password := pPassword;
    SevenZip.SZFileName := FichierSevenZip;
    SevenZip.LZMACompressType := LZMA;
    SevenZip.LZMACompressStrength := FAST;
    SevenZip.VolumeSize := VolumeTaille;
    SevenZip.Files.AddString(Fichier);
    SevenZip.AddRootDir := ExtractFilePath(Fichier);
    SevenZip.AddOptions := [];
    SevenZip.OnProgressAdv := OnProgressAdv;
    SevenZip.Add;
    if (VolumeTaille  > 0) and (VolumeListe <> nil) then
    begin
      VolumeListe.Clear;
      VolumeListe.AddStrings(SevenZip.VolumesWritten);
    end;
    result := SevenZip.LastError = 0;
    //SevenZip.NumberOfFiles;
  finally
    SevenZip.Free;
  end;
end;



function SevenZipperFichierListe(
        Dossier : String;
        Liste:TStrings;
        FichierSevenZip:String;
        VolumeTaille : integer = 0;
        VolumeListe : TStrings=nil;
        OnProgressAdv: T7zProgressAdvEvent =nil;pPassword : string = ''): boolean;
var
  SevenZip : TSevenZip;
  i : integer;
begin
  result := false;


  try
    SevenZip := TSevenZip.create(nil);
    SevenZip.Password := pPassword;
    SevenZip.SZFilename := FichierSevenZip;
    SevenZip.AddRootDir := Dossier;
    SevenZip.LZMACompressType := LZMA;
    SevenZip.LZMACompressStrength := FAST;
    SevenZip.VolumeSize := VolumeTaille;
    SevenZip.OnProgressAdv := OnProgressAdv;
    for i := 0 to Liste.Count - 1 do
      SevenZip.Files.AddString(Liste[i]);
    SevenZip.AddOptions := [AddRecurseDirs];
    SevenZip.Add;
    if (VolumeTaille  > 0) and (VolumeListe <> nil) then
    begin
      VolumeListe.Clear;
      VolumeListe.AddStrings(SevenZip.VolumesWritten);
    end;
    Result := SevenZip.LastError = 0;

  finally
    SevenZip.Free;
  end;
end;



function SevenZipperDossier(
          Dossier : String;
          FichierSevenZip:String;
          VolumeTaille : integer = 0;
          VolumeListe : TStrings = nil;
          OnProgressAdv: T7zProgressAdvEvent =nil;pPassword : string = ''): boolean;
var
  SevenZip : TSevenZip;
begin
  result := false;
  try
    SevenZip := TSevenZip.create(nil);
    SevenZip.Password := pPassword;
    SevenZip.SZFilename := FichierSevenZip;
    SevenZip.LZMACompressType := LZMA;
    SevenZip.LZMACompressStrength := FAST;
    SevenZip.VolumeSize := VolumeTaille;
    SevenZip.Files.AddString(Dossier+'\*.*');
    SevenZip.AddRootDir :=  ExtractFilePath(Dossier);
    SevenZip.OnProgressAdv := OnProgressAdv;
    SevenZip.AddOptions := [AddRecurseDirs];
    SevenZip.Add;
    if (VolumeTaille  > 0) and (VolumeListe <> nil) then
    begin
      VolumeListe.Clear;
      VolumeListe.AddStrings(SevenZip.VolumesWritten);
    end;
    result := SevenZip.LastError = 0;
    
  finally
    SevenZip.Free;
  end;
end;





procedure DeZipperFichier(Fichier : String;FichierZip:String);
var
  Zip : TZip;
  Overwrite : Boolean;
begin

  Zip := TZip.create(nil);
  Zip.Filename := FichierZip;
  Zip.ShowProgressDialog := false;
  Overwrite := (Zip.ExtractOptions * [eoFreshen, eoUpdate, eoTest] = []);
//  if Overwrite then
//    ShowMessage('Ecrasement Valide');
  Zip.ExtractPath := ExtractFilePath(Fichier);
  Zip.Extract;
  Zip.Free;
end;


procedure DeZipperDosssier(Dossier : String;FichierZip:String);
var
  Zip : TZip;
begin
  Zip := TZip.create(nil);
  Zip.Filename := FichierZip;
  Zip.ShowProgressDialog := false;
  Zip.ExtractOptions := [eoUpdate, eoWithPaths];
  Zip.ExtractPath := ExtractFilePath(Dossier);
  Zip.Extract;
  Zip.Free;
end;


procedure ZipperFichier(Fichier : String;FichierZip:String);
var
  Zip : TZip;
begin
  Zip := TZip.create(nil);
  Zip.Filename := FichierZip;
  Zip.ShowProgressDialog := false;

  Zip.FileSpecList.Add(Fichier);
  Zip.AddOptions := [aoUpdate];

  Zip.Add;

  Zip.Free;
end;



procedure ZipperFichierListe(Dossier : String;Liste:TStrings; FichierZip:String);
var
  Zip : TZip;
begin
  Zip := TZip.create(nil);
  Zip.Filename := FichierZip;
  Zip.ShowProgressDialog := false;

  Zip.AddPath := ExtractFilePath(Dossier);
  Zip.FileSpecList.AddStrings(Liste);
  Zip.AddOptions := [aoRecursive, aoFolderEntries];
  Zip.Add;

  Zip.Free;
end;



procedure ZipperDossier(Dossier : String;FichierZip:String);
var
  Zip : TZip;
begin
  Zip := TZip.create(nil);
  Zip.Filename := FichierZip;
  Zip.ShowProgressDialog := false;

  Zip.AddPath := ExtractFilePath(Dossier);
  Zip.FileSpecList.Add('\'+ExtractFileName(Dossier)+'\*.*');
  Zip.AddOptions := [aoRecursive,aoFolderEntries];
  Zip.Add;

  Zip.Free;
end;




function ACConnexionResult(Resultat : TACConnexionResult):integer;
begin
    result := Integer(Resultat);
end;

function ACCodeFonction(Code : TACCodeFonction):Integer;
begin
  result := Integer(Code);
end;


function FileTimeToDateTime(FileTime: TFileTime): TDateTime;
var
   LocalFileTime: TFileTime;
   SystemTime: TSystemTime;
begin
   FileTimeToLocalFileTime(FileTime, LocalFileTime) ;
   FileTimeToSystemTime(LocalFileTime, SystemTime) ;
   Result := SystemTimeToDateTime(SystemTime) ;
end;

procedure ACContenueDossier(Chemin:String;Contenues : TACContenueDossierList;Recursif : boolean = false;NiveauDossier : integer = 0);
var
  sr : TSearchRec;
  Attrs : Integer;
  DateFichier : TDateTime;
  DossierParent : String;
  DateModificationDossier : TDateTime;

begin
  (*
  if Not DirectoryExists(Chemin) then
    exit;

  DateModificationDossier := ACLireFichierInfo(Chemin).Date.Modification;
  Attrs := faAnyFile+faDirectory+faHidden+faSysFile+faArchive;
  if FindFirst(Chemin+'\*.*',Attrs,sr) =  0 then
  begin
    repeat
      if (sr.Name <> '.') and (sr.Name <> '..')then
      begin
        if (faDirectory and sr.Attr) <> 0 then
        begin
          with Contenues.Add do
          begin
            FichierInfo.DossierParent := ExtractFileName(Chemin);
            FichierInfo.NiveauDossier := NiveauDossier;
            FichierInfo.TypeDossier := true;
            FichierInfo.Nom := sr.Name;
            FichierInfo.Date.Modification := FileDateToDateTime(sr.Time);
            FichierInfo.DateModificationDossier := DateModificationDossier;
            if Recursif then
              ACContenueDossier(Chemin+'\'+sr.Name,Contenues,Recursif,NiveauDossier+1);

          end;
        end
        else
        begin
          with Contenues.Add do
          begin
            FichierInfo.TypeDossier := false;
            FichierInfo.Nom := sr.Name;
            FichierInfo.Date.Modification := FileDateToDateTime(sr.Time);
            FichierInfo.Taille := sr.Size;
            FichierInfo.NiveauDossier := NiveauDossier;
            FichierInfo.DateModificationDossier := DateModificationDossier;

          end;
        end;
      end;
    until FindNext(sr)<>0;
  end;
  FindClose(sr);
  *)
end;

procedure ACContenueDossierReconstruire(ContenueLocal : Array of TACFichierInfo;NombreLocal : integer; ContenueDistant : Array of TACFichierInfo;NombreDistant:Integer);
(*
    function ARechercherLeFichierDistant(pFichierLocal : PACFichierInfo):PACFichierInfo;
    var
      i : integer;
    begin
      result := nil;
      for i := 0 to NombreDistant - 1 do
      begin
          if  SameText(pFichierLocal.Nom,ContenueDistant[i].Nom) and
              SameText(pFichierLocal.DossierParent,ContenueDistant[i].DossierParent)
              and (pFichierLocal.NiveauDossier = ContenueDistant[i].NiveauDossier) then
              begin
                  result := @ContenueDistant[i];
              end;

      end;
    end;

    function ARechercherLeDossierDistant(pDossier : String;NiveauDossier : Integer ):PACFichierInfo;
    var
      i : integer;
    begin
      result := nil;
      for i := 0 to NombreDistant - 1 do
      begin
          if  SameText(pDossier,ContenueDistant[i].Nom) and
              and (NiveauDossier = ContenueDistant[i].NiveauDossier) then
              begin
                  result := @ContenueDistant[i];
              end;

      end;
    end;
  *)


var
  DateFichier : TDateTime;
  DossierParent : String;
  i,j : integer;
  pFichierLocal,pFichierDistant : PACFichierInfo;

begin
(*
  // 1 : Rechercher dans l'ensemble de fichiers locaux
  for i := 0 to NombreLocal - 1 do
  begin
    pFichierLocal := @ContenueLocal[i];
    pFichierDistant := ARechercherLeFichierDistant(pFichierLocal);
    if pFichierLocal.TypeDossier then
    begin

    end
    else
    begin
    end;
    // 2 : Rechercher le fichier dans les fichiers distant;

    if pFichierDistant = nil then
    begin
      // 3 : si le fichier n'existe pas en local



    end;


      if
          SameText(pFichierLocal.Nom,pFichierDistant.Nom) and
          SameText(pFichierLocal.DossierParent,pFichierDistant.DossierParent)
          and (pFichierLocal.NiveauDossier = pFichierDistant.NiveauDossier) then
      begin
      (*
          if not pFichierLocal.TypeDossier then
          begin
            case CompareDateTime(pFichierLocal.Date.Modification,pFichierLocal.Date.Distant) of
            -1 : pFichierLocal.Operation :=AcFoLire;
            0 : pFichierLocal.Operation :=AcFoAJour;
            1 : pFichierLocal.Operation :=AcFoEcrire;
            end;
          end;

      end;

    end;


  end;
  *)

end;


function CopyDirectory(Source:String;Destination:string;CopyEvent : TNotifyEvent = nil): boolean;
var
  sr : TSearchRec;
  Attrs : Integer;
begin
//  result := CopyDir(Source,Destination);
//  exit;
  Attrs := faAnyFile+faDirectory+faHidden+faSysFile+faArchive;

  if not DirectoryExists(destination) then
    ForceDirectories(destination);


  if FindFirst(Source+'\*.*',Attrs,sr) =  0 then
  begin
    repeat

      if (sr.Name <> '.') and (sr.Name <> '..')then
      begin
        if (faDirectory and sr.Attr) <> 0 then
          result := CopyDirectory(Source+'\'+Sr.Name,Destination+'\'+Sr.Name,CopyEvent)
        else
          result := CopyFile(pchar(Source+'\'+Sr.Name),pCHar(Destination+'\'+Sr.Name),false);
      end;

    until FindNext(sr)<>0;
  end;
  FindClose(sr);
end;

Function DeleteDirectory(Dir : string): Boolean;
var
  SHFileOpStruct : TSHFileOpStruct;
  DirBuf : array [0..255] of char;
begin
  try
   Fillchar(SHFileOpStruct,Sizeof(SHFileOpStruct),0) ;
   FillChar(DirBuf, Sizeof(DirBuf), 0 ) ;
   StrPCopy(DirBuf, Dir) ;
   with SHFileOpStruct do begin
    Wnd := 0;
    pFrom := @DirBuf;
    wFunc := FO_DELETE;
    fFlags := FOF_ALLOWUNDO;
    fFlags := fFlags or FOF_NOCONFIRMATION;
    fFlags := fFlags or FOF_SILENT;
   end;
    Result := (SHFileOperation(SHFileOpStruct) = 0) ;
   except
    Result := False;
  end;
end;

Function MoveDirectory(FromDir : string;ToDir :String): Boolean;
var
  SHFileOpStruct : TSHFileOpStruct;
  FromBuf : array [0..255] of char;
  ToBuf : array [0..255] of char;
begin
  try
   Fillchar(SHFileOpStruct,Sizeof(SHFileOpStruct),0) ;
   FillChar(FromBuf, Sizeof(FromBuf), 0 ) ;
   StrPCopy(FromBuf, FromDir) ;

   FillChar(ToBuf, Sizeof(ToBuf), 0 ) ;
   StrPCopy(ToBuf, ToDir) ;

   with SHFileOpStruct do begin
    Wnd := 0;
    pFrom := @FromBuf;
    pFrom := @ToBuf;
    wFunc := FO_Move;
    fFlags := FOF_ALLOWUNDO;
    fFlags := fFlags or FOF_NOCONFIRMATION;
    fFlags := fFlags or FOF_SILENT;
   end;
    Result := (SHFileOperation(SHFileOpStruct) = 0) ;
   except
    Result := False;
  end;
end;

{ TACContenueDossier }



constructor TACContenueDossier.Create(pParent: TACContenueDossier);
begin
  Create('');
  Parent := pParent;
  FSyncMode := pParent.SyncMode;
  
end;

procedure TACContenueDossier.Comparer(
  pContenueDossier: TACContenueDossier);
    procedure ComparerDossier(A : TACContenueDossier;B:TACContenueDossier);
    var
      i,j : Integer;
      DossierA,DossierB : TACContenueDossier;
      FichierA,FichierB : TACContenueFichier;
    begin
      // Rechercher le dossier manquant de B dans  A
      for i := 0  to A.Dossiers.Count - 1 do
      begin
        DossierA := A.Dossiers[i];
        DossierA.Info.Operation := AcFoAJour;
        DossierB := B.Dossiers.Noms[DossierA.Nom];

        if DossierB = nil then
        begin
          // le dossier n'est pas present en B
          // Comparer la date A et de B
          Case A.SyncMode of
          ACSyncModeMAJ :
            Case ACCompareDateTime(A.DateModification,B.DateModification) of
            -1: DossierA.Info.Operation := AcFoSupprimerLocal; // A est moin recent donc supprimer en A
            0: DossierA.Info.Operation := AcFoEcrire; // A est plus recent ou egal ecrire dans B
            1: DossierA.Info.Operation := AcFoEcrire;
            end;
          ACSyncModeLecture :
            DossierA.Info.Operation := AcFoSupprimerLocal;
          ACSyncModeEcriture :
            DossierA.Info.Operation := AcFoEcrire;
          ACSyncModeNoSupress :
            DossierA.Info.Operation := AcFoEcrire;
          end;
        end
        else
        begin
          // le dossier existe en B
          // Comparer A et B
          ComparerDossier(DossierA,DossierB);
        end;
      end;

      // Rechercher le dossier manquant de A dans  B
      for i := 0  to B.Dossiers.Count - 1 do
      begin
        DossierB := B.Dossiers[i];
        DossierA := A.Dossiers.Noms[DossierB.Nom];
        if DossierA = nil then
        begin
          // le dossier n'existe pas dans A
          // Comparer la date de A et B
          DossierA := A.Dossiers.Add;
          DossierA.Info := DossierB.Info;
          Case A.SyncMode of
          ACSyncModeMAJ :
            Case ACCompareDateTime(A.DateModification,B.DateModification) of
            -1: DossierA.Info.Operation := AcFoLire; // A est moins recent ou egal Lire de B
            0: DossierA.Info.Operation := AcFoLire;
            1: DossierA.Info.Operation := AcFoSupprimerDistant; // A est plus recent supprimer dans B
            end;
          ACSyncModeLecture :
            DossierA.Info.Operation := AcFoLire;
          ACSyncModeEcriture :
            DossierA.Info.Operation := AcFoSupprimerDistant;
          ACSyncModeNoSupress :
            DossierA.Info.Operation := AcFoLire;
          end;
        end;

      end;

      // Rechercher le fichier manquant de B dans  A
      for i := 0  to A.Fichiers.Count - 1 do
      begin
        FichierA := A.Fichiers[i];
        FichierB := B.Fichiers.Noms[FichierA.Nom];
        if FichierB = nil then
        begin
          // le fichier n'est pas present en B
          // Comparer la date A et de B
          Case A.SyncMode of
          ACSyncModeMAJ :
            Case ACCompareDateTime(A.DateModification,B.DateModification) of
            -1: FichierA.Info.Operation := AcFoSupprimerLocal; // A est moin recent donc supprimer en A
            0: FichierA.Info.Operation := AcFoEcrire;  // A est plus recent ou egal ecrire dans B
            1: FichierA.Info.Operation := AcFoEcrire;
            end;
          ACSyncModeLecture :
            FichierA.Info.Operation := AcFoSupprimerLocal;
          ACSyncModeEcriture :
            FichierA.Info.Operation := AcFoEcrire;
          ACSyncModeNoSupress :
            FichierA.Info.Operation := AcFoEcrire;
          end;
        end
        else
        begin
          // le fichier existe en B
          // Comparer la date des fichier
          Case A.SyncMode of
          ACSyncModeMAJ :
            Case ACCompareDateTime(FichierA.DateModification,FichierB.DateModification) of
            -1:
              if SyncAvanceMode and (FichierA.Info.Taille > SYNC_MODE_SEUIL_TAILLE) then
                FichierA.Info.Operation := AcFoSyncLire // Fichier A est moins recent alors Lire
              else
                FichierA.Info.Operation := AcFoLire; // Fichier A est moins recent alors Lire

            0: FichierA.Info.Operation := AcFoAJour; // Fichier A est a jour ne rien faire
            1:
              if SyncAvanceMode and (FichierA.Info.Taille > SYNC_MODE_SEUIL_TAILLE) then
                  FichierA.Info.Operation := AcFoSyncEcrire // Fichier A est moins recent alors Lire
              else
                FichierA.Info.Operation := AcFoEcrire; // Fichier A est plus recent alors ecrire
            end;
          ACSyncModeLecture :
            if not ACSameDateTime(FichierA.DateModification,FichierB.DateModification) then
              if SyncAvanceMode and (FichierA.Info.Taille > SYNC_MODE_SEUIL_TAILLE) then
                FichierA.Info.Operation := AcFoSyncLire // Fichier A est moins recent alors Lire
              else
                FichierA.Info.Operation := AcFoLire;
          ACSyncModeEcriture :
            if not ACSameDateTime(FichierA.DateModification,FichierB.DateModification) then
              if SyncAvanceMode and (FichierA.Info.Taille > SYNC_MODE_SEUIL_TAILLE) then
                FichierA.Info.Operation := AcFoSyncEcrire // Fichier A est moins recent alors Lire
              else
                FichierA.Info.Operation := AcFoEcrire;
          ACSyncModeNoSupress :
            Case ACCompareDateTime(FichierA.DateModification,FichierB.DateModification) of
            -1:
              if SyncAvanceMode and (FichierA.Info.Taille > SYNC_MODE_SEUIL_TAILLE) then
                FichierA.Info.Operation := AcFoSyncLire // Fichier A est moins recent alors Lire
              else
                FichierA.Info.Operation := AcFoLire; // Fichier A est moins recent alors Lire
            0: FichierA.Info.Operation := AcFoAJour; // Fichier A est a jour ne rien faire
            1:
              if SyncAvanceMode and (FichierA.Info.Taille > SYNC_MODE_SEUIL_TAILLE) then
                FichierA.Info.Operation := AcFoSyncEcrire // Fichier A est moins recent alors Lire
              else
                FichierA.Info.Operation := AcFoEcrire; // Fichier A est plus recent alors ecrire
            end;
          end;
        end;
      end;

      // Rechercher les fichier manquant de A dans  B
      for i := 0  to B.Fichiers.Count - 1 do
      begin
        FichierB := B.Fichiers[i];
        FichierA := A.Fichiers.Noms[FichierB.Nom];
        if FichierA = nil then
        begin
          // le fichier n'est pas present en A
          // Comparer la date A et de B
          FichierA := A.Fichiers.Add;
          FichierA.Info := FichierB.Info;
          Case A.SyncMode of
          ACSyncModeMAJ :
            Case ACCompareDateTime(A.DateModification,B.DateModification) of
            -1: FichierA.Info.Operation := AcFoLire; // A est moin recent donc supprimer en A
            0: FichierA.Info.Operation := AcFoLire;  // A est plus recent ou egal ecrire dans B
            1: FichierA.Info.Operation := AcFoSupprimerDistant;
            end;
          ACSyncModeLecture : FichierA.Info.Operation := AcFoLire;
          ACSyncModeEcriture : FichierA.Info.Operation := AcFoSupprimerDistant;
          ACSyncModeNoSupress : FichierA.Info.Operation := AcFoLire;
          end;
        end;
      end;
    end;
begin
  ComparerDossier(Self,pContenueDossier);
end;

constructor TACContenueDossier.Create(pAdresse: String);
begin
  Adresse := pAdresse;
  Parent := nil;
  Dossiers := TACContenueDossierList.Create(Self);
  Fichiers := TACContenueFichierList.Create(Self);
  SyncMode := ACSyncModeMAJ;
  Journal := TStringList.Create;
  FSyncAvanceMode := false;
end;

destructor TACContenueDossier.Destroy;
begin
  Dossiers.Free;
  Fichiers.Free;
  Journal.Free;
  inherited;
end;

procedure TACContenueDossier.ExporteListeFichierInfo(
  FichiersInfo: TACFichierInfoList);
    function EcrireLaListe(pDossier : TACContenueDossier;pFichierInfo : PACFichierInfo):PACFichierInfo;
    var
      i : Integer;
    begin
        pFichierInfo^ := pDossier.Info;
        inc(pFichierInfo);
        for i := 0 to pDossier.Dossiers.Count - 1 do
          pFichierInfo := EcrireLaListe(pDossier.Dossiers[i],pFichierInfo);


        for i := 0 to pDossier.Fichiers.Count - 1 do
        begin
          pFichierInfo^ := pDossier.Fichiers[i].Info;
          inc(pFichierInfo);
        end;
        Result := pFichierInfo;
    end;

var
  pFichierInfo : PACFichierInfo;

begin
  pFichierInfo := @FichiersInfo[0];
  EcrireLaListe(Self,pFichierInfo)


end;

procedure TACContenueDossier.Generer(Recursif: boolean);
var
  sr : TSearchRec;
  Attrs : Integer;
  DateFichier : TDateTime;
  DossierParent : String;
  DateModificationDossier : TDateTime;
  lDossier : TACContenueDossier;
  lFichier : TACContenueFichier;


begin

  if Parent = nil then
  begin
    if Not DirectoryExists(Adresse) then
      exit;
    Info := ACLireFichierInfo(Adresse);
    Info.NiveauDossier := 0;
  end
  else
    Adresse := Chemin;



  Attrs := faAnyFile+faDirectory+faHidden+faSysFile+faArchive;
  if FindFirst(Adresse+'\*.*',Attrs,sr) =  0 then
  begin
    repeat
      if (sr.Name <> '.') and (sr.Name <> '..')then
      begin
        if (faDirectory and sr.Attr) <> 0 then
        begin
          lDossier := Dossiers.Add;
//          lDossier.Info.DossierParent := ExtractFileName(Adresse);
          lDossier.Info.TypeDossier := true;
          lDossier.Info.Nom := sr.Name;
          lDossier.Info.Date.Modification := FileDateToDateTime(sr.Time);
          lDossier.Info.DateModificationDossier := DateModificationDossier;
          lDossier.Info.NiveauDossier := Info.NiveauDossier + 1;
          if Recursif then
             lDossier.Generer(Recursif);


        end
        else
        begin
          lFichier := Fichiers.Add;
          lFichier.Info.TypeDossier := false;
          lFichier.Info.Nom := sr.Name;
          lFichier.Info.Date.Modification := FileDateToDateTime(sr.Time);
          lFichier.Info.Taille := sr.Size;
          lFichier.Info.DateModificationDossier := DateModificationDossier;
          lFichier.Info.NiveauDossier := Info.NiveauDossier;

        end;
      end;
    until FindNext(sr)<>0;
  end;
  FindClose(sr);

end;
procedure TACContenueDossier.ImporterListeFichierInfo(
  FichiersInfo: TACFichierInfoList; Nombre: Integer);
    function LireLaListe(pFichierInfo : PACFichierInfo;
              pDossier : TACContenueDossier;pNombre : PInteger):PACFichierInfo;
    var
      i : Integer;
      lDossier : TACContenueDossier;
    begin
        pDossier.Info := pFichierInfo^;
        inc(pFichierInfo);
        Dec(pNombre^);
        while pNombre^ > 0 do
        begin
            if pFichierInfo^.TypeDossier then
            begin
              if pFichierInfo^.NiveauDossier > pDossier.Info.NiveauDossier then
              begin
                pFichierInfo := LireLaListe(pFichierInfo,pDossier.Dossiers.Add,pNombre);
              end
              else
                break;
            end
            else
            begin
              if pFichierInfo^.NiveauDossier >= pDossier.Info.NiveauDossier then
              begin
                pDossier.Fichiers.Add.Info := pFichierInfo^;
                inc(pFichierInfo);
                Dec(pNombre^);

              end
              else
                break;

            end;

        end;

        Result := pFichierInfo;
    end;

var
  pFichierInfo : PACFichierInfo;

begin
  pFichierInfo := @FichiersInfo[0];
  LireLaListe(pFichierInfo,Self,@Nombre);


end;

function TACContenueDossier.Nombre: integer;
var
  i : Integer;
begin
  Result := Fichiers.Count+Dossiers.Count;
  
  for i := 0 to Dossiers.Count -1 do
  begin
    Result := Result + Dossiers[i].Nombre;
  end;


end;

function TACContenueDossier.Chemin: string;
begin
  if Parent = nil then
    result := Adresse
  else
    Result := Parent.Chemin+'\'+Info.Nom;
end;

function TACContenueDossier.GetDateModification: TDateTime;
begin
  result := Info.Date.Modification;
end;

function TACContenueDossier.GetNom: string;
begin
    result := Info.Nom;
end;

procedure TACContenueDossier.GenererListeFichierAEcrire(Liste: TStrings;
  Adresse: string);
var
  i : integer;
begin
  for i := 0 to Dossiers.Count - 1 do
  begin
    if Dossiers[i].Info.Operation = AcFoEcrire then
      Liste.Add(Adresse+'\'+Dossiers[i].Chemin)
    else
      Dossiers[i].GenererListeFichierAEcrire(Liste,Chemin);

  end;
  for i := 0 to Fichiers.Count - 1 do
  begin
    if Fichiers[i].Info.Operation = AcFoEcrire then
      Liste.Add(Adresse+'\'+Fichiers[i].Chemin);

  end;

end;

procedure TACContenueDossier.GenererListeFichierALire(Liste: TStrings;
  Adresse: string);
var
  i : integer;
begin
  for i := 0 to Dossiers.Count - 1 do
  begin
    if Dossiers[i].Info.Operation = AcFoLire then
      Liste.Add(Adresse+'\'+Dossiers[i].Chemin)
    else
      Dossiers[i].GenererListeFichierALire(Liste,Chemin);

  end;
  for i := 0 to Fichiers.Count - 1 do
  begin
    if Fichiers[i].Info.Operation = AcFoLire then
      Liste.Add(Adresse+'\'+Fichiers[i].Chemin);

  end;

end;

procedure TACContenueDossier.GenererListeDesOperations(Liste: TStrings;
  Adresse: string; Operation: TACFichierOperation);
var
  i : integer;
begin
  for i := 0 to Dossiers.Count - 1 do
  begin
    if Dossiers[i].Info.Operation = Operation then
      Liste.Add(Adresse+'\'+Dossiers[i].Chemin)
    else
      Dossiers[i].GenererListeFichierALire(Liste,Chemin);

  end;
  for i := 0 to Fichiers.Count - 1 do
  begin
    if Fichiers[i].Info.Operation = Operation then
      Liste.Add(Adresse+'\'+Fichiers[i].Chemin);

  end;
end;


function TACContenueDossier.CheminLocal: string;
begin
  if Parent = nil then
    result := Info.Nom
  else
    Result := Parent.CheminLocal+'\'+Info.Nom;

end;

function TACContenueDossier.GetSyncMode: TACSyncMode;
begin
  if Parent <> nil then
    Result := Parent.SyncMode
  else
    Result := FSyncMode;
end;

procedure TACContenueDossier.SetSyncMode(const Value: TACSyncMode);
begin
  if Parent <> nil then
    Parent.SyncMode := Value
  else
    FSyncMode := Value;

end;

procedure TACContenueDossier.ExporteListeFichierInfoVersStream(
  Stream : TStream);
var
  NombreFichiers : Integer;
  Contenue : TACFichierInfoList;
  Taille : Integer;
begin
  Journal.Add('NombreFichiers := Nombre+1;');
  NombreFichiers := Nombre+1;

  SetLength(Contenue,NombreFichiers);
  Journal.Add('ExporteListeFichierInfo(Contenue);');
  ExporteListeFichierInfo(Contenue);
  Taille := SizeOf(TACFichierInfo) *  NombreFichiers;
  Journal.Add('Stream.Write(Contenue[0],Taille);');
  Stream.Write(Contenue[0],Taille);
  Journal.Add('FreeMemory(Contenue);');
  try
    FreeMemory(Contenue);
  except
  end;
  
end;

procedure TACContenueDossier.ImporterListeFichierInfoDeStream(
  Stream : TStream);
var

  Contenue : TACFichierInfoList;
  NombreFichiers : Integer;

begin
  NombreFichiers := Stream.Size div SizeOf(TACFichierInfo);
  SetLength(Contenue,NombreFichiers);
  Stream.Read(Contenue[0],Stream.Size);
  ImporterListeFichierInfo(Contenue,NombreFichiers);
  try
    FreeMemory(Contenue);
  except

  end;



end;

procedure TACContenueDossier.ExporteListeFichierInfoVersFichier(
  FichierNom: string);
var
  Fichier : TFileStream;
begin

  Journal.Add('Fichier := TFileStream.Create(FichierNom,fmCreate);');
  Fichier := TFileStream.Create(FichierNom,fmCreate);
  Journal.Add('ExporteListeFichierInfoVersStream(Fichier);');
  ExporteListeFichierInfoVersStream(Fichier);
  Journal.Add('Fichier.Free;');
  Fichier.Free;

end;

procedure TACContenueDossier.ImporterListeFichierInfoDeFichier(
  FichierNom: string);
var
  Fichier : TFileStream;
begin
  Fichier := TFileStream.Create(FichierNom,fmOpenRead);
  ImporterListeFichierInfoDeStream(Fichier);
  Fichier.Free;
end;

procedure TACContenueDossier.SetSyncAvanceMode(const Value: boolean);
begin
  FSyncAvanceMode := Value;
end;

{ TACContenueDossierList }

function TACContenueDossierList.Add(Item: TACContenueDossier): Integer;
begin
  result := inherited Add(Item);
end;

function TACContenueDossierList.Add: TACContenueDossier;
begin
  Result := TACContenueDossier.Create(Parent);
  Add(Result);
end;

procedure TACContenueDossierList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;
  inherited;

end;



constructor TACContenueDossierList.Create(pParent: TACContenueDossier);
begin
  Parent := pParent;

end;

procedure TACContenueDossierList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TACContenueDossierList.Destroy;
begin
  Clear;
  inherited;
end;

function TACContenueDossierList.Extract(Item: TACContenueDossier): TACContenueDossier;
begin
  result := inherited Extract(Item);
end;

function TACContenueDossierList.First: TACContenueDossier;
begin
  Result := inherited First;
end;

function TACContenueDossierList.Get(Index: Integer): TACContenueDossier;
begin
  Result := inherited Get(Index);
end;

function TACContenueDossierList.GetNoms(
  Index: String): TACContenueDossier;
var
  i : Integer;
begin
  Result := nil;
  for i := 0 to Count  - 1 do
    if SameText(Items[i].Nom,Index) then
    begin
      Result := Items[i];
      exit;
    end;

end;

function TACContenueDossierList.IndexOf(Item: TACContenueDossier): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TACContenueDossierList.Insert(Index: Integer; Item: TACContenueDossier);
begin
  inherited Insert(Index,Item);
end;

function TACContenueDossierList.Last: TACContenueDossier;
begin
  Result := inherited Last;
end;

procedure TACContenueDossierList.Put(Index: Integer; Item: TACContenueDossier);
begin
  inherited Put(Index,Item);

end;

function TACContenueDossierList.Remove(Item: TACContenueDossier): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;

function ACLireFichierInfo(Fichier : string): TACFichierInfo;
var
  sr  : TSearchRec;
  Attrs : Integer;

begin
//  Result.DossierParent := ExtractFileName(ExtractFilePath(Fichier));
  result.TypeDossier := false;
  result.Taille := 0;
  Attrs := faAnyFile+faDirectory+faHidden+faSysFile+faArchive;
  if FindFirst(Fichier,Attrs,sr) = 0 then
  begin
    Result.Nom := sr.Name;
    Result.Taille := sr.Size;
    Result.Date.Modification := FileDateToDateTime(sr.Time);
  end;
end;


function ACLireFichierDates(Fichier : string): TACFichierDate;
var
  F1 : Integer;

begin

  F1 := FileOpen(Fichier, fmShareDenyWrite);

  GetFileTime(  F1,
                @Result.SystemeTime.CreateTime,
                @Result.SystemeTime.LastAccess,
                @Result.SystemeTime.LastWrite);

  Result.Creation := FileTimeToDateTime(Result.SystemeTime.CreateTime);
  Result.DernierAcces := FileTimeToDateTime(Result.SystemeTime.LastAccess);
  Result.Modification := FileTimeToDateTime(Result.SystemeTime.LastWrite);

  FileClose(F1);
end;

procedure ACEcrireFichierDate(Fichier : string; Info: TACFichierDate);
var
  F1 : Integer;

begin
  F1 := FileOpen(Fichier, fmOpenWrite);

  SetFileTime(  F1,
                @Info.SystemeTime.CreateTime,
                @Info.SystemeTime.CreateTime,
                @Info.SystemeTime.CreateTime);

  FileClose(F1);

end;

{ TACContenueFichier }



function TACContenueFichier.Chemin: string;
begin
  Result := Dossier.Chemin +'\'+Info.Nom;
end;

function TACContenueFichier.CheminLocal: string;
begin
    Result := Dossier.CheminLocal +'\'+Info.Nom;  
end;

constructor TACContenueFichier.Create(pDossier: TACContenueDossier);
begin
  Dossier := pDossier;

end;

destructor TACContenueFichier.Destroy;
begin

  inherited;
end;

function TACContenueFichier.GetDateModification: TDateTime;
begin
  result := Info.Date.Modification;
end;

function TACContenueFichier.GetNom: string;
begin
  result := Info.Nom;
end;

function TACContenueFichier.GetSyncMode: TACSyncMode;
begin
  
end;

function TACContenueFichier.ObtenirAdresse: string;
var
  DossierParent : TACContenueDossier;
begin
  DossierParent := Dossier;
  result := Info.Nom;
  while DossierParent<>nil do
  begin
    if DossierParent.Parent <> nil then
      Result := DossierParent.Info.Nom +'\' +Result
    else
      Result := DossierParent.Adresse +'\' +Result;

    DossierParent := DossierParent.Parent;
  end;
end;

{ TACContenueFichierList }

function TACContenueFichierList.Add(Item: TACContenueFichier): Integer;
begin
  result := inherited Add(Item);
end;

function TACContenueFichierList.Add: TACContenueFichier;
begin
  Result := TACContenueFichier.Create(Dossier);
  Add(Result);
end;

procedure TACContenueFichierList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;    
  inherited;

end;



constructor TACContenueFichierList.Create(pDossier: TACContenueDossier);
begin
  Dossier := pDossier;

end;

procedure TACContenueFichierList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TACContenueFichierList.Destroy;
begin
  Clear;
  inherited;
end;

function TACContenueFichierList.Extract(Item: TACContenueFichier): TACContenueFichier;
begin
  result := inherited Extract(Item);
end;

function TACContenueFichierList.First: TACContenueFichier;
begin
  Result := inherited First;
end;

function TACContenueFichierList.Get(Index: Integer): TACContenueFichier;
begin
  Result := inherited Get(Index);
end;

function TACContenueFichierList.GetNoms(
  Index: String): TACContenueFichier;
var
  i : Integer;
begin
  Result := nil;
  for i := 0 to Count  - 1 do
    if SameText(Items[i].Nom,Index) then
    begin
      Result := Items[i];
      exit;
    end;

end;

function TACContenueFichierList.IndexOf(Item: TACContenueFichier): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TACContenueFichierList.Insert(Index: Integer; Item: TACContenueFichier);
begin
  inherited Insert(Index,Item);
end;

function TACContenueFichierList.Last: TACContenueFichier;
begin
  Result := inherited Last;
end;

procedure TACContenueFichierList.Put(Index: Integer; Item: TACContenueFichier);
begin
  inherited Put(Index,Item);

end;

function TACContenueFichierList.Remove(Item: TACContenueFichier): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;



end.

